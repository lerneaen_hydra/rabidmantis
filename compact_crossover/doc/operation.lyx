#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass scrartcl
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section*
Operation
\begin_inset CommandInset label
LatexCommand label
name "sec:Operation"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
addcontentsline{toc}{section}{Operation}
\end_layout

\begin_layout Plain Layout


\backslash
markboth{Operation}{Operation}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\backslash
 
\end_layout

\end_inset

is a purely analog crossover filter heavily based on National Semiconductor's
 Application Note 346 -- High-Performance Audio Applications of LM833
\begin_inset Foot
status open

\begin_layout Plain Layout
2002 National Semiconductor Corporation, 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
url{http://www.national.com}
\end_layout

\end_inset


\end_layout

\end_inset

.
 This application note lists (among others) a constant-voltage generic active
 crossover network.
 Additionally, 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\backslash
 
\end_layout

\end_inset

 consists of an input infrasonic/ultrasonic filter, internal bias generation,
 and input over-voltage/reverse polarity protection.
 From a signal propagation point of view, the circuit functions as follows;
\end_layout

\begin_layout Standard
A signal applied to terminal 1 on the terminal block is high-pass filtered
 by C8 and R18/R21 to a cutoff frequency of 
\begin_inset Formula 
\[
f_{HP}=\frac{1}{2\pi C_{8}\left(R_{21}+R_{18}\right)}\underset{typ.}{\approx}4.8\mathrm{Hz}
\]

\end_inset

and low-pass filtered by R18 and C9 to a cutoff frequency of
\begin_inset Formula 
\[
f_{LP}=\frac{1}{2\pi R_{18}C_{9}}\underset{typ.}{\approx}58\mathrm{kHz}.
\]

\end_inset

The signal is biased around OPAMP_BIAS (typically 5V) through R21, supplying
 IC4A (and all other amplifiers) with a signal that lies within it's linear
 range.
 The signal is then inverted with unity gain through IC4B, which counteracts
 an inversion the crossover filter section will generate later in order
 to ensure the device's output maintains the same phase as the input (so
 that the high and low frequency outputs as well as the input signal can
 be used together for situations that may require this).
 This generates the signal labeled CONDITIONED, which is fed into the crossover
 network.
\end_layout

\begin_layout Standard
The crossover network is identical to the network shown in the previously
 noted application note, which generates a low-frequency and high-frequency
 signal which when summed exactly equal the input to the crossover network.
 These signals are fed into identical attenuation stages consisting of R5/R6
 and R14/R15, which allow for adjusting the relative gain of each stage
 to compensate for loudspeaker sensitivity differences.
 IC1A and IC1B simply buffer the signal, which is then passed through a
 R4 and R13, and then output to the terminals HIGH_OUT and LOW_OUT.
 Note that shorting SJ1 and SJ2 allows for the removal of the output resistor,
 which otherwise functions to stabilize the output in the presence of capacitive
 load (which may otherwise cause IC1A/IC1B to oscillate).
 If the output from the filter is close to the next stage's input these
 may be shorted.
\end_layout

\begin_layout Standard
When operating in single supply mode the internal bias generator creates
 a 5V bias for the operational amplifiers to work around, giving headroom
 to ensure the devices are kept in their linear range.
 The OPA2134 amplifiers have a specified common mode input range that reaches
 2.5V above the negative supply voltage, making it possible to drive signals
 up to 2Vpp (which would then be internally biased to a signal of 
\begin_inset Formula $5\pm1\mathrm{V}$
\end_inset

; as the crossover filter has a gain of +3dB near the crossover frequency
 this will result in an output of 
\begin_inset Formula $5\pm2\mathrm{V}$
\end_inset

 for some frequencies, leaving 500mV of headroom to the negative supply).
 It is safe to alter the ratio of R17/R19 to alter the total gain.
 If so ensure that 
\begin_inset Formula $1k\Omega<R17,R19<33k\Omega$
\end_inset

 and that the input signal does not cause the internal network to leave
 it's linear operating region.
\end_layout

\begin_layout Standard
The use of a precision zener reference (VR1) raises the power supply rejection
 ratio greatly, and is on the order of 85dB.
 As the positive supply is heavily decoupled with a 100
\begin_inset Formula $\mathrm{uF}$
\end_inset

 capacitor and the input PTC fuse has a minimum resistance of 2.5
\begin_inset Formula $\Omega$
\end_inset

 this gives an additional degree of attenuation at high frequencies.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/ground_loop_1.svg
	width 80text%
	groupId gnd_loop

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Isolation of signal ground and power ground eliminates ground loops.
\begin_inset CommandInset label
LatexCommand label
name "fig:Isolation-of-signal-ground-look"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/ground_loop_2.svg
	width 80text%
	groupId gnd_loop

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Non-isolated signal ground and power ground leads to ground loop.
 
\begin_inset CommandInset label
LatexCommand label
name "fig:non-isolation-of-signal-ground-look-1"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
The isolation of signal ground and power ground is required to remove ground
 loops.
\begin_inset CommandInset label
LatexCommand label
name "fig:gnd_loop"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
In single supply mode there is also a distinction between 
\shape italic
power ground
\shape default
 -- where current that flows out from the negative supply pin on the operational
 amplifiers is sinked -- and 
\shape italic
signal ground
\shape default
 -- the reference level for the input and output signals.
 If this distinction is not made there is the potential for ground loop
 interference.
 This is illustrated in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:gnd_loop"

\end_inset

, where in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Isolation-of-signal-ground-look"

\end_inset

 power ground and signal ground are kept separate in 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\end_layout

\end_inset

.
 This ensures there is no undesired current flow through the signal ground
 conductor.
 In 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:non-isolation-of-signal-ground-look-1"

\end_inset

 however signal ground and power ground are not isolated.
 This creates the potential for current from the power amplifier to flow
 through the signal ground connection and then return through the power
 ground connection, caused by the nonzero resistance from the power ground
 terminal in the power amplifier to the shared grounding point.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\backslash
 
\end_layout

\end_inset

 does not completely separate the two grounds, they are connected with C12
 and R24 (typically 
\begin_inset Formula $100\Omega$
\end_inset

 and 
\begin_inset Formula $100\mathrm{nF}$
\end_inset

), which is done to reduce EMI susceptibility, as well as act as a failsafe
 should the signal ground connector loosen from the power amplifier.
 From a ground-loop perspective this impedance is far higher than any connection
 resistance, reducing the potential ground loop current to an insignificantly
 small amount.
\end_layout

\begin_layout Standard
Finally, as the 5V bias voltage generated by VR1 is referenced to signal
 ground, there is a constant parasitic current that must be sinked by the
 device 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\backslash
 
\end_layout

\end_inset

 is connected to.
 This currently is relatively small, and is given by
\begin_inset Formula 
\[
\frac{V_{+}-V_{bias}}{R16}=\frac{V_{+}-5V}{1800\Omega}=\begin{array}{c}
\underbrace{3.8mA}_{V_{+}=12V}\\
\underbrace{8.3mA}_{V_{+}=20V}
\end{array}.
\]

\end_inset

If this current is not absorbed by the next stage (such as if the signal
 ground input is of high impedance) the signal ground level voltage will
 rise relative to power ground.
 This is limited by R24 and is given by (in the extreme case of an infinitely
 high impedance)
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\[
R24\frac{V_{+}-V_{bias}}{R16}=\frac{V_{+}-5V}{18}=\begin{array}{c}
\underbrace{380mV}_{V_{+}=12V}\\
\underbrace{830mV}_{V_{+}=20V}
\end{array},
\]

\end_inset

which is for all but the most extreme cases safe.
 If this is not the case, then SJ6 can be shorted, which connects the two
 ground levels internally.
 This will result in no voltage difference between the two ground levels
 locally, but may result in a ground loop current as shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:non-isolation-of-signal-ground-look-1"

\end_inset

.
 This is not recommended unless absolutely needed as this reduces the PSRR
 greatly.
\end_layout

\begin_layout Standard
When 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\backslash
 
\end_layout

\end_inset

 is used in infrasonic/ultrasonic filter mode, the solder jumpers SJ3 and
 SJ4 are shorted, thereby removing the effects that the crossover filter
 section would have (the components should then not be mounted).
\end_layout

\begin_layout Standard
When in dual-supply mode, the internal bias section is unneeded and effectively
 removed by shorting SJ5 (and not mounting the unneeded components).
\end_layout

\begin_layout Standard
The input over-voltage protection consists of F1 and Z1.
 At low to moderate over-voltage levels the power dissipation in Z1 heats
 up F1, increasing the resistance in F1 and eventually triggering the fuse.
 At higher levels the fuse triggers itself rapidly, and remains in its triggered
 state until it cools down, for example through a power cycle.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways true
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/compact_crossover.sch.pdf

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Full schematic for 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
projectname
\end_layout

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "fig:schematic-1"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/compact_crossover.brd_top.pdf
	width 60col%
	groupId PCB

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Complete top layer as seen from above.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/compact_crossover.brd_btm.pdf
	width 60col%
	groupId PCB

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Complete bottom layer as seen from below.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
PCB details.
\begin_inset CommandInset label
LatexCommand label
name "fig:Cheapamp-PCB-details."

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/compact_crossover.brd_tplc.pdf
	width 60col%
	groupId PCB

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Top layer component outline as seen from above.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/compact_crossover.brd_bplc.pdf
	width 60col%
	groupId PCB

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Bottom layer component outline as seen from below.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Component placement details.
\begin_inset CommandInset label
LatexCommand label
name "fig:Cheapamp-component-placement"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
