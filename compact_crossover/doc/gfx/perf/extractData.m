function [time data] = extractData(filename)
%Extracts the time and data from the exported LTspice cursor output

fileID = fopen(filename);
fContent = textscan(fileID, '%s'); % reads the whole file into fContent
fclose(fileID);
fContent = fContent{1}; % making fContent to an n x 1 - Matrix

numplots = -1;

while numplots<0
    foo = isstrprop(fContent{-numplots},'digit');   %check if the conent of line n contains numbers
    if(foo(1) == 1)
       numplots = -1*numplots; 
    else
       numplots = numplots - 1; 
    end
end
numplots = numplots - 2; %This is now the number of plots in the file

time = 0;
data = zeros(1,numplots);
foo = zeros(1,numplots);

i = 2+numplots;
while(i<=length(fContent)-numplots) %scan over the elements that contain useful data
    time = [time;str2double(fContent{i})];
    i = i+1;
    for j=1:numplots
        foo(j)=sscanf(fContent{i},'%f');
        i = i+1;
    end
    data = [data;foo];
end

time(1)=[];
data(1,:)=[];