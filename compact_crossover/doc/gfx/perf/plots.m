clc
clear all
close all
format short eng

figure()
[time data] = extractData('low_high_sum.txt');
set(0,'DefaultAxesColorOrder',[0 0 0],...
      'DefaultAxesLineStyleOrder','-|--|-.')
h=plot(time,data+5);
set(gcf, 'PaperPosition', [0 0 7 4]);
set(h(2),'linewidth',3);
set(h(3),'linewidth',3);
grid on
title('1Vpp Step response, 1kHz crossover frequency')
xlabel('Time [s]')
ylabel('Output amplitude [V]')
legend('Sum','High F','Low F')
axis([0 4e-3 4 6])
print('-deps','low_high_sum')

figure()
[time data]=extractData('crossover.txt');
set(0,'DefaultAxesColorOrder',[0 0 0],...
      'DefaultAxesLineStyleOrder','--|-|-.')
h=semilogx(time,20*log10(data));
set(gcf, 'PaperPosition', [0 0 7 4]);
set(h(1),'linewidth',3);
set(h(3),'linewidth',3);
legend('High F','Sum','Low F','location','south')
grid on
axis([20 20e3 -20 0])
title('System frequency response, 1kHz crossover frequency')
xlabel('Frequency [Hz]')
ylabel('Relative gain [dB]')
print('-deps','crossover')

figure()
[time data]=extractData('ultrasonic_infrasonic.txt');
set(0,'DefaultAxesColorOrder',[0 0 0],...
      'DefaultAxesLineStyleOrder','--|-|-.')
h=semilogx(time,20*log10(data));
set(gcf, 'PaperPosition', [0 0 7 4]);
set(h(1),'linewidth',3);
set(h(3),'linewidth',3);
grid on
legend('High f','Output sum','Low f','location','south')
axis([.3 1e6 -20 0])
title('System frequency response, 1kHz crossover frequency')
xlabel('Frequency [Hz]')
ylabel('Output amplitude [V]')
print('-deps','ultrasonic_infrasonic')

