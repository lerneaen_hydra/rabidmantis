[AC Analysis]
{
   Npanes: 1
   {
      traces: 3 {2,0,"abs(V(high))"} {3,0,"abs(V(low))"} {4,0,"abs(V(high)+V(low))"}
      X: ('M',0,0.1,0,1e+006)
      Y[0]: (' ',0,1e-009,20,10)
      Y[1]: ('m',0,-0.001,0.0002,0.001)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
   }
}
[Transient Analysis]
{
   Npanes: 1
   {
      traces: 3 {524290,0,"V(high)+V(low)"} {268959747,0,"V(high)"} {268959748,0,"V(low)"}
      X: ('m',1,0,0.0004,0.004)
      Y[0]: (' ',1,-2.4,0.4,2.4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-2.4,0.4,2.4)
      Log: 0 0 0
      GridStyle: 1
   }
}
