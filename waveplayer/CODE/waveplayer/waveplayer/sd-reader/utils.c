#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>

bool read_next_line_file(struct fat_file_struct* fd, char* linedest, uint16_t chars){
	bool retval = false;
	uint16_t i = 0;
	bool finished = false;
	while(!finished){
		char dummy;
		if(fat_read_file(fd,(uint8_t*)&dummy,1) > 0){
			retval = true;
			if(i < chars - 1){	//Reserve one character for the null padding
				*(linedest + i) = dummy;
				i++;
			}
			if(dummy == '\n' || dummy == '\r'){
				finished = true;
			}
		}else{
			finished = true;
		}
	}
	*(linedest + i) = '\0';	//Ensure output is terminated
	return retval;
}

uint8_t find_file_in_dir(struct fat_fs_struct* fs, struct fat_dir_struct* dd, const char* name, struct fat_dir_entry_struct* dir_entry){
	(void) fs;	//Suppress warning about unused parameter
	while(fat_read_dir(dd, dir_entry)){
		if(strcmp(dir_entry->long_name, name) == 0){
			fat_reset_dir(dd);
			return 1;
		}
	}
	return 0;
}

struct fat_file_struct* open_file_in_dir(struct fat_fs_struct* fs, struct fat_dir_struct* dd, const char* name){
	struct fat_dir_entry_struct file_entry;
	if(!find_file_in_dir(fs, dd, name, &file_entry)){
		return 0;
	}
	
	return fat_open_file(fs, &file_entry);
}

void sd_reader_print_info(void){
	printf_P(PSTR(MSG_STARTUP_MEMCARD_INFO));
	printf_P(PSTR("\tManuf.:0x%x\n"),disk_info.manufacturer);
	printf_P(PSTR("\tOEM:   %s\n"),disk_info.oem);
	printf_P(PSTR("\tProd:  %s\n"),disk_info.product);
	printf_P(PSTR("\tRev:   %d\n"),disk_info.revision);
	printf_P(PSTR("\tSerial:0x%x\n"),disk_info.serial);
	printf_P(PSTR("\tDate:  %d/%d [Y/M]\n"),2000+disk_info.manufacturing_year /* Year is offset by 2000 */,disk_info.manufacturing_year);
	printf_P(PSTR("\tSize:  %d [MiB]\n"),disk_info.capacity/1024/1024);
}