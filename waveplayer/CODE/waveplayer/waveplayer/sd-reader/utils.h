/*
 * utils.h
 *
 * Created: 2/27/2014 4:25:33 PM
 *  Author: lock
 */ 


#ifndef UTILS_H_
#define UTILS_H_

#include <stdbool.h>
#include "fat.h"
#include "partition.h"
#include "sd_raw.h"

uint8_t find_file_in_dir(struct fat_fs_struct* fs, struct fat_dir_struct* dd, const char* name, struct fat_dir_entry_struct* dir_entry);
struct fat_file_struct* open_file_in_dir(struct fat_fs_struct* fs, struct fat_dir_struct* dd, const char* name);
void sd_reader_print_info(void);
//Reads chars from the next line in fd, writing to *linedest, and removing any remaining characters from the input line before returning
bool read_next_line_file(struct fat_file_struct* fd, char* linedest, uintptr_t chars);

#endif /* UTILS_H_ */