/** @file Simple and lightweight software debouncing library.
 * Completely platform independent, requires C99. */

#ifndef BTN_DEBOUNCE_H_
#define BTN_DEBOUNCE_H_

#include <stdbool.h>
#include <stdint.h>

/************* Include .h files as needed to define BTN_DEBOUNCE_NUM_BTNS **************/
#include "../board/board.h"
#define BTN_DEBOUNCE_NUM_BTNS	BOARD_NUM_BTNS
/***************************************************************************************/

/** @brief Master button debounce structure.
 * One button debounce structure is needed per project, may be global if desired. */
struct btn_debounce_t {
	bool (*get_btn)(uint8_t num);			//<!- Function pointer to function that returns the raw button state for some button num (0 - num_btns)
	uint8_t mem[BTN_DEBOUNCE_NUM_BTNS];		//<!- Function pointer to array of num_btns elements, storing the 6 most recent button states, the debounced state, and a latch indicator
};

/** @brief Idle function for debounce. Should run periodically at around 2-3 times faster than the bounce interval (~10-30ms is reasonable) */
void btn_debounce_idle(struct btn_debounce_t * const btn_handle);

/** @brief Gets the num'th button state from btn_handle and returns the debounced state */
bool btn_debounce_get_state(struct btn_debounce_t * const btn_handle, const uint8_t num);

/** @brief Returns true if the num'th button state has changed since the last call to this function and the current button state matches state */
bool btn_debounce_get_latch(struct btn_debounce_t * const btn_handle, const uint8_t num, bool state);

#endif /* BTN_DEBOUNCE_H_ */