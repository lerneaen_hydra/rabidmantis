#include "btn_debounce.h"
#include <stddef.h>

/** @brief Bit-mask for button sample data (logged raw button state) */
#define BTN_SAMP_bm		0x3F
/** @brief Bit-mask for debounced button state */
#define BTN_STATE_bm	0x40
/** @brief Bit-mask for latched new button state */
#define BTN_LATCH_STATE_bm	0x80

void btn_debounce_idle(struct btn_debounce_t * const btn_handle){
	if(btn_handle == NULL || btn_handle->get_btn == NULL){
		return;
	}
		
	for(uint8_t i = 0; i <BTN_DEBOUNCE_NUM_BTNS; i++){
		//Shift up the stored data by one bit and add the new data to the LSB, leaving the higher bit(s) untouched
		btn_handle->mem[i] = (((btn_handle->mem[i] << 1) | (btn_handle->get_btn(i))) & BTN_SAMP_bm) | (btn_handle->mem[i] & ~BTN_SAMP_bm);
		
		if(btn_handle->mem[i] & BTN_STATE_bm){			//If the state bit is set the debounced state is a depressed button
			if((btn_handle->mem[i] & BTN_SAMP_bm) == 0){				//If 6 most recent samples are cleared, clear the button state bit and set the latch bit
				btn_handle->mem[i] &= ~(BTN_STATE_bm);
				btn_handle->mem[i] |= BTN_LATCH_STATE_bm;
			}
		}else{											//Else the state bit is cleared, so the debounced state is a released button
			if((btn_handle->mem[i] & BTN_SAMP_bm) == BTN_SAMP_bm){		//If the 6 most recent samples are set, set the button state bit and set the latch bit
				btn_handle->mem[i] |= BTN_STATE_bm;
				btn_handle->mem[i] |= BTN_LATCH_STATE_bm;
			}
		}
	}
}

bool btn_debounce_get_state(struct btn_debounce_t * const btn_handle, const uint8_t num){
	bool retval = false;
	if(btn_handle == NULL){
		return retval;
	}
	
	if(num >= BTN_DEBOUNCE_NUM_BTNS){
		return retval;
	}
	
	//Check the state bit, this contains the debounced button state
	if(btn_handle->mem[num] & BTN_STATE_bm){
		retval = true;
	}
	return retval;
}

bool btn_debounce_get_latch(struct btn_debounce_t * const btn_handle, const uint8_t num, bool state){
	bool retval = false;
	if(btn_handle == NULL){
		return retval;
	}
	
	if(num >= BTN_DEBOUNCE_NUM_BTNS){
		return retval;
	}
	
	const bool btn_state = !!(btn_handle->mem[num] & BTN_STATE_bm);	//Conver the current button status to a boolean value
	if(btn_state == state){
		//Check the latch bit, if set clear it and return true if the current button state maches the state we're checking for
		if(btn_handle->mem[num] & BTN_LATCH_STATE_bm){
			btn_handle->mem[num] &= ~(BTN_LATCH_STATE_bm);
			retval = true;
		}	
	}
	return retval;
}