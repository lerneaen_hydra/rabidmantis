/*
 * ctrl.h
 *
 * Created: 4/8/2014 12:48:11 PM
 *  Author: lock
 */ 


#ifndef CTRL_H_
#define CTRL_H_


/** @brief Idle function for external line controls */
void ctrl_idle(void);


#endif /* CTRL_H_ */