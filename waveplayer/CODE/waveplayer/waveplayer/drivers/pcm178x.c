#include "pcm178x.h"
#include "../board/board.h"
#include "../flex_settings/flex_settings.h"

//Volume/attenuator related defines
#define PCM178x_LVOL_REG	16
#define PCM178x_RVOL_REG	17

//Volume/attenuator config defines
#define PCM178x_DAMS_REG	21
#define PCM178x_DAMS_VAL	1

//Polarity reversal config defines
#define PCM178X_DREV_REG	22
#define PCM178x_DREV_VAL	1

/** @brief Writes a single data byte to a specific register */
static void pcm178x_raw_write(uint8_t reg, uint8_t data);

void pcm178x_init(void){
	//Don't change the default oversampling rate, as this is good for the system setup
	//Don't apply digital de-emphasis (the source WAVE material doesn't have any applied emphasis)
	//Don't change the data format (left-justified, MSB-first)
	//Don't change the filter's rolloff (sharp rolloff -> ~60db attenuation in stop band, negligible (<0.05db) pass-band ripple)
	
	//Reverse the output polarity to preserve the total system polarity
	pcm178x_raw_write(PCM178X_DREV_REG, PCM178x_DREV_VAL);
	//Set the DAMS bit to enable 0 - 100db attenuation in 1db steps
	pcm178x_raw_write(PCM178x_DAMS_REG, PCM178x_DAMS_VAL);
	//Set the output volume to zero (muted) initially
	pcm178x_set_vol(PCM178X_MAX_ATTN_db-1);
}

void pcm178x_set_vol(int8_t db_attn){
	if(db_attn < PCM178X_MAX_ATTN_db){
		db_attn = PCM178X_MAX_ATTN_db;
	}
	if(db_attn > 0){
		db_attn = 0;
	}
	const uint8_t dataval = UINT8_MAX + db_attn;
	pcm178x_raw_write(PCM178x_LVOL_REG, dataval);
	pcm178x_raw_write(PCM178x_RVOL_REG, dataval);
}

static void pcm178x_raw_write(uint8_t reg, uint8_t data){
	reg &= 0b01111111;	//Ensure MSB of reg is zero; all comms with PCM178x has MSB set to zero
	uint16_t dataword = reg << 8 | data;	//Construct data word

	DAC_SPI_DIS_SS();
	
	for(uint16_t i = 0; i < ((10ULL*12)/BOARD_MIN_FS_khz); i++){
		BOARD_DELAY_MIN_100NS();	//PCM1780 Datasheet specifies a minimum wait time of ~12us for a sample-rate of 1kHz, determine delay based on actual minimum sample rate used
	}
	
	DAC_SPI_DIS_SCK();
	BOARD_DELAY_MIN_100NS();
	DAC_SPI_EN_SS();
	BOARD_DELAY_MIN_100NS();
	for(int8_t i = 15; i >= 0; i--){		//Bit-bang the SPI data word
		if(dataword & (1<<i)){		//Set up current data bit
			DAC_SPI_EN_MOSI();
		}else{
			DAC_SPI_DIS_MOSI();
		}
		BOARD_DELAY_MIN_100NS();
		DAC_SPI_EN_SCK();
		BOARD_DELAY_MIN_100NS();
		DAC_SPI_DIS_SCK();		
	}
	DAC_SPI_DIS_SS();
}