/** @file Basic driver for the software control interface of the PCM1780 */

#ifndef PCM178X_H_
#define PCM178X_H_

#include <stdint.h>

/** @brief Maximum attenuation in DAC (decibels) */
#define PCM178X_MAX_ATTN_db	-100

/** @brief Sets up the DAC for use and initializes the volume to zero (muted). Must be called after supplying power to the DAC */
void pcm178x_init(void);

/** @brief Sets the DAC output attenuation to a specified value.
 * @param db_attn The attenuation level, expressed in whole decibels, from PCM178X_MAX_ATTN_db to 0db.
 *                 Attenuations levels below PCM178X_MAX_ATTN_db db will enable an output mute. */
void pcm178x_set_vol(int8_t db_attn);

#endif /* PCM178X_H_ */