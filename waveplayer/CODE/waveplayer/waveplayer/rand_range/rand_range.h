#ifndef RAND_RANGE_H_
#define RAND_RANGE_H_

#include <stdlib.h>
#include <stdint.h>

/** @brief Generates a random number between minval and maxval that is guaranteed uniform using the standard C PRNG.
 * Note; don't forget to seed the PRNG using srand at suitable time(s).
 * @param minval The minimum allowable value to return
 * @param maxval The maximum allowable value to return
 * @return The next random number in the range minval <= x <= maxval */
inline unsigned int rand_range(int minval, int maxval){
	if(minval > maxval){
		int temp  = minval;
		minval = maxval;
		maxval = temp; 
	}
	if(minval == maxval){
		return minval;
	}
	
	//We now know minval < maxval and there are at least two valid return values
	const int raw_rand = rand();		//Raw random value, in range [0 - RAND_MAX]
	const int range = maxval - minval + 1;	//The range of values we want to generate
	const int rem = RAND_MAX % range;
	const int bucket = RAND_MAX / range;
	
	if(raw_rand < RAND_MAX - rem){		//If the raw random value we got does not lie in the small last bucket, we have a safe value to return
		return minval + raw_rand / bucket;
	}else{
		//Otherwise, try again...
		return rand_range(minval, maxval);
	}
}

#endif /* RAND_RANGE_H_ */