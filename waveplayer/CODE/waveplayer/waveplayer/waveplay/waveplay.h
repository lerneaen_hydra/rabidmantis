/*
 * waveplay.h
 *
 * Created: 2/20/2014 2:59:13 PM
 *  Author: lock
 */ 


#ifndef WAVEPLAY_H_
#define WAVEPLAY_H_

#include "../board/board.h"

#define ERRMSG_NO_FILE			"ERR: could not find file '%s'.\n"
#define ERRMSG_CHUNKID			"ERR: Wave header chunkid incorrect; %.4s\n"
#define ERRMSG_FORMAT			"ERR: Wave header format incorrect; %.4s\n"
#define ERRMSG_SUBCHUNK1_ID		"ERR: Wave header subchunk1id incorrect; %.4s\n"
#define ERRMSG_AUDIO_FORMAT		"ERR: Wave header specifies non-PCM format.\n"
#define ERRMSG_NUM_CHANNELS		"ERR: Wave header specifies %u channels, only 1 or 2 supported.\n"
#define ERRMSG_SAMPLE_RATE		"ERR: Wave header specifies sample rate %lu, supported rates are 8000, 11025, 16000, 22100, 32000, 44100, and 48000 Hz.\n"
#define	ERRMSG_BIT_DEPTH		"ERR: Wave header specifies bit depth %u, supported values are 8, 16, or 24"
#define ERRMSG_HDRFAIL			"ERR: Could not open file, header incorrect/corrupt.\n"
#define ERRMSG_AUTOSKIP_PREFIX	"ERR: Could not detect start of wave data content; "
#define ERRMSG_AUTOSKIP_LEN		"scanned past end of file.\n"
#define ERRMSG_AUTOSKIP_NUM		"maximum number of unrecognized fields skipped.\n"


#define ERRMSG_FILESIZE_PREFIX	"ERR: File size error too large; "
#define WARNMSG_FILESIZE_PREFIX	"WARN02: "
#define MSG_FILESIZE_BODY		 "Wave header specifies a total size of %lu bytes, file is actually %lu bytes."
#define WARNMSG_FILESIZE_SUFFIX	" Assuming smallest value.\n"
#define ERRMSG_FILESIZE_SUFFIX	"\n"


#define WARNMSG_NONSTANDARD_WAVE	"WARN01: Unexpected wave header field detected! Attempting to bypass field \"%.4s\" with %u bytes\n"

#define STATUSMSG_STARTUP			"MSG01: Started playback of file with %u channel(s), %lu Hz sample rate, %u bits per sample, and a size of %lu bytes.\n"
#define STATUSMSG_AUTOSKIP_SUCCESS	"MSG02: Found valid wave data header!\n"

/** @brief Full wave header initialization snippet */
#define WAVE_HEADER_INIT {{0},0,{0},{0},0,0,0,0,0,0,0,{0},0}
	
/** @brief Maximum number of unrecognized wave header fields to skip */
#define WAVE_AUTOSKIP_FIELDS_NUM	10

/** @brief Maximum acceptable filesize difference between wave header and filesystem */
#define WAVE_FILESIZE_MAXERR	16

/** @brief Wave file header structure */
struct wave_hdr_t {
	//RIFF chunk descriptor
	char chunk_id[4];			// Contains "RIFF" for a valid file
	uint32_t chunk_size;		// Size of entire file (bytes) excluding this entry and chunk_id (IE. total size - 8)
	char format[4];				// Contains "WAVE" for a valid file
	
	//Format chunk descriptor
	char subchunk1_id[4];		// Contains "fmt " (fmt followed by space) for a valid file
	uint32_t subchunk1_size;	// The size of the rest of this chunk (bytes); must be 16 for a valid file
	uint16_t audio_format;		// Must be 1 for a valid file (indicates PCM data)
	uint16_t num_channels;		// Contains the number of channels. Only 1 (mono) and 2 (stereo) are supported for Waveplayer
	uint32_t sample_rate;		// Sample rate for data. Supported rates are listed in wave_sr_t.
	uint32_t byte_rate;			// Must be equal to sample_rate * num_channels * bits_per_sample / 8
	uint16_t block_align;		// Must be equal to num_channels * bits_per_sample / 8
	uint16_t bits_per_sample;	// Bit depth per sample. Must be 8 or 16 for support in Waveplayer.
	
	//Data sub-chunk
	char subchunk2_id[4];		// Contains "data" for a valid file
	uint32_t subchunk2_size;	// num_channels * bits_per_sample / 8 * N_SAMPLES, where N_SAMPLES is the total number of samples. This gives the total size of the file.
};

/** @brief Attempts to open and start playback of a file in the current directory, returns true if success and false on failure.
 * NOTE: Will close and stop playback of any currently open file!
 * If verbose mode is enabled diagnostics are also printed */
bool waveplay_open_play_file(const char* filename);

/** @brief Attempts to open and start playback of the next file in the current directory.
 * The "next" file if shuffle is false is the file with a filename that
 * immediately proceeds the last played file in alphabetical order. If no file
 * has yet been played this is the first file in alphabetical order. If no
 * proceeding file can be found (IE. if the last played file was the last file
 * in alphabetical order) the first file is played.
 * If shuffle is true the next file will be uniformly selected among the files
 * in the current directory, excluding the currently playing file. 
 * @return True if starting the next file succeeded, false otherwise */
bool waveplay_open_play_next_file(bool shuffle);

/** @brief Idle function that feeds DMA buffer with raw audio samples.
 * Should be run in a relatively fast context to reduce the risk of buffer underruns */
void waveplay_idle(void);

/** @brief Low-priority idle function. Handles autoplay functionality.
 * Should be run at the lowest possible context, preferably not in interrupt */
void waveplay_idle_lowpri(void);

/** @brief Resumes playback of currently open wave file */
void waveplay_resume(void);

/** @brief Pauses playback of currently open wave file */
void waveplay_pause(void);

/** @brief Toggles the current pause/play status of the currently open file */
void waveplay_playpause(void);

/** @brief Returns true if currently playing a file */
bool waveplay_get_active(void);

/** @brief Stops playback and closes currently open wave file */
void waveplay_stop(void);

#endif /* WAVEPLAY_H_ */