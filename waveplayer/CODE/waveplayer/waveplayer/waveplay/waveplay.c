/*
 * waveplay.c
 *
 * Created: 2/20/2014 2:59:20 PM
 *  Author: lock
 */ 

#include "waveplay.h"
#include <stdio.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "../globals.h"
#include "../shell/shell.h"
#include "../board/board.h"
#include "../sd-reader/fat.h"
#include "../sd-reader/utils.h"
#include "../sd-reader/byteordering.h"
#include "../flex_settings/flex_settings.h"
#include "../systime/systime.h"
#include "../drivers/pcm178x.h"
#include "../rand_range/rand_range.h"

/** @brief Number of bytes in wave file not included in chunk_size entry */
#define CHUNK_SIZE_OFFSET	8

/** @brief Sample value for 8-bit unsigned wave files that corresponds to an "at-rest" (zero) speaker position */
#define UNSIGNED_WAVE_ZERO	128

/** @brief Internal type for current waveplayer state */
enum waveplay_state_t {waveplay_state_idle, waveplay_state_active_prefix, waveplay_state_active_prefix_wait, waveplay_state_active, waveplay_state_active_postfix};

/** @brief Internal variable for current system state */
static volatile enum waveplay_state_t waveplay_state = waveplay_state_idle;

/** @brief Signalling flags for controlling the playback status of the current file */
static volatile bool waveplay_pauseflag = false;
static volatile bool waveplay_stopflag = false;
static volatile bool waveplay_autoplay_ok = false;

/** @brief Adds a block of 8-bit samples to the output buffer.
 * @param lrsamples Pointer to array of unsigned 8-bit samples, left and right,
 * starting with the left sample, where a value of 0x00 corresponds to some
 * minimum output, 0xFF corresponds to the maximum output, and 0x7F corresponds
 * to some zero value.
 * @param samples The number of valid samples in *lrsamples, up to
 * WAVEPLAY_BLOCKSAMPS. Values less than WAVEPLAY_BLOCKSAMPS will result in
 * sending zero (idle) padding samples to the DAC.
 *  */
void waveplay_bufwrite_8bit(const uint8_t * lrsamples, uint8_t samples);

/** @brief Similar to waveplay_bufwrite_8bit, but for *signed* little-endian 16-bit samples (0x00 -> zero, 0x7F -> max, 0x80 -> min) */
void waveplay_bufwrite_16bit(const int16_t * lrsamples, uint8_t samples);

static struct fat_file_struct* file = NULL;

/** @brief Wave header for currently playing wave file */
static volatile struct wave_hdr_t wave_hdr = WAVE_HEADER_INIT;

/** @brief The file-name of the last/currently playing file */
static char last_filename[32] = {'\0'};
	
static inline bool waveplay_check_wav_extension(const char* str){
	bool retval = false;
	uint8_t len = strnlen(str,32);	//Limit check to 32 characters
	if(len >= 4){
		if(strncmp_P(&str[len-4],PSTR(".wav"),4) == 0){	//Check the last four characters
			retval = true;
		}
	}
	return retval;
}

bool waveplay_open_play_next_file(bool shuffle){
	bool retval = false;
	//If another file is currently playing back, stop playback
	if(waveplay_state != waveplay_state_idle){
		if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
			shell_printf_ext_P(PSTR(MSG_PLAYBACK_RESTART));
		}
		waveplay_stop();
		while(waveplay_state != waveplay_state_idle){};
	}
	
	//Switch to the maximum clock speed to reduce file load times. Speed restored by waveplay_open_file() function call
	board_set_clk_flow_ctrl(MAXSPEED_FCPU);
	
	if(shuffle){
		//Play a random file (but not the current/last file)
		//Find the number of files in the current directory
		struct fat_dir_entry_struct dir_entry;
		fat_reset_dir(dd);
		uint16_t n_files = 0;
		uint16_t n_valid_files = 0;
		while(fat_read_dir(dd, &dir_entry)){
			n_files++;	
			if(waveplay_check_wav_extension(dir_entry.long_name)){
				n_valid_files++;
			}
		}
		
		//Verify there are enough files to read, otherwise quit
		if(n_valid_files <= 1){
			if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
				shell_printf_ext_P(PSTR(ERROR_NO_FILES));
			}
			board_set_clk_flow_ctrl(STANDBY_FCPU);
			return retval;
		}
				
		//Find a new file to play
		bool finished = false;
		while(!finished){
			fat_reset_dir(dd);
			uint16_t file = rand_range(0,n_valid_files);
			for(uint16_t i = 0; i < file; i++){	//Get the i'th valid file, skipping over invalid file types
				do{
					fat_read_dir(dd, &dir_entry);	
				}while(!waveplay_check_wav_extension(dir_entry.long_name));
			}
			//If we happened to select the file we just played, try again
			if(waveplay_check_wav_extension(dir_entry.long_name)){
				if(strncmp(last_filename,dir_entry.long_name,sizeof(last_filename)/sizeof(last_filename[0])) != 0){
					finished = true;
				}	
			}
		}
		if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
			shell_printf_ext_P(PSTR(MSG_SHUFFLE_START),dir_entry.long_name);
		}
		retval = waveplay_open_play_file(dir_entry.long_name);
	}else{
		//Find the number of files in the current directory
		struct fat_dir_entry_struct dir_entry;
		fat_reset_dir(dd);
		uint16_t n_files = 0;
		while(fat_read_dir(dd, &dir_entry)){
			n_files++;
		}
		
		if(n_files == 0){
			if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
				shell_printf_ext_P(PSTR(ERROR_NO_FILES));
			}
			board_set_clk_flow_ctrl(STANDBY_FCPU);
			return retval;
		}
		
		//Find the next file to play
		bool finished = false;
		char candidate[32];
		while(!finished){
			fat_reset_dir(dd);
			memset(candidate,'\0',sizeof(candidate)/sizeof(candidate[0]));
			for(uint16_t i = 0; i < n_files; i++){
				fat_read_dir(dd,&dir_entry);
				if(waveplay_check_wav_extension(dir_entry.long_name)){
					const bool after_lastfile = (strncmp(last_filename,dir_entry.long_name,sizeof(candidate)/sizeof(candidate[0])) < 0) || (last_filename[0] == '\0');
					const bool before_candidate = (strncmp(candidate,dir_entry.long_name,sizeof(candidate)/sizeof(candidate[0])) > 0) || (candidate[0] == '\0');
					//If the new file name is after the last file name and less than the candidate file name
					if(after_lastfile && before_candidate){
						strncpy(candidate,dir_entry.long_name,sizeof(candidate)/sizeof(candidate[0]));
					}
				}
			}
			if(strnlen(candidate,sizeof(candidate)/sizeof(candidate[0])) == 0){
				//If candidate is unchanged, then lastfile was the last file in alphabetical order, restart scan
				last_filename[0] = '\0';
			}else{
				finished = true;
			}
		}
		if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
			shell_printf_ext_P(PSTR(MSG_ORDERED_START),candidate);
		}
		retval = waveplay_open_play_file(candidate);
	}
	return retval;
}

bool waveplay_open_play_file(const char* filename){
	struct fat_dir_entry_struct dir_entry;
	
	//If another file is currently playing back, stop playback
	if(waveplay_state != waveplay_state_idle){
		if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
			shell_printf_ext_P(PSTR(MSG_PLAYBACK_RESTART));
		}
		waveplay_stop();
		while(waveplay_state != waveplay_state_idle){};
	}
	
	//Switch to the maximum clock speed to reduce file load times
	board_set_clk_flow_ctrl(MAXSPEED_FCPU);
	
	fat_reset_dir(dd);
	bool filefound = find_file_in_dir(fs,dd,filename,&dir_entry);
	if(filefound == false){
		if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
			shell_printf_ext_P(PSTR(ERRMSG_NO_FILE),filename);
		}
	}else{
		file = open_file_in_dir(fs,dd,filename);
		if(file == NULL){
			//Should never occur, but test anyway
			return false;
		}
		//Update the last-file memory element
		strncpy(&last_filename[0],filename,sizeof(last_filename)/sizeof(last_filename[0]));
		
		//Could open file, now seed the wave header
		uint8_t buf[sizeof(struct wave_hdr_t)/sizeof(uint8_t)];
		fat_read_file(file,buf,sizeof(buf)/sizeof(uint8_t));
				
		struct wave_hdr_t wave_hdr_temp = WAVE_HEADER_INIT;
		
		memcpy(&wave_hdr_temp.chunk_id, &buf[0],4);
		memcpy(&wave_hdr_temp.chunk_size, &buf[4],4);		
		memcpy(&wave_hdr_temp.format, &buf[8],4);
		memcpy(&wave_hdr_temp.subchunk1_id, &buf[12],4);
		memcpy(&wave_hdr_temp.subchunk1_size, &buf[16],4);
		memcpy(&wave_hdr_temp.audio_format, &buf[20],2);		
		memcpy(&wave_hdr_temp.num_channels, &buf[22],2);
		memcpy(&wave_hdr_temp.sample_rate, &buf[24],4);
		memcpy(&wave_hdr_temp.byte_rate, &buf[28],4);
		memcpy(&wave_hdr_temp.block_align, &buf[32],2);
		memcpy(&wave_hdr_temp.bits_per_sample, &buf[34],2);
		memcpy(&wave_hdr_temp.subchunk2_id, &buf[36],4);
		memcpy(&wave_hdr_temp.subchunk2_size, &buf[40],4);
		
		//Now, ensure that the acquired data is something we can parse
		bool parse_fail = false;
		bool verbose = setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO);
		
		if(strncmp_P(wave_hdr_temp.chunk_id,PSTR("RIFF"),sizeof(wave_hdr_temp.chunk_id)/sizeof(uint8_t))){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_CHUNKID),wave_hdr_temp.chunk_id);
			}
		}
		
		uint32_t filesize_error;
		bool use_fs_size = false;
		
		if(wave_hdr_temp.chunk_size + CHUNK_SIZE_OFFSET > dir_entry.file_size){
			filesize_error = wave_hdr_temp.chunk_size + CHUNK_SIZE_OFFSET - dir_entry.file_size;
			use_fs_size = true;
		}else{
			filesize_error = dir_entry.file_size - (wave_hdr_temp.chunk_size + CHUNK_SIZE_OFFSET);
		}		

		if(filesize_error){
			if(filesize_error > WAVE_FILESIZE_MAXERR){
				parse_fail = true;
				if(verbose){
					shell_printf_ext_P(PSTR(ERRMSG_FILESIZE_PREFIX));
					shell_printf_ext_P(PSTR(MSG_FILESIZE_BODY),(unsigned long) wave_hdr_temp.chunk_size + CHUNK_SIZE_OFFSET, (unsigned long) dir_entry.file_size);
					shell_printf_ext_P(PSTR(ERRMSG_FILESIZE_SUFFIX));
				}
			}else{
				if(verbose){
					shell_printf_ext_P(PSTR(WARNMSG_FILESIZE_PREFIX));
					shell_printf_ext_P(PSTR(MSG_FILESIZE_BODY),(unsigned long) wave_hdr_temp.chunk_size + CHUNK_SIZE_OFFSET, (unsigned long) dir_entry.file_size);
					shell_printf_ext_P(PSTR(WARNMSG_FILESIZE_SUFFIX));
				}
				if(use_fs_size){
					wave_hdr_temp.chunk_size = dir_entry.file_size - CHUNK_SIZE_OFFSET;
				}
			}
		}
		
		if(strncmp_P(wave_hdr_temp.format,PSTR("WAVE"),sizeof(wave_hdr_temp.chunk_id)/sizeof(uint8_t))){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_FORMAT),wave_hdr_temp.format);
			}
		}
		
		if(strncmp_P(wave_hdr_temp.subchunk1_id,PSTR("fmt "),sizeof(wave_hdr_temp.subchunk1_id)/sizeof(uint8_t))){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_SUBCHUNK1_ID),wave_hdr_temp.format);
			}
		}
		
		if(wave_hdr_temp.audio_format != 1){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_AUDIO_FORMAT),wave_hdr_temp.format);
			}
		}
		
		if(wave_hdr_temp.num_channels != 1 && wave_hdr_temp.num_channels != 2){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_NUM_CHANNELS),wave_hdr_temp.num_channels);
			}
		}
		
		uint32_t sr = wave_hdr_temp.sample_rate;	//variable used only to increase readability of the following if() statement
		
		if(sr != 8000 && sr != 11025 && sr != 16000 && sr != 22050 && sr != 32000 && sr != 44100 && sr != 48000){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_SAMPLE_RATE),(long unsigned)sr);
			}
		}
		
		if(wave_hdr_temp.bits_per_sample != 8 && wave_hdr_temp.bits_per_sample != 16){
			parse_fail = true;
			if(verbose){
				shell_printf_ext_P(PSTR(ERRMSG_BIT_DEPTH),(long unsigned)sr);
			}
		}
		
		//Add minimal support for unknown data fields by skipping over unrecognized ID's
		//This assumes a structure similar to that of the RIFF, fmt, and data chunks with
		//4 characters indicating the type of field and a uint32 that indicates the length
		//in bytes.
		
		for(uint8_t i = 0;; i++){
			//If we detect the data field, we're done! yay!
			if(!strncmp_P(wave_hdr_temp.subchunk2_id,PSTR("data"),sizeof(wave_hdr_temp.subchunk2_id)/sizeof(uint8_t))){
				if(verbose && i != 0){
					shell_printf_ext_P(PSTR(STATUSMSG_AUTOSKIP_SUCCESS));
				}
				break;
			}
			if(i >= WAVE_AUTOSKIP_FIELDS_NUM){
				parse_fail = true;
				if(verbose){
					shell_printf_ext_P(PSTR(ERRMSG_AUTOSKIP_PREFIX));
					shell_printf_ext_P(PSTR(ERRMSG_AUTOSKIP_NUM));	
				}
				break;
			}
			if(verbose){
				shell_printf_ext_P(PSTR(WARNMSG_NONSTANDARD_WAVE),wave_hdr_temp.subchunk2_id,wave_hdr_temp.subchunk2_size);
			}
			int32_t temp = 0;
			fat_seek_file(file,&temp,FAT_SEEK_CUR);	//Get the current position in the open file
					
			if(temp + wave_hdr_temp.subchunk2_size > dir_entry.file_size){	//If we need to skip past the length of the file, we're boned =(
				parse_fail = true;
				if(verbose){
					shell_printf_ext_P(PSTR(ERRMSG_AUTOSKIP_PREFIX));
					shell_printf_ext_P(PSTR(ERRMSG_AUTOSKIP_LEN));	
				}
				break;
			}
					
			//Move past the current, unknown, field, and read the next field
			temp = wave_hdr_temp.subchunk2_size;
			fat_seek_file(file,&temp,FAT_SEEK_CUR);
			uint8_t tempbuf[(sizeof(wave_hdr_temp.subchunk2_id)+sizeof(wave_hdr_temp.subchunk2_size))/sizeof(uint8_t)];
			fat_read_file(file,&tempbuf[0],sizeof(tempbuf)/sizeof(uint8_t));
			memcpy(&wave_hdr_temp.subchunk2_id, &tempbuf[0],4);
			memcpy(&wave_hdr_temp.subchunk2_size, &tempbuf[4],4);
		}
		
		
		// If there was an issue parsing the header, we can't continue
		if(parse_fail){
			shell_printf_ext_P(PSTR(ERRMSG_HDRFAIL));
			fat_close_file(file);
			board_set_clk_flow_ctrl(STANDBY_FCPU);
			return false;
		}
		
		// Otherwise, the file was correct, so start playback
		if(verbose){
			shell_printf_ext_P(PSTR(STATUSMSG_STARTUP),wave_hdr_temp.num_channels, wave_hdr_temp.sample_rate, wave_hdr_temp.bits_per_sample, wave_hdr_temp.chunk_size + CHUNK_SIZE_OFFSET);
		}
		
		//Switch clock frequency based on file samplerate
		enum wave_sr_t wave_sr;
		switch(wave_hdr_temp.sample_rate){
		case 8000:
			wave_sr = wave_sr_8khz;
			break;
		case 11025:
			wave_sr = wave_sr_11_025khz;
			break;
		case 16000:
			wave_sr = wave_sr_16khz;
			break;
		case 22050:
			wave_sr = wave_sr_22_050khz;
			break;
		case 32000:
			wave_sr = wave_sr_32khz;
			break;
		case 44100:
			wave_sr = wave_sr_44_1khz;
			break;
		case 48000:
			wave_sr = wave_sr_48khz;
			break;
		default:	//Should never occur
			shell_printf_ext_P(PSTR(ERRMSG_HDRFAIL));
			fat_close_file(file);
			board_set_clk_flow_ctrl(STANDBY_FCPU);
			return false;
		}
		board_set_clk_flow_ctrl((enum board_fcpu_t)wave_sr);
		
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
			wave_hdr = wave_hdr_temp;	
		}
		
		waveplay_state = waveplay_state_active_prefix;
		return true;
	}
	
	
	fat_close_file(file);
	board_set_clk_flow_ctrl(STANDBY_FCPU);
	return false;
}

bool waveplay_get_active(void){
	return waveplay_state != waveplay_state_idle;
}

void waveplay_resume(void){
	waveplay_pauseflag = false;
	if(waveplay_get_active() && setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
		shell_puts_ext_P(PSTR(MSG_PLAYBACK_RESUME));
	}
}

void waveplay_pause(void){
	waveplay_pauseflag = true;
	if(waveplay_get_active() && setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
		shell_puts_ext_P(PSTR(MSG_PLAYBACK_PAUSE));
	}
}

void waveplay_playpause(void){
	waveplay_pauseflag = !waveplay_pauseflag;
	if(waveplay_get_active() && setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
		if(waveplay_pauseflag){
				shell_puts_ext_P(PSTR(MSG_PLAYBACK_PAUSE));
		}else{
			shell_puts_ext_P(PSTR(MSG_PLAYBACK_RESUME));
		}
	}
}

void waveplay_stop(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		waveplay_stopflag = true;
		waveplay_autoplay_ok = false;	
	}
}

void waveplay_idle_lowpri(void){
	if(setting_get_int(PARAM_AUTOPLAY_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
		if(waveplay_autoplay_ok){
			waveplay_autoplay_ok = false;
			asm volatile ("" : : : "memory");	//Insert a memory barrier to ensure waveplay_autoplay_ok is cleared before the call to the function. As we're using LTO this order may otherwise not be preserved
			waveplay_open_play_next_file(setting_get_int(PARAM_SHUFFLE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO));
		}
	}else{
		waveplay_autoplay_ok = false;
	}
}

void waveplay_idle(void){
	static systime_t lowpwr_timeout;		//Timeout time for enabling low-power mode after playback completed
	static systime_t analog_valid_timeout;	//Timeout time for removing mute after applying analog voltage on startup and holding analog voltage after applying mute at end of playback.
	static uint32_t bytes_rem = 0;			//Number of bytes remaining in current wave file
	static bool output_mute = false;		//Shadow variable of current output mute state
	
	struct wave_hdr_t wave_hdr_shdw;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		wave_hdr_shdw = wave_hdr;
	}
	
	//Check if there's been a buffer underrun, and if so handle it
	if(g_dac_buffer_underrun_latch){
		shell_puts_ext_P(PSTR(ERROR_BUFFER_UNDERRUN));
		waveplay_stopflag = true;
		g_dac_buffer_underrun_latch = false;
	}
	
	switch(waveplay_state){
	default:
		//Should never occur
		waveplay_state = waveplay_state_idle;
		break;
	case waveplay_state_idle:
		if(systime_get_delay_passed(lowpwr_timeout)){
			if(setting_get_int(PARAM_LOWPWR_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
				board_set_pwrdn_analog(true);
			}	
		}
		if(waveplay_stopflag){	//Clear a stop flag set by the buffer underrun latch
			waveplay_stopflag = false;
		}
		break;
	case waveplay_state_active_prefix:
		//If we are powered down, turn on analog power and start a long (100's of milliseconds) wait
		if(board_get_pwrdn_analog()){
			analog_valid_timeout = systime_get_delay(MS2US(ANALOG_MUTE_STAB_TIME_LONG_ms));
			board_set_pwrdn_analog(false);
		}else{	//Else we are powered up
			//Set the startup-delay to zero
			analog_valid_timeout = systime_get_delay(MS2US(ANALOG_MUTE_STAB_TIME_SHORT_ms));
		}
		board_set_cheapamp_mute(true);
		output_mute = true;
		board_set_dis_dac();	//Ensure DMA module is stopped
		g_dac_buffer_underrun_latch = false;
		waveplay_pauseflag = false;
		waveplay_autoplay_ok = false;
		bytes_rem = wave_hdr_shdw.subchunk2_size;
						
		//Feed DAC with initial zero data
		while(DMA_BUF_GET_QUERY_WRITE()){
			const uint8_t finalbuf[WAVEPLAY_BLOCKSAMPS] = {0};
			waveplay_bufwrite_8bit(&finalbuf[0],WAVEPLAY_BLOCKSAMPS);
		}
		board_set_en_dac();	//Start DMA module and DAC
		waveplay_state = waveplay_state_active_prefix_wait;
		//Flow into the wait state directly, don't break here
	case waveplay_state_active_prefix_wait:
		if(!systime_get_delay_passed(analog_valid_timeout)){
			//Analog output is not yet stable, continue to feed DAC with zero data
			while(DMA_BUF_GET_QUERY_WRITE()){
				const uint8_t finalbuf[WAVEPLAY_BLOCKSAMPS] = {0};
				waveplay_bufwrite_8bit(&finalbuf[0],WAVEPLAY_BLOCKSAMPS);
			}
		}else{
			//Analog output is stable, unmute, ramp up volume to final value, and transition to active state
			board_set_cheapamp_mute(false);
			output_mute = false;
			waveplay_state = waveplay_state_active;	
			pcm178x_set_vol(setting_get_int(PARAM_VOLUME_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO));
		}
		break;
	case waveplay_state_active:
		if(bytes_rem == (uint32_t)0 || waveplay_stopflag){
			//If we're stopping playback because the file has ended we may want to continue playback
			if(!waveplay_stopflag){
				waveplay_autoplay_ok = true;
			}else{
				waveplay_autoplay_ok = false;
			}
			//File is finished or we got a stop command, disable playback
			waveplay_stopflag = false;
			waveplay_state = waveplay_state_active_postfix;
			break;
		}
		
		//Write DMA samples based on the wave header
		if(wave_hdr_shdw.num_channels == 1 && wave_hdr_shdw.bits_per_sample == 8){
			//One mono 8-bit channel, read and duplicate data for left and right channels, finally output to bufwrite
			while(DMA_BUF_GET_QUERY_WRITE()){
				if(!waveplay_pauseflag && !output_mute){
					uint8_t finalbuf[WAVEPLAY_BLOCKSAMPS];
					uint8_t tempbuf[WAVEPLAY_BLOCKSAMPS/2];
					
					uint16_t bytes_to_read;
					if(bytes_rem > sizeof(tempbuf)/sizeof(tempbuf[0])){
						bytes_to_read = sizeof(tempbuf)/sizeof(tempbuf[0]);
						bytes_rem -= bytes_to_read;
					}else{
						bytes_to_read = bytes_rem;
						bytes_rem = 0;
					}
					
					fat_read_file(file,tempbuf,bytes_to_read);
					
					WAVEPLAY_BLOCKSAMPS_t i = 0;
					WAVEPLAY_BLOCKSAMPS_t j = 0;
					//Duplicate left and right channels for the final buffer
					while(j < sizeof(tempbuf)/sizeof(uint8_t)){
						finalbuf[i++] = tempbuf[j];
						finalbuf[i++] = tempbuf[j++];
					}
					waveplay_bufwrite_8bit(&finalbuf[0],bytes_to_read*2);	//As bytes_to_read applies to one channel multiply by two to get the total number of samples
				}else{
					const uint8_t finalbuf[WAVEPLAY_BLOCKSAMPS] = {0};
					waveplay_bufwrite_8bit(&finalbuf[0],WAVEPLAY_BLOCKSAMPS);
				}
			}

			
		
		}else if(wave_hdr_shdw.num_channels == 2 && wave_hdr_shdw.bits_per_sample == 8){
			//Two stereo 8-bit channels, directly read and output to bufwrite
			while(DMA_BUF_GET_QUERY_WRITE()){
				if(!waveplay_pauseflag && !output_mute){
					uint8_t buf[WAVEPLAY_BLOCKSAMPS];
					uint16_t bytes_to_read;
					if(bytes_rem > sizeof(buf)/sizeof(uint8_t)){
						bytes_to_read = sizeof(buf)/sizeof(uint8_t);
						bytes_rem -= bytes_to_read;
					}else{
						bytes_to_read = bytes_rem;
						bytes_rem = 0;
					}
					fat_read_file(file,buf,bytes_to_read);
					waveplay_bufwrite_8bit(&buf[0],bytes_to_read);	//As we're working with 2 8-bit channels bytes_to_read exactly matches the total number of samples
				}else{
					const uint8_t buf[WAVEPLAY_BLOCKSAMPS] = {0};
					waveplay_bufwrite_8bit(&buf[0],WAVEPLAY_BLOCKSAMPS);
				}
			}
			
			
		}else if(wave_hdr_shdw.num_channels == 1 && wave_hdr_shdw.bits_per_sample == 16){
			//One mono 16-bit channel, read and duplicate data for left and right channels, finally output to bufwrite
			while(DMA_BUF_GET_QUERY_WRITE()){
				if(!waveplay_pauseflag && !output_mute){
					int16_t finalbuf[WAVEPLAY_BLOCKSAMPS];
					uint8_t tempbuf[WAVEPLAY_BLOCKSAMPS/2*2];	//Mono audio means we need to duplicate the left and right channels, 16-bit sample depth means we need 2x as many bytes as samples
					uint16_t bytes_to_read;
					if(bytes_rem > sizeof(tempbuf)/sizeof(uint8_t)){
						bytes_to_read = sizeof(tempbuf)/sizeof(uint8_t);
						bytes_rem -= bytes_to_read;
					}else{
						bytes_to_read = bytes_rem;
						bytes_rem = 0;
					}
					fat_read_file(file,tempbuf,bytes_to_read);
					for(WAVEPLAY_BLOCKSAMPS_t i = 0; i < sizeof(tempbuf)/sizeof(uint8_t); i += 2){
						//Construct 16-bit sample from raw 8-bit data
						int16_t temp = tempbuf[i] + (tempbuf[i+1]<<8);
						//Place reconstructed sample into both left and right buffer
						finalbuf[i] = temp;
						finalbuf[i+1] = temp;
					}
					waveplay_bufwrite_16bit(&finalbuf[0],bytes_to_read);	//bytes_to_read applies to one 16-bit channel, the second argument to waveplay_bufwrite_16bit is the number of samples to output which is bytes_to_read / 2 * 2, which simplifies to bytes_to_read
				}else{
					const int16_t finalbuf[WAVEPLAY_BLOCKSAMPS] = {0};
					waveplay_bufwrite_16bit(&finalbuf[0],WAVEPLAY_BLOCKSAMPS);	
				}
			}
			
			
		}else if(wave_hdr_shdw.num_channels == 2 && wave_hdr_shdw.bits_per_sample == 16){
			//Two stereo 16-bit channels. In this specific case the raw wave data almost
			//directly matches the output to the DMA buffer (only requiring a endian swap).
			//As speed is starting to be a problem at these data rates directly
			//write to the DMA memory space
			while(DMA_BUF_GET_QUERY_WRITE()){
				if(!waveplay_pauseflag && !output_mute){
					uint8_t tempbuf[WAVEPLAY_BLOCKSAMPS*2];
					uint16_t bytes_to_read;
					if(bytes_rem > sizeof(tempbuf)/sizeof(tempbuf[0])){
						bytes_to_read = sizeof(tempbuf)/sizeof(tempbuf[0]);
						bytes_rem -= bytes_to_read;
					}else{
						bytes_to_read = bytes_rem;
						bytes_rem = 0;
					}

					fat_read_file(file,tempbuf,bytes_to_read);
					//If this is the last sample, pad the remaining bytes with zeros
					if(bytes_rem == 0){
						for(uint8_t i = bytes_to_read; i < sizeof(tempbuf)/sizeof(tempbuf[0]); i++){
							tempbuf[i] = 0;
						}	
					}
					register uint8_t* addr = DMA_BUF_GET_WRITE_ADDR();
					register uint8_t i = 0;
					while(i < WAVEPLAY_BLOCKSAMPS*2){
						*(addr) = tempbuf[i+1];
						*(addr + 1) = tempbuf[i];
						addr += 2;
						i += 2;
					}
					DMA_BUF_INC_WRITE_ADDR();
				}else{
					const int16_t dummy[WAVEPLAY_BLOCKSAMPS] = {0};
					waveplay_bufwrite_16bit(&dummy[0],WAVEPLAY_BLOCKSAMPS);
				}
			}
		}else{	//Should never occur
			waveplay_state = waveplay_state_idle;
		}
		break;
	case waveplay_state_active_postfix:
		{
			//Immediately mute output and supply dummy data to DAC for a short period
			static systime_t postfix_mute_delay;
			if(!output_mute){
				postfix_mute_delay = systime_get_delay(MS2US(MUTE_SET_DELAY_ms));
				board_set_cheapamp_mute(true);
				output_mute = true;
			}
			
			if(!systime_get_delay_passed(postfix_mute_delay)){
				//Wait for mute to propagate, afterwards disable DAC and start a low-power timer
				while(DMA_BUF_GET_QUERY_WRITE()){
					uint8_t dummy[WAVEPLAY_BLOCKSAMPS] = {0};
					waveplay_bufwrite_8bit(&dummy[0],0);
				}
			}else{
				board_set_dis_dac();
				lowpwr_timeout = systime_get_delay(S2US(LOWPWR_TIMEOUT_s));
				waveplay_state = waveplay_state_idle;
				fat_close_file(file);
				if(setting_get_int(PARAM_VERBOSE_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
					if(bytes_rem == 0){
						shell_puts_ext_P(PSTR(MSG_PLAYBACK_FINISH));
					}else{
						shell_puts_ext_P(PSTR(MSG_PLAYBACK_STOP));
					}
				}
				board_set_clk_flow_ctrl(STANDBY_FCPU);
			}	
		}
		break;
	}
}

void waveplay_bufwrite_8bit(const uint8_t * lrsamples, uint8_t samples){
	if(DMA_BUF_GET_QUERY_WRITE()){
		uint8_t* addr = DMA_BUF_GET_WRITE_ADDR();
		for(WAVEPLAY_BLOCKSAMPS_t i = 0; i < WAVEPLAY_BLOCKSAMPS; i++){
			if(samples){
				//For an 8-bit unsigned sample, convert the unsigned value to signed representation and then zero-pad the remaining bytes
				*(addr++) = -(INT8_MAX - *(lrsamples++));
				*(addr++) = 0;
				samples--;
			}else{
				*(addr++) = 0;
				*(addr++) = 0;
			}
		}
		DMA_BUF_INC_WRITE_ADDR();
	}
}

void waveplay_bufwrite_16bit(const int16_t * lrsamples, uint8_t samples){
	if(DMA_BUF_GET_QUERY_WRITE()){
		uint8_t* addr = DMA_BUF_GET_WRITE_ADDR();
		for(WAVEPLAY_BLOCKSAMPS_t i = 0; i < WAVEPLAY_BLOCKSAMPS; i++){
			if(samples){
				int16_t dummy = *(lrsamples++);
				//Incoming data is formatted as little-endian, so write lower byte first (as the audio interface is big-endian), followed by top byte, and finally zero-pad
				*(addr++) = (uint8_t) (dummy >> 8) & 0xFF;
				*(addr++) = (uint8_t) dummy & 0xFF;
				samples--;
			}else{
				*(addr++) = 0;
				*(addr++) = 0;
			}
			
		}
		DMA_BUF_INC_WRITE_ADDR();
	}
}