#ifndef GLOBALS_H_
#define GLOBALS_H_

/** @file Contains all global variables for project */

#include <stdint.h>
#include <stdbool.h>
#include "config.h"

/** @brief Global DAC sample buffer */
extern volatile uint8_t g_dmasrc[DAC_DMATFR_BYTES * DAC_DATABUF_NUM_DMATFRS];
/** @brief Indicates the latched buffer underrun status in g_dac_buffer */
extern volatile bool g_dac_buffer_underrun_latch;
/** @brief Indicates the current block of data being transferred by the DMA module.
 * Bytes in the range &g_dmasrc[g_dma_cnt * DAC_DMATFR_BYTES] to &g_dmasrc[(1+g_dma_cnt) * DAC_DMATFR_BYTES] are being sent */
extern volatile uint8_t g_dma_tail;
/** @brief Indicates the most recently added block of data that to the output buffer */
extern volatile uint8_t g_dma_head;

/** @brief SD-card / filesystem-related globals */
extern struct sd_raw_info disk_info;
extern struct fat_dir_struct* dd;
extern struct fat_dir_entry_struct directory;
extern struct fat_fs_struct* fs;
extern struct partition_struct* partition;

/** @brief Button debounce structure */
extern struct btn_debounce_t g_btn_debounce;

#endif /* GLOBALS_H_ */