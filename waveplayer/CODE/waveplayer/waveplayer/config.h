/*
 * config.h
 *
 * Created: 2/12/2014 4:55:11 PM
 *  Author: lock
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_/** @brief Number of control line inputs */
#define BOARD_CTRL_NUM	6

/** @brief Baud rate to use for serial communication */
#define UART_BAUD 57600

/** @brief Clock rate to run at when idle (not playing audio) */
#define STANDBY_FCPU board_fcpu_1_024

/** @brief Maximum clock rate, used when executing time-consuming commands */
#define MAXSPEED_FCPU board_fcpu_24_576

/** @brief Time to wait for analog voltages to stabilize before applying digital outputs to DAC */
#define ANALOG_VCC_STAB_TIME_ms	30

/** @brief Button debounce period [ms] */
#define BTN_DEOUNCE_PER_ms	10

/** @brief Long time to wait for analog output voltage to stabilize before removing mute, applicable when switching on the analog power */
#define ANALOG_MUTE_STAB_TIME_LONG_ms 200

/** @brief Long time to wait for analog output voltage to stabilize before removing mute, applicable when analog power is already present and DAC only needs initialization */
#define ANALOG_MUTE_STAB_TIME_SHORT_ms 100

/** @brief Maximum expressible ADC value */
#define BOARD_CTRL_ADCMAX	127

/** @brief Hysteresis in potentiometer input before issuing a volume change [dB] */
#define CTRL_HYST_db	2

/** @brief Maximum command length on CLI (total number of characters, including whitespace, for command and argument(s)) */
#define CMD_MAXLEN	63

/** @brief System tick period [us] */
#define SYSTICK_PER_us 500

/** @brief Analog volume control ADC call period */
#define ANALOG_VOLUME_PER_ms	300

/** @brief Propogation delay for mute signal (analog signal is held valid this long after muting the output) */
#define MUTE_SET_DELAY_ms 50

/** @brief Time to hold LED status during start-up blink. Total blink time is 2 x this value */
#define STARTUP_LED_BLINK_DELAY_ms	500

/** @brief Software flow control character codes */
#define UART_XOFF	0x13
#define UART_XON	0x11
/** @brief Time to wait after sending XOFF before switching clock sources */
#define UART_XOFF_DELAY_ms 10

/** @brief Period between continuous XON packets in idle loop */
#define UART_XON_ROBUST_PER_s	5

/** @brief Number of bytes to transfer per DMA transaction */
#define DAC_DMATFR_BYTES	128

/** @brief Seconds to maintain active mode after playback is completed  */
#define LOWPWR_TIMEOUT_s	5

/** @brief Number of user-configurable input buttons */
#define BOARD_NUM_BTNS	6

#if DAC_DMATFR_BYTES % 4 != 0
#error DAC_DMATFR_BYTES must be a multiple of four!
#endif

#if DAC_DMATFR_NUM_SAMPS > 256
#error DAC_DMATFR_NUM_SAMPS must be <= 256, DMA peripheral can only queue 256 bytes per transfer
#endif

/** @brief Number of samples (left + right) in a single DMA block transfer */
#define WAVEPLAY_BLOCKSAMPS (DAC_DMATFR_BYTES/DAC_DATA_SAMPBYTES)

/** @brief Define to an unsigned type that can contain at least WAVEPLAY_BLOCKSAMPS */
#define WAVEPLAY_BLOCKSAMPS_t uint8_t

/** @brief Number of DMA transfers that are issued per complete cycle of the DAC output buffer
 * Total RAM usage is DAC_DATABUF_NUM_DAMTFRS * DAC_DMATFR_BYTES */
#define DAC_DATABUF_NUM_DMATFRS		10

/** @brief Number of control line inputs */
#define BOARD_CTRL_NUM	6

/** @brief Comment symbol indicating config line is a comment */
#define CONFFILE_COMMENT '#'

/** @brief Default settings */
#define PARAM_VERBOSE_TITLE			"verbose"
#define PARAM_VERBOSE_TYPE			integer
#define PARAM_VERBOSE_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_VERBOSE_VAL			1
#define PARAM_VERBOSE_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_VERBOSE_MIN			0
#define PARAM_VERBOSE_MIN_PERM		FLEX_PERM_READ
#define PARAM_VERBOSE_MAX			1
#define PARAM_VERBOSE_MAX_PERM		FLEX_PERM_READ

#define PARAM_LOWPWR_TITLE			"lowpwr"
#define PARAM_LOWPWR_TYPE			integer
#define PARAM_LOWPWR_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_LOWPWR_VAL			1
#define PARAM_LOWPWR_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_LOWPWR_MIN			0
#define PARAM_LOWPWR_MIN_PERM		FLEX_PERM_READ
#define PARAM_LOWPWR_MAX			1
#define PARAM_LOWPWR_MAX_PERM		FLEX_PERM_READ

#define PARAM_VOLUME_TITLE			"volume"
#define PARAM_VOLUME_TYPE			integer
#define PARAM_VOLUME_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_VOLUME_VAL			-100
#define PARAM_VOLUME_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_VOLUME_MIN			-100
#define PARAM_VOLUME_MIN_PERM		FLEX_PERM_READ
#define PARAM_VOLUME_MAX			0
#define PARAM_VOLUME_MAX_PERM		FLEX_PERM_READ

#define PARAM_CTRLVOL_TITLE			"c_vol_en"
#define PARAM_CTRLVOL_TYPE			integer
#define PARAM_CTRLVOL_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_CTRLVOL_VAL			0
#define PARAM_CTRLVOL_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_CTRLVOL_MIN			0
#define PARAM_CTRLVOL_MIN_PERM		FLEX_PERM_READ
#define PARAM_CTRLVOL_MAX			1
#define PARAM_CTRLVOL_MAX_PERM		FLEX_PERM_READ

#define PARAM_SHUFFLE_TITLE			"shuffle"
#define PARAM_SHUFFLE_TYPE			integer
#define PARAM_SHUFFLE_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_SHUFFLE_VAL			0
#define PARAM_SHUFFLE_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_SHUFFLE_MIN			0
#define PARAM_SHUFFLE_MIN_PERM		FLEX_PERM_READ
#define PARAM_SHUFFLE_MAX			1
#define PARAM_SHUFFLE_MAX_PERM		FLEX_PERM_READ

#define PARAM_AUTOPLAY_TITLE		"autoplay"
#define PARAM_AUTOPLAY_TYPE			integer
#define PARAM_AUTOPLAY_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_AUTOPLAY_VAL			1
#define PARAM_AUTOPLAY_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_AUTOPLAY_MIN			0
#define PARAM_AUTOPLAY_MIN_PERM		FLEX_PERM_READ
#define PARAM_AUTOPLAY_MAX			1
#define PARAM_AUTOPLAY_MAX_PERM		FLEX_PERM_READ

#define PARAM_PAUSE_TITLE			"btn_pause"
#define PARAM_PAUSE_TYPE			integer
#define PARAM_PAUSE_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_PAUSE_VAL				-1
#define PARAM_PAUSE_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_PAUSE_MIN				-1
#define PARAM_PAUSE_MIN_PERM		FLEX_PERM_READ
#define PARAM_PAUSE_MAX				(2*BOARD_NUM_BTNS-1)
#define PARAM_PAUSE_MAX_PERM		FLEX_PERM_READ

#define PARAM_RESUME_TITLE			"btn_resume"
#define PARAM_RESUME_TYPE			integer
#define PARAM_RESUME_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_RESUME_VAL			-1
#define PARAM_RESUME_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_RESUME_MIN			-1
#define PARAM_RESUME_MIN_PERM		FLEX_PERM_READ
#define PARAM_RESUME_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_RESUME_MAX_PERM		FLEX_PERM_READ

#define PARAM_TGL_PAUSE_TITLE		"btn_tgl_pause"
#define PARAM_TGL_PAUSE_TYPE		integer
#define PARAM_TGL_PAUSE_FLEXTYPE	FLEX_TYPE_INTEGER
#define PARAM_TGL_PAUSE_VAL			-1
#define PARAM_TGL_PAUSE_VAL_PERM	FLEX_PERM_READWRITE
#define PARAM_TGL_PAUSE_MIN			-1
#define PARAM_TGL_PAUSE_MIN_PERM	FLEX_PERM_READ
#define PARAM_TGL_PAUSE_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_TGL_PAUSE_MAX_PERM	FLEX_PERM_READ

#define PARAM_STOP_TITLE			"btn_stop"
#define PARAM_STOP_TYPE				integer
#define PARAM_STOP_FLEXTYPE			FLEX_TYPE_INTEGER
#define PARAM_STOP_VAL				-1
#define PARAM_STOP_VAL_PERM			FLEX_PERM_READWRITE
#define PARAM_STOP_MIN				-1
#define PARAM_STOP_MIN_PERM			FLEX_PERM_READ
#define PARAM_STOP_MAX				(2*BOARD_NUM_BTNS-1)
#define PARAM_STOP_MAX_PERM			FLEX_PERM_READ

#define PARAM_PLAYNEXT_TITLE		"btn_playnext"
#define PARAM_PLAYNEXT_TYPE			integer
#define PARAM_PLAYNEXT_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_PLAYNEXT_VAL			-1
#define PARAM_PLAYNEXT_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_PLAYNEXT_MIN			-1
#define PARAM_PLAYNEXT_MIN_PERM		FLEX_PERM_READ
#define PARAM_PLAYNEXT_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_PLAYNEXT_MAX_PERM		FLEX_PERM_READ

#define PARAM_01_WAV_TITLE			"btn_01.wav"
#define PARAM_01_WAV_TYPE			integer
#define PARAM_01_WAV_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_01_WAV_VAL			-1
#define PARAM_01_WAV_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_01_WAV_MIN			-1
#define PARAM_01_WAV_MIN_PERM		FLEX_PERM_READ
#define PARAM_01_WAV_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_01_WAV_MAX_PERM		FLEX_PERM_READ

#define PARAM_02_WAV_TITLE			"btn_02.wav"
#define PARAM_02_WAV_TYPE			integer
#define PARAM_02_WAV_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_02_WAV_VAL			-1
#define PARAM_02_WAV_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_02_WAV_MIN			-1
#define PARAM_02_WAV_MIN_PERM		FLEX_PERM_READ
#define PARAM_02_WAV_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_02_WAV_MAX_PERM		FLEX_PERM_READ

#define PARAM_03_WAV_TITLE			"btn_03.wav"
#define PARAM_03_WAV_TYPE			integer
#define PARAM_03_WAV_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_03_WAV_VAL			-1
#define PARAM_03_WAV_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_03_WAV_MIN			-1
#define PARAM_03_WAV_MIN_PERM		FLEX_PERM_READ
#define PARAM_03_WAV_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_03_WAV_MAX_PERM		FLEX_PERM_READ

#define PARAM_04_WAV_TITLE			"btn_04.wav"
#define PARAM_04_WAV_TYPE			integer
#define PARAM_04_WAV_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_04_WAV_VAL			-1
#define PARAM_04_WAV_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_04_WAV_MIN			-1
#define PARAM_04_WAV_MIN_PERM		FLEX_PERM_READ
#define PARAM_04_WAV_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_04_WAV_MAX_PERM		FLEX_PERM_READ

#define PARAM_05_WAV_TITLE			"btn_05.wav"
#define PARAM_05_WAV_TYPE			integer
#define PARAM_05_WAV_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_05_WAV_VAL			-1
#define PARAM_05_WAV_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_05_WAV_MIN			-1
#define PARAM_05_WAV_MIN_PERM		FLEX_PERM_READ
#define PARAM_05_WAV_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_05_WAV_MAX_PERM		FLEX_PERM_READ

#define PARAM_06_WAV_TITLE			"btn_06.wav"
#define PARAM_06_WAV_TYPE			integer
#define PARAM_06_WAV_FLEXTYPE		FLEX_TYPE_INTEGER
#define PARAM_06_WAV_VAL			-1
#define PARAM_06_WAV_VAL_PERM		FLEX_PERM_READWRITE
#define PARAM_06_WAV_MIN			-1
#define PARAM_06_WAV_MIN_PERM		FLEX_PERM_READ
#define PARAM_06_WAV_MAX			(2*BOARD_NUM_BTNS-1)
#define PARAM_06_WAV_MAX_PERM		FLEX_PERM_READ


#define SW_VERSION	"0.9"

#define SW_DATE		__DATE__

#define STARTUP_TEXT	"\n"\
" ___      _    _    _ __  __          _   _\n"\
"| _ \\__ _| |__(_)__| |  \\/  |__ _ _ _| |_(_)___\n"\
"|   / _` | '_ \\ / _` | |\\/| / _` | ' \\  _| (_-<\n"\
"|_|_\\__,_|_.__/_\\__,_|_|_ |_\\__,_|_||_\\__|_/__/\n"\
"\\ \\    / /_ ___ _____| _ \\ |__ _ _  _ ___ _ _\n"\
" \\ \\/\\/ / _` \\ V / -_)  _/ / _` | || / -_) '_|\n"\
"  \\_/\\_/\\__,_|\\_/\\___|_| |_\\__,_|\\_, \\___|_|\n"\
"                                 |__/\n"\
"\n"\
"Version "SW_VERSION", "SW_DATE". http://www.rabidmantis.se\n"\
"Type 'help' and press enter for a list of commands.\n\n"


#define CONFIGFILE_NAME	"waveplay_config.txt"
#define WAVEPLAY_SIGNATURE	"#!waveplay_configfile"

#define ERROR_STARTUP_MEMCARD_NOT_FOUND "ERR01: Memory card error; could not detect card. Check connection and restart device.\n"
#define ERROR_STARTUP_MEMCARD_NO_PARTITION "ERR02: Memory card partition error; could not detect any partition.\n"
#define ERROR_STARTUP_MEMCARD_NO_FS "ERR03: Memory card filesystem error; could not detect a valid FAT16/FAT32 partition.\n"
#define ERROR_STARTUP_MEMCARD_NO_DISKINFO "ERR04: Memory card error; could not read disk information.\n"
#define ERROR_BUFFER_UNDERRUN	"ERR05: Audio stream buffer underrun, could not read data from memory card quickly enough.\nPossible resolutions;\n\t- Defragment memory card\n\t- Format memory card with larger block size\n\t- Switch memory cards\n\t- Play mono audio and/or audio with 8-bit depth\n"
#define ERROR_NO_FILES "ERR06: Not enough valid .wav files found! (1 file needed if shuffle disabled, 2 files needed if shuffle enabled.)\n"
#define MSG_STARTUP_OPENFAIL "ERR07: Failed to open "CONFIGFILE_NAME"\n"
#define MSG_STARTUP_BADSIG	"ERR08: Opened "CONFIGFILE_NAME", but detected bad signature.\nFirst line must be exactly\""WAVEPLAY_SIGNATURE"\"\n"

#define MSG_STARTUP_MEMCARD_STD_MBR "MSG01: Memory card found, standard MBR detected.\n"
#define MSG_STARTUP_MEMCARD_SFLPY_MBR "MSG02: Memory card found with \"supperfloppy\" structure (no MBR).\n"
#define MSG_STARTUP_MEMCARD_INFO "Detected card parameters;\n"
#define MSG_PLAYBACK_RESTART "MSG03: Playback in progress, stopping current file.\n"
#define MSG_PLAYBACK_FINISH "MSG04: Playback completed.\n"
#define MSG_PLAYBACK_STOP "MSG05: Playback stopped.\n"
#define MSG_PLAYBACK_PAUSE "MSG06: Playback paused.\n"
#define MSG_PLAYBACK_RESUME "MSG07: Playback resumed.\n"
#define MSG_FS_BUSY "MSG08: Cannot browse filesystem while playback active.\n"
#define MSG_PLAYBACK_ACTIVE "MSG09: Playback active.\n"
#define MSG_PLAYBACK_INACTIVE "MSG10: Playback inactive.\n"
#define MSG_VOLUME_CHANGE "MSG11: External control triggered volume change to %d dB.\n"
#define MSG_SHUFFLE_START "MSG12: Randomized file playback of \"%s\".\n"
#define MSG_ORDERED_START "MSG13: Ordred file playback of \"%s\".\n"
#define MSG_STARTUP_OPENPREFIX	"MSG14: Found configuration file, starting to apply commands;\n"
#define MSG_STARTUP_OPENPOSTFIX "MSG15: Application of configuration file commands completed.\n"

#define MSG_STARTUP_NOCONFIGFILE "\
MSG14: No configuration file found on memory card. Waveplayer will use default input configuration.\n\
To change configuration, place a plain text file (ISO/IEC 8859-1 / Windows 1252 / ASCII encoding) named\n\
\""CONFIGFILE_NAME"\" with contents structured as in the following template file in the root directory;\n\
-- START OF FILE --\n\
"WAVEPLAY_SIGNATURE"\n\
#Turn on verbose mode\n\
set -p verbose 1\n\
#Enable low-power mode. Disable to keep the analog output active at cost of power consumption.\n\
set -p lowpwr 1\n\
#Set the default volume. A value of -100 gives minimum volume, a value of -13 gives a\n\
#typical consumer maximum line-level signal, a value of 0 gives a signal amplitude of\n\
#approximately 4Vpp\n\
set -p volume -65\n\
#Enables the external volume control potentiometer. Connect a 10kOhm potentiometer from IN0 to\n\
#ground. Overrides the default volume.\n\
set -p c_vol_en 1\n\
#Enables random playback order when playing the next audio track.\n\
set -p shuffle 1\n\
#Enables automatically playing the next audio track when the current track is finished.\n\
set -p autoplay 1\n\
\n\
#All remaining settings allow for binding a button/input to a certain event.\n\
#Setting a parameter to the range of 0 - 5 will cause the associated event to occur\n\
#when INP0 - INP5 goes from a logic high to logic low level. A parameter's value in the\n\
#range 6 - 11 will cause the associated event to occur when INP0 - INP5 goes from\n\
#a logic low to logic high level. The following configuration sets up waveplayer for a\n\
#hardware configuration of 5 normally open buttons connected to INP1 through INP5, where;\n\
# - Pressing button 1 triggers the next track to be played\n\
# - Pressing button 2 pauses playback, releasing button 2 resumes playback\n\
# - Pressing button 3 stops playback\n\
# - Pressing button 4 triggers playback of the file \"01.wav\" (if it exists)\n\
# - Pressing button 5 triggers playback of the file \"02.wav\" (if it exists)\n\
set -p btn_playnext 1\n\
set -p btn_pause 2\n\
set -p btn_resume 8\n\
set -p btn_stop 3\n\
set -p btn_01.wav 4\n\
set -p btn_02.wav 5\n\
#The following functions are not used in this example and are left unused in this example\n\
#btn_tgl_pause, btn_03.wav --- btn_06.wav\n\
#See the documentation for more thorough descriptions of how these event bindings work.\n\
#Note: The last line to parse must have a newline character in order to be parsed.\n\
--- END OF FILE ---\n\
"


#endif /* CONFIG_H_ */