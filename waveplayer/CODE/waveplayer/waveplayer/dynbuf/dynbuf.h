/*
 * dynbuf.h
 * An improved ring buffer. Each ring buffer can be of different length (up to DYNBUF_INDEXTYPE) and payload type.
 * This is at cost of a more cumbersome interface and requiring the user to cast to the stored type.
 *
 * NOTE: Be sure to enable optimization so that function calls are inlined! This reduces the footprint and execution time significantly!
 *
 * Usage example: A buffer with 16 elements of 4 bytes in length (int32_t). Allocate space and use it:
 *
 * static dynbuf_t buffer={0,0,16,4,{[0 ... 16*4]=0}};	//this uses GCC specific functionality!
 * int32_t bar=1;
 * dynbuf_write(&bar,&buffer);
 * bar++;
 * dynbuf_write(&bar,&buffer);
 * bar++;
 * dynbuf_write(&bar,&buffer);
 * bar++;
 *
 * //buffer now contains 1,2,3
 *
 * *(int32_t *)dynbuf_query_read(&buffer);	//returns zero
 * dynbuf_query_write(&buffer);	//returns nonzero
 * *(int32_t *)dynbuf_peek(&buffer);		//returns 1
 * *(int32_t *)dynbuf_peek(&buffer);		//returns 1
 * *(int32_t *)dynbuf_read(&buffer);		//returns 1
 * *(int32_t *)dynbuf_peek_newest(&buffer); //returns 3
 * *(int32_t *)dynbyf_read(&buffer);		//returns 2
 * dynbuf_query_read(&buffer);	//returns nonzero
 * dynbuf_query_write(&buffer);	//returns nonzero
 * *(int32_t *)dynbuf_read(&buffer);		//returns 3
 * dynbuf_query_read(&buffer);	//returns zero
 * dynbuf_query_write(&buffer);	//returns nonzero
 *
 *  Created on: Dec 5, 2011
 *      Author: hydra
 */

#ifndef DYNBUF_H_
#define DYNBUF_H_

#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Index type for buffer, choose the smallest type that can contain the number of elements and the size of each element
 */
#define DYNBUF_INDEXTYPE	uint8_t

/**
 * @brief Index type 2 for buffer, choose smallest type that can contain the total size of a buffer, in bytes
 */
#define DYNBUF_INDEXTYPE2	uint16_t

/**
 * @brief Struct used for storing variable data for buffer. Be sure to initialize before use!
 */
typedef struct dynbyf_t{
	DYNBUF_INDEXTYPE head;		//set to 0 on init
	DYNBUF_INDEXTYPE tail;		//set to 0 on init
	DYNBUF_INDEXTYPE length;	//set to the number of elements to store
	DYNBUF_INDEXTYPE data_size;	//set to the number of bytes per element
	uint8_t data[]; 			//must be length*data_size bytes in length
} dynbuf_t;

/**
 * @brief Checks the number of elements available in the buffer
 * @param buffer_meta the buffer to check
 * @return the number of data elements available in the buffer (number of elements of length data_size)
 */
inline DYNBUF_INDEXTYPE dynbuf_query_numel_free(dynbuf_t* buffer_meta){
	return buffer_meta->length - (buffer_meta->head - buffer_meta->tail + buffer_meta->length)%buffer_meta->length - 1;
}

/**
 * @brief Checks whether there is space in a buffer to add another element
 * @param buffer_len	The length of the buffer (indices)
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @return nonzero if there is space free
 */
inline bool dynbuf_query_write(dynbuf_t* buffer_meta){
	bool retval;
	DYNBUF_INDEXTYPE dummy = buffer_meta->length - (buffer_meta->head - buffer_meta->tail + buffer_meta->length)%buffer_meta->length - 1;
	if(dummy){
		retval = true;
	}else{
		retval = false;
	}
	return retval;
}

/**
 * @brief Checks whether there is something to read from a buffer
 * @param buffer_len	The length of the buffer (indices)
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @return nonzero if there an element to read
 */
inline bool dynbuf_query_read(dynbuf_t* buffer_meta){
	return (buffer_meta->tail!=buffer_meta->head);
}

/**
 * @brief Checks the number of elements currently stored in the buffer
 * @param buffer_meta the buffer to check
 * @return the number of data elements stored in the buffer (number of elements of length data_size)
 */
inline DYNBUF_INDEXTYPE dynbuf_query_numel_used(dynbuf_t* buffer_meta){
	DYNBUF_INDEXTYPE el_free = buffer_meta->length - (buffer_meta->head - buffer_meta->tail + buffer_meta->length)%buffer_meta->length - 1;
	return buffer_meta->length - el_free -1;
}

/**
 * @brief Writes data to a buffer
 * @param data			Pointer to the data to write
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @param buffer		Pointer to the buffer to write to
 */
inline void dynbuf_write(void *data, dynbuf_t* buffer_meta){
	buffer_meta->head=(buffer_meta->head+1)%buffer_meta->length;	//update position to write to
	for(DYNBUF_INDEXTYPE i=0;i<buffer_meta->data_size;i++){		//copy data from *data to *buffer[head] byte for byte
		buffer_meta->data[ buffer_meta->head * ((DYNBUF_INDEXTYPE2)buffer_meta->data_size) + i ] = *(uint8_t *)( ((uint8_t*)data) + i);
	}
}

/**
 * @brief Reads data from a buffer
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @param buffer		Pointer to the buffer to write to
 * @return				Pointer to data element from buffer
 */
inline void* dynbuf_read(dynbuf_t* buffer_meta){
	buffer_meta->tail = (buffer_meta->tail+1)%buffer_meta->length;
	void* retval = (void*) (&buffer_meta->data[0]+((buffer_meta->tail) * ((DYNBUF_INDEXTYPE2)buffer_meta->data_size) ));
	return retval;
}

/**
 * @brief Reads data from a buffer whithout marking the read data as used
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @param buffer		Pointer to the buffer to write to
 * @return				Pointer to data element from buffer
 */
inline void* dynbuf_peek(dynbuf_t* buffer_meta){
	void* retval = (void*) (&buffer_meta->data[0]+((buffer_meta->tail+1)%buffer_meta->length * ((DYNBUF_INDEXTYPE2)buffer_meta->data_size) ));
	return retval;
}

/**
 * @brief Reads the newest data from a buffer without marking anything in the buffer as read
 */
inline void* dynbuf_peek_newest(dynbuf_t* buffer_meta){
	void* retval = (void*) (&buffer_meta->data[0]+(buffer_meta->head * ((DYNBUF_INDEXTYPE2)buffer_meta->data_size) ));
	return retval;
}

/**
 * @brief Reads the newest data from a buffer
 */
inline void* dynbuf_read_newest(dynbuf_t* buffer_meta){
	void* retval = (void*) (&buffer_meta->data[0]+(buffer_meta->head * ((DYNBUF_INDEXTYPE2)buffer_meta->data_size) ));
	buffer_meta->head = (buffer_meta->head - 1) % buffer_meta->length;
	return retval;
}

/**
 * @brief Reads the n'th newest value from a buffer without marking anything in the buffer as read
 * @param n the element to read (values larger than buffer_meta.length will wrap around. Ie if length=4, n=5 will check the same element as n=1). The newest element is element 0
 * @param buffer_meta the buffer to check
 * @return Pointer to n'th data element
 */
inline void* dynbuf_peek_n(DYNBUF_INDEXTYPE n, dynbuf_t* buffer_meta){
	void* retval = (void*) (&buffer_meta->data[0]+( ( (buffer_meta->head - n + buffer_meta->length)%buffer_meta->length ) * ((DYNBUF_INDEXTYPE2)buffer_meta->data_size) ));
	return retval;
}

/**
 * @brief Resets the buffer to an initial state (metadata only)
 */
inline void dynbuf_reset(dynbuf_t* buffer_meta){
	buffer_meta->tail=0;
	buffer_meta->head=0;
}

#endif /* DYNBUF_H_ */
