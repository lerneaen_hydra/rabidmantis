/*
 * dynbuf.c
 *
 *  Created on: Dec 5, 2011
 *      Author: hydra
 */

#include "dynbuf.h"

extern bool dynbuf_query_write(dynbuf_t* buffer_meta);
extern DYNBUF_INDEXTYPE dynbuf_query_numel_free(dynbuf_t* buffer_meta);
extern bool dynbuf_query_read(dynbuf_t* buffer_meta);
extern DYNBUF_INDEXTYPE dynbuf_query_numel_used(dynbuf_t* buffer_meta);
extern void dynbuf_write(void *data, dynbuf_t* buffer_meta);
extern void* dynbuf_read(dynbuf_t* buffer_meta);
extern void* dynbuf_peek(dynbuf_t* buffer_meta);
extern void* dynbuf_peek_newest(dynbuf_t* buffer_meta);
extern void* dynbuf_read_newest(dynbuf_t* buffer_meta);
extern void* dynbuf_peek_n(DYNBUF_INDEXTYPE n, dynbuf_t* buffer_meta);
extern void dynbuf_reset(dynbuf_t* buffer_meta);