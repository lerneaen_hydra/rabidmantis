#include "ctrl.h"
#include <stdlib.h>
#include <stdint.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include "drivers/pcm178x.h"
#include "board/board.h"
#include "flex_settings/flex_settings.h"
#include "systime/systime.h"
#include "waveplay/waveplay.h"
#include "btn_debounce/btn_debounce.h"
#include "shell/shell.h"

/** @brief Internal function for button events that call void(*fun)(void) functions */
static void ctrl_btn_fun_vv(FLEX_IDX_T param, void(*fun)(void));
static void ctrl_btn_fun_vv(FLEX_IDX_T param, void(*fun)(void)){
	if(fun == NULL){
		return;
	}
	
	FLEX_INT_T btn_setting = setting_get_int(param,FLEX_PARAM_VAL, FLEX_FORCE_SUDO);
	if(btn_setting >= 0){
		bool btn_press_flank = true;	//True if this event is to occur on a press (not pressed -> pressed) button flank
		if(btn_setting >= BOARD_NUM_BTNS){
			btn_press_flank = false;
			btn_setting -= BOARD_NUM_BTNS;	//Convert raw button value to a value in the range of 0 - (BOARD_NUM_BTNS - 1)
		}
		if(btn_debounce_get_latch(&g_btn_debounce,btn_setting,btn_press_flank)){
			(*fun)();
		}
	}
}

/** @brief Internal function for button events that call bool(*fun)(const char*) functions */
static void ctrl_btn_fun_bs(FLEX_IDX_T param, bool(*fun)(const char*), const char* str);
static void ctrl_btn_fun_bs(FLEX_IDX_T param, bool(*fun)(const char*), const char* str){
	if(fun == NULL){
		return;
	}
	
	FLEX_INT_T btn_setting = setting_get_int(param,FLEX_PARAM_VAL, FLEX_FORCE_SUDO);
	if(btn_setting >= 0){
		bool btn_press_flank = true;	//True if this event is to occur on a press (not pressed -> pressed) button flank
		if(btn_setting >= BOARD_NUM_BTNS){
			btn_press_flank = false;
			btn_setting -= BOARD_NUM_BTNS;	//Convert raw button value to a value in the range of 0 - (BOARD_NUM_BTNS - 1)
		}
		if(btn_debounce_get_latch(&g_btn_debounce,btn_setting,btn_press_flank)){
			(*fun)(str);
		}
	}
}

/** @brief Internal function for button events that call bool(*fun)(const char*) functions */
static void ctrl_btn_fun_bb(FLEX_IDX_T param, bool(*fun)(bool),bool arg);
static void ctrl_btn_fun_bb(FLEX_IDX_T param, bool(*fun)(bool),bool arg){
	if(fun == NULL){
		return;
	}
	
	FLEX_INT_T btn_setting = setting_get_int(param,FLEX_PARAM_VAL, FLEX_FORCE_SUDO);
	if(btn_setting >= 0){
		bool btn_press_flank = true;	//True if this event is to occur on a press (not pressed -> pressed) button flank
		if(btn_setting >= BOARD_NUM_BTNS){
			btn_press_flank = false;
			btn_setting -= BOARD_NUM_BTNS;	//Convert raw button value to a value in the range of 0 - (BOARD_NUM_BTNS - 1)
		}
		if(btn_debounce_get_latch(&g_btn_debounce,btn_setting,btn_press_flank)){
			(*fun)(arg);
		}
	}
}

void ctrl_idle(void){
	//Apply button settings
	ctrl_btn_fun_vv(PARAM_PAUSE_IDX,waveplay_pause);
	ctrl_btn_fun_vv(PARAM_RESUME_IDX,waveplay_resume);
	ctrl_btn_fun_vv(PARAM_TGL_PAUSE_IDX,waveplay_playpause);
	ctrl_btn_fun_vv(PARAM_STOP_IDX,waveplay_stop);
	ctrl_btn_fun_bb(PARAM_PLAYNEXT_IDX, waveplay_open_play_next_file,setting_get_int(PARAM_SHUFFLE_IDX,FLEX_PARAM_VAL,FLEX_FORCE_SUDO));
	ctrl_btn_fun_bs(PARAM_01_WAV_IDX,waveplay_open_play_file,"01.wav");
	ctrl_btn_fun_bs(PARAM_02_WAV_IDX,waveplay_open_play_file,"02.wav");
	ctrl_btn_fun_bs(PARAM_03_WAV_IDX,waveplay_open_play_file,"03.wav");
	ctrl_btn_fun_bs(PARAM_04_WAV_IDX,waveplay_open_play_file,"04.wav");
	ctrl_btn_fun_bs(PARAM_05_WAV_IDX,waveplay_open_play_file,"05.wav");
	ctrl_btn_fun_bs(PARAM_06_WAV_IDX,waveplay_open_play_file,"06.wav");

	//Apply volume control at a low rate when in idle and continuously when active
	{
		static systime_t vol_per = 0;
		if(systime_get_delay_passed(vol_per) || waveplay_get_active()){
			vol_per = systime_get_delay(MS2US(ANALOG_VOLUME_PER_ms));
			if(setting_get_int(PARAM_CTRLVOL_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
				//If an input is defined as controlling the volume
				static int8_t vol;
				//Get the raw analog value
				uint8_t vol_raw = board_get_analog_vol();
				//Reverse the input to get a value from 0 to BOARD_CTRL_ADCMAX, where 0 -> minumum volume (pot disconnected) and BOARD_CTRL_ADCMAX -> maximum volume
				vol_raw = BOARD_CTRL_ADCMAX - vol_raw;
				//Hacky conversion to 0 - 100 with deadband at either end. This assumes the volume interface needs values in the range of 0 - 100
				//We know/assume vol_raw is already in the range of 0 - 127
				//Add 27/2 bits of deadband on low side (13 bits)
				if(vol_raw < 13){
					vol_raw = 0;
					}else{
					vol_raw -= 13;
				}
				if(vol_raw > 100){
					vol_raw = 100;
				}
	
				if(abs(vol_raw - vol) >= CTRL_HYST_db){
					if(vol_raw > vol){
						vol = vol_raw - (CTRL_HYST_db - 1);
					}else{
						vol = vol_raw + (CTRL_HYST_db - 1);
					}
					int8_t vol_neg = vol + PCM178X_MAX_ATTN_db;
					const int8_t vol_neg_max = setting_get_int(PARAM_VOLUME_IDX, FLEX_PARAM_MAX, FLEX_FORCE_SUDO);
					if(vol_neg > vol_neg_max){
						vol_neg = vol_neg_max;
					}
					setting_set_int(PARAM_VOLUME_IDX, FLEX_PARAM_VAL, vol_neg,FLEX_FORCE_NORMAL);
					if(setting_get_int(PARAM_VERBOSE_IDX,FLEX_PARAM_VAL,FLEX_FORCE_SUDO)){
						shell_printf_ext_P(PSTR(MSG_VOLUME_CHANGE),vol_neg);
					}
				}
			}
		}	
	}
}