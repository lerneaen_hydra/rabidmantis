/* Rudimentary shell-like interface for embedded systems.
 * Implements a shell-like interface for running various tasks with
 * flexible arguments, such as "blink -s 0.1 -b asd -def"
 * Writes to/reads from the terminal.h library, and offers a flexible set up.
 * All commands except for 'help' must be defined in command.h/.c
 */

#ifndef SHELL_H_
#define SHELL_H_

#include <stdint.h>


/** @brief Define to use STDIO for I/O functionality. */
#define SHELL_STDIO

#ifdef SHELL_STDIO
#include <stdio.h>
#endif

#include "../config.h"
#include "terminal.h"
#include "uart.h"

/** @brief Define to add functions relevant when using this library with Atmel AVR processors */
#define SHELL_AVR

/** @brief Define to add support for extracting floating-point arguments from input lines */
//#define SHELL_FLOAT

#ifdef SHELL_STDIO
/** @brief Define to enable debug mode, will output some verbose help text on actions. Requires SHELL_STDIO to be defined */
//#define SHELL_DBG
#endif

/** @brief Define to enable the help command */
#define SHELL_HELP_EN

/** @brief Start-of-line character for terminal input */
#define SHELL_RDY	">"

/** @brief Valid exit codes for commands called by the shell. */
typedef enum shell_ret_t {cmd_exit_success = 0, cmd_exit_missing_arg, cmd_exit_invalid_arg} shell_ret_t;

#ifdef SHELL_STDIO
/** @brief Initialization function for STDIO functionality */
void shell_init(void);
#endif

/** @brief Idle function for shell functionality, run as often as possible in a low-priority context. */
void shell_idle(void);

/** @brief Puts a character on the shell output, blocking until it is done.
 * Must be used by functions *outside* of commands called by the shell */
void shell_putc_ext(char data);

/** @brief Puts a string on the shell output, blocking until it is done.
 * Must be used by functions *outside* of commands called by the shell */
void shell_puts_ext(char* str);

/** @brief Puts a character on the shell output, blocking until it is done.
 * Must be used by functions *inside* of commands called by the shell  */
void shell_putc_int(char data);

/** @brief Puts a string on the shell output, blocking until it is done.
 * Must be used by functions *inside* of commands called by the shell  */
void shell_puts_int(char* str);


#ifdef SHELL_AVR
/** @brief Functions equivalent to their namesake above, but that accept a
 * string stored in program memory */
void shell_putc_ext_P(const char* data);
void shell_puts_ext_P(const char* str);
void shell_putc_int_P(const char* data);
void shell_puts_int_P(const char* str);
#endif

#ifdef SHELL_STDIO
/** @brief Equivalent to shell_puts_ext(_P) but with formatted strings like printf */
void shell_printf_ext(const char* format,...);
void shell_printf_ext_P(const char* format,...);
#endif

/** @brief Process a complete command from some external location.
 * @param line String of data to process. Up to CMD_MAXLEN characters (excluding null-terminator) are read. */
void shell_process_extline(const char* line);

/************************************************************************/
/* Utility functions for commands using the shell						*/
/************************************************************************/

/**
 * @brief Check the most recent input line for the existence of arg.
 * @param arg Argument to check for. Set to zero-length string ("") to look
 *			for a single argument without an argument prefix that is last in
 *			the input line. (EG. foo in "command -baz qux foo").
 * @return Pointer to position of argument in line (points first character of
 *			whitespace before argument, ie the zeroth element of " -foo 1.0"),
 *			NULL if no match found.
 */
const char* shell_check_arg(char *arg);

/**
 * @brief Extract the argument to arg, excluding whitespace, from the most recent
 *			line, storing in dest, with up to len characters, including
 *			null terminator. If length(arg) >= len truncation will occur. The
 *			first len characters will be extracted. Always null-terminates.
 * If arg is not found a string with "\0" is written.
 */
void shell_extract_arg(char *arg, char *dest, int len);

#ifdef SHELL_FLOAT
/**
 * @brief Extracts the argument to arg, interpreted as a floating point number,
 * 			from the most recent line, writing the floating point number
 * 			to *dest. Should the argument not be able to be interpreted as
 * 			a floating point number (such as an argument like "foo"), or no
 * 			argument be found, nothing is written to *dest.
 * 			Returns cmd_exit_success if the argument was successfully converted,
 * 			cmd_exit_missing_arg if the argument was not found at all, and
 * 			cmd_exit_invalid_arg if the argument could not be converted to a
 * 			Floating point number.
 *			NOTE: Call with *arg "" to extract the implicit (last) argument.
 */
enum shell_ret_t shell_extract_float_arg(char *arg, float* dest);
#endif

/**
 * @brief Extracts the argument to arg, interpreted as a long int,	from the most
 * 			recent line, writing the number	to *dest. Should the argument not
 * 			be able to be interpreted as an integer (such as an argument like
 * 			"foo"), or no argument be found, nothing is written to *dest.
 * 			Returns cmd_exit_success if the argument was successfully converted,
 * 			cmd_exit_missing_arg if the argument was not found at all, and
 * 			cmd_exit_invalid_arg if the argument could not be converted to an
 * 			integer.
 * 			NOTE; an input of "12.6" will be converted to 12! Call with *arg
 *			"" to extract the implicit (last) argument.
 */
enum shell_ret_t shell_extract_int_arg(char *arg, int32_t* dest);

/**
 * @brief Extracts the length of the argument parameter arg, excluding
 * whitespace, from	the most recent line. Note; if using to allocate space
 * for storing the argument, allocate shell_extract_arg_len(...)+1 bytes to
 * have space for null termination.
 * @return The length of the argument if found, zero if not found.
 */
int shell_extract_arg_len(char *arg);

#endif /* SHELL_H_ */
