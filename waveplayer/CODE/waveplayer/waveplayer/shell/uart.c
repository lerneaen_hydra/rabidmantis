/*
 * uart.c
 *
 *  Created on: Sep 18, 2013
 *      Author: desel-lock
 */

#include "uart.h"
#include "../dynbuf/dynbuf.h"

static dynbuf_t uart_rxbuf = {0,0,UART_RX_BUFSIZE,1,{[0 ... UART_RX_BUFSIZE*1]=0}};

static bool (*fp_get_txfull)() = NULL;
static void (*fp_set_txbuf)(char data) = NULL;

void uart_init(bool (*get_txfull)(), void (*set_txbuf)(char data)){
	fp_get_txfull = get_txfull;
	fp_set_txbuf = set_txbuf;
}

char uart_getc(void){
	char retval = '\0';
	while(!dynbuf_query_read(&uart_rxbuf)){};
	retval = *(char*)dynbuf_read(&uart_rxbuf);
	return retval;
}

int uart_available(void){
	return dynbuf_query_read(&uart_rxbuf);
}

void uart_putc(char data){
	if(fp_get_txfull == NULL || fp_set_txbuf == NULL){
		return;
	}
	while(fp_get_txfull()){};
	fp_set_txbuf(data);
}

void uart_puts(char* str){
	while(*str != '\0'){
		uart_putc(*str);
		str++;
	}
}

void uart_rx_fifo_isr(char rxbyte){
	if(dynbuf_query_write(&uart_rxbuf)){
		dynbuf_write(&rxbyte, &uart_rxbuf);
	}
}
