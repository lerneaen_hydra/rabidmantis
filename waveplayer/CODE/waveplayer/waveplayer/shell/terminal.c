#include "terminal.h"
#include "uart.h"
#include "shell.h"
#include "../dynbuf/dynbuf.h"

static dynbuf_t term_buf = {0,0,TERMINAL_BUFSIZE,1,{[0 ... TERMINAL_BUFSIZE*1]=0}};
static int16_t lines = 0;	/** Number of lines in buffer */
static int16_t row = 0;		/** Number of characters in current row */

void terminal_idle(void){
	while(uart_available()){
		if(!dynbuf_query_write(&term_buf)){
			/* If the terminal input buffer is full then discard the new data.
			 * Note: Under normal circumstances the terminal input buffer will
			 * never fully fill up, as only TERMINAL_BUFSIZE - 1 non-CR/LF
			 * characters are added, ensuring that there's always space to add
			 * a single CR/LF, which triggers an action in shell.h/.c
			 */
			if(uart_getc() == '\b'){
				dynbuf_read(&term_buf);
				uart_putc('\b');			//---
				uart_putc(' ');				//"remove" one character from the echo'd output
				uart_putc('\b');			//---
				row--;
			}
		}else{
			char data = uart_getc();
			if(data >= ' ' && data <= '~' && row < CMD_MAXLEN){	//If we got an 'ordinary' character and there's space in the row (if there's no space disregard the character)
				uart_putc(data);				//Echo it
				dynbuf_write(&data, &term_buf);	//Add it to the local buffer
				row++;							//And increment the current character count
			}else if(data == '\b' && row > (signed)(sizeof(SHELL_RDY)/sizeof(char) - 1) ){		//If it was a backspace character and there's something in the terminal buffer...
				char peek = *(char*)dynbuf_peek(&term_buf);
				if(peek != '\n'){				//...and it's not the start of a row
					uart_putc('\b');			//---
					uart_putc(' ');				//"remove" one character from the echo'd output
					uart_putc('\b');			//---
					dynbuf_read_newest(&term_buf);		//And remove it from the local buffer
					row--;						//Decrement the current row count
				}else{
					uart_putc('\a');		//Otherwise make a beep on the host (can't backspace at start of line)
				}
			}else if(data == '\n' || data == '\r'){	//If it was a newline character of some sort
				uart_putc('\r');			//Output a CR...
				uart_putc('\n');			//...and a LF
				bool invalid_newline = 0;
				if(dynbuf_query_read(&term_buf)){
					char peek = *(char*)dynbuf_peek(&term_buf);
					if(peek == '\n'){			//If we just added a newline...
						invalid_newline = true;	//...don't add another
					}
				}
				if(!invalid_newline){					//If it was valid...
					char dummy = '\n';					//...add a single newline character
					dynbuf_write(&dummy, &term_buf);
					lines++;							//Update the total number of lines available...
					row = 0;							//...and reset the characters in row count.
				}
			}else{					//Else data was invalid...
				uart_putc('\a');	//...and output a beep on the host
			}
		}
	}
}

uint16_t terminal_line_available(void){
	return lines;
}

char terminal_getc(void){
	while(!lines) {}; //Wait until there's input
	while(!dynbuf_query_read(&term_buf)) {};	//Safety check to ensure raw data is available
	char retval = *(char*) dynbuf_read(&term_buf);
	if(retval == '\n'){
		lines--;
	}
	return retval;
}

char terminal_getc_incomplete(void){
	char retval = '\0';
	if(dynbuf_query_read(&term_buf)){
		char lastchar = *(char*) dynbuf_peek(&term_buf);
		if(lastchar == '\r' || lastchar == '\n'){	//If the most recent character was a newline...
			lastchar = '\0';						//...don't remove it and return a NULL character
		}else{
			dynbuf_read(&term_buf);					//Otherwise, remove it from the buffer
		}
		retval = lastchar;
	}
	return retval;
}

void terminal_putc(char data){
	row++;
	if(data == '\n'){
		uart_putc('\r');
	}
	if(data == '\n' || data == '\r'){
		row = 0;
	}
	uart_putc(data);
}

void terminal_puts(char* str){
	while(*str != '\0'){
		terminal_putc(*str);
		str++;
	}
}
