
/************************************************************************

Implement functions to check for the existence of an argument (checks the internal
string for the argument, returns nonzero if exists) and to extract the argument
(checks the internal string, writes the data to a string pointer)

 ************************************************************************/

#include <string.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include "shell.h"

#ifdef SHELL_STDIO
#ifdef SHELL_AVR
#include <avr/pgmspace.h>
#endif
#endif

#include "terminal.h"
#include "command.h"

#ifdef SHELL_HELP_EN
/** @brief Error message on invalid command, preceded by input command. */
#define SHELL_INVALID_CMD	"; command not found. Enter 'help' for a list of valid commands."
/** @brief Error message on invalid argument to command */
#define SHELL_INVALID_ARG	"\nInvalid argument(s)! See help <command> for usage."
/** @brief Error message on missing argument to command */
#define SHELL_MISSING_ARG	"\nMissing argument(s)! See help <command> for usage."
#else /* SHELL_HELP_EN */
/** @brief Error message on invalid command, preceded by input command. */
#define SHELL_INVALID_CMD	"; command not found."
/** @brief Error message on invalid argument to command */
#define SHELL_INVALID_ARG	"\nInvalid argument(s)!"
/** @brief Error message on missing argument to command */
#define SHELL_MISSING_ARG	"\nMissing argument(s)!"
#endif  /* SHELL_HELP_EN */

/** @brief Help list indentation size */
#define SHELL_INDENT		"    "

/** @brief Argument identifier prefix; tokens with this prefix are viewed as potential arguments.
 * Note that the argument value may also have this character, ie. if set to
 * "-", then "-foo -1.2" is legal. Keep in mind there will be ambiguity if
 * argument names are also numerical in this case!
 */
#define SHELL_ARG_PREFIX "-"

/** @brief Whitespace (delimiting) character for input.
 * This character separates different argument names and argument values, eg if
 * set to ' ' (space) then "foo -a sdf -b 1.0" would result in the command
 * "foo", argument "a" set to "sdf", and argument "b" set to "1.0".
 */
#define SHELL_WHITESPACE " "

/** @brief Lax whitespace (delimiting) characters. This should include
 * SHELL_WHITESPACE and newline/similar characters. Use covers situations
 * such as "foo -a -b", where "-b" does not have an ordinary whitespace suffix.
 */
#define SHELL_WHITESPACE_LAX SHELL_WHITESPACE"\r\n"

#ifdef SHELL_HELP_EN
/************************************************************************
 *	Help command.
 * The help command, executed without arguments, prints a list of the
 * valid commands. The help command, with the argument "<arg>", displays
 * the help line for the command labeled <arg>. */

/** @brief Help command name */
#define SHELL_HELP_STR "help"

/** @brief String to print before listing valid commands */
#define SHELL_HELP_PREFIX_STR "The following commands are available;\n"

/** @brief String to print after listing valid commands */
#define SHELL_HELP_SUFFIX_STR "Enter help <command> for help on a command.\nEnter <command> to execute a command.\nNote: only the first copy of an argument is matched, IE. 'foo -c bar -c qux'\nis equivalent to 'foo -c bar'."

/** @brief Help command function */
shell_ret_t shell_help_cmd(void);
/************************************************************************/
#endif /* SHELL_HELP_EN */

#ifndef SHELL_AVR
/** @brief Gets a command name location, result is pointer to content */
#define SHELL_NAME(x)	((char*)shell_cmd_array[(x)].cmd_name)

#ifdef SHELL_HELP_EN
/** @brief Gets a command's help location, result is pointer to PROGMEM with content */
#define SHELL_HELP(x)	((char*)shell_cmd_array[(x)].cmd_help)
#endif /* SHELL_HELP_EN */

/** @brief Dereferences a func. ptr location, result is pointer function pointer */
#define SHELL_FP(x) 	(shell_cmd_array[(x)].fp)

#else

/** @brief Gets a command name location, result is pointer to content */
#define SHELL_NAME(x)	((char*)pgm_read_word(&shell_cmd_array[(x)].cmd_name))

#ifdef SHELL_HELP_EN
/** @brief Gets a command's help location, result is pointer to PROGMEM with content */
#define SHELL_HELP(x)	((char*)pgm_read_word(&shell_cmd_array[(x)].cmd_help))
#endif /* SHELL_HELP_EN */

/** @brief Dereferences a func. ptr location, result is pointer function pointer */
#define SHELL_FP(x) 	((shell_cmd_fp_t)pgm_read_word(&shell_cmd_array[(x)].fp))

#endif

/** @brief Character-by-character string-reversing function. */
static void str_reverse(char s[]);

/**
 *	@brief Call before writing to shell_puts() or shell_putc() outside of the
 *	functions in command.c
 *	Clears out any existing characters from the display and the input buffer.
 */
void shell_extmsg_prefix(void);

/**
 *	@brief Call after writing to shell_puts() or shell_putc() outside of the
 *	functions in command.c
 *	Writes the shell-ready symbol.
 */
void shell_extmsg_cleanup(void);

#ifdef SHELL_FLOAT
/** @brief A safe string to float function. Returns nonzero if invalid string input */
static int cmd_str2flt(char *str, float* dest);
#endif
/** @brief A safe string to integer function. Returns nonzero if invalid string input */
static int cmd_str2int(char* str, long int* dest);

static char shell_inputline[CMD_MAXLEN + 1] = {0};		//Buffer of input
static bool shell_extline_avail = false;				//Indicates a new external line is available

/** @brief All STDIO-related functions */
#ifdef SHELL_STDIO
int shell_stream_putc(char c, FILE *stream);
int shell_stream_getc(FILE *stream);

static FILE shell_str = FDEV_SETUP_STREAM(shell_stream_putc, shell_stream_getc, _FDEV_SETUP_RW);

void shell_init(void){
	stdout = stdin = &shell_str;
}

int shell_stream_putc(char c, FILE *stream){
	(void)stream;	//Suppress unused parameter warning
	shell_putc_int(c);
	return 0;
}

int shell_stream_getc(FILE *stream){
	(void)stream;	//Suppress unused parameter warning
	return terminal_getc();
}
#endif

void shell_process_extline(const char* line){
	//Call first to wipe out any residual SHELL_RDY symbol on the output
	shell_idle();
	//Copy external line to internal shell command line
	strncpy(shell_inputline,line,CMD_MAXLEN);
	//Remove any residual new-lines
	for(uint8_t i = 0; i < CMD_MAXLEN; i++){
		if(shell_inputline[i] == '\r' || shell_inputline[i] == '\n'){
			shell_inputline[i] = '\0';
		}
	}
	shell_inputline[CMD_MAXLEN] = '\0';
	shell_extmsg_prefix();
	shell_puts_int_P(PSTR("<executing> "));
	shell_puts_int(shell_inputline);
	shell_putc_int('\n');
	//Act on the input line
	shell_extline_avail = true;
	shell_idle();
	shell_puts_int_P(PSTR("<execution completed>\n"));
}

void shell_idle(void){
	static int cmd_finished = 1;
	terminal_idle();
	if(cmd_finished){
		#ifndef SHELL_AVR
			shell_puts_int("\n"SHELL_RDY);
		#else
			shell_puts_int_P(PSTR("\n"SHELL_RDY));
		#endif
		cmd_finished = 0;
	}
	if(terminal_line_available() || shell_extline_avail){		//If there's a new line available to get
		if(!shell_extline_avail){	//If the source is not externally fed in, read from the terminal library and do some cleanup, otherwise shell_inputline has been set up beforehand
			for(uint16_t i = 0; i < sizeof(shell_inputline)/sizeof(char); i++){
				shell_inputline[i] = '\0';
			}
			for(uint16_t i = 0; terminal_line_available() && ( i < sizeof(shell_inputline)/sizeof(char) - 1 ); i++){
				shell_inputline[i] = terminal_getc();	//Feed in the new copy. Guaranteed to be null terminated from the previous clear operation.
			}	
		}else{
			shell_extline_avail = false;
		}
		/* Try to match the specified command to the stored array of commands */
		int cmd_len = strcspn(shell_inputline,SHELL_WHITESPACE_LAX);	//The length of the input command
		if(cmd_len == 0){		//If the input was just a newline, do nothing.
			#ifndef SHELL_AVR
				shell_puts_int(SHELL_RDY);
			#else
				shell_puts_int_P(PSTR(SHELL_RDY));
			#endif
			return;
		}
		char cmd[CMD_MAXLEN];
		strncpy(cmd,shell_inputline,cmd_len);
		cmd[cmd_len] = '\0';	//Now cmd contains the specified command with null termination
		shell_cmd_fp_t fp = NULL;
		int valid_command = 0;	//Stores whether the input was a valid command or not
#ifdef SHELL_HELP_EN
#ifndef SHELL_AVR
		if(strncmp(cmd,SHELL_HELP_STR,SHELL_CMD_MAX_LEN) == 0){	//Check if the command was the internal help command
#else
		if(strncmp_P(cmd,PSTR(SHELL_HELP_STR),SHELL_CMD_MAX_LEN) == 0){	//Check if the command was the internal help command
#endif
			fp = &shell_help_cmd;
			valid_command = 1;
		}else{											//If not, check if it was an external command
#endif /* SHELL_HELP_EN */
			for(uint16_t j = 0; j < sizeof(shell_cmd_array)/sizeof(shell_cmd_t); j++){
#ifndef SHELL_AVR
				if(strncmp(cmd, SHELL_NAME(j),SHELL_CMD_MAX_LEN) == 0){
#else
				if(strncmp_P(cmd, SHELL_NAME(j),SHELL_CMD_MAX_LEN) == 0){
#endif
					#ifdef SHELL_DBG
					printf("Found match! idx: %d\nHelp string:", j);
					shell_puts_int(SHELL_HELP(j));
					shell_putc_int('\n');
					#endif
					fp = SHELL_FP(j);
					valid_command = 1;
					break;
				}
			}
#ifdef SHELL_HELP_EN
		}
#endif /* SHELL_HELP_EN */
		if(fp != NULL){									//If a matching command was found
			#ifdef SHELL_DBG
			shell_puts_int("Executing command ");
			shell_puts_int(cmd);
			printf(", using function pointer address %lu\n",(long int)fp);
			#endif
			shell_ret_t retval = (*fp)();
			#ifdef SHELL_DBG
			shell_puts_int("\nCommand execution finished");
			#endif
			if(retval == cmd_exit_invalid_arg){
				#ifndef SHELL_AVR
				shell_puts_int(SHELL_INVALID_ARG);
				#else
				shell_puts_int_P(PSTR(SHELL_INVALID_ARG));
				#endif
			}else if(retval == cmd_exit_missing_arg){
				#ifndef SHELL_AVR
				shell_puts_int(SHELL_MISSING_ARG);
				#else
				shell_puts_int_P(PSTR(SHELL_MISSING_ARG));
				#endif
			}
			//Else command executed happily and made bunnies and sunshine!

		}
		if(!valid_command){	//Requested command not found
			shell_puts_int(cmd);
			#ifndef SHELL_AVR
			shell_puts_int(SHELL_INVALID_CMD);
			#else
			shell_puts_int_P(PSTR(SHELL_INVALID_CMD));
			#endif
		}
		cmd_finished = 1;
	}
}

void shell_extmsg_prefix(void){
	for(uint16_t i = 0; i < sizeof(SHELL_RDY)/sizeof(char) - 1; i++){
		shell_putc_int('\b');
	}
	while(terminal_getc_incomplete() != '\0'){	//Remove any incomplete characters from the input buffer and clear them from display
		shell_putc_int('\b');
		shell_putc_int(' ');
		shell_putc_int('\b');
	}
}

void shell_extmsg_cleanup(void){
	#ifndef SHELL_AVR
	shell_puts_int(SHELL_RDY);					//Re-print the ready character
	#else
	shell_puts_int_P(PSTR(SHELL_RDY));
	#endif
}

#ifdef SHELL_HELP_EN
shell_ret_t shell_help_cmd(){
	shell_ret_t retval = cmd_exit_success;
	int arglen = shell_extract_arg_len("");
	if(arglen){	//Command-specific help

		char arg[CMD_MAXLEN];	//Allocate space for argument and null termination.
		shell_extract_arg("",&arg[0],arglen+1);

		bool match = false;
		for(uint16_t i = 0; i < sizeof(shell_cmd_array)/sizeof(shell_cmd_t); i++){
			if(strcmp_P(arg,SHELL_NAME(i)) == 0){
				//printf("Found match with idx %u\n",i);
				char* dummy = SHELL_HELP(i);
				shell_puts_int_P(dummy);
				match = true;
				break;
			}
		}
		if(!match){
			retval = cmd_exit_invalid_arg;
		}
	}else{									//List of commands
		#ifndef SHELL_AVR
		shell_puts_int(SHELL_HELP_PREFIX_STR);
		#else
		shell_puts_int_P(PSTR(SHELL_HELP_PREFIX_STR));
		#endif
		for(uint16_t i = 0; i < sizeof(shell_cmd_array)/sizeof(shell_cmd_t); i++){
			#ifndef SHELL_AVR
			shell_puts_int(SHELL_INDENT);
			shell_puts_int(SHELL_NAME(i));
			shell_puts_int("\n");
			#else
			shell_puts_int_P(PSTR(SHELL_INDENT));
			shell_puts_int_P(SHELL_NAME(i));
			shell_puts_int_P(PSTR("\n"));
			#endif
		}
		#ifndef SHELL_AVR
		shell_puts_int(SHELL_HELP_SUFFIX_STR);
		#else
		shell_puts_int_P(PSTR(SHELL_HELP_SUFFIX_STR));
		#endif
	}
	return retval;
}
#endif /* SHELL_HELP_EN */

void shell_putc_int(char data){
	terminal_putc(data);
}

void shell_puts_int(char* str){
	terminal_puts(str);
}

void shell_putc_ext(char data){
	shell_extmsg_prefix();
	shell_putc_int(data);
	shell_extmsg_cleanup();
}

void shell_puts_ext(char* str){
	shell_extmsg_prefix();
	shell_puts_int(str);
	shell_extmsg_cleanup();
}

#ifdef SHELL_AVR
void shell_putc_ext_P(const char* data){
	shell_extmsg_prefix();
	shell_putc_int_P(data);
	shell_extmsg_cleanup();
}

void shell_puts_ext_P(const char* str){
	shell_extmsg_prefix();
	shell_puts_int_P(str);
	shell_extmsg_cleanup();
}

void shell_putc_int_P(const char* data){
	terminal_putc(pgm_read_byte(data));
}

void shell_puts_int_P(const char* str){
	char data;
	for(;;){
		data = pgm_read_byte(str++);
		if(data == '\0'){
			break;
		}
		terminal_putc(data);
	}
}
#endif

#ifdef SHELL_STDIO
void shell_printf_ext(const char* format,...){
	shell_extmsg_prefix();
	va_list args;
	va_start(args,format);
	vprintf(format, args);
	va_end(args);
	shell_extmsg_cleanup();
}

void shell_printf_ext_P(const char* format,...){
	shell_extmsg_prefix();
	va_list args;
	va_start(args,format);
	vfprintf_P(stdout, format, args);
	va_end(args);
	shell_extmsg_cleanup();
}
#endif

const char* shell_check_arg(char *arg){
	int lenarg = strlen(arg);
	const char* retval = NULL;
	if(lenarg == 0){
		//We're looking for foo with the input types "command foo" or "command ... -bar qux foo", but not "command ... -bar qux"
		//This means we need to store up to the two last tokens and make a distinction as follows;
		//	command foo				-> return pointer to position of foo
		//  command -foo			-> return pointer to position of foo
		//	command -bar qux foo	-> return pointer to position of foo
		//	command -bar foo		-> return null pointer
		// IE. if the second last element does not contain an argument prefix character we have detected the presence of some argument
		char input_shdw[CMD_MAXLEN+1];
		strncpy(input_shdw, shell_inputline, CMD_MAXLEN);
		input_shdw[CMD_MAXLEN] = '\0';	//Ensure string is null-terminated
		str_reverse(input_shdw);
		
		//Get the two last tokens from the input line
		char* test[2];
		test[0] = strtok_P(input_shdw,PSTR(SHELL_WHITESPACE SHELL_WHITESPACE_LAX));
		test[1] = strtok_P(NULL,PSTR(SHELL_WHITESPACE SHELL_WHITESPACE_LAX));
		
		//Check to make sure they both exist (non-zero length) and the second last does not contain the argument prefix character
		bool res[2] = {false};
		for(uint8_t i = 0; i < sizeof(res)/sizeof(res[0]); i++){
			if(strlen(test[i]) != 0){
				if(strstr(test[i],SHELL_ARG_PREFIX) == NULL || i==0){
					res[i] = true;
				}
			}
		}
		
		if(res[0] && res[1]){
			size_t offset = strlen(shell_inputline) - strlen(test[0]) - 1;
			retval = (const char*) shell_inputline + offset;
		}		
	}else{	//We're looking for something on the order of "... -foo ..."
		int lenesc = strlen(SHELL_ARG_PREFIX);
		int lenws = strlen(SHELL_WHITESPACE);
		char substring[CMD_MAXLEN];	//Allocate space for the substring to search for
		substring[0] = '\0';	//Clear first element, strcat will guaranteed start at first null element.

		strcat(substring,SHELL_WHITESPACE);				//Construct a copy of the argument syntax to search for
		strcat(substring,SHELL_ARG_PREFIX);
		strcat(substring,arg);

		#ifdef SHELL_DBG
		shell_puts_int("    Check arg; generated substring: '");
		shell_puts_int(substring);
		shell_puts_int("\'\n");
		#endif

		const char* candidate = strstr(shell_inputline, substring);	//If != NULL a candidate position was found
		while(candidate != NULL){
			#ifdef SHELL_DBG
			printf("    Candidate found at: %d\n",candidate - shell_inputline);
			shell_puts_int("    Following character: ");
			shell_putc_int(*(candidate + lenws + lenesc + lenarg));
			shell_putc_int('\n');
			#endif
			//If the character after the candidate is whitespace, the candidate position was a match!
			if(strchr(SHELL_WHITESPACE_LAX,*(candidate + lenws + lenesc + lenarg)) != NULL){
				retval = candidate;
				#ifdef SHELL_DBG
				shell_puts_int("Match!\n");
				#endif
				break;
				}else{
				candidate = strstr(candidate + 1, substring);
				#ifdef SHELL_DBG
				printf("No match, keep on looking from %d\n",candidate - shell_inputline);
				#endif
			}
		}

		#ifdef SHELL_DBG
		if(retval != NULL){
			printf("    Matched argument! Ptr: %lu\n",(long int)retval);
		}else{
			shell_puts_int("    Did not match argument.\n");
		}
		#endif	
	}
 	return retval;
}

void shell_extract_arg(char *arg, char *dest, int maxlen){
	const char* idx = shell_check_arg(arg);	//Check if the argument appears somewhere in the line
	if(idx != NULL){
		if(strlen(arg) == 0){
			//We simply want the last element of shell_inputline
			char input_shdw[CMD_MAXLEN+1];
			strncpy(input_shdw,shell_inputline, CMD_MAXLEN);
			input_shdw[CMD_MAXLEN] = '\0';	//Ensure the last element is null-padded
			char* pos;
			
			pos = strtok_P(input_shdw,PSTR(SHELL_WHITESPACE SHELL_WHITESPACE_LAX));
			while (pos != NULL){
				strncpy(dest,pos,maxlen);	//Somewhat inefficient method, as we copy *all* the unused parameters to dest, but as input_shdw is short this isn't a big deal
				pos = strtok_P(NULL,PSTR(SHELL_WHITESPACE SHELL_WHITESPACE_LAX));
			}
		}else{
			int lenarg = strlen(arg);
			int lenesc = strlen(SHELL_ARG_PREFIX);
			int lenws = strlen(SHELL_WHITESPACE);

			idx += lenws + lenesc + lenarg + lenws;	//Index now points to first element of argument data

			int arglen = strcspn(idx,SHELL_WHITESPACE_LAX);

			maxlen--;	//Reserve last element for null-pad
			int minlen = maxlen > arglen ? arglen : maxlen;	//Only copy as many characters as is needed

			strncpy(dest,idx,minlen);

			*(dest + minlen) = '\0';	//Zero-pad last element	
		}
	}
}

int shell_extract_arg_len(char *arg){
	int arglen = 0;
	const char* idx = shell_check_arg(arg);
	if(idx != NULL){
		if(strlen(arg) == 0){
			//We simply want the length of the last element of shell_inputline
			char input_shdw[CMD_MAXLEN+1];
			strncpy(input_shdw,shell_inputline, CMD_MAXLEN);
			input_shdw[CMD_MAXLEN] = '\0';	//Ensure the last element is null-padded
			char* pos;
			
			pos = strtok_P(input_shdw,PSTR(SHELL_WHITESPACE SHELL_WHITESPACE_LAX));
			while (pos != NULL){
				arglen = strlen(pos);	//Somewhat inefficient method, as we write the length of *all* the unused parameters, but as input_shdw is short this isn't a big deal
				pos = strtok_P(NULL,PSTR(SHELL_WHITESPACE SHELL_WHITESPACE_LAX));
			}
		}else{
			int lenarg = strlen(arg);
			int lenesc = strlen(SHELL_ARG_PREFIX);
			int lenws = strlen(SHELL_WHITESPACE);

			idx += lenws + lenesc + lenarg + lenws;	//Index now points to first element of argument data

			arglen = strcspn(idx,SHELL_WHITESPACE_LAX);	
		}
	}
	return arglen;
}

/** @brief A safe string to integer function. Returns nonzero if invalid string input */
static int cmd_str2int(char* str, long int* dest){
	int retval = 0;
	char *endptr;
	errno = 0;
	long int num = strtol(str,&endptr,10);
	if((errno == ERANGE && (num == LONG_MAX || num == LONG_MIN))
	|| (errno != 0 && num == 0)
	|| endptr == str){	// input was not numerically valid, see http://linux.die.net/man/3/strtol
		retval = 1;
	}else{
		*dest = num;
	}
	return retval;
}

#ifdef SHELL_FLOAT
/** @brief A safe string to float function. Returns nonzero if invalid string input */
static int cmd_str2flt(char *str, float* dest){
	int retval = 0;
	char *endptr;
	errno = 0;
	float num = (float) strtod(str,&endptr);
	if((errno == ERANGE && num == num)
	|| (errno != 0 && num == 0)
	|| endptr == str){	// input was not numerically valid, see http://linux.die.net/man/3/strtod
		retval = 1;
	}else{
		*dest = num;
	}
	return retval;
}

enum shell_ret_t shell_extract_float_arg(char *arg, float* dest){
	enum shell_ret_t retval;
	char dummy[CMD_MAXLEN+1];
	float val;
	if(shell_check_arg(arg)){
		shell_extract_arg(arg, dummy, sizeof(dummy)/sizeof(char));
		if(cmd_str2flt(dummy,&val)){
			retval = cmd_exit_invalid_arg;
		}else{
			retval = cmd_exit_success;
			*dest = val;
		}
	}else{
		retval = cmd_exit_missing_arg;
	}
	return retval;
}
#endif

enum shell_ret_t shell_extract_int_arg(char *arg, int32_t* dest){
	enum shell_ret_t retval;
	char dummy[CMD_MAXLEN+1];
	long int val;
	if(shell_check_arg(arg)){
		shell_extract_arg(arg, dummy, sizeof(dummy)/sizeof(char));
		if(cmd_str2int(dummy,&val)){
			retval = cmd_exit_invalid_arg;
		}else{
			retval = cmd_exit_success;
			*dest = val;
		}
	}else{
		retval = cmd_exit_missing_arg;
	}
	return retval;
}


static void str_reverse(char s[]){
	size_t length = strlen(s) ;
	size_t i, j;
	char c;

	for (i = 0, j = length - 1; i < j; i++, j--){
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}