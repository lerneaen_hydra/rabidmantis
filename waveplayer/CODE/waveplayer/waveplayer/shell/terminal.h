/* Basic terminal-like library. Echos input to the serial interface, sanitizes
 * line endings, and when a line is finished marks it as available and gives the
 * ability to output it.
 * Note that all the functions here need to be called from the same context!
 * Typically this is not an issue, is this will be done purely in the main loop
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <stdbool.h>
#include <stdint.h>
#include "../config.h"

/************************************************************************

************************************************************************/

/** The total buffer size for the terminal. */
#define TERMINAL_BUFSIZE	(CMD_MAXLEN + 1)

/** @brief Housekeeping function for terminal data parsing.
	This function will check the input buffer, clean out non-supported
	characters, manage CR,LF,CRLF line endings, and output the sanitized
	output data to internal storage which can be read with the other supporting
	functions.
*/
void terminal_idle(void);

/** @brief Returns nonzero if there is one or more lines available. */
uint16_t terminal_line_available(void);

/** @brief Returns the first/next character in the line. */
char	terminal_getc(void);

/** @Brief Returns the first/next recieved character, which may not be part of a complete line */
char terminal_getc_incomplete(void);

/** @brief Gets the *most recent* character in the current unterminated line.
 *	This will remove the character from the current line, meaning it will
 *	not appear in terminal_getc() when the line is terminated.
 *	If there is no character, or the character is '\n' or '\r', '\0' is returned.
 *	@return The most recent character, or '\0' if a newline or no character is available
 */
char terminal_getc_incomplete(void);

/** @brief Puts the character on the terminal, blocking until it is done. */
void	terminal_putc(char data);

/** @brief Puts a string on the terminal, blocking until it is done. */
void terminal_puts(char* str);

#endif /* TERMINAL_H_ */
