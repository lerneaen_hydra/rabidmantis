#include <string.h>
#include <stdbool.h>
#include "command.h"

#ifndef SHELL_STDIO	//If we don't have STDIO, use stringencoders library to converter numerical types to strings
#include "../stringencoders/modp_numtoa.h"
#endif

#include "../flex_settings/flex_settings.h"
#include "../board/board.h"
#include "../waveplay/waveplay.h"
#include "../sd-reader/fat.h"
#include "../sd-reader/utils.h"

#ifdef SHELL_STDIO	//If we do have STDIO, define some wrapper functions for the functions used in stringencoders
static inline void modp_itoa10(int32_t value, char* buf){
#ifndef SHELL_AVR
	sprintf(buf,"%ld",value);
#else
	sprintf_P(buf,PSTR("%ld"),value);
#endif
}

#ifdef FLEX_FLOAT_SUPPORT
static inline void modp_dtoa2(double value, char* buf, int precision){
#ifndef SHELL_AVR
	sprintf(buf, "%.*f",precision, value);
#else
	sprintf_P(buf, PSTR("%.*f"),precision, value);
#endif
}
#endif
#endif

/** brief Headers for general-purpose 'set' and 'get' commands */
static void print_param_header(void);
static void print_param(FLEX_IDX_T idx);
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)
#define CMD_SET_ARG_PARAM	"p"
#define CMD_SET_ARG_SUDO	"sudo"
#define CMD_SET_ARG_MAX		"max"
#define CMD_SET_ARG_MIN		"min"
#ifndef SHELL_AVR
static const char cmd_set_cmd[] = "set";
static const char cmd_set_help[] = "Usage: 'set -"CMD_SET_ARG_PARAM" <param> <new value>'\n"
		"	-"CMD_SET_ARG_PARAM" <param>; parameter to apply change to.\n"
		"	<new value>; new value to apply.\n"
		"	-"CMD_SET_ARG_SUDO"; forces write (ignores parameter write permission).\n"
		"	-"CMD_SET_ARG_MAX"; applies change to parameter's maximum value.\n"
		"	-"CMD_SET_ARG_MIN"; applies change to parameter's minimum value.\n"
		"Sets a new value to a parameter. Limited to range of minimum and maximum values.";
#else
static const PROGMEM char cmd_set_cmd[] = "set";
static const PROGMEM char cmd_set_help[] = "Usage: 'set -"CMD_SET_ARG_PARAM" <param> <new value>'\n"
"	-"CMD_SET_ARG_PARAM" <param>; parameter to apply change to.\n"
"	<new value>; new value to apply.\n"
"	-"CMD_SET_ARG_SUDO"; forces write (ignores parameter write permission).\n"
"	-"CMD_SET_ARG_MAX"; applies change to parameter's maximum value.\n"
"	-"CMD_SET_ARG_MIN"; applies change to parameter's minimum value.\n"
"Sets a new value to a parameter. Limited to range of minimum and maximum values.";
#endif

shell_ret_t cmd_set_f(void);
#ifndef SHELL_AVR
static const char cmd_get_cmd[] = "get";
static const char cmd_get_help[] = "Usage: 'get (no arguments)'\nList all parameters and their properties.";
#else
static const PROGMEM char cmd_get_cmd[] = "get";
static const PROGMEM char cmd_get_help[] = "Usage: 'get' (no arguments)\nList all parameters and their properties.";
#endif
shell_ret_t cmd_get_f(void);

/** FAT filesystem handling commands */

#define CMD_LS_NAME "[NAME]"
static const PROGMEM char cmd_ls_cmd[] = "ls";
static const PROGMEM char cmd_ls_help[] = "Usage: 'ls' (no arguments).\nLists the contents of the current directory.";
shell_ret_t cmd_ls_f(void){
	shell_ret_t retval = cmd_exit_success;
	if(waveplay_get_active()){
		shell_puts_int_P(PSTR(MSG_FS_BUSY));
		return cmd_exit_success;
	}
	
	struct fat_dir_entry_struct dir_entry;
	
	//Switch to high clock rate if currently in idle
	enum board_fcpu_t initial_fcpu = board_get_clk();
	bool switched_fcpu = false;
	if(initial_fcpu == STANDBY_FCPU){
		board_set_clk_flow_ctrl(MAXSPEED_FCPU);
		switched_fcpu = true;
	}
	
	//Print header
	fat_reset_dir(dd);
	fat_read_dir(dd, &dir_entry);
	shell_puts_int_P(PSTR(CMD_LS_NAME));
	uint8_t spaces = sizeof(dir_entry.long_name) - strlen(CMD_LS_NAME)/sizeof(char) + 5;
	while(spaces--){
		shell_putc_int(' ');
	}
	shell_puts_int_P(PSTR("[Size (bytes)]"));
	
	fat_reset_dir(dd);	//Getting the directory attributes (long_name) modifies the directory handle, so reset it to get the zero'th element
	
	//Print contents
	while(fat_read_dir(dd, &dir_entry)){
		shell_putc_int('\n');
		uint8_t spaces = sizeof(dir_entry.long_name) - strlen(dir_entry.long_name) + 4;
		shell_puts_int(dir_entry.long_name);
		shell_putc_int(dir_entry.attributes & FAT_ATTRIB_DIR ? '/' : ' ');
		while(spaces--){
			shell_putc_int(' ');
		}
		printf_P(PSTR("%lu"),dir_entry.file_size);
	}
	
	if(switched_fcpu){
		board_set_clk_flow_ctrl(initial_fcpu);
	}
	
	return retval;
}

static const PROGMEM char cmd_cd_cmd[] = "cd";
static const PROGMEM char cmd_cd_help[] = "Usage: 'cd <dir>'.\nMoves to directory <dir>, relative to the current directory.";
shell_ret_t cmd_cd_f(void){
	if(!shell_check_arg("")){
		return cmd_exit_missing_arg;
	}
	if(waveplay_get_active()){
		shell_puts_int_P(PSTR(MSG_FS_BUSY));
		return cmd_exit_success;
	}
	char newdir[shell_extract_arg_len("") + 1];
	shell_extract_arg("",newdir,sizeof(newdir));
	
	struct fat_dir_entry_struct subdir_entry;
	if(find_file_in_dir(fs, dd, newdir, &subdir_entry)){
		struct fat_dir_struct* dd_new = fat_open_dir(fs, &subdir_entry);
		if(dd_new){
			fat_close_dir(dd);
			dd = dd_new;
			return cmd_exit_success;
		}
	}
	printf_P(PSTR("Error: could not find directory: %s\n"),newdir);
	return cmd_exit_invalid_arg;
}

static const PROGMEM char cmd_diskinfo_cmd[] = "diskinfo";
static const PROGMEM char cmd_diskinfo_help[] = "Usage: 'diskinfo' (no arguments).\nDisplays various disk information.";
shell_ret_t cmd_diskinfo_f(void){
	if(waveplay_get_active()){
		shell_puts_int_P(PSTR(MSG_FS_BUSY));
		return cmd_exit_success;
	}
	shell_ret_t retval = cmd_exit_success;
	sd_reader_print_info();
	return retval;
}

static const PROGMEM char cmd_play_cmd[] = "play";
static const PROGMEM char cmd_play_help[] = "Usage: 'play <filename>'.\nStarts playback of file <filename> in the current directory.";
shell_ret_t cmd_play_f(void){
	shell_ret_t retval = cmd_exit_success;
	if(!shell_check_arg("")){
		return cmd_exit_missing_arg;
	}
	char filename[shell_extract_arg_len("") + 1];
	shell_extract_arg("",filename,sizeof(filename));
	
	waveplay_open_play_file(filename);
	
	return retval;
}

static const PROGMEM char cmd_next_cmd[] = "next";
static const PROGMEM char cmd_next_help[] = "Usage: 'next'.\nPlays the next track, randomly if "PARAM_SHUFFLE_TITLE" is set, in alphabetical order otherwise.\n";
shell_ret_t cmd_next_f(void){
	waveplay_open_play_next_file(setting_get_int(PARAM_SHUFFLE_IDX,FLEX_PARAM_VAL,FLEX_FORCE_SUDO));
	return cmd_exit_success;
}

static const PROGMEM char cmd_stop_cmd[] = "stop";
static const PROGMEM char cmd_stop_help[] = "Usage: 'stop'.\nStops playback of any currently playing file";
shell_ret_t cmd_stop_f(void){
	waveplay_stop();
	return cmd_exit_success;
}

static const PROGMEM char cmd_pause_cmd[] = "pause";
static const PROGMEM char cmd_pause_help[] = "Usage: 'pause'.\nPauses playback of any currently playing file.\nNOTE: Device does *NOT* enter low power mode when paused!";
shell_ret_t cmd_pause_f(void){
	waveplay_pause();
	return cmd_exit_success;
}

static const PROGMEM char cmd_resume_cmd[] = "resume";
static const PROGMEM char cmd_resume_help[] = "Usage: 'resume'.\nResumes playback of any currently paused file.\n";
shell_ret_t cmd_resume_f(void){
	waveplay_resume();
	return cmd_exit_success;
}

static const PROGMEM char cmd_playpause_cmd[] = "playpause";
static const PROGMEM char cmd_playpause_help[] = "Usage: 'playpause'.\nToggles the play/pause state of the currently playing file.\nNOTE: Device does *NOT* enter low power mode when paused!";
shell_ret_t cmd_playpause_f(void){
	waveplay_playpause();
	return cmd_exit_success;
}

static const PROGMEM char cmd_busy_cmd[] = "busy";
static const PROGMEM char cmd_busy_help[] = "Usage: 'busy'.\n Outputs the current playback status.\n";
shell_ret_t cmd_busy_f(void){
	if(waveplay_get_active()){
		shell_puts_int_P(PSTR(MSG_PLAYBACK_ACTIVE));
	}else{
		shell_puts_int_P(PSTR(MSG_PLAYBACK_INACTIVE));
	}
	return cmd_exit_success;
}

/******************************************************************************
  	  	  	  	  	  All application-specific commands
******************************************************************************/


/************************************************************************/
/* Master list of commands, populate with the desired commands          */
/************************************************************************/

/** @brief Shell command array, all valid commands must be contained here */
// IMPORTANT! BE SURE TO UPDATE SHELL_NUM_CMD WHEN CHANGING BELOW!
//Note; The order of commands in help list is the same as this order.
#ifndef SHELL_AVR
const shell_cmd_t shell_cmd_array[SHELL_NUM_CMD] = {
#else
const shell_cmd_t shell_cmd_array[SHELL_NUM_CMD] PROGMEM = {
#endif
	{
		cmd_busy_cmd,
		cmd_busy_help,
		&cmd_busy_f
	},
	{
		cmd_stop_cmd,
		cmd_stop_help,
		&cmd_stop_f
	},
	{
		cmd_pause_cmd,
		cmd_pause_help,
		&cmd_pause_f
	},
	{
		cmd_resume_cmd,
		cmd_resume_help,
		&cmd_resume_f
	},
	{
		cmd_playpause_cmd,
		cmd_playpause_help,
		&cmd_playpause_f
	},
	{
		cmd_get_cmd,
		cmd_get_help,
		&cmd_get_f
	},
	{
		cmd_set_cmd,
		cmd_set_help,
		&cmd_set_f
	},
	{
		cmd_ls_cmd,
		cmd_ls_help,
		&cmd_ls_f
	},
	{
		cmd_cd_cmd,
		cmd_cd_help,
		&cmd_cd_f
	},
	{
		cmd_diskinfo_cmd,
		cmd_diskinfo_help,
		&cmd_diskinfo_f
	},
	{
		cmd_play_cmd,
		cmd_play_help,
		&cmd_play_f
	},
	{
		cmd_next_cmd,
		cmd_next_help,
		&cmd_next_f
	}
};

shell_ret_t cmd_get_f(void){
	print_param_header();
	int i;
	for(i = 0; i < FLEX_NUM_PARAM; i++){
		print_param(i);
		shell_putc_int('\n');
	}
	return cmd_exit_success;
}

shell_ret_t cmd_set_f(void){
	shell_ret_t retval;
	if(shell_check_arg(CMD_SET_ARG_PARAM) == NULL || shell_check_arg("") == NULL){
		return cmd_exit_missing_arg;
	}
	enum flex_force_t nv_force = FLEX_FORCE_NORMAL;
	if(shell_check_arg(CMD_SET_ARG_SUDO)){
		nv_force = FLEX_FORCE_SUDO;
	}

	if(shell_check_arg(CMD_SET_ARG_MAX) && shell_check_arg(CMD_SET_ARG_MIN)){
		return cmd_exit_invalid_arg;
	}

	enum flex_param_t nv_param = FLEX_PARAM_VAL;
	if(shell_check_arg(CMD_SET_ARG_MAX)){
		nv_param = FLEX_PARAM_MAX;
	}
	if(shell_check_arg(CMD_SET_ARG_MIN)){
		nv_param = FLEX_PARAM_MIN;
	}

	char param[CMD_MAXLEN + 1];

	shell_extract_arg(CMD_SET_ARG_PARAM, param, sizeof(param)/sizeof(char));

	FLEX_IDX_T idx = setting_get_idx(param);
	if(idx < 0){
		return cmd_exit_invalid_arg;
	}else{
#ifdef FLEX_FLOAT_SUPPORT
		float value_flt;
#endif
		int32_t value_int;
		enum shell_ret_t conversion;
		switch(setting_get_type(idx)){
		case FLEX_TYPE_INTEGER:
			conversion = shell_extract_int_arg("", &value_int);
			if(conversion != cmd_exit_success){
				return cmd_exit_invalid_arg;
			}
			if(setting_set_int(idx,nv_param,value_int,nv_force)){
				retval = cmd_exit_success;
			}else{
				retval = cmd_exit_invalid_arg;
			}
			print_param_header();
			print_param(idx);
			shell_putc_int('\n');
			break;
#ifdef FLEX_FLOAT_SUPPORT
		case FLEX_TYPE_REAL:
			conversion = shell_extract_float_arg("", &value_flt);
			if(conversion != cmd_exit_success){
				return cmd_exit_invalid_arg;
			}
			if(setting_set_real(idx,nv_param,value_flt,nv_force)){
				retval = cmd_exit_success;
			}else{
				retval = cmd_exit_invalid_arg;
			}
			print_param_header();
			print_param(idx);
			shell_putc_int('\n');
			break;
#endif
		case FLEX_TYPE_STRING:
			if(strnlen(param,sizeof(param)/sizeof(char)) > FLEX_SETTINGS_STR_LEN){
				return cmd_exit_invalid_arg;
			}else{
				retval = cmd_exit_success;
			}
			char dummy[CMD_MAXLEN+1];
			shell_extract_arg("", dummy,sizeof(dummy)/sizeof(char));
			setting_set_str(idx, dummy, nv_force);
			print_param_header();
			print_param(idx);
			shell_putc_int('\n');
			break;
		default:
			retval = cmd_exit_invalid_arg;
		}
	}
	return retval;
}

/** @brief Prints len characters, starting with *strptr and adding ' ' as needed */
static void print_padded(char* strptr, int len);

static void print_param_header(void){
#ifdef PARAM_UNITS_EN
#ifndef SHELL_AVR
	shell_puts_int("PARAM    |UNIT    |TYPE     |VALUE          |MINIMUM        |MAXIMUM\n");
#else
	shell_puts_int_P(PSTR("PARAM    |UNIT    |TYPE     |VALUE          |MINIMUM        |MAXIMUM\n"));
#endif
#else
#ifndef SHELL_AVR
	shell_puts_int("PARAM    |TYPE     |VALUE          |MINIMUM        |MAXIMUM\n");
#else
	shell_puts_int_P(PSTR("PARAM        |TYPE     |VALUE          |MINIMUM        |MAXIMUM\n"));
#endif
#endif
	#define TITLELEN	FLEX_SETTINGS_TITLE_LEN
	#define UNITLEN		FLEX_SETTINGS_UNIT_LEN		//Number of characters in "type" heading
	#define TYPELEN		9		//Number of characters in "type" heading
	#define VALUELEN	15		//Number of character in "value", "minimum" and "maximum" headings
	//Yes, ugly with defines here, whine whine, but done for simplicity
}

static void print_padded(char* strptr, int len){
	int i;
	int stringend;
	for(i = 0, stringend = 0; i < len; i++){
		if(!stringend){
			if(*strptr != '\0'){
				shell_putc_int(*strptr);
				strptr++;
			}else{
				stringend = 1;
				shell_putc_int(' ');
			}
		}else{
			shell_putc_int(' ');
		}
	}
}

static void print_param(FLEX_IDX_T idx){
#define PRINT_NA		"<N/A>"
#define PRINT_SEP		'|'
#define PRINT_NONE		" [-]"
#define PRINT_R			" [r]"
#define PRINT_W			" [w]"
#define PRINT_RW		"[rw]"
#define PRINT_INTEGER	"INTEGER"
#define PRINT_REAL		"REAL"
#define PRINT_STRING	"STRING"
	//Output name, pad with spaces until TITLELEN characters have been output
	char* titleptr = setting_get_title(idx);
	print_padded(titleptr, TITLELEN);
	shell_putc_int(PRINT_SEP);

#ifdef PARAM_UNITS_EN
	//Output unit, pad with spaces until UNITLEN characters have been output
	char* unitptr = setting_get_unit(idx);
	print_padded(unitptr, UNITLEN);
	shell_putc_int(PRINT_SEP);
#endif

	//Output type
	char type[TYPELEN+1];
	char* typeptr = &type[0];
	switch(setting_get_type(idx)){
		case FLEX_TYPE_INTEGER:
		#ifndef SHELL_AVR
			strncpy(type,PRINT_INTEGER,TYPELEN);
		#else
			strncpy_P(type,PSTR(PRINT_INTEGER),TYPELEN);
		#endif
			break;
#ifdef FLEX_FLOAT_SUPPORT
		case FLEX_TYPE_REAL:
		#ifndef SHELL_AVR
			strncpy(type,PRINT_REAL,TYPELEN);
		#else
			strncpy_P(type,PSTR(PRINT_REAL),TYPELEN);
		#endif
			break;
#endif
		case FLEX_TYPE_STRING:
		#ifndef SHELL_AVR
			strncpy(type,PRINT_STRING,TYPELEN);
		#else
			strncpy_P(type,PSTR(PRINT_STRING),TYPELEN);
		#endif
			break;
		default:
			break;
	}
	type[TYPELEN] = '\0';
	print_padded(typeptr,TYPELEN);
	shell_putc_int(PRINT_SEP);

	//Output value and permission
	char dummy[VALUELEN + 1];
	dummy[0] = '\0';
	char* dummyptr = &dummy[0];
	int readresult = true;
	switch(setting_get_type(idx)){
	case FLEX_TYPE_INTEGER:
		if(setting_get_perm(idx, FLEX_PARAM_VAL) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_VAL) == FLEX_PERM_READ){
			modp_itoa10(setting_get_int(idx,FLEX_PARAM_VAL,FLEX_FORCE_NORMAL),dummy);
		}
		break;
#ifdef FLEX_FLOAT_SUPPORT
	case FLEX_TYPE_REAL:
		if(setting_get_perm(idx, FLEX_PARAM_VAL) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_VAL) == FLEX_PERM_READ){
			modp_dtoa2(setting_get_real(idx,FLEX_PARAM_VAL,FLEX_FORCE_NORMAL),dummy,FLOATPREC);
		}
		break;
#endif
	case FLEX_TYPE_STRING:
		if(setting_get_perm(idx, FLEX_PARAM_VAL) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_VAL) == FLEX_PERM_READ){
			readresult = setting_get_string(idx,dummy,FLEX_FORCE_NORMAL);
		}
		break;
	default:
		break;
	}
	if(readresult == false){
		strcpy(dummy,PRINT_NA);	//Only enter if looking at a string type
	}
	print_padded(dummyptr,VALUELEN - 4);
	switch(setting_get_perm(idx,FLEX_PARAM_VAL)){
	case FLEX_PERM_READWRITE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_RW);
	#else
		shell_puts_int_P(PSTR(PRINT_RW));
	#endif
		break;
	case FLEX_PERM_READ:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_R);
	#else
	shell_puts_int_P(PSTR(PRINT_R));
	#endif
		break;
	case FLEX_PERM_WRITE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_W);
	#else
		shell_puts_int_P(PSTR(PRINT_W));
	#endif
		break;
	case FLEX_PERM_NONE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_NONE);
	#else
		shell_puts_int_P(PSTR(PRINT_NONE));
	#endif
		break;
	default:
		break;
	}
	shell_putc_int(PRINT_SEP);

	// Output minimum and permission
	dummy[0] = '\0';
	switch(setting_get_type(idx)){
	case FLEX_TYPE_INTEGER:
		if(setting_get_perm(idx, FLEX_PARAM_MIN) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_MIN) == FLEX_PERM_READ){
			modp_itoa10(setting_get_int(idx,FLEX_PARAM_MIN,FLEX_FORCE_NORMAL),dummy);
		}
		break;
#ifdef FLEX_FLOAT_SUPPORT
	case FLEX_TYPE_REAL:
		if(setting_get_perm(idx, FLEX_PARAM_MIN) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_MIN) == FLEX_PERM_READ){
			modp_dtoa2(setting_get_real(idx,FLEX_PARAM_MIN,FLEX_FORCE_NORMAL),dummy,FLOATPREC);
		}
		break;
#endif
	case FLEX_TYPE_STRING:
		strcpy(dummy,PRINT_NA);
		break;
	default:
		break;
	}
	print_padded(dummyptr,VALUELEN - 4);
	switch(setting_get_perm(idx,FLEX_PARAM_MIN)){
	case FLEX_PERM_READWRITE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_RW);
	#else
		shell_puts_int_P(PSTR(PRINT_RW));
	#endif
		break;
	case FLEX_PERM_READ:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_R);
	#else
		shell_puts_int_P(PSTR(PRINT_R));
	#endif
		break;
	case FLEX_PERM_WRITE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_W);
	#else
		shell_puts_int_P(PSTR(PRINT_W));
	#endif
		break;
	case FLEX_PERM_NONE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_NONE);
	#else
		shell_puts_int_P(PSTR(PRINT_NONE));
	#endif
		break;
	default:
		break;
}
	shell_putc_int(PRINT_SEP);

	// Output maximum and permission
	dummy[0] = '\0';
	switch(setting_get_type(idx)){
	case FLEX_TYPE_INTEGER:
		if(setting_get_perm(idx, FLEX_PARAM_MAX) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_MAX) == FLEX_PERM_READ){
			modp_itoa10(setting_get_int(idx,FLEX_PARAM_MAX,FLEX_FORCE_NORMAL),dummy);
		}
		break;
#ifdef FLEX_FLOAT_SUPPORT
	case FLEX_TYPE_REAL:
		if(setting_get_perm(idx, FLEX_PARAM_MAX) == FLEX_PERM_READWRITE || setting_get_perm(idx, FLEX_PARAM_MAX) == FLEX_PERM_READ){
			modp_dtoa2(setting_get_real(idx,FLEX_PARAM_MAX,FLEX_FORCE_NORMAL),dummy,FLOATPREC);
		}
		break;
#endif
	case FLEX_TYPE_STRING:
		strcpy(dummy,STR(FLEX_SETTINGS_STR_LEN));
		break;
	default:
		break;
	}
	print_padded(dummyptr,VALUELEN - 4);
	switch(setting_get_perm(idx,FLEX_PARAM_MAX)){
	case FLEX_PERM_READWRITE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_RW);
	#else
		shell_puts_int_P(PSTR(PRINT_RW));
	#endif
		break;
	case FLEX_PERM_READ:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_R);
	#else
		shell_puts_int_P(PSTR(PRINT_R));
	#endif
		break;
	case FLEX_PERM_WRITE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_W);
	#else
		shell_puts_int_P(PSTR(PRINT_W));
	#endif
		break;
	case FLEX_PERM_NONE:
	#ifndef SHELL_AVR
		shell_puts_int(PRINT_NONE);
	#else
		shell_puts_int_P(PSTR(PRINT_NONE));
	#endif
	break;
		default:
	break;
	}
}
