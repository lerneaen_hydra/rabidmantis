/*
 * uart.h
 *
 *  Created on: Sep 17, 2013
 *      Author: desel-lock
 */

#ifndef UART_H_
#define UART_H_
#include <stdbool.h>
#include <stddef.h>

/* Very rudimentary uart library. Input is stored in a FIFO buffer and
 * flushed out through uart_getc(). Output is written to UART peripheral
 * register and blocks if busy.
 * Note; assumes the UART peripheral has been initialized! */

/**
 * @brief Input buffer size (characters).
 */
#define UART_RX_BUFSIZE		16

/** @brief Initializes the UART submodule with function pointers to the UART hardware */
void uart_init(bool (*get_txfull)(), void (*set_txbuf)(char data));

/** @brief Function to call on UART RX interrupt with recieved data byte */
void uart_rx_fifo_isr(char rxbyte);

/**
 * @brief Queues one byte of data for transmission
 * This is done immediately if there is space left in the TX buffer, if
 * there is no space left this function blocks until there is space.
 * @param data the character to queue for transfer.
 */
void uart_putc(char data);

/** @brief Pushes a string (null-terminated) through the UART TX buffer.
 * This will block and may take a long time to execute! */
void uart_puts(char* str);

/**
 * @brief Get one byte of data and remove it from the input buffer.
 * Will block until a character has been received.
 * @return The oldest data byte received
 */
char uart_getc(void);

/**
 * @brief Checks if there is data available to read from the input
 * @return The number of characters stored in the buffer.
 */
int uart_available(void);

#endif /* UART_H_ */
