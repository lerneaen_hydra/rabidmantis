#include "globals.h"
#include "board/board.h"
#include "systime/systime.h"
#include "btn_debounce/btn_debounce.h"

#include "sd-reader/sd_raw.h"
#include "sd-reader/fat.h"
#include "sd-reader/partition.h"

volatile uint8_t g_dmasrc[DAC_DMATFR_BYTES * DAC_DATABUF_NUM_DMATFRS];
volatile bool g_dac_buffer_underrun_latch = false;
volatile uint8_t g_dma_tail = 0;
volatile uint8_t g_dma_head = 0;

struct sd_raw_info disk_info;
struct fat_dir_struct* dd;
struct fat_dir_entry_struct directory;
struct fat_fs_struct* fs;
struct partition_struct* partition;

struct btn_debounce_t g_btn_debounce = {
	.get_btn = &board_get_btn,
	.mem = {0}
};