#ifndef BAUD_CALC_H_
#define BAUD_CALC_H_

/** @file Calculates the largest acceptable BSEL and BSCALE values for a given
 *	peripheral clock frequency and baud rate.
 *		Usage example;
 *  
 *  #define F_CPU 32000000
 *  #define BAUDRATE 9600
 *  USARTn0.BAUDCTRLA = BSEL(F_CPU,BAUDRATE) & 0xFF;
 *  USARTn0.BAUDCTRLB = (BSCALE(F_CPU,BAUDRATE) << USART_BSCALE_gp) | ((BSEL(F_CPU,BAUDRATE) >> 8) & USART_BSEL_gm);
 */

/** @brief Define when the double speed mode (CLK2X) is used */
//#define BAUD_CALC_CLK2X


#define _BAUD_BSEL_FROM_BSCALE(f_per,baud,bscale) (				\
((bscale) < 0) ?												\
(int)((((float)(f_per)/(8*(float)(baud)))-1)*(1<<-(bscale)))	\
: (int)((float)(f_per)/((1<<(bscale))*8*(float)(baud)))-1 )

#define _BSCALE(f_per,baud) (									\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-7) < 4096) ? -7 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-6) < 4096) ? -6 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-5) < 4096) ? -5 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-4) < 4096) ? -4 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-3) < 4096) ? -3 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-2) < 4096) ? -2 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,-1) < 4096) ? -1 :			\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,0) < 4096) ? 0 :				\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,1) < 4096) ? 1 :				\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,2) < 4096) ? 2 :				\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,3) < 4096) ? 3 :				\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,4) < 4096) ? 4 :				\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,5) < 4096) ? 5 :				\
(_BAUD_BSEL_FROM_BSCALE(f_per,baud,6) < 4096) ? 6 :				\
7 )

#ifdef BAUD_CALC_CLK2X
#define BSEL(f_per,baud)	_BAUD_BSEL_FROM_BSCALE(f_per,baud,_BSCALE(f_per,baud))
#define BSCALE(f_per,baud) ((_BSCALE(f_per,baud)<0) ? (16+_BSCALE(f_per,baud)) : _BSCALE(f_per,baud))
#else
#define BSEL(f_per,baud)	_BAUD_BSEL_FROM_BSCALE((f_per/2.0),baud,_BSCALE((f_per/2.0),baud))
#define BSCALE(f_per,baud) ((_BSCALE((f_per/2.0),baud)<0) ? (16+_BSCALE((f_per/2.0),baud)) : _BSCALE((f_per/2.0),baud))
#endif

#endif /* BAUD_CALC_H_ */