/** @file Contains all peripheral and hardware drivers/defines */

#ifndef BOARD_H_
#define BOARD_H_

#include <stdbool.h>
#include <avr/io.h>
#include <limits.h>
#include <util/atomic.h>
#include "../globals.h"
#include "../config.h"

/** @brief Defines for status LEDs */
#define LED0_PORT	PORTD
#define LED0_PIN	0
#define LED1_PORT	PORTC
#define LED1_PIN	4

/** @brief Defines for SD card
 * NOTE: These data lines use system peripherals, to make changes the
 * associated peripheral must also be updated! */
#define SD_SCK_PORT		PORTC
#define SD_SCK_PIN		5
#define SD_MISO_PORT	PORTC
#define SD_MISO_PIN		6
#define SD_MISO_PINnCTRL PIN6CTRL
#define SD_MOSI_PORT	PORTC
#define SD_MOSI_PIN		7
#define SD_CS_PORT		PORTD
#define SD_CS_PIN		1
#define SD_PERIP		SPIC

/** @brief SD SPI macros used by the SD-card library during initialization */
#define BOARD_SD_SELECT() SD_CS_PORT.OUTCLR = (1<<SD_CS_PIN)
#define BOARD_SD_UNSELECT() SD_CS_PORT.OUTSET = (1<<SD_CS_PIN)

/** @brief Sets up the SPI peripheral for SD-card intialization at a low (<400kHz) data rate.
 * With an SPI clock prescaler of 64 we can have a core clock of
 * 0.4 * 64 = 25.6MHz. We're safe as we're not ever running over 24.5 MHz */
#define BOARD_SD_SPI_INIT() (SD_PERIP.CTRL = SPI_ENABLE_bm | SPI_MASTER_bm | (0<<SPI_MODE_gp) /* SPI clock polarity/phase */ | SPI_PRESCALER_DIV64_gc)

#define BOARD_SD_SPI_SET_FULLSPEED()	do{		\
											SD_PERIP.CTRL &= ~SPI_PRESCALER_gm;						\
											SD_PERIP.CTRL |= SPI_PRESCALER_DIV4_gc | SPI_CLK2X_bm;	\
										}while(0)

#define BOARD_SD_WRITE_BYTE(b)			(SD_PERIP.DATA = b)
#define	BOARD_SD_WAIT_WRITE_BYTE()		while(!(SD_PERIP.STATUS & SPI_IF_bm)){}
#define	BOARD_SD_CLEAR_WRITE_FLAG()		do{		\
											volatile uint8_t dummy = SD_PERIP.STATUS;	/* Read STATUS register and then ... */\
											dummy = SD_PERIP.DATA;						/* ...DATA register to clear the IF flag */\
											(void) dummy;								/* Suppress warning about unused parameter */\
										}while(0)
#define BOARD_SD_GET_DATA()				SD_PERIP.DATA
/** @brief Defines for DAC SPI control interface
 * Included here as the bit-banged SPI protocol directly accesses the data lines */
#define DAC_SPI_CTRL_SCK_PIN	6
#define DAC_SPI_CTRL_SCK_PORT	PORTA
#define DAC_SPI_CTRL_MOSI_PIN	7
#define DAC_SPI_CTRL_MOSI_PORT	PORTA
#define DAC_SPI_CTRL_SS_PIN		0
#define DAC_SPI_CTRL_SS_PORT	PORTC

/** @brief Macros for the DAC SPI control interface */
#define DAC_SPI_EN_SS()		(DAC_SPI_CTRL_SS_PORT.OUTCLR = 1 << DAC_SPI_CTRL_SS_PIN)	/* Enables the !SS pin (brings logic low) */
#define DAC_SPI_DIS_SS()	(DAC_SPI_CTRL_SS_PORT.OUTSET = 1 << DAC_SPI_CTRL_SS_PIN)	/* Disables the !SS pin (brings logic high) */
#define DAC_SPI_EN_MOSI()	(DAC_SPI_CTRL_MOSI_PORT.OUTSET = 1 << DAC_SPI_CTRL_MOSI_PIN)/* Brings the MOSI pin high */
#define DAC_SPI_DIS_MOSI()	(DAC_SPI_CTRL_MOSI_PORT.OUTCLR = 1 << DAC_SPI_CTRL_MOSI_PIN)/* Brings the MOSI pin low */
#define DAC_SPI_EN_SCK()	(DAC_SPI_CTRL_SCK_PORT.OUTSET = 1 << DAC_SPI_CTRL_SCK_PIN)	/* Brings the SCK pin high */
#define DAC_SPI_DIS_SCK()	(DAC_SPI_CTRL_SCK_PORT.OUTCLR = 1 << DAC_SPI_CTRL_SCK_PIN)	/* Brings the SCK pin low */
#define DAC_SPI_GET_SS()	((DAC_SPI_CTRL_SS_PORT.OUT & (1 << DAC_SPI_CTRL_SS_PIN))? 0 : 1) /* Returns the !SS pin state (logic low -> true) */

/** @brief Delay function that delays for at least 100ns over all operating frequencies */
#define BOARD_DELAY_MIN_100NS()	do{							\
									asm volatile ("nop");	\
									asm volatile ("nop");	\
									asm volatile ("nop");	\
								}while(0)

/** @brief Minimum supported sample rate, used for determining a delay for communications with DAC */
#define BOARD_MIN_FS_khz	8
/** @brief Number of bytes in a single DAC sample (left or right) */
#define DAC_DATA_SAMPBYTES	2

/** @brief USART module to use for UART interface
 * NOTE: This peripheral is dependent on other defines; to make changes the
 * remaining associated defines must also be updated! */
#define USARTREG	USARTD0

/** @brief Interrupt vector for USART RX complete */
#define BOARD_USART_RX_vect	(USARTD0_RXC_vect)

/** @brief Defines related to DAC interface
* NOTE: These settings are dependent on other defines; to make changes the
* remaining associated defines must also be updated! */
#define DAC_DATA_UART					USARTC0
#define DMA_A_vect						EDMA_CH0_vect
#define DMA_B_vect						EDMA_CH1_vect
// The DMA ISR is shared between error and transfer complete, so we need to check the source...
#define DMA_A_CHECK_ERRISR()			(EDMA.CH0.CTRLB & EDMA_CH_ERRIF_bm)
#define DMA_B_CHECK_ERRISR()			(EDMA.CH1.CTRLB & EDMA_CH_ERRIF_bm)
//...and acknowledge the ISR manually
#define DMA_A_ACK_ERRISR()				(EDMA.CH0.CTRLB |= EDMA_CH_ERRIF_bm)
#define DMA_B_ACK_ERRISR()				(EDMA.CH1.CTRLB |= EDMA_CH_ERRIF_bm)
#define DMA_A_ACK_TFRISR()				(EDMA.CH0.CTRLB |= EDMA_CH_TRNIF_bm)
#define DMA_B_ACK_TFRISR()				(EDMA.CH1.CTRLB |= EDMA_CH_TRNIF_bm)

// In each ISR, update the DMA peripheral to use the next block of buffered data
// It's important to write to low register before high register in order to correctly load 16-bit value
#define DMA_A_UPDATE_SRCADDR()			do{													\
												uint8_t tail_shadow = (uint8_t) g_dma_tail;	\
												uint16_t addr = ( (uint16_t)(&g_dmasrc[tail_shadow*DAC_DMATFR_BYTES]) );	\
												tail_shadow++;								\
												if(tail_shadow >= DAC_DATABUF_NUM_DMATFRS){	\
													tail_shadow = 0;						\
												}											\
												g_dma_tail = (volatile uint8_t) tail_shadow;\
												EDMA.CH0.ADDRL = addr & 0xFF;				\
												EDMA.CH0.ADDRH = (addr >> 8) & 0xFF;		\
										}while(0)

#define DMA_B_UPDATE_SRCADDR()			do{													\
												uint8_t tail_shadow = (uint8_t) g_dma_tail;	\
												uint16_t addr = ( (uint16_t)(&g_dmasrc[tail_shadow*DAC_DMATFR_BYTES]) );	\
												tail_shadow++;								\
												if(tail_shadow >= DAC_DATABUF_NUM_DMATFRS){	\
													tail_shadow = 0;						\
												}											\
												g_dma_tail = (volatile uint8_t) tail_shadow;\
												EDMA.CH1.ADDRL = addr & 0xFF;				\
												EDMA.CH1.ADDRH = (addr >> 8) & 0xFF;		\
										}while(0)
// After each complete DMA transfer, we need to queue another
#define DMA_A_QUEUE_TFR()				(EDMA.CH0.CTRLA |= EDMA_CH_REPEAT_bm)
#define DMA_B_QUEUE_TFR()				(EDMA.CH1.CTRLA |= EDMA_CH_REPEAT_bm)
// Returns true if there is a block of data that is available to read
#define DMA_BUF_GET_QUERY_READ()		(g_dma_tail == g_dma_head ? false : true)

// Returns true if there is a free block available to write to the DMA buffer
// NOTE: We compare with g_dma_head + 2 as the FIFO buffer requires both one
// free slot and the DMA module is working on the oldest element in the buffer!
#define DMA_BUF_GET_QUERY_WRITE()		( ( ( (g_dma_head + 2) % DAC_DATABUF_NUM_DMATFRS) == g_dma_tail) ? false : true)

// Returns the base offset for the next empty slot of data
#define DMA_BUF_GET_WRITE_ADDR()		((uint8_t*)&g_dmasrc[g_dma_head*DAC_DMATFR_BYTES])

// Increments the base offset after a write to the DMA buffer
#define DMA_BUF_INC_WRITE_ADDR()		(g_dma_head = (g_dma_head + 1) % DAC_DATABUF_NUM_DMATFRS)

/** @brief Interrupt vector for systime timer */
#define BOARD_SYSTIME_vect (TCC4_CCA_vect)
/** @brief Interrupt vector for output buffer-filling timer */
#define BOARD_OUT_BUF_vect (TCC5_CCA_vect)

/** @brief Enumerated type for achievable core clock frequencies */
enum board_fcpu_t {board_fcpu_1_024 = 0,	board_fcpu_4_096,	board_fcpu_5_6448,	board_fcpu_8_192,	board_fcpu_11_2896,	board_fcpu_16_384,	board_fcpu_22_5792,	board_fcpu_24_576,	board_fcpu_num};
/** @brief Enumerated type for achievable output sample rates. NOTE: THESE MUST MATCH IN ORDER AND INITIAL VALUE THE CORE CLOCK FREQUENCIES IN board_fcpu_t! */
enum wave_sr_t {wave_sr_null = 0,			wave_sr_8khz,		wave_sr_11_025khz,	wave_sr_16khz,		wave_sr_22_050khz,	wave_sr_32khz,		wave_sr_44_1khz,	wave_sr_48khz,		wave_sr_num};

/** @brief Enumerated type for input control lines */
enum board_ctrl_inp_t {board_ctrl_inp_0 = 0, board_ctrl_inp_1, board_ctrl_inp_2, board_ctrl_inp_3, board_ctrl_inp_4, board_ctrl_inp_5, board_ctrl_inp_num};

/** @brief Enumerated type for status LEDs */
enum board_led_t {board_led_0 = 0, board_led_1, board_led_num};

/** @brief Sets up all GPIO pins (direction, qualification, etc) */
void board_init_io(void);

/** @brief Sets up the UART module for serial communication.
  * Does not configure the baud-rate registers, a call to board_set_clk() must
  * be performed before attempting to use the UART module. */
void board_init_uart(void);

/** @brief Performs all start-up initialization of the clock subsystem.
 *  @param board_init_fcpu	Core clock frequency to switch to after initialization */
void board_init_clk(enum board_fcpu_t board_init_fcpu);

/** @brief Sets up the systime timer */
void board_init_systime(void);

/** @brief Initializes peripherals for communications with the DAC */
void board_init_i2s(void);

/** @brief Returns true if the UART TX buffer is full */
bool board_get_uart_txfull(void);
/** @brief Queues a byte for transmission in the uart module */
void board_set_uart_txbuf(char data);
/** @brief Gets the most recently recieved byte from the uart module*/
char board_get_uart_rxbuf(void);

/** @brief Gets the logic status of a control input line.
 *  @return True if the line is actively driven (logic low voltage) */
bool board_get_ctrl(enum board_ctrl_inp_t board_ctrl_inp);

/** @brief Returns the current core clock frequency */
enum board_fcpu_t board_get_clk(void);

/** @brief Re-configures the clock system for a new clock frequency.
 * Preserves the baud rate for the UART module and systime timer, but
 * temporarily disables UART the receiver for a short period while the clock
 * rate is changing.
 * (It is up the host to ensure that no comm's are in progress before calling
 * this function).
 * @param board_new_fcpu The new core clock frequency to switch to
 * @praram force Set true to force switching to the new clock frequency even if
 *		already running at the new clock frequency (only needed internally) */
void board_set_clk(enum board_fcpu_t board_new_fcpu, bool force);

/** @brief Similar to the board_set_clk function, but issues an XOFF packet
 * before switching clock rates and an XON packet afterwards. */
void board_set_clk_flow_ctrl(enum board_fcpu_t board_new_fcpu);

/** @brief Sets (turns on) a specified LED */
__attribute__((always_inline)) inline void board_set_led(enum board_led_t led){
	switch(led){
		case board_led_0:
		LED0_PORT.OUTCLR = (1<<LED0_PIN);
		break;
		case board_led_1:
		LED1_PORT.OUTCLR = (1<<LED1_PIN);
		break;
		default:
		break;
	}
}

/** @brief Clears (turns off) a specified LED */
__attribute__((always_inline)) inline void board_clr_led(enum board_led_t led){
	switch(led){
		case board_led_0:
		LED0_PORT.OUTSET = (1<<LED0_PIN);
		break;
		case board_led_1:
		LED1_PORT.OUTSET = (1<<LED1_PIN);
		break;
		default:
		break;
	}
}

/** @brief Returns the raw button state for button num (0 - (BOARD_NUM_BTNS - 1)) */
bool board_get_btn(uint8_t num);

/** @brief Temporarily turns on, triggers new ADC conversion for the volume control, returns the result and turns off the ADC peripheral */
uint8_t board_get_analog_vol(void);

/** @brief Sets the analog peripheral powerdown state (true -> powered down)
 * NOTE: Will block for a long time (milliseconds) when disabling powered down mode! */
void board_set_pwrdn_analog(bool state);
/** @brief Returns the current analog power down state */
bool board_get_pwrdn_analog(void);

/** @brief Sets the cheapamp mute input to a specified state */
void board_set_cheapamp_mute(bool state);

/** @brief Disables the DAC and stops any ongoing communications with it
 * NOTE: Does *not* disable the analog circuitry to reduce power consumption! */
void board_set_dis_dac(void);

/** @brief Enables, configures, and starts supplying data to the DAC
 * NOTE: Be sure to enable the analog power before calling!
 * NOTE: Be sure to have filled the DAC buffer before calling! */
void board_set_en_dac(void);

/** @brief Calculates bsel and bscale values for a given clock frequency */
void board_calc_bsel_bscale(uint16_t *bsel, uint8_t* bscale, enum board_fcpu_t fcpu);

#endif //BOARD_H_