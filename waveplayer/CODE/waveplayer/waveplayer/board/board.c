#include "board.h"
#include <stddef.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "baud_calc.h"
#include "ccpwite.h"
#include "../config.h"
#include "../shell/uart.h"
#include "../systime/systime.h"
#include "../globals.h"
#include "../drivers/pcm178x.h"

/** @brief Port with control line inputs */
#define BOARD_CTRL_PORT PORTA
/** @brief Base offset (pin for input 0) for control line inputs */
#define BOARD_CTRL_OFFSET	0
/** @brief ADC peripheral used for control lines */
#define BOARD_CTRL_ADC ADCA
/** @brief Sample window open for this number of ADC clocks. Relatively uncritical, should be somewhat large. Must be <= 63 */
#define BOARD_CTRL_ADC_SAMPTIME 63
/** @brief ADC channel used for measurement */
#define BOARD_CTRL_ADC_CH BOARD_CTRL_ADC.CH0
/** @brief Hardware pin used for volume adjustment */
#define BOARD_ADC_VOLCHAN	ADC_CH_MUXPOS_PIN0_gc

/** @brief ADC calibration registers */
#define BOARD_CTRL_ADC_CALH ADCACAL0
#define BOARD_CTRL_ADC_CALL ADCACAL1

/** @brief Defines for DAC data interface
 * NOTE: These data lines use system peripherals, to make changes the
 * associated peripheral must also be updated! */
#define DAC_DATA_LRCLK_PIN				5
#define DAC_DATA_LRCLK_PORT				PORTD
#define DAC_DATA_TIMER					TCD5
#define	DAC_DATA_TIMER_CLKSRC			TC45_CLKSEL_EVCH0_gc
#define DAC_DATA_TIMER_DEFCNT			1

#define DAC_DATA_BCLK_PORT				PORTC
#define DAC_DATA_BCLK_PIN				1
#define DAC_DATA_BCLK_PINnCTRL			PIN1CTRL
#define DAC_DATA_BCLK_SENSE_PIN			2
#define DAC_DATA_BCLK_SENSE_PINnCTRL	PIN2CTRL
#define DAC_DATA_BCLK_SENSE_EVMUX_gc	EVSYS_CHMUX_PORTC_PIN2_gc
#define DAC_DATA_BCLK_SENSE_CH_MUX		CH0MUX	/* Channel mux for event system */
#define DAC_DATA_BDATA_PORT				PORTC
#define DAC_DATA_BDATA_PIN				3
#define DAC_DATA_OVERSAMPLE				512		/* Oversampling rate for DAC */
#define DAC_DATA_WORDLEN				(2*8*DAC_DATA_SAMPBYTES)	/* Number of data bits per sample in data stream */
#define DAC_DATA_PRESCALE				(DAC_DATA_OVERSAMPLE/(2*DAC_DATA_WORDLEN))	/* Prescaling factor, system clocks per bit on data interface */

/** @brief DAC master clock defines
 * NOTE: These data lines use system peripherals, to make changes the
 * associated peripheral must also be updated! */
#define DAC_MCLK_PORT	PORTD
#define DAC_MCLK_PIN	4

/** @brief Defines for control lines for cheapamp */
#define CHEAPAMP_SHDN_PORT	PORTD
#define CHEAPAMP_SHDN_PIN	2
#define CHEAPAMP_MUTE_PORT	PORTD
#define CHEAPAMP_MUTE_PIN	3

/** @brief Defines for UART interface
 * NOTE: These data lines use system peripherals, to make changes the
 * associated peripheral must also be updated! */
#define UART_PORT		PORTD
#define UART_TX_PIN		7
#define UART_RX_PIN		6

/** @brief Timer to use for systime library */
#define SYSTIME_TIMER TCC4

/** @brief Timer to use for filling output buffer */
#define OUT_BUF_TIMER TCC5

/** @brief Returns the value to write to the PLLCTRL register to achieve a specific clock frequency */
inline void board_calc_clkctrl(enum board_fcpu_t fcpu, uint8_t* pllctrl_shdw, uint8_t* psctrl_shdw);

/** @brief Sets up the systime timer for a certain clock frequency */
static inline void board_set_systime_per(enum board_fcpu_t new_fcpu);

/** @brief Returns the current analog power down state */
bool board_get_pwrdn_analog(void){
	return (CHEAPAMP_SHDN_PORT.OUT & (1<<CHEAPAMP_SHDN_PIN)) ? false : true;
}

/** @brief Reads a calibration byte from an offset */
static uint8_t board_read_calbyte(uint8_t idx);

static uint8_t board_read_calbyte(uint8_t idx){
	uint8_t retval;
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	retval = pgm_read_byte(idx);
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	return retval;
}

/** @brief Sets up the DAC IO pins for normal mode operation */
static inline void board_set_dacio_normal(void){
	//Set up DAC SPI control interface
	DAC_SPI_CTRL_SCK_PORT.OUTCLR = (1<<DAC_SPI_CTRL_SCK_PIN);
	DAC_SPI_CTRL_SCK_PORT.DIRSET = (1<<DAC_SPI_CTRL_SCK_PIN);
	
	DAC_SPI_CTRL_MOSI_PORT.OUTCLR = (1<<DAC_SPI_CTRL_MOSI_PIN);
	DAC_SPI_CTRL_MOSI_PORT.DIRSET = (1<<DAC_SPI_CTRL_MOSI_PIN);
	
	DAC_SPI_CTRL_SS_PORT.OUTSET = (1<<DAC_SPI_CTRL_SS_PIN);	//Drive !SS pin high to disable SPI input on DAC
	DAC_SPI_CTRL_SS_PORT.DIRSET = (1<<DAC_SPI_CTRL_SS_PIN);
	
	//Set up DAC data pins
	DAC_DATA_LRCLK_PORT.DIRSET = 1<<DAC_DATA_LRCLK_PIN;
	DAC_DATA_BCLK_PORT.DIRSET = 1<<DAC_DATA_BCLK_PIN;
	DAC_DATA_BDATA_PORT.DIRSET =  1<<DAC_DATA_BDATA_PIN;
	
	//Set up DAC master clock pin
	DAC_MCLK_PORT.DIRSET = (1<<DAC_MCLK_PIN);
}

/** @brief Sets up the DAC IO pins for powered-down mode */
static inline void board_set_dacio_pwrdn(void){
	//Set up DAC SPI control interface
	DAC_SPI_CTRL_SCK_PORT.DIRCLR = (1<<DAC_SPI_CTRL_SCK_PIN);
	DAC_SPI_CTRL_MOSI_PORT.DIRCLR = (1<<DAC_SPI_CTRL_MOSI_PIN);
	DAC_SPI_CTRL_SS_PORT.DIRCLR = (1<<DAC_SPI_CTRL_SS_PIN);
		
	//Set up DAC data pins
	DAC_DATA_LRCLK_PORT.DIRCLR = 1<<DAC_DATA_LRCLK_PIN;
	DAC_DATA_BCLK_PORT.DIRCLR = 1<<DAC_DATA_BCLK_PIN;
	DAC_DATA_BDATA_PORT.DIRCLR =  1<<DAC_DATA_BDATA_PIN;
		
	//Set up DAC master clock pin
	DAC_MCLK_PORT.DIRCLR = (1<<DAC_MCLK_PIN);
}

uint8_t board_get_analog_vol(void){
	uint8_t retval = 0;
	//Set up peripheral
	//Enable ADC
	BOARD_CTRL_ADC.CTRLA = ADC_ENABLE_bm;
	//Limit ADC current consumption, set up for signed measurement and 8-bit resolution
	BOARD_CTRL_ADC.CTRLB = ADC_CURRLIMIT_HIGH_gc | ADC_CONMODE_bm | ADC_RESOLUTION_8BIT_gc;
	//Set up ADC reference to use Vcc/1.6
	BOARD_CTRL_ADC.REFCTRL = ADC_REFSEL_INTVCC_gc;
	//Prescale for ADC clock speed of 100kHz at minimum active clock frequency
	BOARD_CTRL_ADC.PRESCALER = ADC_PRESCALER_DIV128_gc;
	//Set up sample time to a reasonably large value
	BOARD_CTRL_ADC.SAMPCTRL = BOARD_CTRL_ADC_SAMPTIME & ADC_SAMPVAL_gm;
	//Load ADC calibration data
	BOARD_CTRL_ADC.CALH = board_read_calbyte(offsetof(NVM_PROD_SIGNATURES_t,BOARD_CTRL_ADC_CALH));
	BOARD_CTRL_ADC.CALL = board_read_calbyte(offsetof(NVM_PROD_SIGNATURES_t,BOARD_CTRL_ADC_CALL));
	
	//Set up channel to use external input
	BOARD_CTRL_ADC_CH.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
		
	//Set up channel to use desired IO pin for positive input.
	BOARD_CTRL_ADC_CH.MUXCTRL = BOARD_ADC_VOLCHAN;
	//Clear interrupt flag first, write one to clear
	BOARD_CTRL_ADC_CH.INTFLAGS |= ADC_CH_IF_bm;
	//Trigger new start of conversion
	BOARD_CTRL_ADC_CH.CTRL |= ADC_CH_START_bm;
	//Wait until conversion is complete
	while(!(BOARD_CTRL_ADC_CH.INTFLAGS & ADC_CH_IF_bm)){};
	//Store converted value
	retval = BOARD_CTRL_ADC_CH.RES;
	
	//Turn off peripheral. Wipe entire contents as tests show simply turning off ADC_ENABLE isn't enough
	memset(&BOARD_CTRL_ADC, 0, sizeof(BOARD_CTRL_ADC));
	memset(&BOARD_CTRL_ADC_CH, 0, sizeof(BOARD_CTRL_ADC_CH));
	return retval;
}

bool board_get_btn(uint8_t num){
	bool retval = false;
	if(num >= BOARD_NUM_BTNS){
		return retval;
	}
	if((BOARD_CTRL_PORT.IN & (1<<(num + BOARD_CTRL_OFFSET))) == false){	//There is a pull-up to VCC so a low value signifies the button is pressed
		retval = true;
	}
	return retval;
}

void board_init_i2s(void){
	//Initialize UART module, event system, and DMA.
	//We want to send a 32-bit samples, toggling LRCLK on every 32nd falling flank on BCK.
	//The DMA peripheral is used to offload processing power needs
	
	//Configure UART module as SPI master
	DAC_DATA_UART.CTRLA = 0x00;
	DAC_DATA_UART.CTRLB = USART_TXEN_bm;					//Enable UART transmitter (but not reciever)
	DAC_DATA_UART.CTRLC = USART_CMODE_MSPI_gc | (1<<1);		//Enable operation as SPI master and set UCPHA (not included in <avr/io.h>)
	DAC_DATA_BCLK_PORT.DAC_DATA_BCLK_PINnCTRL = PORT_INVEN_bm;	//Effectively equal to setting the CPOL bit in an SPI module
	DAC_DATA_UART.BAUDCTRLA = (DAC_DATA_PRESCALE - 1) & 0xFF;		//DAC_DATA_PRESCALE will always fit into BAUDCTRLA (as it's far less than 255 for all possible values)
	
	//Configure LRCLK functionality
	DAC_DATA_TIMER.CTRLA = DAC_DATA_TIMER_CLKSRC;						//Clock timer on event channel activity
	EVSYS.DAC_DATA_BCLK_SENSE_CH_MUX = DAC_DATA_BCLK_SENSE_EVMUX_gc;	//Generate events on sense pin falling flank
	DAC_DATA_TIMER.CTRLB = TC45_WGMODE_SINGLESLOPE_gc;					//Set timer to normal operation mode
	DAC_DATA_TIMER.CTRLC = 0x00;
	DAC_DATA_TIMER.CTRLD = 0x00;
	DAC_DATA_TIMER.CTRLE = TC45_CCBMODE_COMP_gc;						//Override normal port operation
	DAC_DATA_TIMER.CNT = DAC_DATA_TIMER_DEFCNT;		//Reset timer count value to ensure correct sync between bit clock and LRCLK
	DAC_DATA_TIMER.PER = DAC_DATA_WORDLEN - 1;	//One whole sample (left and right data)
	DAC_DATA_TIMER.CCB = (DAC_DATA_WORDLEN/2);	//One half sample (left / right data)
	//Run last, as this enables the peripheral
	
	//Configure the output buffer timer
	OUT_BUF_TIMER.CTRLB = TC45_WGMODE_NORMAL_gc;
	OUT_BUF_TIMER.INTCTRLB = TC45_CCAINTLVL_LO_gc;
	//Schedule interrupts to trigger so that nominally one block can be written per ISR call
	OUT_BUF_TIMER.PER =	(DAC_DATA_OVERSAMPLE/(8*2*DAC_DATA_SAMPBYTES)*DAC_DMATFR_BYTES);
	OUT_BUF_TIMER.CTRLA = TC45_CLKSEL_DIV8_gc;
}

void board_set_dis_dac(void){
	EDMA.CTRL &= ~(EDMA_ENABLE_bm);		//Disable any ongoing DMA communications when completed
	while(EDMA.CTRL & EDMA_ENABLE_bm){};//Wait for DMA module to finish
	g_dma_tail = 0;
	g_dma_head = 0;
	board_clr_led(board_led_0);				//Turn off active-LED
}

void board_set_en_dac(void){
	board_init_i2s();	//Reset I2S setup
	pcm178x_init();		//Setup DAC defaults
		
	//Set up DMA controller, use channels 0 and 1 for I2S communications
	EDMA.CTRL = EDMA_RESET_bm;			//Ensure EDMA controller is fully reset to default values
	while(EDMA.CTRL & EDMA_RESET_bm){}; //Wait for reset to complete (will block if a transfer is in progress)
	
	//Set up DMA channels 0 and 1 for double buffering
	EDMA.CTRL = EDMA_DBUFMODE_BUF01_gc;
	EDMA.CH0.CTRLA = EDMA_CH_REPEAT_bm | EDMA_CH_SINGLE_bm;
	EDMA.CH1.CTRLA = EDMA_CH_REPEAT_bm | EDMA_CH_SINGLE_bm;
	
	//Set error interrupt level to low and transmission complete interrupt level to high
	EDMA.CH0.CTRLB = EDMA_CH_ERRINTLVL0_bm | EDMA_CH_TRNINTLVL0_bm | EDMA_CH_TRNINTLVL1_bm;
	EDMA.CH1.CTRLB = EDMA_CH_ERRINTLVL0_bm | EDMA_CH_TRNINTLVL0_bm | EDMA_CH_TRNINTLVL1_bm;
	
	EDMA.CH0.ADDRCTRL = EDMA_CH_RELOAD_TRANSACTION_gc | EDMA_CH_DIR_INC_gc;	//Reload DMA source address on transaction complete, increment source address on each byte transferred
	EDMA.CH1.ADDRCTRL = EDMA_CH_RELOAD_TRANSACTION_gc | EDMA_CH_DIR_INC_gc;

	EDMA.CH0.TRIGSRC = EDMA_CH_TRIGSRC_USARTC0_DRE_gc;
	EDMA.CH1.TRIGSRC = EDMA_CH_TRIGSRC_USARTC0_DRE_gc;
	
	#if DAC_DMATFR_BYTES == 256
	EDMA.CH0.TRFCNTL = 0;	//Copy n bytes per DMA transfer
	EDMA.CH1.TRFCNTL = 0;
	#else
	EDMA.CH0.TRFCNTL = DAC_DMATFR_BYTES;	//Copy n bytes per DMA transfer
	EDMA.CH1.TRFCNTL = DAC_DMATFR_BYTES;
	#endif
	
	DMA_A_UPDATE_SRCADDR();
	DMA_B_UPDATE_SRCADDR();
	
	EDMA.CTRL |= EDMA_ENABLE_bm;			//Enable EDMA module
	EDMA.CH0.CTRLA |= EDMA_CH_ENABLE_bm;	//Trigger channel 0 to start the data transfer
	board_set_led(board_led_0);				//Turn on active-LED
}

void board_init_io(void){
	//Set up control lines (A0 - A5)
	PORTCFG.MPCMASK = 0x3F;
	PORTA.PIN0CTRL = PORT_OPC_PULLUP_gc;
	
	// Set up DAC pins to a powered down state, we'll active them later as needed
	board_set_dacio_pwrdn();
	
	//Set up the sense pin to generate events on negative flank which will later be used to clock the LRCLK timer
	DAC_DATA_BCLK_PORT.DIRCLR = (1<<DAC_DATA_BCLK_SENSE_PIN);
	DAC_DATA_BCLK_PORT.DAC_DATA_BCLK_SENSE_PINnCTRL = PORT_ISC_RISING_gc;
	
	//Set up LED pins
	LED0_PORT.OUTSET = (1<<LED0_PIN);
	LED0_PORT.DIRSET = (1<<LED0_PIN);
	LED1_PORT.OUTSET = (1<<LED1_PIN);
	LED1_PORT.DIRSET = (1<<LED1_PIN);
	
	//Set up SD-card SPI pins
	SD_SCK_PORT.OUTCLR = (1<<SD_SCK_PIN);	//SCK line should idle low
	SD_SCK_PORT.DIRSET = (1<<SD_SCK_PIN);
	
	SD_MISO_PORT.DIRCLR = (1<<SD_MISO_PIN);
	SD_MISO_PORT.SD_MISO_PINnCTRL = (PORT_OPC_PULLUP_gc);	//Enable pullup
	
	SD_MOSI_PORT.OUTSET = (1<<SD_MOSI_PIN);	//MOSI line should idle high
	SD_MOSI_PORT.DIRSET = (1<<SD_MOSI_PIN);
	
	SD_CS_PORT.OUTSET = (1<<SD_CS_PIN);		//CS line should idle high
	SD_CS_PORT.DIRSET = (1<<SD_CS_PIN);
	
	//Set up cheapamp control lines, for now set (bring low) mute and shutdown pins
	//Note that this disables the analog circuitry, which will be enabled later on.
	CHEAPAMP_MUTE_PORT.OUTCLR = (1<<CHEAPAMP_MUTE_PIN);
	CHEAPAMP_MUTE_PORT.DIRSET = (1<<CHEAPAMP_MUTE_PIN);
	CHEAPAMP_SHDN_PORT.OUTCLR = (1<<CHEAPAMP_SHDN_PIN);
	CHEAPAMP_SHDN_PORT.DIRSET = (1<<CHEAPAMP_SHDN_PIN);
	
	//Set up DAC master clock pins
	PORTCFG.CLKOUT |= PORTCFG_CLKEVPIN_PIN4_gc | PORTCFG_CLKOUT_PD7_gc;
	
	//Set up UART pins
	UART_PORT.REMAP |= (PORT_USART0_bm);	//Set remap bit to move UART module to high nibble
	UART_PORT.OUTSET = (1<<UART_TX_PIN);	//TX line should idle high
	UART_PORT.DIRSET = (1<<UART_TX_PIN);
	UART_PORT.DIRCLR = (1<<UART_RX_PIN);	
}

void board_init_uart(void){
	USARTREG.CTRLA = USART_RXCINTLVL_MED_gc;	//Enable medium-priority interrupt on UART RX complete
	USARTREG.CTRLB = USART_RXEN_bm | USART_TXEN_bm;	//Enable Rx and Tx for UART
	USARTREG.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_CHSIZE_8BIT_gc;	//Configure UART for asynchronous comms, 1 start bit, 1 stop bit, 8 data bits, no parity
}

void board_init_systime(void){
	//Hacky initialization as datasheet seems to be incorrect (?)
	//We're setting up for triggering OVFINT, but we're using the CCA ISR...
	//Based on http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=1066160
	SYSTIME_TIMER.CTRLB = TC45_WGMODE_NORMAL_gc;
	SYSTIME_TIMER.INTCTRLB = TC45_CCAINTLVL_MED_gc;
	SYSTIME_TIMER.CTRLA = TC45_CLKSEL_DIV1_gc;
}

void board_init_clk(enum board_fcpu_t board_init_fcpu){
	OSC.XOSCCTRL = OSC_FRQRANGE_2TO9_gc | OSC_XOSCSEL_XTAL_16KCLK_gc;	//Configure input for crystal oscillator with a stabilization delay of 16k clocks	
	
	//Set up the PLL for some arbitrary frequency to begin with
	uint8_t pllctrl, psctrl;
	board_calc_clkctrl(board_init_fcpu, &pllctrl, &psctrl);
	CCPWrite(&OSC.PLLCTRL, pllctrl);
	CCPWrite(&CLK.PSCTRL, psctrl);
	
	OSC.CTRL = OSC_XOSCEN_bm | OSC_PLLEN_bm;			//Enable power to the external oscillator and PLL
	while(!(OSC.STATUS & OSC_PLLRDY_bm)){};				//Wait until PLL has stabilized
	CCPWrite(&CLK.CTRL, CLK_SCLKSEL_PLL_gc);			//Switch to PLL as clock source (2MHz RC oscillator left on, used later when changing clock frequency)
	board_set_clk(board_init_fcpu, true);						//Finally, switch to desired frequency and update all registers that are based on clock frequency
}

bool board_get_uart_txfull(void){
	bool retval;
	if(USARTREG.STATUS & USART_DREIF_bm){
		retval = false;
	}else{
		retval = true;
	}
	return retval;
}

void board_set_uart_txbuf(char data){
	USARTREG.DATA = data;
}

char board_get_uart_rxbuf(void){
	return USARTREG.DATA;
}

bool board_get_ctrl(enum board_ctrl_inp_t board_ctrl_inp){
	if(board_ctrl_inp >= board_ctrl_inp_num){
		return false;
	}
	//Return the n'th bit of the input data register. Mask away any upper bits.
	return (((BOARD_CTRL_PORT.IN) >> (board_ctrl_inp + BOARD_CTRL_OFFSET)) & 0x01);
}

enum board_fcpu_t board_get_clk(void){
	enum board_fcpu_t retval = board_fcpu_num;
	uint8_t shdw_pllctrl = OSC.PLLCTRL;
	uint8_t shdw_psctrl = CLK.PSCTRL;
	//Relatively inefficient algorithm, step through all valid PLLCTRL values and see what we've got.
	//As there are only a handful of valid values this is acceptably fast.
	uint8_t pllctrl, psctrl;
	for(uint8_t i = 0; i < (uint8_t) board_fcpu_num; i++){
		board_calc_clkctrl((enum board_fcpu_t)i, &pllctrl, &psctrl);
		if(shdw_psctrl == psctrl && shdw_pllctrl == pllctrl){
			retval = (enum board_fcpu_t)i;
		}
	}
	return retval;
}

static inline void board_set_systime_per(enum board_fcpu_t new_fcpu){
	uint16_t period;
	switch(new_fcpu){
		case board_fcpu_1_024:
		period = (SYSTICK_PER_us * 1.024);
		break;
		case board_fcpu_4_096:
		period = (SYSTICK_PER_us * 4.096);
		break;
		case board_fcpu_5_6448:
		period = (SYSTICK_PER_us * 5.6448);
		break;
		case board_fcpu_8_192:
		period = (SYSTICK_PER_us * 8.192);
		break;
		case board_fcpu_11_2896:
		period = (SYSTICK_PER_us * 11.2896);
		break;
		case board_fcpu_16_384:
		period = (SYSTICK_PER_us * 16.384);
		break;
		case board_fcpu_22_5792:
		period = (SYSTICK_PER_us * 22.5792);
		break;
		default:	//Insert default clause that will result in largest systime period
		case board_fcpu_24_576:
		period = (SYSTICK_PER_us * 24.576);
		break;
	}
	SYSTIME_TIMER.PER = period;
}

void board_set_pwrdn_analog(bool state){
	if(state && !board_get_pwrdn_analog()){
		//We want to transition to a low-power state (IE. power down the analog circuitry)
		//First, ensure all control lines to the DAC are low to ensure that
		//we're not leaking power through the I/O pins
		
		board_set_dacio_pwrdn();
		//We don't need to wait before turning off the power as the IO lines turn off quickly
		
		//Finally, turn off the analog power and power indicator
		CHEAPAMP_SHDN_PORT.OUTCLR = (1<<CHEAPAMP_SHDN_PIN);
		board_clr_led(board_led_1);
	}
	if(!state && board_get_pwrdn_analog()){
		//First, apply analog supply voltage and wait for a small time for it
		//to rise before apply high logic levels to the digital control pins
		CHEAPAMP_SHDN_PORT.OUTSET = (1<<CHEAPAMP_SHDN_PIN);
		
		systime_t delay = systime_get_delay(MS2US(ANALOG_VCC_STAB_TIME_ms));
		while(!systime_get_delay_passed(delay)){};
		
		//After delay has passed, set pins to normal (output) state, set power indicator, and disable the !mute line
		board_set_dacio_normal();
		board_set_led(board_led_1);
	}
}

void board_set_cheapamp_mute(bool state){
	if(state){
		CHEAPAMP_MUTE_PORT.OUTCLR = (1<<CHEAPAMP_MUTE_PIN);
	}else{
		CHEAPAMP_MUTE_PORT.OUTSET = (1<<CHEAPAMP_MUTE_PIN);
	}
}

void board_set_clk_flow_ctrl(enum board_fcpu_t board_new_fcpu){
	uart_putc(UART_XOFF);
	systime_t delay = systime_get_delay(MS2US(UART_XOFF_DELAY_ms));	//Wait a suitable time to ensure all buffers are flushed
	while(!systime_get_delay_passed(delay)){};	
	board_set_clk(board_new_fcpu, false);
	uart_putc(UART_XON);
}

void board_set_clk(enum board_fcpu_t board_new_fcpu, bool force){
	//Check that we're not already running at the desired clock frequency
	if(!force && board_new_fcpu == board_get_clk()){
		return;
	}
	
	//Pre-calculate the baud-rate registers to use when the clock has been switched.
	uint16_t bsel = 0;
	uint8_t bscale = 0;
	board_calc_bsel_bscale(&bsel, &bscale, board_new_fcpu);
	
	//For safety, make sure 2MHz oscillator is active
	OSC.CTRL = OSC_RC2MEN_bm;
	//Wait until 2MHz clock is stable (which it should already be)
	while(!(OSC.STATUS & OSC_RC2MRDY_bm)){};
	
	//Run this block atomically. This should be relatively fast as the PLL stabilizes
	//on the order of 25us.
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		CCPWrite(&CLK.CTRL, CLK_SCLKSEL_RC2M_gc);
		
		//Disable the PLL in order to be able to change parameters
		OSC.CTRL &= ~(OSC_PLLEN_bm);
		//Set up PLL for new clock rate
		uint8_t pllctrl, psctrl;
		board_calc_clkctrl(board_new_fcpu, &pllctrl, &psctrl);
		CCPWrite(&OSC.PLLCTRL, pllctrl);
		CCPWrite(&CLK.PSCTRL, psctrl);
		//Power up PLL now that new values are loaded
		OSC.CTRL |= OSC_PLLEN_bm;
		//Wait for PLL to stabilize
		while(!(OSC.STATUS & OSC_PLLRDY_bm)){};
	
		//Switch to PLL as clock source and update peripherals
		CCPWrite(&CLK.CTRL, CLK_SCLKSEL_PLL_gc);
		board_set_systime_per(board_new_fcpu);
		USARTREG.BAUDCTRLA = bsel & 0xFF;
		USARTREG.BAUDCTRLB = (bscale << USART_BSCALE_gp) | ((bsel >> 8) & USART_BSEL_gm);
	}
}

inline void board_calc_clkctrl(enum board_fcpu_t fcpu, uint8_t* pllctrl_shdw, uint8_t* psctrl_shdw){
	uint8_t pllctrl = OSC_PLLSRC_XOSC_gc;	//Shadow of data to write to PLLCTRL register; all modes want external oscillator PLL source
	uint8_t psctrl = CLK_PSBCDIV_1_1_gc;
	switch(fcpu){	//Select clock frequency based on input
	default:
	case board_fcpu_1_024:
		//In this case, F_out = F_in/4, so set up for a multiplication factor of 1/4
		pllctrl |= ((1)<<(OSC_PLLFAC_gp)) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_4_gc;
		break;
	case board_fcpu_4_096:
		//In this case, F_out = F_in, so set up for a multiplication factor of 1
		pllctrl |= ((1)<<(OSC_PLLFAC_gp)) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_1_gc;
		break;
	case board_fcpu_5_6448:
		//F_out = 11/8 F_in
		pllctrl |= ((11)<<OSC_PLLFAC_gp) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_8_gc;
		break;
	case board_fcpu_8_192:
		//F_out = 2 * F_in, so set up for multiplication factor of 2
		pllctrl |= ((2)<<(OSC_PLLFAC_gp)) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_1_gc;
		break;
	case board_fcpu_11_2896:
		//F_out = 11/4 F_in
		pllctrl |= ((11)<<OSC_PLLFAC_gp) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_4_gc;
		break;
	case board_fcpu_16_384:
		// F_out = 4 * F_in
		pllctrl |= ((4)<<(OSC_PLLFAC_gp)) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_1_gc;
		break;
	case board_fcpu_22_5792:
		// F_out ~= 11/2 * F_in
		pllctrl |= ((11)<<(OSC_PLLFAC_gp)) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_2_gc;
		break;
	case board_fcpu_24_576:
		// F_out = 6 * F_in
		pllctrl |= ((6)<<(OSC_PLLFAC_gp)) & OSC_PLLFAC_gm;
		psctrl |= CLK_PSADIV_1_gc;
		break;
	}
	*pllctrl_shdw = pllctrl;
	*psctrl_shdw = psctrl;
}

void board_calc_bsel_bscale(uint16_t *bsel, uint8_t* bscale, enum board_fcpu_t fcpu){
	if(bsel == NULL || bscale == NULL){
		return;
	}
	switch(fcpu){
		case board_fcpu_1_024:
		*bsel = BSEL(1024000ULL, UART_BAUD);
		*bscale = BSCALE(1024000ULL, UART_BAUD);
		break;
		case board_fcpu_4_096:
		*bsel = BSEL(4096000ULL, UART_BAUD);
		*bscale = BSCALE(4096000ULL, UART_BAUD);
		break;
		case board_fcpu_5_6448:
		*bsel = BSEL(5644800ULL, UART_BAUD);
		*bscale = BSCALE(5644800ULL, UART_BAUD);
		break;
		case board_fcpu_8_192:
		*bsel = BSEL(8192000ULL, UART_BAUD);
		*bscale = BSCALE(8192000ULL, UART_BAUD);
		break;
		case board_fcpu_11_2896:
		*bsel = BSEL(11289600ULL, UART_BAUD);
		*bscale = BSCALE(11289600ULL, UART_BAUD);
		break;
		case board_fcpu_16_384:
		*bsel = BSEL(16384000ULL, UART_BAUD);
		*bscale = BSCALE(16384000ULL, UART_BAUD);
		break;
		case board_fcpu_22_5792:
		*bsel = BSEL(22579200ULL, UART_BAUD);
		*bscale = BSCALE(22579200ULL, UART_BAUD);
		break;
		case board_fcpu_24_576:
		*bsel = BSEL(24576000ULL, UART_BAUD);
		*bscale = BSCALE(24576000ULL, UART_BAUD);
		break;
		default:
		break;
	}
}