/** @file A general-purpose stereo wave-file (PCM) playback device, with
 * configurable triggers and various settings. See more at www.rabidmantis.se */

/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */	

/* Ensure the device fuses are set to the following values;
 * WDWP = 8CLK
 * WDP = 8KCLK
 * BOOTRST = APPLICATION
 * BODPD = SAMPLED
 * RSTDISBL = [ ]
 * SUT = 64MS
 * WDLOCK = [X]
 * BODACT = SAMPLED
 * EESAVE = [ ]
 * BODLVL = 2V4
 * FDACT5 = [ ]
 * FDACT4 = [ ]
 * VALUE = 0x3F
 * 
 * FUSEBYTE1 = 0x0A (valid)
 * FUSEBYTE2 = 0xFD (valid)
 * FUSEBYTE4 = 0xF1 (valid)
 * FUSEBYTE5 = 0xDB (valid)
 * FUSEBYTE6 = 0xFF (valid)
 */
// Note; fusebyte 0 and 3 are not used/defined, so leave them at a default state (0xFF)
const char fusedata[] __attribute__((section(".fuse"))) = {0xFF, 0x0A, 0xFD, 0xFF, 0xF1, 0xDB, 0xFF};
 
 #include "waveplayer.h"
#include <util/atomic.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include "globals.h"
#include "board/board.h"
#include "shell/shell.h"
#include "flex_settings/flex_settings.h"
#include "systime/systime.h"
#include "waveplay/waveplay.h"
#include "sd-reader/sd_raw.h"
#include "sd-reader/fat.h"
#include "sd-reader/partition.h"
#include "sd-reader/utils.h"
#include "drivers/pcm178x.h"
#include "ctrl.h"
#include "btn_debounce/btn_debounce.h"

/************************************************************************
 Notes for manual;
 - When low power mode is disabled there are still some pops in the analog output on start/stop transitions.
 These are small (< 1mv) but noticable at high attenuation levels. To reduce these, operate at high(er) volume
 levels or read the mute pin status.
 - Cheapamp mute needs several seconds to stabilize (?), so for short files there are residual pops/clicks at around -40db.
 - Typical idle-state current consumption is 2.4mA for supply voltages below 24V.
************************************************************************/

/** @brief Dummy function used to print the contents of a sample configuration file.
 * Made as a function to reduce code size as this is called from multiple locations */
static void print_templatefile(void){
	shell_puts_ext_P(PSTR(MSG_STARTUP_NOCONFIGFILE));
}

int main(void){
	ATOMIC_BLOCK(ATOMIC_FORCEON){
		//Perform low-level initialization with interrupts disabled
		board_init_io();		
		board_init_clk(MAXSPEED_FCPU);	//Start up with the maximum clock rate and return to the lowest once startup has completed
		board_init_systime();
		board_init_uart();
		board_init_i2s();
		board_set_dis_dac();	//Force DMA module and status LED to some safe state
		uart_init(board_get_uart_txfull, board_set_uart_txbuf);
		shell_init();
		srand(board_get_analog_vol());	//Seed the RNG with something at least a little more random than a constant...
		PMIC.CTRL |= PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm;	//Enable execution of all interrupt priority levels
	}
	//Toggle the LEDs to signal a reset
	{
		board_set_led(board_led_0);
		board_clr_led(board_led_1);	
		systime_t dummy = systime_get_delay(MS2US(STARTUP_LED_BLINK_DELAY_ms));
		while(!systime_get_delay_passed(dummy)){
			wdt_reset();
		}
		board_set_led(board_led_1);
		board_clr_led(board_led_0);
		dummy = systime_get_delay(MS2US(STARTUP_LED_BLINK_DELAY_ms));
		while(!systime_get_delay_passed(dummy)){
			wdt_reset();
		}
	
		board_clr_led(board_led_0);
		board_clr_led(board_led_1);
	}
	
	//Do remaining initialization that needs interrupts enabled
	board_set_pwrdn_analog(true);	//Force off analog power, will turn on as needed later on
	
	if(!sd_raw_init()){
		shell_puts_int_P(PSTR(ERROR_STARTUP_MEMCARD_NOT_FOUND));
		for(;;){
			core_idle();	
		}
	}
	
	//Initialize memory card subsystem
	
	shell_puts_int_P(PSTR(STARTUP_TEXT));	
	
	//Open the memory card contents, halt on error
	partition = partition_open(sd_raw_read, sd_raw_read_interval, 0, 0,0);
	if(!partition){	//If failed to open partition, assume device is a superfloppy (no MBR)
		partition =  partition_open(sd_raw_read, sd_raw_read_interval, 0, 0,-1);
		if(!partition){
			shell_puts_int_P(PSTR(ERROR_STARTUP_MEMCARD_NO_PARTITION));
			for(;;){
				core_idle();
			}
		}else{
			shell_puts_int_P(PSTR(MSG_STARTUP_MEMCARD_SFLPY_MBR));
		}
	}else{
		shell_puts_int_P(PSTR(MSG_STARTUP_MEMCARD_STD_MBR));
	}
	
	fs = fat_open(partition);
	
	if(!fs){
		shell_puts_int_P(PSTR(ERROR_STARTUP_MEMCARD_NO_FS));
		for(;;){
			core_idle();
		}
	}
	
	fat_get_dir_entry_of_path(fs, "/", &directory);
	
	dd = fat_open_dir(fs, &directory);
	if(!dd){
		shell_puts_int_P(PSTR(ERROR_STARTUP_MEMCARD_NO_FS));
		for(;;){
			core_idle();
		}
	}
	
	if(!sd_raw_get_info(&disk_info)){
		shell_puts_int_P(PSTR(ERROR_STARTUP_MEMCARD_NO_DISKINFO));
		for(;;){
			core_idle();
		}
	}
	sd_reader_print_info();
	
	struct fat_dir_entry_struct subdir_entry;
	if(find_file_in_dir(fs,dd,CONFIGFILE_NAME,&subdir_entry)){
		struct fat_file_struct* fd = open_file_in_dir(fs, dd, CONFIGFILE_NAME);
		if(!fd){
			shell_puts_int_P(PSTR(MSG_STARTUP_OPENFAIL));
		}else{
			shell_puts_int_P(PSTR(MSG_STARTUP_OPENPREFIX));
			char dummy[CMD_MAXLEN+1];
			bool firstline = true;
			bool sigfail = false;
			while(read_next_line_file(fd,dummy,CMD_MAXLEN)){
				if(firstline){
					firstline = false;
					if(strncmp_P(dummy,PSTR(WAVEPLAY_SIGNATURE),sizeof(WAVEPLAY_SIGNATURE) - 1) != 0){	//Compare to length of string - 1 to only scan the data characters, it's possible that the read line has some '\n' or '\r' character as its last character and not the '\0' character that WAVEPLAY_SIGNATURE has
						sigfail = true;
						shell_puts_ext_P(PSTR(MSG_STARTUP_BADSIG));
						print_templatefile();
						break;
					}
				}else{
					if(dummy[0] != CONFFILE_COMMENT && dummy[0] != '\n' && dummy[0] != '\r'){
						shell_process_extline(dummy);
					}	
				}
			}
			if(!sigfail){
				shell_puts_ext_P(PSTR(MSG_STARTUP_OPENPOSTFIX));	
			}
		}
		fat_close_file(fd);
	}else{
		print_templatefile();
	}
	
	//If the loaded settings didn't start playback, reset the clock speed back to idle
	if(!waveplay_get_active()){
		board_set_clk_flow_ctrl(STANDBY_FCPU);	//Return to idle clock speed
	}
	
	//If the loaded settings set low-power mode to disabled, apply this now
	if(!setting_get_int(PARAM_LOWPWR_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO)){
		board_set_pwrdn_analog(false);
	}
	
    for(;;){
		core_idle();
		shell_idle();
	}	
}

void core_idle(void){
	wdt_reset();
	
	//Run low-priority waveplayer idle loop
	waveplay_idle_lowpri();
	
	//Run debounce occasionally
	{
		static systime_t debounce_delay = 0;
		if(systime_get_delay_passed(debounce_delay)){
			debounce_delay = systime_get_delay(MS2US(BTN_DEOUNCE_PER_ms));
			btn_debounce_idle(&g_btn_debounce);
		}
	}	
	
	//Apply input from control lines
	ctrl_idle();
	
	//Apply new volume if the set level has changed.
	//Note that the set volume is always applied to the DAC on startup so we don't need to worry about missed messages
	{
		static int8_t volume_shdw = 0;
		const int8_t volume_sp = setting_get_int(PARAM_VOLUME_IDX, FLEX_PARAM_VAL, FLEX_FORCE_SUDO);
		if(volume_shdw != volume_sp){
			volume_shdw = volume_sp;
			pcm178x_set_vol(volume_shdw);
		}	
	}
	
	//Re-seed the RNG occasionally. We don't have much in the way of entropy in this system, but the analog input is something at least...
	{
		static systime_t rng_seed_delay = 0;
		if(systime_get_delay_passed(rng_seed_delay) && waveplay_get_active()){
			rng_seed_delay = systime_get_delay(MS2US(10 + board_get_analog_vol()));	//Add some random delay, which is at least some reasonable value
			int foo = rand();
			srand(systime_get() ^ foo);	//Use what entropy we have in the RNG and add the effect of some semi-random value
		}else{
			rng_seed_delay = systime_get_delay(0);
			volatile int foo = rand();	//If we're not active, spam requests from the RNG. As the button inputs are relatively asynchronous this ensure calls will give different values
			(void) foo;
		}
	}
	
	//It's always safe to send XON packets, as XOFF packets are only sent in
	//the same or higher contexts.
	{
		static systime_t xondelay = 0;
		if(systime_get_delay_passed(xondelay)){
			xondelay = systime_get_delay(S2US(UART_XON_ROBUST_PER_s));
			uart_putc(UART_XON);
		}	
	}
	
}

ISR(BOARD_OUT_BUF_vect){
	waveplay_idle();
}

ISR(DMA_A_vect){
	if(DMA_A_CHECK_ERRISR()){	//Occurs when disabling the DMA module at the end of a file
		DMA_A_ACK_ERRISR();
	}else{ //Else the interrupt trigger was transfer complete
		DMA_A_ACK_TFRISR();
		if(DMA_BUF_GET_QUERY_READ()){
			DMA_A_UPDATE_SRCADDR();
			DMA_A_QUEUE_TFR();
		}else{
			board_set_dis_dac();
			g_dac_buffer_underrun_latch = true;
		}
	}
}

ISR(DMA_B_vect){
	if(DMA_B_CHECK_ERRISR()){	//Occurs when disabling the DMA module at the end of a file
		DMA_B_ACK_ERRISR();
	}else{ //Else the interrupt trigger was transfer complete
		DMA_B_ACK_TFRISR();
		if(DMA_BUF_GET_QUERY_READ()){
			DMA_B_UPDATE_SRCADDR();
			DMA_B_QUEUE_TFR();	
		}else{
			board_set_dis_dac();
			g_dac_buffer_underrun_latch = true;
		}
	}
}

ISR(BOARD_SYSTIME_vect){
	systime_update();
}

ISR(BOARD_USART_RX_vect){
	uart_rx_fifo_isr(board_get_uart_rxbuf());
}

ISR(BADISR_vect){
	for(;;){};	//Generate a trap interrupt that causes a watchdog timeout
}