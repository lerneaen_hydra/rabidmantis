/*
 * waveplayer.h
 *
 * Created: 2/13/2014 11:23:05 AM
 *  Author: lock
 */ 


#ifndef WAVEPLAYER_H_
#define WAVEPLAYER_H_

/** @brief Calls the core idle functions that are to be run at all times */
void core_idle(void);

#endif /* WAVEPLAYER_H_ */