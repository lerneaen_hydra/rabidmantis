#include <string.h>
#include <stdbool.h>
#include "flex_settings.h"

static const struct flex_setting_t default_param[FLEX_NUM_PARAM] = SETTING_DEF_STRUCT;
struct flex_setting_t RAM_param[FLEX_NUM_PARAM] = SETTING_DEF_STRUCT;

void setting_reset_defaults(void){
	FLEX_IDX_T i;
	for(i = 0; i < FLEX_NUM_PARAM; i++){
		ATOMIC(RAM_param[i] = default_param[i]);
	}
}

char* setting_get_title(FLEX_IDX_T idx){
	char* retval = NULL;
	if(idx < FLEX_NUM_PARAM){
		retval = &RAM_param[idx].title[0];	//As this is only read after initialization this is thread-safe
	}
	return retval;
}

#ifdef PARAM_UNITS_EN
char* setting_get_unit(FLEX_IDX_T idx){
	char* retval = NULL;
	if(idx < FLEX_NUM_PARAM){
		retval = &RAM_param[idx].unit[0];	//As this is only read after initialization this is thread-safe
	}
	return retval;
}
#endif

//NOTE: THIS IS NOT THREAD-SAFE! (But not really an issue as this is only performed on startup)
FLEX_IDX_T setting_set_title(FLEX_IDX_T idx, char* title){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM){
		strncpy(&RAM_param[idx].title[0], title, FLEX_SETTINGS_TITLE_LEN);
		RAM_param[idx].title[FLEX_SETTINGS_TITLE_LEN] = '\0'; //strncpy may output a non null-terminated string, ensure output is null-terminated
		retval = 1;
	}
	return retval;
}

#ifdef PARAM_UNITS_EN
//NOTE: THIS IS NOT THREAD-SAFE! (But not really an issue as this is only performed on startup)
FLEX_IDX_T setting_set_unit(FLEX_IDX_T idx, char* unit){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM){
		strncpy(&RAM_param[idx].unit[0], unit, FLEX_SETTINGS_UNIT_LEN);
		RAM_param[idx].unit[FLEX_SETTINGS_UNIT_LEN] = '\0'; //strncpy may output a non null-terminated string, ensure output is null-terminated
		retval = 1;
	}
	return retval;
}
#endif

FLEX_IDX_T setting_get_idx(char* title){
	FLEX_IDX_T retval = -1;
	FLEX_IDX_T i;
	for(i = 0; i < FLEX_NUM_PARAM ; i++){
		if(strncmp(title, &RAM_param[i].title[0], FLEX_SETTINGS_TITLE_LEN) == 0){
			retval = i;
		}
	}
	return retval;
}

enum flex_type_t setting_get_type(FLEX_IDX_T idx){
	enum flex_type_t retval = FLEX_TYPE_NUM;
	if(idx < FLEX_NUM_PARAM){
		ATOMIC(retval = RAM_param[idx].flex_type);
	}
	return retval;
}

FLEX_IDX_T setting_set_type(FLEX_IDX_T idx, enum flex_type_t type){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM){
		ATOMIC(RAM_param[idx].flex_type = type);
		retval = 1;
	}
	return retval;
}

enum flex_perm_t setting_get_perm(FLEX_IDX_T idx, enum flex_param_t param){
	enum flex_perm_t retval = FLEX_PERM_NUM;
	if(idx < FLEX_NUM_PARAM){
		if(RAM_param[idx].flex_type == FLEX_TYPE_STRING){
			if(param == FLEX_PARAM_VAL){
				ATOMIC(retval = RAM_param[idx].x.str.val_perm);
			}else{
				retval = FLEX_PERM_NONE;
			}
		}else{
			switch(param){
			case FLEX_PARAM_VAL:
				ATOMIC(retval = RAM_param[idx].x.num.val_perm);
				break;
			case FLEX_PARAM_MIN:
				ATOMIC(retval = RAM_param[idx].x.num.min_perm);
				break;
			case FLEX_PARAM_MAX:
				ATOMIC(retval = RAM_param[idx].x.num.max_perm);
				break;
			default:
				break;
			}
		}
	}
	return retval;
}

FLEX_IDX_T setting_set_perm(FLEX_IDX_T idx, enum flex_param_t param, enum flex_perm_t perm){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM){
		if(RAM_param[idx].flex_type == FLEX_TYPE_STRING){
			ATOMIC(RAM_param[idx].x.str.val_perm = perm);
		}else{
			switch(param){
			case FLEX_PARAM_VAL:
				ATOMIC(RAM_param[idx].x.num.val_perm = perm);
				retval = 1;
				break;
			case FLEX_PARAM_MIN:
				ATOMIC(RAM_param[idx].x.num.min_perm = perm);
				retval = 1;
				break;
			case FLEX_PARAM_MAX:
				ATOMIC(RAM_param[idx].x.num.max_perm = perm);
				retval = 1;
				break;
			default:
				break;
			}
		}
	}
	return retval;
}

#ifdef FLEX_FLOAT_SUPPORT
float setting_get_real(FLEX_IDX_T idx, enum flex_param_t param, enum flex_force_t sudo){
	float retval = 0;
	if(idx < FLEX_NUM_PARAM &&
			RAM_param[idx].flex_type == FLEX_TYPE_REAL &&
			(RAM_param[idx].x.num.val_perm == FLEX_PERM_READ || RAM_param[idx].x.num.val_perm == FLEX_PERM_READWRITE || sudo == FLEX_FORCE_SUDO)){
		switch(param){
			case FLEX_PARAM_VAL:
				ATOMIC(retval = RAM_param[idx].x.num.val.real);
				break;
			case FLEX_PARAM_MIN:
				ATOMIC(retval = RAM_param[idx].x.num.min.real);
				break;
			case FLEX_PARAM_MAX:
				ATOMIC(retval = RAM_param[idx].x.num.max.real);
				break;
			default:
				break;
		}
	}
	return retval;
}
#endif

FLEX_INT_T setting_get_int(FLEX_IDX_T idx, enum flex_param_t param, enum flex_force_t sudo){
	FLEX_INT_T retval = 0;
	if(idx < FLEX_NUM_PARAM &&
			RAM_param[idx].flex_type == FLEX_TYPE_INTEGER &&
			(RAM_param[idx].x.num.val_perm == FLEX_PERM_READ || RAM_param[idx].x.num.val_perm == FLEX_PERM_READWRITE || sudo == FLEX_FORCE_SUDO)){
		switch(param){
			case FLEX_PARAM_VAL:
				ATOMIC(retval = RAM_param[idx].x.num.val.integer);
				break;
			case FLEX_PARAM_MIN:
				ATOMIC(retval = RAM_param[idx].x.num.min.integer);
				break;
			case FLEX_PARAM_MAX:
				ATOMIC(retval = RAM_param[idx].x.num.max.integer);
				break;
			default:
				break;
		}
	}
	return retval;
}

FLEX_IDX_T setting_get_string(FLEX_IDX_T idx, char* dest, enum flex_force_t sudo){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM &&
			RAM_param[idx].flex_type == FLEX_TYPE_STRING &&
			(RAM_param[idx].x.str.val_perm == FLEX_PERM_READ || RAM_param[idx].x.str.val_perm == FLEX_PERM_READWRITE || sudo == FLEX_FORCE_SUDO)){
		ATOMIC(strncpy(dest, &RAM_param[idx].x.str.string[0], FLEX_SETTINGS_STR_LEN));
		dest[FLEX_SETTINGS_STR_LEN] = '\0';	//strncpy may output a non null-terminated string, ensure output is null-terminated
		retval = 1;
	}else{
		*dest = '\0';	//Null-terminate destination if input was invalid as a safety
	}
	return retval;
}

#ifdef FLEX_FLOAT_SUPPORT
FLEX_IDX_T setting_set_real(FLEX_IDX_T idx, enum flex_param_t param, float val, enum flex_force_t sudo){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM &&
			RAM_param[idx].flex_type == FLEX_TYPE_REAL &&
			(RAM_param[idx].x.num.val_perm == FLEX_PERM_WRITE || RAM_param[idx].x.num.val_perm == FLEX_PERM_READWRITE || sudo == FLEX_FORCE_SUDO)){
		switch(param){
		float stored_min;
		float stored_max;
		float stored_val;
		case FLEX_PARAM_VAL:
			ATOMIC(stored_min = RAM_param[idx].x.num.min.real);
			ATOMIC(stored_max = RAM_param[idx].x.num.max.real);
			if(val >= stored_min && val <= stored_max){
				ATOMIC(RAM_param[idx].x.num.val.real = val);
				retval = 1;
			}
			break;
		case FLEX_PARAM_MIN:
			ATOMIC(RAM_param[idx].x.num.min.real = val);
			ATOMIC(stored_val = RAM_param[idx].x.num.val.real);
			if(val > stored_val){
				ATOMIC(RAM_param[idx].x.num.val.real = val);
			}
			retval = 1;
			break;
		case FLEX_PARAM_MAX:
			ATOMIC(RAM_param[idx].x.num.max.real = val);
			ATOMIC(stored_val = RAM_param[idx].x.num.val.real);
			if(val < stored_val){
				ATOMIC(RAM_param[idx].x.num.val.real = val);
			}
			retval = 1;
			break;
		default:
			break;
		}
	}
	return retval;
}
#endif

FLEX_IDX_T setting_set_int(FLEX_IDX_T idx, enum flex_param_t param, FLEX_INT_T new_setting, enum flex_force_t sudo){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM &&
			RAM_param[idx].flex_type == FLEX_TYPE_INTEGER &&
			(RAM_param[idx].x.num.val_perm == FLEX_PERM_WRITE || RAM_param[idx].x.num.val_perm == FLEX_PERM_READWRITE || sudo == FLEX_FORCE_SUDO)){
		switch(param){
		FLEX_INT_T stored_val;
		FLEX_INT_T stored_min;
		FLEX_INT_T stored_max;
		case FLEX_PARAM_VAL:
			ATOMIC(stored_min = RAM_param[idx].x.num.min.integer);
			ATOMIC(stored_max = RAM_param[idx].x.num.max.integer);
			if(new_setting >= stored_min &&  new_setting <= stored_max){
				ATOMIC(RAM_param[idx].x.num.val.integer = new_setting);
				retval = 1;
			}
			break;
		case FLEX_PARAM_MIN:
			ATOMIC(RAM_param[idx].x.num.min.integer = new_setting);
			ATOMIC(stored_val = RAM_param[idx].x.num.val.integer);
			if(new_setting > stored_val){
				ATOMIC(RAM_param[idx].x.num.val.integer = new_setting);
			}
			retval = 1;
			break;
		case FLEX_PARAM_MAX:
			ATOMIC(RAM_param[idx].x.num.max.integer = new_setting);
			ATOMIC(stored_val = RAM_param[idx].x.num.val.integer);
			if(new_setting < stored_val){
				ATOMIC(RAM_param[idx].x.num.val.integer = new_setting);
			}
			retval = 1;
			break;
		default:
			break;
		}
	}
	return retval;
}

FLEX_IDX_T setting_set_str(FLEX_IDX_T idx, char* val, enum flex_force_t sudo){
	FLEX_IDX_T retval = 0;
	if(idx < FLEX_NUM_PARAM &&
			RAM_param[idx].flex_type == FLEX_TYPE_STRING &&
			(RAM_param[idx].x.str.val_perm == FLEX_PERM_WRITE || RAM_param[idx].x.str.val_perm == FLEX_PERM_READWRITE || sudo == FLEX_FORCE_SUDO) ){
		ATOMIC(strncpy(&RAM_param[idx].x.str.string[0],val,FLEX_SETTINGS_STR_LEN));
		ATOMIC(RAM_param[idx].x.str.string[FLEX_SETTINGS_STR_LEN] = '\0');
		retval = 1;
	}
	return retval;
}
