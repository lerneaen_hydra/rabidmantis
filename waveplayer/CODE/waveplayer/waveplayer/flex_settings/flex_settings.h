#ifndef FLEX_SETTINGS_H_
#define FLEX_SETTINGS_H_

/** General-purpose setting/parameter library.
 * Allows for setting and reading parameter settings, minimum/maximum values
 * with per-parameter permissions.
 *
 * *MOST* functions are fully thread-safe and re-entrant by disabling global
 * interrupts before entering critical sections and restoring the interrupt
 * state when completed. Functions that are not thread-safe are heavily noted.
 *
 * Note; when changing a parameter's value a comparison is made to the set
 * maximum and minimum values; these comparisons are *not* made with interrupts
 * disabled, creating the potential for an old (but correctly read) limit to be
 * used for the comparison. To ensure that the correct comparison is made (and
 * thereby ensuring that min <= val <= max, for the updated values of min and
 * max) ensure that min/max is updated completely before or completely after
 * changing val.
 */

#include <stdint.h>
#include "../config.h"

/** @brief Define when compiling for an Atmel AVR platform to use functions in AVRLIBC */
#define FLEX_AVR_MODE

/** @brief Define to add support for floating point types */
//#define FLEX_FLOAT_SUPPORT

/** @brief If there is support for floating point types there's no need to reduce the integer type below 32 bits.
 * However, if there is not support for floating point types it may be useful to reduce the size of the integer types */
#ifdef FLEX_FLOAT_SUPPORT
#define FLEX_INT_T	int32_t
#else
#define FLEX_INT_T	int16_t
#endif

#ifdef FLEX_AVR_MODE
#include <util/atomic.h>
#define ATOMIC(_code)	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){ _code; }
#else
#error Add custom definition for code that generates an atomic block (interrupts temporarily disabled)	
#endif

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/** @brief Maximum length for setting names (excluding null-termination) */
#define FLEX_SETTINGS_TITLE_LEN	13

#ifdef PARAM_UNITS_EN
/** @brief Maximum length for unit names (excluding null-termination) */
#define FLEX_SETTINGS_UNIT_LEN	8
#endif

/** @brief Maximum length for string type settings (excluding null-termination) */
#define FLEX_SETTINGS_STR_LEN		6

/** @brief Number of parameters */
#define FLEX_NUM_PARAM			17

/** @brief Internal type, indexing parameter list. FLEX_NUM_PARAM (*NOT* FLEX_NUM_PARAM-1) must fit in this, and must be signed */
#define FLEX_IDX_T				int8_t

/** @brief Index (array element in master settings array) for each setting.
 * -----------------------------BIG WARNING!-----------------------------------
 * THE INDEX FOR A PARAMETER **MUST** MATCH THE ORDER THAT THE SETTINGS ARE
 * DEFINED IN SETTING_DEF_STRUCT!
 * -----------------------------BIG WARNING!---------------------------------- */
#define PARAM_VERBOSE_IDX	0
#define PARAM_LOWPWR_IDX	1
#define PARAM_VOLUME_IDX	2
#define PARAM_CTRLVOL_IDX	3
#define PARAM_SHUFFLE_IDX	4
#define PARAM_AUTOPLAY_IDX	5
#define PARAM_PAUSE_IDX		6
#define PARAM_RESUME_IDX	7
#define PARAM_TGL_PAUSE_IDX	8
#define PARAM_STOP_IDX		9
#define PARAM_PLAYNEXT_IDX	10
#define PARAM_01_WAV_IDX	11
#define PARAM_02_WAV_IDX	12
#define PARAM_03_WAV_IDX	13
#define PARAM_04_WAV_IDX	14
#define PARAM_05_WAV_IDX	15
#define PARAM_06_WAV_IDX	16

/** @brief Generates an initializer for the internal settings struct array.
 * -----------------------------BIG WARNING!-----------------------------------
 * THE INDEX FOR A PARAMETER **MUST** MATCH THE ORDER THAT THE SETTINGS ARE
 * DEFINED IN SETTING_DEF_STRUCT!
 * -----------------------------BIG WARNING!---------------------------------- */


#define SETTING_DEF_STRUCT							\
{													\
	SETTING_DEF_STRUCT_PARAM_NUM(VERBOSE),			\
	SETTING_DEF_STRUCT_PARAM_NUM(LOWPWR),			\
	SETTING_DEF_STRUCT_PARAM_NUM(VOLUME),			\
	SETTING_DEF_STRUCT_PARAM_NUM(CTRLVOL),			\
	SETTING_DEF_STRUCT_PARAM_NUM(SHUFFLE),			\
	SETTING_DEF_STRUCT_PARAM_NUM(AUTOPLAY),			\
	SETTING_DEF_STRUCT_PARAM_NUM(PAUSE),			\
	SETTING_DEF_STRUCT_PARAM_NUM(RESUME),			\
	SETTING_DEF_STRUCT_PARAM_NUM(TGL_PAUSE),		\
	SETTING_DEF_STRUCT_PARAM_NUM(STOP),				\
	SETTING_DEF_STRUCT_PARAM_NUM(PLAYNEXT),			\
	SETTING_DEF_STRUCT_PARAM_NUM(01_WAV),			\
	SETTING_DEF_STRUCT_PARAM_NUM(02_WAV),			\
	SETTING_DEF_STRUCT_PARAM_NUM(03_WAV),			\
	SETTING_DEF_STRUCT_PARAM_NUM(04_WAV),			\
	SETTING_DEF_STRUCT_PARAM_NUM(05_WAV),			\
	SETTING_DEF_STRUCT_PARAM_NUM(06_WAV)			\
}

#ifdef PARAM_UNITS_EN
#define SETTING_DEF_STRUCT_PARAM_NUM(name)						\
{																\
	.title = PARAM_##name##_TITLE,								\
	.unit = PARAM_##name##_UNIT,								\
	.flex_type = PARAM_##name##_FLEXTYPE,						\
	.x.num = {	.val.PARAM_##name##_TYPE = PARAM_##name##_VAL,	\
				.val_perm = PARAM_##name##_VAL_PERM,			\
				.min.PARAM_##name##_TYPE = PARAM_##name##_MIN,	\
				.min_perm = PARAM_##name##_MIN_PERM,			\
				.max.PARAM_##name##_TYPE = PARAM_##name##_MAX,	\
				.max_perm = PARAM_##name##_MAX_PERM				\
			}													\
}
#else
#define SETTING_DEF_STRUCT_PARAM_NUM(name)						\
{																\
	.title = PARAM_##name##_TITLE,								\
	.flex_type = PARAM_##name##_FLEXTYPE,						\
	.x.num = {	.val.PARAM_##name##_TYPE = PARAM_##name##_VAL,	\
				.val_perm = PARAM_##name##_VAL_PERM,			\
				.min.PARAM_##name##_TYPE = PARAM_##name##_MIN,	\
				.min_perm = PARAM_##name##_MIN_PERM,			\
				.max.PARAM_##name##_TYPE = PARAM_##name##_MAX,	\
				.max_perm = PARAM_##name##_MAX_PERM				\
			}													\
}
#endif

#ifdef PARAM_UNITS_EN
#define SETTING_DEF_STRUCT_PARAM_STR(name)						\
{																\
	.title = PARAM_##name##_TITLE,								\
	.unit = PARAM_##name##_UNIT,								\
	.flex_type = PARAM_##name##_FLEXTYPE,						\
	.x.str = {	.val_perm = PARAM_##name##_VAL_PERM,			\
				.string = PARAM_##name##_VAL					\
			}													\
}
#else
#define SETTING_DEF_STRUCT_PARAM_STR(name)						\
{																\
	.title = PARAM_##name##_TITLE,								\
	.flex_type = PARAM_##name##_FLEXTYPE,						\
	.x.str = {	.val_perm = PARAM_##name##_VAL_PERM,			\
				.string = PARAM_##name##_VAL					\
			}													\
}
#endif

/** @brief Identifier of type of parameter stored */
enum flex_param_t {FLEX_PARAM_VAL = 0, FLEX_PARAM_MIN, FLEX_PARAM_MAX};

/** @brief Identifier of permission of parameter stored */
enum flex_perm_t {FLEX_PERM_NONE = 0, FLEX_PERM_READ, FLEX_PERM_WRITE, FLEX_PERM_READWRITE, FLEX_PERM_NUM};

/** @brief Identifier of type of parameter stored */
#ifdef FLEX_FLOAT_SUPPORT
enum flex_type_t {FLEX_TYPE_INTEGER = 0, FLEX_TYPE_REAL, FLEX_TYPE_STRING, FLEX_TYPE_NUM};
#else
enum flex_type_t {FLEX_TYPE_INTEGER = 0, FLEX_TYPE_STRING, FLEX_TYPE_NUM};
#endif

/** @brief Identifier of force of set/get commands ("sudo"-like behavior overrides permission status) */
enum flex_force_t {FLEX_FORCE_NORMAL = 0, FLEX_FORCE_SUDO};

/** @brief Internal-use struct for numerical storage types */
struct flex_setting_t_numerical{
	union {
		FLEX_INT_T integer;
#ifdef FLEX_FLOAT_SUPPORT
		float	real;
#endif
	} val;
	enum flex_perm_t val_perm;

	union {
		FLEX_INT_T integer;
#ifdef FLEX_FLOAT_SUPPORT
		float	real;
#endif
	} min;
	enum flex_perm_t min_perm;

	union {
		FLEX_INT_T integer;
#ifdef FLEX_FLOAT_SUPPORT
		float	real;
#endif
	} max;
	enum flex_perm_t max_perm;
};

/** @brief Internal-use struct for string storage types */
struct flex_setting_t_string{
	enum flex_perm_t val_perm;
	char string[FLEX_SETTINGS_STR_LEN+1];
};

/** @brief Individual parameter structure. Note that as string types have no
 * meaningful max/min and associated permission parameters space can be saved
 * by using these bytes for the string instead; this is the cause for the
 * flex_setting_t_string and flex_setting_t_numerical types. */
struct flex_setting_t{
	char title[FLEX_SETTINGS_TITLE_LEN+1];	//Title of attribute. MUST BE UNIQUE!
#ifdef PARAM_UNITS_EN
	char unit[FLEX_SETTINGS_UNIT_LEN+1];		//Units of attribute (eg. A, ohm, kg*m/s^2, etc)
#endif
	enum flex_type_t flex_type;				//Type of attribute
	union{
		struct flex_setting_t_string	str;
		struct flex_setting_t_numerical	num;
	} x;
};

/** @brief Gets the number of settings stored in total. */
#define SETTING_NUM()	FLEX_NUM_PARAM

/** @brief Gets the maximum length of titles. */
#define SETTING_TITLE_LEN() FLEX_SETTINGS_TITLE_LEN

/** @brief Gets the maximum length of titles, expressed as a string. */
#define SETTING_TITLE_LEN_STR()	STR(FLEX_SETTINGS_TITLE_LEN)

/** @brief Gets the maximum length of string types. */
#define SETTING_PARAM_LEN() FLEX_SETTINGS_STR_LEN

/** @brief Gets the maximum length of string types, expressed as a string. */
#define SETTING_PARAM_LEN_STR()	STR(FLEX_SETTINGS_STR_LEN)

/** @brief Seeds all parameter settings (including titles and types) with the default values.
 * Intended to be called on startup.
 * WARNING! THIS FUNCTION IS NOT THREAD SAFE! ALL CALLS SHOULD BE MADE WITH INTERRUPTS DISABLED! */
void setting_seed_defaults_startup(void);

/** @brief Seeds the typically changed parameter settings (value/min/max and assocated permissions).
 * This is fully thread safe and can safely be called during runtime. */
void setting_seed_defaults_runtime(void);

/** @brief Gets the title of a parameter with index idx.
 * @param	idx The index of the title to get
 * @return	Pointer to '\0' if invalid idx, otherwise pointer to title, at most FLEX_SETTINGS_TITLE_LEN + 1 elements in length */
char* setting_get_title(FLEX_IDX_T idx);

#ifdef PARAM_UNITS_EN
/** @brief Gets the unit of a parameter with index idx.
 * @param	idx The index of the title to get
 * @return	Pointer to '\0' if invalid idx, otherwise pointer to unit, at most FLEX_SETTINGS_UNIT_LEN + 1 elements in length */
char* setting_get_unit(FLEX_IDX_T idx);
#endif

/** @brief Sets the title of a parameter with index idx. Returns 0 if invalid idx, 1 else.
 * Note; Primarily intended for use on startup when seeding settings from nonvolatile memory.
 * WARNING! THIS FUNCTION IS NOT THREAD SAFE! ALL CALLS SHOULD BE MADE WITH INTERRUPTS DISABLED!
 * @param idx	The index of the title to set
 * @param title	Pointer to character array of at least NV_SETTINGS_TITLE_LEN + 1 elements in length to set title to */
FLEX_IDX_T setting_set_title(FLEX_IDX_T idx, char* title);

#ifdef PARAM_UNITS_EN
/** @brief Sets the unit of a parameter with index idx. Returns 0 if invalid idx, 1 else.
 * Note; Primarily intended for use on startup when seeding settings from nonvolatile memory.
 * WARNING! THIS FUNCTION IS NOT THREAD SAFE! ALL CALLS SHOULD BE MADE WITH INTERRUPTS DISABLED!
 * @param idx	The index of the unit to set
 * @param title	Pointer to character array of at least FLEX_SETTINGS_UNIT_LEN + 1 elements in length to set unit to */
FLEX_IDX_T setting_set_unit(FLEX_IDX_T idx, char* title);
#endif

/** @brief Gets the index of a title, if it exists. Returns non-negative index if found, negative if not */
FLEX_IDX_T setting_get_idx(char* title);

/**@brief Gets the type of data stored at an index
 * @param idx	The index of the title to set
 * @return The type of data stored at idx, FLEX_TYPE_NUM if idx is invalid */
enum flex_type_t setting_get_type(FLEX_IDX_T idx);

/** @brief Sets the type of data stored in an index.
 * Note; Primarily intended for use on startup when seeding setting from nonvolatile memory
 * @param idx The index of the permission parameter to set
 * @param enum flex_param_t The type of permission parameter to set to
 * @return Nonzero if setting the permission succeeded */
FLEX_IDX_T setting_set_type(FLEX_IDX_T idx, enum flex_type_t type);

/**@brief Gets the permission of data stored in an index
 * @param	idx The index of the permission parameter to read
 * @param	enum flex_param_t	The type of permission parameter to read
 * @return 	The permission of data stored at idx, FLEX_PERM_NUM if idx is invalid or param invalid */
enum flex_perm_t setting_get_perm(FLEX_IDX_T idx, enum flex_param_t param);

/**@brief Sets the permission of data stored in an index
 * @param idx The index of the permission parameter to set
 * @param param The type of permission to set.
 * @return Nonzero if succeeded, zero otherwise.*/
FLEX_IDX_T setting_set_perm(FLEX_IDX_T idx, enum flex_param_t param, enum flex_perm_t perm);

#ifdef FLEX_FLOAT_SUPPORT
/** @brief Gets value/min/max of a real (float) at index idx.
 * @param idx The index to read from.
 * @param param The value to read.
 * @param sudo Set to FLEX_FORCE_SUDO to ignore permission attribute
 * @return The value stored, zero if invalid index, param type, if content was
 * not real type, or permission was not READ or READWRITE
 */
float setting_get_real(FLEX_IDX_T idx, enum flex_param_t param, enum flex_force_t sudo);
#endif

/** @brief Gets value/min/max of an integer at index idx.
 * @param idx The index to read from.
 * @param param The value to read.
 * @param sudo Set to FLEX_FORCE_SUDO to ignore permission attribute
 * @return The value stored, 0 if invalid index, param type, or if content was
 * not integer type, or permission was not READ or READWRITE
 */
FLEX_INT_T setting_get_int(FLEX_IDX_T idx, enum flex_param_t param, enum flex_force_t sudo);

/** @brief Gets value of a string at index idx.
 * Note; This call will block interrupts for longer than other data types! Length
 * based on FLEX_SETTINGS_PARAM_LEN.
 * Note; String min parameter not applicable, max parameter always FLEX_SETTINGS_PARAM_LEN
 * @param idx The index to read from.
 * @param dest	Character array of at least FLEX_SETTINGS_PARAM_LEN elements to copy data to
 * @param sudo Set to FLEX_FORCE_SUDO to ignore permission attribute
 * @return Nonzero if value successfully written, 0 if invalid index, or if
 * content was not string type, or permission was not READ or READWRITE
 */
FLEX_IDX_T setting_get_string(FLEX_IDX_T idx, char* dest, enum flex_force_t sudo);

#ifdef FLEX_FLOAT_SUPPORT
/** @brief Sets value/min/max of a real (float) at index idx.
 * @param idx The index to set.
 * @param param The type of value to set.
 * @param val The new value to set.
 * @param sudo Set to FLEX_FORCE_SUDO to ignore permission attribute
 * @return Nonzero if successful, zero otherwise.
 */
FLEX_IDX_T setting_set_real(FLEX_IDX_T idx, enum flex_param_t param, float val, enum flex_force_t sudo);
#endif

/** @brief Sets value/min/max of an integer at index idx.
 * @param idx The index to set.
 * @param param The type of value to set. If setting the value and the value is
 * 		not min <= val <= max the value is not changed. If the permission is
 * 		not WRITE or READWRITE the value is	not changed.
 * @param val The new value to set.
 * @param sudo Set to FLEX_FORCE_SUDO to ignore permission attribute
 * @return Nonzero if successful, zero otherwise.
 */
FLEX_IDX_T setting_set_int(FLEX_IDX_T idx, enum flex_param_t param, FLEX_INT_T val, enum flex_force_t sudo);

/** @brief Sets the value of a string at index idx.
 * @param idx The index to set.
 * @param val The new value to set. If the permission is not WRITE or READWRITE
 * the value is not changed.
 * @param sudo Set to FLEX_FORCE_SUDO to ignore permission attribute
 * @return Nonzero if successful, zero otherwise.
 */
FLEX_IDX_T setting_set_str(FLEX_IDX_T idx, char* val, enum flex_force_t sudo);

#endif /* FLEX_SETTINGS_H_ */
