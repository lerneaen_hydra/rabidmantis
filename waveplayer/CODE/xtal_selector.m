% Super-crude script for determining the best crystal (giving the lowest clock error) for common crystals and available clock multiplier/divisors.

clc
clear all
close all
format short eng

ub = [40;31;31;14;14];
lb = [1;1;1;1;1];

options = gaoptimset('PopulationSize',100, 'TolFun', 1e-9);

[x,fval,exitflag] = ga(@clock_error,5,[],[],[],[],lb,ub,[],[1,2,3,4,5],options);

fs = [44.1e3, 48e3];

ps = 512;

m = fs * ps;

k1 = (1:31);
k2 = [2,4,6,8,10,12,16,24,32,48,64,128,256,512];
fxtal = [4,4.096,4.194304,4.433619, 4.5, 4.608, 4.9152, 5, 5.0688, 6, 6.144, 6.5536, 6.7458,7.3728, 7.68, 8, 8.192, 8.912, 9.216 , 9.8304,9.84375,10. 10.24, 11, 11.059, 11.289, 12, 12.288, 12.5, 13, 13.2256, 13.5, 13.56, 14, 14.3181, 14.4756, 14.7456, 15, 15.36, 16];

disp('44.1kHz clock target')
m(1)
disp('44.1kHz clock achieved')
fxtal(x(1))*k1(x(2))/k2(x(4))

disp('48kHz clock target')
m(2)
disp('48kHz clock acheived')
fxtal(x(1))*k1(x(3))/k2(x(5))

disp('clock [Mhz], mult, div @ 44.1kHz, mult, div @ 48kHz')
[fxtal(x(1)); k1(x(2)); k2(x(4)); k1(x(3)); k2(x(5))]'