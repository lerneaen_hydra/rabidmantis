clc
clear all
close all
format short eng

% Simple calculations for the A8499 buck converter

Vin = 50;   % input voltage [V]
Vout = 5; % output voltage [V]
Vf = 0.6;   % shottky diode forward voltage [V]
Iout = .6; % average output current [A]
Rl = 0.5;   % inductor series resistance [Ohm]
L = 22e-6;   % inductor inductance [H]
Rtset_factor = 2;   %safety factor for Rtset resistor [-]
Cesr = 100e-3;

% derived parameters

io_ratio = Vin / Vout;
Rdson = 1;    % integrated mosfet on-resistance
Rtset_min = 0.185 * io_ratio; % minimum value of rtset resistor [kOhm]
if(Rtset_min < 13)
    Rtset_min = 13;
end
Rtset = Rtset_min * Rtset_factor;
toff = Rtset / 1.2e7;  % converter off-time [s]
Vloff = Vout + Vf + Iout*Rl;
Iripple = Vloff * toff / L;
Vlon = Vin - Iout * Rdson - Iout * Rl - Vout;
ton = Iripple * L / Vlon;
fsw = 1 / (ton + toff);
Iout_max = 2.2 * toff / L;
Vripple = Iripple * Cesr;

disp('Specified values give:')
disp(['Use RTSET resistor of ',num2str(Rtset),' kOhm'])
disp(['Ontime = ',num2str(ton*1e6),' us'])
disp(['Offtime = ',num2str(toff*1e6),' us'])
disp(['F_sw = ',num2str(fsw/1e3),'kHz'])
disp(['Ripple current = ',num2str(Iripple),' A'])
disp(['Output ripple voltage = ',num2str(Vripple),' V'])