clc
clear all
format short eng

T_s = 100e-3;

tau = 1;    %motor time constant in seconds, a NV-parameter
i_crit = 1; %maxmim current in motor for one time constant, a NV-parameter


% Generate a sample motor output sequence
i_out = [i_crit*ones(1,5*round(tau/T_s)),zeros(1,2*round(tau/T_s)),1.5*i_crit*ones(1,0.5*round(tau/T_s)),i_crit/2*ones(1,5*round(tau/T_s))];
tt = linspace(0,length(i_out)*T_s/tau,length(i_out));
sn = zeros(size(i_out));

i2_crit = i_crit^2 * ones(1,length(i_out));

s0 = 0;
alpha = T_s/tau;
sn(1) = alpha * i_out(1).^2 + (1 - alpha)*s0;

for i = 2 : length(i_out)
    sn(i) = alpha * i_out(i).^2 + (1 - alpha) * sn(i-1);
end

figure(1);
stairs(tt,[i_out;sn;i2_crit]');
legend('Output current','Motor temperature','Critical temperature')
title('Internally calculated I^2T losses')
xlabel('Time, n time constants')
ylabel('Output current and temperature')