/* Basic terminal-like library. Echos input to the serial interface, sanitizes
 * line endings, and when a line is finished marks it as available and gives the
 * ability to output it.
 * Note that all the functions here need to be called from the same context!
 * Typically this is not an issue, is this will be done purely in the main loop
 * 
 * Note; Up/down arrow behavior depends on functions in the shell.h file,
 * clear these out if using terminal.h/.c without the shell.
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

/************************************************************************

************************************************************************/

#include <inttypes.h>
#include <stdio.h>

/** The total buffer size for the terminal. Choose 2^n values for speed. Must be > TERMINAL_MAXROWLENGTH */
#define TERMINAL_BUFSIZE	128

/** The maximum row length. Non-return characters beyond this length will be dropped. Must be <= 254 */
#define TERMINAL_MAXROWLENGTH	79

/** ANSI escape code character for cursor up (up-arrow) */
#define ESC_UPARROW 'A'

/** ANSI escape code character for cursor down (down-arrow) */
#define ESC_DOWNARROW	'B'

/** @brief Housekeeping function for terminal data parsing.
	Must run at least every (8*(INPUT_BUFFER_LENGTH-FLOW_CONTROL_MARGIN) / BAUDRATE
	seconds in order to not send XON/XOFF's. This function will check the input
	buffer, clean out non-supported characters, manage CR,LF,CRLF line endings,
	and output the sanitized output data to internal storage which can be read
	with the other supporting functions.
*/
void terminal_idle(void);

/** @brief Returns nonzero if there is one or more lines available. */
uint8_t terminal_line_available(void);

/** @brief Returns the first/next character in the line. */
char	terminal_getc(void);

/** @brief Gets the *most recent* character in the current unterminated line.
 *	This will remove the character from the current line, meaning it will
 *	not appear in terminal_getc() when the line is terminated.
 *	If there is no character, or the character is '\n' or '\r', '\0' is returned.
 *	@return The most recent character, or '\0' if a newline or no character is available
 */
char terminal_getc_incomplete(void);

/** @brief Puts the character on the terminal, blocking until it is done. */
void	terminal_putc(char data);

/** @brief File reference for IO
 */
extern FILE terminal_str;

/** @brief Initializes the stream setup  */
inline void terminal_stream_init(void){
	stdout = stdin = &terminal_str;
}

#endif /* TERMINAL_H_ */