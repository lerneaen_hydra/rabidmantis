/* Very rudimentary uart library. Offer R/W support and software flow
 * control for input data. Input is stored in a circular buffer and flushed out
 * through uart_getc(). Output is written to DATA register and blocks if busy.
 */

#ifndef UART_H_
#define UART_H_

#include <inttypes.h>

/**
 * @brief Input buffer size (characters). Choose 2^n sized values for speed.
	The maximum value is controlled by dynbuf's INDEXTYPE2.
 */
#define UART_RX_BUFSIZE		128

/**
 * @brief Define to enable software flow control for incoming data
 */
#define UART_FC

/**
 * @brief If the number of free bytes in the receive buffer is below this an XOFF command is sent.
 * Note that when more than this number of bytes are free an XON command will be sent. (Giving a 2-byte hysteresis)
 */
#ifdef UART_FC
	#define UART_FC_OVERHEAD 64
#endif

/**
 * @brief Define the XON and XOFF characters used (typically not changed)
 */
#if defined(UART_FC)
	#define UART_FC_XON		0x11
	#define UART_FC_XOFF	0x13
#endif

/**
 * @brief Queues one byte of data for transmission
 * This is done immediately if there is space left in the TX buffer, if
 * there is no space left this function blocks until there is space.
 * @param data the character to queue for transfer.
 */
void uart_putc(char data);

/**
 * @brief Get one byte of data and remove it from the input buffer if a line is complete
 * @return The oldest data byte received
 */
char uart_getc(void);

/**
 * @brief Checks if there is data available to read from the input
 * @return Nonzero if there is data available.
 */
uint8_t uart_available(void);

#endif /* UART_H_ */
