/* Rudimentary shell-like interface for embedded systems.
 * Implements a shell-like interface for running various tasks with
 * flexible arguments, such as "blink -s 0.1 -b asd -def" 
 * Writes to STDOUT, reads from the terminal.h library, and offers a
 * flexible set up.
 * All commands except for 'help' must be defined in command.h/.c
 */

#ifndef SHELL_H_
#define SHELL_H_

#include <stdint.h>
#include "terminal.h"

/** @brief Define to enable debug mode, will output some verbose help text on actions */
//#define SHELL_DBG

/** @brief Set to the number of old input lines to store and be able to switch to */
// Must be <= INT8_MAX
#define SHELL_NUMLINES	10

/** @brief Valid exit codes for commands called by the shell. */
typedef enum shell_ret_t {cmd_exit_success=0, cmd_exit_missing_arg, cmd_exit_invalid_arg} shell_ret_t;
	
/** @brief Idle function for shell functionality, run as often as possible. */
void shell_idle(void);

/** @brief Sets up streams for input and output */
inline void shell_stream_init(void){
	terminal_stream_init();
}

/** 
 *	@brief Call before writing to STDOUT outside of the functions in command.c
 *	Clears out any existing characters from the display and the input buffer.
 */
void shell_extmsg_prefix(void);

/** 
 *	@brief Call after writing to STDOUT outside of the functions in command.c
 *	Writes the shell-ready symbol.
 */
void shell_extmsg_cleanup(void);

/** @brief Gets the idx'th most recent input line, from 0 up to SHELL_NUMLINES - 1 */
char* shell_history(uint8_t idx);

/************************************************************************/
/* Utility functions for commands using the shell						*/
/************************************************************************/

/**
 * @brief Check the most recent input line for the existence of arg.
 * @return Pointer to position of argument in line (points first character of
			whitespace before argument, ie the zeroth element of " -foo 1.0"),
			NULL if no match found.
 */
const char* shell_check_arg(char *arg);

/**
 * @brief Extract the argument arg, excluding whitespace, from the most recent
 *			line, storing in dest, with up to len characters, including
 *			null terminator. If length(arg) >= len truncation will occur. The
 *			first len characters will be extracted. Always null-terminates.
 * If arg is not found a string with "\0" is written.
 */
void shell_extract_arg(char *arg, char *dest, uint8_t len);

/**
 * @brief Extracts the length of the argument parameter arg, excluding
 * whitespace, from	the most recent line. Note; if using to allocate space
 * for storing the argument, allocate shell_extract_arg_len(...)+1 bytes to
 * have space for null termination.
 * @return The length of the argument if found, zero if not found.
 */
uint8_t shell_extract_arg_len(char *arg);

#endif /* SHELL_H_ */