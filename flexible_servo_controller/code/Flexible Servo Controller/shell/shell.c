
/************************************************************************

Implement functions to check for the existence of an argument (checks the internal
string for the argument, returns nonzero if exists) and to extract the argument
(checks the internal string, writes the data to a string pointer)

 ************************************************************************/

#include "shell.h"
#include <string.h>
#include <avr/pgmspace.h>
#include "command.h"

/** @brief Error message on invalid command, preceded by input command. */
#define SHELL_INVALID_CMD	"; command not found. Enter 'help' for a list of valid commands."

/** @brief Error message on invalid argument to command */
#define SHELL_INVALID_ARG	"\nInvalid argument(s)! See help -c <command> for usage."

/** @brief Error message on missing argument to command */
#define SHELL_MISSING_ARG	"\nMissing argument(s)! See help -c <command> for usage."

/** @brief Help list indentation size */
#define SHELL_INDENT		"    "

/** @brief Argument identifier prefix; tokens with this prefix are viewed as potential arguments.
 * Note that the argument value may also have this character, ie. if set to
 * "-", then "-foo -1.2" is legal. Keep in mind there will be ambiguity if
 * argument names are also numerical in this case!
 */
#define SHELL_ARG_PREFIX "-"

/** @brief Whitespace (delimiting) character for input.
 * This character separates different argument names and argument values, eg if
 * set to ' ' (space) then "foo -a sdf -b 1.0" would result in the command
 * "foo", argument "a" set to "sdf", and argument "b" set to "1.0".
 */
#define SHELL_WHITESPACE " "

/** @brief Lax whitespace (delimiting) characters. This should include
 * SHELL_WHITESPACE and newline/similar characters. Use covers situations
 * such as "foo -a -b", where "-b" does not have an ordinary whitespace suffix.
 */
#define SHELL_WHITESPACE_LAX SHELL_WHITESPACE"\r\n"

/** @brief Start-of-line character for terminal input */
#define SHELL_RDY	">"

/************************************************************************
 *	Help command.
 * The help command, executed without arguments, prints a list of the
 * valid commands. The help command, with the argument "-<flag> <arg>", displays
 * the help line for the command labeled <arg>. */  

/** @brief Help command name */
#define SHELL_HELP "help"

/** @brief Help argument label */
#define SHELL_HELP_ARG "c"

/** @brief String to print before listing valid commands */
#define SHELL_HELP_PREFIX "The following commands are available;\n"

/** @brief String to print after listing valid commands */
#define SHELL_HELP_SUFFIX "Enter help -c <command> for help on a command.\nEnter <command> to execute a command.\nNote: only the first copy of an argument is matched, IE. 'help -c foo -c bar'\nis equivalent to 'help -c foo'."

/** @brief Help command function */
shell_ret_t shell_help_cmd(void);
/************************************************************************/

/** @brief Gets a command name location, result is pointer to PROGMEM with content */
#define SHELL_PROGMEM_NAME(x)	((char*)pgm_read_word(&shell_cmd_array[(x)].cmd_name))

/** @brief Gets a command's help location, result is pointer to PROGMEM with content */
#define SHELL_PROGMEM_HELP(x)	((char*)pgm_read_word(&shell_cmd_array[(x)].cmd_help))

/** @brief Dereferences a func. ptr location, result is pointer function pointer */
#define SHELL_PROGMEM_FP(x) ((shell_cmd_fp_t)pgm_read_word(&shell_cmd_array[(x)].fp))

static char shell_inputlines[SHELL_NUMLINES][TERMINAL_MAXROWLENGTH+1] = {{0}};	//History of input, shell_inputlines[0] stores the most recent history

void shell_idle(void){
	static uint8_t newline = 1;
	terminal_idle();
	if(newline){
		printf_P(PSTR("\n"SHELL_RDY));
		newline = 0;
	}
	if(terminal_line_available()){		//If there's a new line available to get
		if(strncmp(shell_inputlines[0],shell_inputlines[1],sizeof(shell_inputlines[0])/sizeof(char)) != 0 &&
			shell_inputlines[0][0] != '\n' &&
			shell_inputlines[0][0] != '\r'){	//If the input lines are different and not just newline
			for(int8_t i = SHELL_NUMLINES - 1; i >= 0; i--){			//Shift down the history
				strncpy(shell_inputlines[i],shell_inputlines[i-1],sizeof(shell_inputlines[0])/sizeof(char));
			}
		}		
		for(uint8_t i = 0; i < sizeof(shell_inputlines[0])/sizeof(char); i++){
			shell_inputlines[0][i] = '\0';
		}
		for(uint8_t i = 0; terminal_line_available() && ( i < sizeof(shell_inputlines[0])/sizeof(char) - 1 ); i++){
			shell_inputlines[0][i] = terminal_getc();	//Feed in the new copy. Guaranteed to be null terminated from the previous clear operation.
		}
// 		printf("history;\n");
// 		for(uint8_t i = 0; i < SHELL_NUMLINES; i++){
// 			printf("idx %d, string %s",i,shell_inputlines[i]);
// 		}
		/* Try to match the specified command to the stored array of commands */
		uint8_t cmd_len = strcspn(shell_inputlines[0],SHELL_WHITESPACE_LAX);	//The length of the input command
		if(cmd_len == 0){		//If the input was just a newline, do nothing.
			printf_P(PSTR(SHELL_RDY));
			return;
		}
		char cmd[cmd_len+1];	
		strncpy(cmd,shell_inputlines[0],cmd_len);
		cmd[cmd_len] = '\0';	//Now cmd contains the specified command with null termination
		shell_cmd_fp_t fp = NULL;
		uint8_t valid_command = 0;	//Stores whether the input was a valid command or not
		if(strncmp_P(cmd,PSTR(SHELL_HELP),SHELL_CMD_MAX_LEN) == 0){	//Check if the command was the internal help command
			fp = &shell_help_cmd;
			valid_command = 1;
		}else{											//If not, check if it was an external command
			for(uint8_t i = 0; i < sizeof(shell_cmd_array)/sizeof(shell_cmd_t); i++){
				if(strncmp_P(cmd, SHELL_PROGMEM_NAME(i),SHELL_CMD_MAX_LEN) == 0){
					#ifdef SHELL_DBG
					printf_P(PSTR("Found match! idx:%u\n"),i);
					printf_P(PSTR("Help string:"));
					printf_P(SHELL_PROGMEM_HELP(i));
					printf_P(PSTR("\n"));
					#endif
					fp = SHELL_PROGMEM_FP(i);
					valid_command = 1;
					break;
				}
			}
		}
		if(fp != NULL){									//If a matching command was found
			#ifdef SHELL_DBG
			printf_P(PSTR("Executing command %s, using function pointer address %u\n"),cmd,(int)fp);
			#endif
			shell_ret_t retval = (*fp)();
			#ifdef SHELL_DBG
			printf_P(PSTR("\nCommand execution finished"));
			#endif
			if(retval == cmd_exit_invalid_arg){
				printf_P(PSTR(SHELL_INVALID_ARG));
			}else if(retval == cmd_exit_missing_arg){
				printf_P(PSTR(SHELL_MISSING_ARG));
			}
			//Else command executed happily and made bunnies and sunshine!
			
		}
		if(!valid_command){	//Requested command not found
			printf("%s",cmd);
			printf_P(PSTR(SHELL_INVALID_CMD));
		}
		newline = 1;
	}
}

void shell_extmsg_prefix(void){
	for(uint8_t i = 0; i < sizeof(SHELL_RDY)/sizeof(char) - 1; i++){
		terminal_putc('\b');	//	
	}
	while(terminal_getc_incomplete() != '\0'){	//Remove any incomplete characters from the input buffer and clear them from display
		terminal_putc('\b');
		terminal_putc(' ');
		terminal_putc('\b');
	}
}

void shell_extmsg_cleanup(void){
	printf_P(PSTR(SHELL_RDY));					//Re-print the ready character
}

char* shell_history(uint8_t idx){
	char* retval = NULL;
	if(idx < SHELL_NUMLINES){
		retval = &shell_inputlines[idx][0];
	}
	return retval;
}

shell_ret_t shell_help_cmd(){
	shell_ret_t retval = cmd_exit_success;
	uint8_t arglen = shell_extract_arg_len(SHELL_HELP_ARG);
	if(arglen){	//Command-specific help
		
		//printf("Command-specific help. Arglen: %u\n",arglen);
		
		char arg[arglen+1];	//Allocate space for argument and null termination.
		shell_extract_arg(SHELL_HELP_ARG,&arg[0],arglen+1);
		
		//printf("Got argument: '%s'\n",arg);
		
		for(uint8_t i = 0; i < sizeof(shell_cmd_array)/sizeof(shell_cmd_t); i++){
			if(strcmp_P(arg,SHELL_PROGMEM_NAME(i)) == 0){
				//printf("Found match with idx %u\n",i);
				printf_P(SHELL_PROGMEM_HELP(i));
				break;
			}
		}				
	}else{									//List of commands
		printf_P(PSTR(SHELL_HELP_PREFIX));
		for(uint8_t i = 0; i < sizeof(shell_cmd_array)/sizeof(shell_cmd_t); i++){
			printf_P(PSTR(SHELL_INDENT));
			printf_P(SHELL_PROGMEM_NAME(i));
			printf_P(PSTR("\n"));
		}
		printf_P(PSTR(SHELL_HELP_SUFFIX));
	}
	return retval;
}

const char* shell_check_arg(char *arg){
	uint8_t lenarg = strlen(arg);
	uint8_t lenesc = strlen(SHELL_ARG_PREFIX);
	uint8_t lenws = strlen(SHELL_WHITESPACE);
	char substring[lenarg + lenesc + lenws + 1];	//Allocate space for the substring to search for
	substring[0] = '\0';	//Clear first element, strcat will guaranteed start at first null element.
	
	strcat(substring,SHELL_WHITESPACE);				//Construct a copy of the argument syntax to search for
	strcat(substring,SHELL_ARG_PREFIX);
	strcat(substring,arg);
	
	#ifdef SHELL_DBG
	printf_P(PSTR("    Check arg; generated substring: '%s'\n"),substring);
	#endif
	
	const char* retval = NULL;
	const char* candidate = strstr(shell_inputlines[0], substring);	//If != NULL a candidate position was found
	while(candidate != NULL){
		#ifdef SHELL_DBG
		printf_P(PSTR("    Candidate found at: %u\n"),(unsigned int)(candidate - shell_inputlines[0]));
		printf_P(PSTR("    Following character: %c\n"),*(candidate + lenws + lenesc + lenarg));
		#endif
		//If the character after the candidate is whitespace, the candidate position was a match!
		if(strchr(SHELL_WHITESPACE_LAX,*(candidate + lenws + lenesc + lenarg)) != NULL){
			retval = candidate;
			break;
			#ifdef SHELL_DBG
			printf_P(PSTR("Match!\n"));
			#endif
		}else{
			candidate = strstr(candidate + 1, substring);
			#ifdef SHELL_DBG
			printf_P(PSTR("No match, keep on looking from %d!\n"),candidate - shell_inputlines[0]);
			#endif
		}
	}
		
	#ifdef SHELL_DBG
	if(retval != NULL){	
		printf_P(PSTR("    Matched argument! Ptr: %u\n"),(unsigned int)retval);
	}else{
		printf_P(PSTR("    Did not match argument.\n"));
	}
	#endif
	
 	return retval;
}

void shell_extract_arg(char *arg, char *dest, uint8_t maxlen){
	const char* idx = shell_check_arg(arg);	//Check if the argument appears somewhere in the line
	if(idx != NULL){
		uint8_t lenarg = strlen(arg);
		uint8_t lenesc = strlen(SHELL_ARG_PREFIX);
		uint8_t lenws = strlen(SHELL_WHITESPACE);
		
		idx += lenws + lenesc + lenarg + lenws;	//Index now points to first element of argument data
		
		uint8_t arglen = strcspn(idx,SHELL_WHITESPACE_LAX);
		
		maxlen--;	//Reserve last element for null-pad
		uint8_t minlen = maxlen > arglen ? arglen : maxlen;	//Only copy as many characters as is needed
		
		strncpy(dest,idx,minlen);
		
		*(dest + minlen) = '\0';	//Zero-pad last element
		
	}
}

uint8_t shell_extract_arg_len(char *arg){
	uint8_t arglen = 0;
	const char* idx = shell_check_arg(arg);
	if(idx != NULL){
		uint8_t lenarg = strlen(arg);
		uint8_t lenesc = strlen(SHELL_ARG_PREFIX);
		uint8_t lenws = strlen(SHELL_WHITESPACE);
		
		idx += lenws + lenesc + lenarg + lenws;	//Index now points to first element of argument data
		
		arglen = strcspn(idx,SHELL_WHITESPACE_LAX);
	}
	return arglen;
}