#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#include "../dynbuf/dynbuf.h"

#define UART_TX_BUSY() ((USARTD1.STATUS & USART_DREIF_bm) == 0)	/** Define to a macro that returns zero when the UART TX register can receive data */
#define UART_TX_BYTE(x) (USARTD1.DATA = (x))					/** Define to a macro that places 'x' into the UART transmit register */
#define UART_RX_BYTE	USARTD1.DATA							/** Define to register to read UART data from */
#define UART0_RX_INT		USARTD1_RXC_vect					/** Define to the UART recieve interrupt vector */

static dynbuf_t buffer_rx={0,0,UART_RX_BUFSIZE,1,{[0 ... UART_RX_BUFSIZE*1]=0}};	/** receive buffer struct */

#ifdef UART_FC
enum fc_in_state {xon_sent=0,xoff_sent,};
static enum fc_in_state fc_in_state = xon_sent;
#endif

void uart_putc(char data){
	while(UART_TX_BUSY()){};	//Wait until data is transmitted
	UART_TX_BYTE(data);	//Output it
}

char uart_getc(void){
	char retval;
	while(!dynbuf_query_read(&buffer_rx));	//block until data is available
	retval=*(char*)dynbuf_read(&buffer_rx);

#ifdef UART_FC	//If when reading data from the input buffer, XOFF has been sent and it now is empty enough issue an XON command
	if( (fc_in_state == xoff_sent) && (dynbuf_query_numel_free(&buffer_rx) > UART_FC_OVERHEAD) ){
	   	uart_putc(UART_FC_XON);
		fc_in_state = xon_sent;
	}
#endif
	
	return retval;
}

uint8_t uart_available(void){
	return dynbuf_query_read(&buffer_rx);
}

ISR(UART0_RX_INT){
	char data=UART_RX_BYTE;
	
	if(dynbuf_query_write(&buffer_rx)){
		dynbuf_write(&data,&buffer_rx);	//Add new data if there's space free
	}
	
#ifdef UART_FC	//If when receiving data the input buffer is filling up and XON has been sent issue an XOFF command
		if( (fc_in_state == xon_sent) && (dynbuf_query_numel_free(&buffer_rx) < UART_FC_OVERHEAD) ){
    		uart_putc(UART_FC_XOFF);
			fc_in_state = xoff_sent;
		}
#endif
}
