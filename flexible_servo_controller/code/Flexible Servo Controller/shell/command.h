/* Stores commands used in the shell and the functions they perform. Very application-dependent! */

#ifndef COMMAND_H_
#define COMMAND_H_

#include <avr/pgmspace.h>
#include "shell.h"

/** @brief Must be exactly equal to number of commands stored in shell_cmd_array! */
#define SHELL_NUM_CMD	9

/** @brief Maximum length of command name, including null-terminator*/
#define SHELL_CMD_MAX_LEN	11

typedef shell_ret_t (*shell_cmd_fp_t)(void);

/** @brief General structure for commands stored in the shell. */
typedef struct shell_cmd_t{
	const char *cmd_name;
	const char *cmd_help;
	const shell_cmd_fp_t fp;
}shell_cmd_t;

/** @brief Master array of commands */
extern const shell_cmd_t shell_cmd_array[SHELL_NUM_CMD] PROGMEM;

#endif /* COMMAND_H_ */