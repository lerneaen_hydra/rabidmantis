#include "command.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include "../board.h"
#include "../control/control.h"
#include "../util.h"
#include "../nv_settings/eeprom.h"
#include "../syslog/syslog.h"
#include "../control/cc.h"
#include "../control/control.h"
#include "../plot/plot.h"
#include "../state.h"

/** @brief A safe string to integer function. Returns nonzero if invalid string input */
static uint8_t cmd_str2int(char* str, int32_t* dest){
	uint8_t retval = 0;
	char *endptr;
	errno = 0;
	int32_t num = strtol(str,&endptr,10);
	if((errno == ERANGE && (num == INT32_MAX || num == INT32_MIN))
	|| (errno != 0 && num == 0)
	|| endptr == str){	// input was not numerically valid, see http://linux.die.net/man/3/strtol
		retval = 1;
	}else{
		*dest = num;
	}
	return retval;
}

/** @brief A safe string to float function. Returns nonzero if invalid string input */
static uint8_t cmd_str2flt(char *str, float* dest){
	uint8_t retval = 0;
	char *endptr;
	errno = 0;
	float num = strtod(str,&endptr);
	if((errno == ERANGE && num == num)
	|| (errno != 0 && num == 0)
	|| endptr == str){	// input was not numerically valid, see http://linux.die.net/man/3/strtod
		retval = 1;
	}else{
		*dest = num;
	}
	return retval;
}

static void print_param_header(void);
static void print_param(NV_IDX_T idx);
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

// /** Sample command */
// #define BATS_ARG "n"
// static const PROGMEM char cmd_bats_cmd[] = "bats";
// static const PROGMEM char cmd_bats_help[] = "Usage: 'bats [-"BATS_ARG" <num>]'.\nCannonically, set "BATS_ARG" to 4.";
// shell_ret_t cmd_bats_f(void){
// 	shell_ret_t retval = cmd_exit_missing_arg;
// 	printf_P(PSTR("Flap flap flap!\n"));
// 	if(shell_check_arg(BATS_ARG)){
// 		char param[shell_extract_arg_len(BATS_ARG)+1];
// 		shell_extract_arg(BATS_ARG,&param[0],sizeof(param)/sizeof(char));
// 		int32_t num;
// 		if(cmd_str2int(&param[0],&num)){
// 			return cmd_exit_invalid_arg;
// 		}
// 		printf_P(PSTR("Na"));
// 		while(--num){
// 			printf_P(PSTR("na"));
// 		}
// 		printf_P(PSTR(".... BATMAN!"));
// 		retval = cmd_exit_success;
// 	}
// 	return retval;
// }

/** User commands, modify as needed */

static const PROGMEM char cmd_timings_cmd[] = "timings";
static const PROGMEM char cmd_timing_help[] = "Usage: 'timings' (no arguments).\nDisplays fixed (compile-time) timings.";
#define TIMING_TEXT "\
Fixed timings;\n\
\tPID controller frequency: %lu Hz\n\
\tCurrent controller frequency; %lu Hz\n\
\tMotor time-constant calculation period %.3f ms\n\
\tCharge-pump recommended minimum frequency %lu Hz\n\
\tCharge-pump timeout period %.3f ms\n\
\tCharge-pump required level transitions to turn on %d\n\
\tCharge-pump worst-case turn-on time %.3f ms"

#define CMD_ALL_ARG	"a"
#define CMD_PARAM_ARG	"p"
#define CMD_PARAM_VAL	"v"
static const PROGMEM char cmd_set_val_cmd[] = "set";
static const PROGMEM char cmd_set_val_help[] = "Usage: 'set -"CMD_PARAM_ARG" <param> -"CMD_PARAM_VAL" <new value>'\n	-"CMD_PARAM_ARG" <param>; parameter to apply change to\n	-"CMD_PARAM_VAL" <new value>; new value to apply.\nSets a new value to a parameter. Must lie within minimum and maximum values.\nDoes not update nonvolatile memory (EEPROM).";

static const PROGMEM char cmd_get_val_cmd[] = "get";
static const PROGMEM char cmd_get_val_help[] = "Usage: 'get [ -"CMD_ALL_ARG" | -"CMD_PARAM_ARG" <name>]'\n	-"CMD_ALL_ARG"; list all parameters and their properties\n	-"CMD_PARAM_ARG" <name>; lists parameter <name> and its properties\nGets all/one parameter(s) current setting(s).";

static const PROGMEM char cmd_save_cmd[] = "save";
static const PROGMEM char cmd_save_help[] = "Usage: 'save' (no arguments).\nSaves all settings to nonvolatile memory (EEPROM).";

#define TUNE_POS_STEP	"pos"	// Position-step tuning
#define TUNE_VEL_STEP	"vel"	// Velocity-step tuning
#define TUNE_ACCEL_STEP	"acc"	// Acceleration-step tuning
#define TUNE_SHOW_REF	"ref"
#define TUNE_SHOW_ERR	"err"
#define TUNE_SHOW_PLANT "plant"
#define TUNE_SHOW_DPOW	"d"
#define TUNE_SHOW_PPOW	"p"
#define TUNE_SHOW_IPOW	"i"
#define TUNE_SHOW_TPOW	"t"
#define TUNE_H			"h"			// Entire plot_h of plot application (rows in terminal)
#define TUNE_LEN		"l"		// Length of tuning run (seconds)
#define TUNE_CSV_OUT	"csv"		// Output in CSV format
#define TUNE_ERROR_INACTIVE	"Error: output not active.\n"
#define TUNE_POS_STEP_MSG	"Position step of %d counts over %g seconds.\n"
#define TUNE_VEL_STEP_MSG	"Velocity step of %g counts/second.\n(Giving a a maximum movement of %d counts over %g seconds.)\n"
#define TUNE_ACCEL_STEP_MSG "Acceleration step of %g counts/second^2.\n(Giving a maximum velocity of %g counts/second\nand a movement of %d counts over %g seconds.)\n"
// Removed due to flash space constraints
//#define TUNE_ERROR_MISSING_MOTION "Error: tune types not specified.\n"
//#define TUNE_ERROR_MISSING_LEN "Error: tune length not specified.\n"
//#define TUNE_ERR_NOPLOTS	"Error: no parameters specified to plot.\n"
//#define TUNE_ERR_NOSTEP		"Error: no step type specified.\n"
#define TUNE_WARN_STEPSIZE_PREFIX	"Warning: motion too large, reduced to "
#define TUNE_WARN_STEPSIZE_POS		"%d counts.\n"
#define TUNE_WARN_STEPSIZE_VEL		"%g counts/second.\n(Reduce "TUNE_LEN" to maintain velocity.)\n"
#define TUNE_WARN_STEPSIZE_ACCEL	"%g counts/second^2.\n(Reduce "TUNE_LEN" to maintain acceleration.)\n"
#define TUNE_BUSY_SYMBOLS	{'\\','|','/','-'}
#define	TUNE_BUSY_MSG	"Tune in progress... "
#define TUNE_TOPPLOT_TITLE "[R]eference, [P]lant, [E]rror (encoder counts);\n"
#define TUNE_BTMPLOT_TITLE "[D]erivative],[P]roportional, [I]ntegral, [T]otal output (amps);\n"
#define TUNE_OUTPUT_HEADER "Using k_df: %.5g, k_d: %.5g, k_p: %.5g, k_i: %.5g\n"
#define TUNE_PLOT_MAXERR "Maximum recorded tracking error: %.5g (encoder counts)\n"
#define CSV_FORMAT	"%.5g;"
#define CSV_SEP		";"
#define CSV_HEADER	"Time;Ref.;Plant;Error;Derivative;Prop.;Int.;Net total"
static const PROGMEM char cmd_tune_cmd[] = "tune";
static const PROGMEM char cmd_tune_help[] = "\
Useage 'tune [-"TUNE_POS_STEP" <val> | -"TUNE_VEL_STEP" <val> | -"TUNE_ACCEL_STEP" <val>] -"TUNE_LEN" <length> [-"TUNE_SHOW_REF" | ...\n\
... -"TUNE_SHOW_PLANT" | -"TUNE_SHOW_ERR" | -"TUNE_SHOW_DPOW" | -"TUNE_SHOW_PPOW" | -"TUNE_SHOW_IPOW" | -"TUNE_SHOW_TPOW"] (-"TUNE_H" <height>) (-"TUNE_CSV_OUT")'\n\
\t-"TUNE_POS_STEP" <val>; apply a position step of <val> encoder counts.\n\
\t-"TUNE_VEL_STEP" <val>; apply a velocity step of <val> encoder counts / second.\n\
\t-"TUNE_ACCEL_STEP" <val>; apply an acceleration step of <val> encoder\n\t\tcounts / second^2.\n\
\t-"TUNE_LEN" <val>; length of tuning operation (seconds).\n\
\t-"TUNE_SHOW_REF"; show setpoint (reference) position.\n\
\t-"TUNE_SHOW_PLANT"; show plant (measured) position.\n\
\t-"TUNE_SHOW_ERR"; show tracking error.\n\
\t-"TUNE_SHOW_DPOW"; show the derivative contribution.\n\
\t-"TUNE_SHOW_PPOW"; show the proportional contribution.\n\
\t-"TUNE_SHOW_IPOW"; show the integral contribution.\n\
\t-"TUNE_SHOW_TPOW"; show the net total output current.\n\
\t-"TUNE_H" <height>; set the plot height to <height> rows. (Default "STR(TUNE_H_D)" rows.)\n\
\t-"TUNE_CSV_OUT"; output as CSV data instead of plot.\n\
Applies a position, velocity, or acceleration step and plots the results or\n\
outputs them as csv data.";

#define CAL_IOFFSET_DELAY_US	1	//Compensation for propogation delay in DAC output to comparator input
#define CAL_IOFFSET_INTRO		"Preparing to calibrate offset current.\nThe output MUST NOT rotate!\nPress enter to continue\n"
#define CAL_IOFFSET_ERR			"Magnitude of calibrated i_offset too large!\nWas the output rotating? If not, there's some internal error... =/\n"
#define CAL_IOFFSET_SUCCESS		"Calibrated value stored in \'%s\' parameter.\nBe sure to save to EEPROM before powering down!"
static const PROGMEM char cmd_calibrate_cmd[] = "calibrate";
static const PROGMEM char cmd_calibrate_help[] = "Usage: 'calibrate' (no arguments).\n\
Calibrates analog circuitry in the device, greatly reducing the offset current.\n\
A calibration need only be performed once. Make sure the output is not\n\
rotating before calibration, and save the results with the 'save' command when\n\
completed.\n";

static const PROGMEM char cmd_rst_cmd[] = "reset";
static const PROGMEM char cmd_rst_help[] = "Usage: 'reset' (no arguments).\nResets (restarts) the device.";

#define CMD_LOAD_DEFAULT "default"
static const PROGMEM char cmd_load_cmd[] = "load";
static const PROGMEM char cmd_load_help[] = "Usage: 'load (-"CMD_LOAD_DEFAULT")'\n	-"CMD_LOAD_DEFAULT"; Resets settings to defaults.\nLoads all settings from nonvolatile memory (EEPROM), overwriting unsaved\nsettings\nNOTE: Input steps on quadrature input may be lost on calling this command!";

static const PROGMEM char cmd_log_dev_cmd[] = "log_dev";
static const PROGMEM char cmd_log_dev_help[] = "See source code";

#define LOG_CLEAR	"clear"
static const PROGMEM char cmd_log_cmd[] = "log";
static const PROGMEM char cmd_log_help[] = "Usage: 'log (-"LOG_CLEAR")'.\n	-"LOG_CLEAR"; clear the log.\nPrint (display) all logged errors in order of occurrence\n(and optionally clear them).";

shell_ret_t cmd_tune_f(void){	
	//First determine the length of the tuning operation, find the divisor!
	if(!shell_check_arg(TUNE_LEN)){	// Make sure argument exists...
		//printf_P(PSTR(TUNE_ERROR_MISSING_LEN));
		return cmd_exit_missing_arg;// ...otherwise quit
	}
	char param_len[shell_extract_arg_len(TUNE_LEN)+1];
	float tunelen;
	shell_extract_arg(TUNE_LEN,param_len,sizeof(param_len)/sizeof(char));
	if(cmd_str2flt(param_len,&tunelen)){
		return cmd_exit_invalid_arg;
	}
	if(tunelen < (1.0 * TUNE_N_SAMPLES) / CONTROL_PID_FREQ){
		tunelen = (1.0 * TUNE_N_SAMPLES) / CONTROL_PID_FREQ;
	}else if(tunelen > (1.0 * TUNE_N_SAMPLES * CONTROL_TUNE_DIVISOR_MAX) / CONTROL_PID_FREQ){
		tunelen = (1.0 * TUNE_N_SAMPLES * CONTROL_TUNE_DIVISOR_MAX) / CONTROL_PID_FREQ;
	}
	CONTROL_TUNE_DIVISOR_T div = round(CONTROL_PID_FREQ * tunelen / TUNE_N_SAMPLES - 1);
	tunelen = ((((float)div) + 1)*TUNE_N_SAMPLES)/CONTROL_PID_FREQ;
	
	//Find the hight of the plot
	uint16_t plot_h = TUNE_H_D;	// Use a default plot_h if unspecified
	if(shell_check_arg(TUNE_H)){
		char param_height[shell_extract_arg_len(TUNE_H)+1];
		shell_extract_arg(TUNE_H,param_height,sizeof(param_height)/sizeof(char));
		int32_t dummy;
		if(cmd_str2int(param_height,&dummy)){
			return cmd_exit_invalid_arg;
		}
		if(dummy < 0 || dummy > UINT16_MAX){
			return cmd_exit_invalid_arg;
		}else{
			plot_h = dummy;
		}
	}
	
	// If a tune is already in progress for some reason, exit
	if(control_tune_query_busy()){
		return cmd_exit_success;
	}
	// If the output is inactive, exit
	if(state_get_state() != state_active){
		printf_P(PSTR(TUNE_ERROR_INACTIVE));
		return cmd_exit_success;
	}
	
	// If we have not returned yet, input is valid.
	// Clear any old data and start seeding the reference levels
	control_tune_reset();
	if(shell_check_arg(TUNE_POS_STEP)){
		char step_size[shell_extract_arg_len(TUNE_POS_STEP)+1];
		shell_extract_arg(TUNE_POS_STEP,step_size,sizeof(step_size)/sizeof(char));
		int32_t amp;	//Total step amplitude, in encoder counts
		if(cmd_str2int(step_size,&amp)){
			//printf_P(PSTR(TUNE_ERROR_MISSING_MOTION));
			return cmd_exit_missing_arg;
		}
		if(amp < INT16_MIN || amp > INT16_MAX){
			if(amp > INT16_MAX){
				amp = INT16_MAX;	
			}else{
				amp = INT16_MIN;	
			}
			printf_P(PSTR(TUNE_WARN_STEPSIZE_PREFIX""TUNE_WARN_STEPSIZE_POS),amp);
		}
		printf_P(PSTR(TUNE_POS_STEP_MSG),(int16_t)amp,(double)tunelen);
		for(uint16_t i = 0; i < TUNE_N_SAMPLES; i++){
			if(i < TUNE_STEP_DELAY){
				control_tune_add_ref(0);
			}else if(i < (TUNE_N_SAMPLES + TUNE_STEP_DELAY) / 2){
				control_tune_add_ref(amp);
			}else{
				control_tune_add_ref(0);
			}
		}
	}else if(shell_check_arg(TUNE_VEL_STEP)){
		char step_size[shell_extract_arg_len(TUNE_VEL_STEP)+1];
		shell_extract_arg(TUNE_VEL_STEP,step_size,sizeof(step_size)/sizeof(char));
		float vel;	//Velocity of motion, in counts / second
		if(cmd_str2flt(step_size,&vel)){
			//printf_P(PSTR(TUNE_ERROR_MISSING_MOTION));
			return cmd_exit_missing_arg;
		}
		float t_cv;			//Time spent in constant-velocity mode, per direction
		t_cv = (tunelen - 2.0 * TUNE_STEP_DELAY / CONTROL_PID_FREQ) / 2;	//tunelen = 2 * t_cv + 2 * t_step_delay
		float amp_f = round(vel * t_cv);	//Calculate the total amplitude of the motion
		int16_t amp;
		if(amp_f > INT16_MAX || amp_f < INT16_MIN){
			if(amp_f > INT16_MAX){
				amp = INT16_MAX;	
			}else{
				amp = INT16_MIN;	
			}
			vel = amp / t_cv;
			printf_P(PSTR(TUNE_WARN_STEPSIZE_PREFIX""TUNE_WARN_STEPSIZE_VEL),(double)vel);
		}else{
			amp = amp_f;
		}
		printf_P(PSTR(TUNE_VEL_STEP_MSG),(double)vel,amp,(double)tunelen);
		float d_encoder;	//Encoder delta per time-step
		d_encoder = vel * (div+1) / CONTROL_PID_FREQ;
		float dummy = 0;	//Stores last position of reference level
		uint16_t k_switch = round(TUNE_N_SAMPLES/2);
		uint16_t k_stop = TUNE_N_SAMPLES - TUNE_STEP_DELAY;
		for(uint16_t i = 0; i < TUNE_N_SAMPLES; i++){
			if(i < TUNE_STEP_DELAY){
				control_tune_add_ref(0);
			}else if(i < k_switch){
				dummy += d_encoder;
				control_tune_add_ref(round(dummy));
			}else if(i < k_stop){
				dummy -= d_encoder;
				control_tune_add_ref(round(dummy));
			}else if(i < TUNE_N_SAMPLES - 1){
				control_tune_add_ref(dummy);
			}else{
				control_tune_add_ref(0);
			}
		}

	}else if(shell_check_arg(TUNE_ACCEL_STEP)){
		char step_size[shell_extract_arg_len(TUNE_ACCEL_STEP)+1];
		shell_extract_arg(TUNE_ACCEL_STEP,step_size,sizeof(step_size)/sizeof(char));
		float acc;	//Acceleration of motion, in counts / second^2
		if(cmd_str2flt(step_size,&acc)){
			//printf_P(PSTR(TUNE_ERROR_MISSING_MOTION));
			return cmd_exit_missing_arg;
		}
		float t_ca;			//Time spent in constant-acceleration mode, per quadrant (acc up, brake up, acc down, brake down)
		t_ca = (tunelen - (2.0 * TUNE_STEP_DELAY) / CONTROL_PID_FREQ) / 4;	//tunelen = 4 * t_ca + 2 * t_step_delay
		float amp_f = round(acc * t_ca * t_ca);	//Calculate the total amplitude of the motion, 1/2*a*2*t_ca^2
		int16_t amp;
		if(amp_f > INT16_MAX || amp_f < INT16_MIN){
			if(amp_f > INT16_MAX){
				amp = INT16_MAX;	
			}else{
				amp = INT16_MIN;	
			}
			acc = amp / (t_ca * t_ca);	// acc = 2 * amp / (2*t_ca^2)
			printf_P(PSTR(TUNE_WARN_STEPSIZE_PREFIX""TUNE_WARN_STEPSIZE_ACCEL),(double)acc);
		}else{
			amp = amp_f;
		}
		printf_P(PSTR(TUNE_ACCEL_STEP_MSG),(double)acc,sqrt(fabs(2*acc*amp)),amp,(double)tunelen);
		float k_pos = 0;
		float k_vel = 0;
		float k_acc = acc / ((CONTROL_PID_FREQ/(div+1.0))*(CONTROL_PID_FREQ/(div+1.0)));
		uint16_t n_ca = floor(t_ca * CONTROL_PID_FREQ / (div+1.0));
		for(uint16_t i = 0; i < TUNE_N_SAMPLES; i++){
			if(i < TUNE_STEP_DELAY){
				control_tune_add_ref(0);
			}else if(i < TUNE_STEP_DELAY + n_ca){
				k_vel += k_acc;
				k_pos += k_vel;
				control_tune_add_ref(round(k_pos));
			}else if(i < TUNE_STEP_DELAY + 3*n_ca){
				k_vel -= k_acc;
				k_pos += k_vel;
				control_tune_add_ref(round(k_pos));
			}else if(i < TUNE_STEP_DELAY + 4*n_ca){
				k_vel += k_acc;
				k_pos += k_vel;
				control_tune_add_ref(round(k_pos));
			}else if(i < TUNE_N_SAMPLES - 1){
				control_tune_add_ref(round(k_pos));
			}else{
				control_tune_add_ref(0);
			}
		}
	}else{	// Else step type not defined, exit
		//printf_P(PSTR(TUNE_ERR_NOSTEP));
		return cmd_exit_missing_arg;
	}
	
	// Start the tuning run
	control_tune_start(div);
	//Wait until the tuning is complete
	char busymarker[] = TUNE_BUSY_SYMBOLS;
	uint8_t busymarker_idx = 0;
	uint8_t totchars = printf_P(PSTR(TUNE_BUSY_MSG" %c"),busymarker[0]);
	while(control_tune_query_busy()){
		wdt_reset();
		if(get_systime() == 0){
			while(get_systime() == 0){}
				printf_P(PSTR("\b%c"),busymarker[busymarker_idx]);
				busymarker_idx++;
				if(busymarker_idx >= sizeof(busymarker)/sizeof(char)){
					busymarker_idx = 0;
				}
		}
	}
	for(uint8_t i = 0; i < totchars; i++){
		printf_P(PSTR("\b"));
	}
	printf_P(PSTR("\n"));
	printf_P(PSTR(TUNE_OUTPUT_HEADER),
		setting_get_real_title(TITLE_KDF,SETTING_VAL),
		setting_get_real_title(TITLE_KD,SETTING_VAL),
		setting_get_real_title(TITLE_KP,SETTING_VAL),
		setting_get_real_title(TITLE_KI,SETTING_VAL));
	float maxerr = 0;
	for(uint8_t i = 0; i < TUNE_N_SAMPLES; i++){
		float candidate = round(fabs(control_tune_query_err(i)));
		if(candidate > maxerr){
			maxerr = candidate;
		}
	}
	printf_P(PSTR(TUNE_PLOT_MAXERR),maxerr);
	if(!shell_check_arg(TUNE_CSV_OUT)){
		struct plot_setting_t setting = {
			.x_len = TUNE_N_SAMPLES,
			.y_len = plot_h,
			.y_max = 0,
			.y_min = 0,
			.x_ticks = TUNE_XTICKS,
			.y_ticks = plot_h/TUNE_YTICK_DENSITY,
			.y_label = 1,
			.x_label = tunelen,
			#ifdef PLOT_AA
			.aa = TUNE_ANTIALIAS,
			#endif
		};
		
		//Now generate function pointer and label arrays
		uint8_t topidx = 0;
		uint8_t btmidx = 0;
		float (*fp_array_top[3])(uint16_t);	//Size must match maximum number of functions in top plot!
		char label_array_top[3];			//Size must match maximum number of functions in top plot!
		float (*fp_array_btm[4])(uint16_t);	//Size must match maximum number of functions in bottom plot!
		char label_array_btm[4];			//Size must match maximum number of functions in bottom plot!
		if(shell_check_arg(TUNE_SHOW_REF)){
			fp_array_top[topidx] = &control_tune_query_ref;
			label_array_top[topidx] = TUNE_REF_CHAR;
			topidx++;
		}
		if(shell_check_arg(TUNE_SHOW_ERR)){
			fp_array_top[topidx] = &control_tune_query_err;
			label_array_top[topidx] = TUNE_ERR_CHAR;
			topidx++;
		}
		if(shell_check_arg(TUNE_SHOW_PLANT)){
			fp_array_top[topidx] = &control_tune_query_plant;
			label_array_top[topidx] = TUNE_PLANT_CHAR;
			topidx++;
		}
		if(shell_check_arg(TUNE_SHOW_DPOW)){
			fp_array_btm[btmidx] = &control_tune_query_dpow;
			label_array_btm[btmidx] = TUNE_DERIV_CHAR;
			btmidx++;
		}
		if(shell_check_arg(TUNE_SHOW_PPOW)){
			fp_array_btm[btmidx] = &control_tune_query_ppow;
			label_array_btm[btmidx] = TUNE_PROP_CHAR;
			btmidx++;
		}
		if(shell_check_arg(TUNE_SHOW_IPOW)){
			fp_array_btm[btmidx] = &control_tune_query_ipow;
			label_array_btm[btmidx] = TUNE_INTEGRAL_CHAR;
			btmidx++;
		}
		if(shell_check_arg(TUNE_SHOW_TPOW)){
			fp_array_btm[btmidx] = &control_tune_query_tpow;
			label_array_btm[btmidx] = TUNE_TOTAL_CHAR;
			btmidx++;
		}
		if(!topidx && !btmidx){
			//printf_P(PSTR(TUNE_ERR_NOPLOTS));
			return cmd_exit_missing_arg;
		}
		if(topidx){
			printf_P(PSTR(TUNE_TOPPLOT_TITLE));
			plot(fp_array_top,label_array_top,topidx,&setting);	
		}
		if(btmidx){
			printf_P(PSTR(TUNE_BTMPLOT_TITLE));
			plot(fp_array_btm,label_array_btm,btmidx,&setting);	
		}
	}else{
		printf_P(PSTR(CSV_HEADER"\n"));
		for(uint16_t i = 0; i < TUNE_N_SAMPLES; i++){
			wdt_reset();
			printf_P(PSTR(CSV_FORMAT""CSV_FORMAT""CSV_FORMAT""CSV_FORMAT""CSV_FORMAT""CSV_FORMAT""CSV_FORMAT""CSV_FORMAT"\n"),
				tunelen * i / TUNE_N_SAMPLES, control_tune_query_ref(i), control_tune_query_plant(i),control_tune_query_err(i),
				control_tune_query_dpow(i),control_tune_query_ppow(i),control_tune_query_ipow(i),control_tune_query_tpow(i));
		}
	}	
	return cmd_exit_success;
}

shell_ret_t cmd_rst_f(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		volatile uint8_t dummy = RST.CTRL | RST_SWRST_bm;
		// RST.CTRL must be written to within 4 clock cycles of writing to CCP
		CCP = CCP_IOREG_gc;
		RST.CTRL = dummy;
	}
	return cmd_exit_success;
}

shell_ret_t cmd_calibrate_f(void){
	printf_P(PSTR(CAL_IOFFSET_INTRO));
	while(!terminal_line_available()){
		terminal_idle();
		wdt_reset();
	}
	while(terminal_line_available()){
		terminal_getc();
	}
	int16_t dac_cal_pos = DAC_MAXVAL;
	int16_t dac_cal_neg = 0;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){	//Ensure nothing else touches the DAC output
		board_bridge_en_brake_open();	//Let the current decay to zero
		for(uint16_t i = 0; i < CAL_DELAY; i++){
			wdt_reset();
			_delay_ms(1);
		}
		set_pos_dac(dac_cal_pos);
		set_neg_dac(dac_cal_neg);
		_delay_us(CAL_IOFFSET_DELAY_US);
		while(CHECK_AC_POS()){
			set_pos_dac(dac_cal_pos--);
			_delay_us(CAL_IOFFSET_DELAY_US);
		}
		while(CHECK_AC_NEG()){
			set_neg_dac(dac_cal_neg++);
			_delay_us(CAL_IOFFSET_DELAY_US);
		}
	}
	//The current controller will bring back the output to a safe state post-calibration.
	float i_offset_cal = (((dac_cal_pos + dac_cal_neg) / 2.0) - CC_ZERO) / CC_SCALING;
	float i_dac_offset = fabs( (dac_cal_pos - dac_cal_neg)/CC_SCALING );
	printf_P(PSTR("Calibrated i_offset: %.3g A (with a DAC mismatch of %.3g A)\n"),i_offset_cal,i_dac_offset);
	if(fabs(i_offset_cal) > I_OFFSET_MAX){
		printf_P(PSTR(CAL_IOFFSET_ERR));
	}
	setting_set_real_title(TITLE_I_OFFSET,SETTING_VAL,i_offset_cal);
	printf_P(PSTR(CAL_IOFFSET_SUCCESS),TITLE_I_OFFSET);
	return cmd_exit_success;
}

// Development function to add log events to test functionality
// shell_ret_t cmd_log_dev_f(void){
// 	shell_ret_t retval = cmd_exit_success;
// 	if(shell_check_arg("a")){
// 		char arg[shell_extract_arg_len("a")+1];
// 		shell_extract_arg("a",arg,sizeof(arg)/sizeof(char));
// 		syslog_add(atoi(arg));
// 	}
// 	return retval;
// }

shell_ret_t cmd_log_f(void){
	shell_ret_t retval = cmd_exit_success;
	syslog_t num_error = syslog_query_read();
	if(num_error){
		printf_P(PSTR("Logged errors (oldest to newest);\n"));
		while(num_error){
			syslog_print(--num_error);
		}
		if(shell_check_arg(LOG_CLEAR)){
			syslog_clear_all();
			printf_P(PSTR("Log now cleared."));
		}
	}else{
		printf_P(PSTR("No logged errors."));
	}		
	return retval;
}

shell_ret_t cmd_load_f(void){
	shell_ret_t retval = cmd_exit_success;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		board_bridge_en_brake_open();	//Bring the output to a safe state for the long atomic (interrupt-blocking) instructions to perform...
		controller_reset();				//Reset the controller
		if(shell_check_arg(CMD_LOAD_DEFAULT)){
			printf_P(PSTR("Settings reset to defaults\n"));
			eeprom_reset_defaults();
		}
		if(eeprom_eeprom2ram()){
			printf_P(PSTR("Loaded data, CRC check failed.\nStored CRC: 0x%04X\nCalcd. CRC: 0x%04X\nSettings reset to defaults.\n"),eeprom_get_crc_stored(),eeprom_get_crc_calc());
			eeprom_reset_defaults();
		}else{
			printf_P(PSTR("Loaded data, CRC check passed with 0x%04X.\n"),eeprom_get_crc_stored());
		}
	}	
	return retval;
}

shell_ret_t cmd_save_f(void){
	eeprom_ram2eeprom();
	printf_P(PSTR("Saved settings with CRC 0x%04X.\n"),eeprom_get_crc_stored());
	return cmd_exit_success;
}

shell_ret_t cmd_set_val_f(void){
	shell_ret_t retval = cmd_exit_missing_arg;
	if(shell_check_arg(CMD_PARAM_ARG) == NULL || shell_check_arg(CMD_PARAM_VAL) == NULL){	//Make sure <param> and <new value> are defined
		return retval;
	}
	char param[shell_extract_arg_len(CMD_PARAM_ARG)+1];
	char val[shell_extract_arg_len(CMD_PARAM_VAL)+1];
	shell_extract_arg(CMD_PARAM_ARG,param,sizeof(param)/sizeof(char));	//param now contains <param>
	shell_extract_arg(CMD_PARAM_VAL,val,sizeof(val)/sizeof(char));		//val nor contains <new value>
	if(strstr(param,NV_HIDDEN_HDR) != NULL){	//If <param> starts with the hidden parameter string header, exit.
		return retval;
	}
	NV_IDX_T idx = setting_get_idx(param);
	if(idx >= 0){	//if <param> matches a paramter title
		float num_val_dbl;
		int32_t num_val_int;
		switch(setting_get_type_idx(idx)){
			case SETTING_REAL_T:
				if(cmd_str2flt(val,&num_val_dbl)){
					return cmd_exit_invalid_arg;
				}
				if(!setting_set_real_idx(idx,SETTING_VAL,num_val_dbl)){	//Try to store the new number, fails if outside limit bounds.
					retval = cmd_exit_success;	
				}else{
					retval = cmd_exit_invalid_arg;
				}
				print_param_header();
				print_param(idx);
				break;
			case SETTING_INTEGER_T:
				if(cmd_str2int(val,&num_val_int)){
					return cmd_exit_invalid_arg;
				}
				if(!setting_set_int_idx(idx,SETTING_VAL,num_val_int)){
					retval = cmd_exit_success;
				}else{
					retval = cmd_exit_invalid_arg;
				}
				print_param_header();
				print_param(idx);
				break;
			default:	//Should never occur, if it does something is broken in the source code!
				retval = cmd_exit_invalid_arg;
				break;
		}
	}else{			//no match found for <param>
		retval = cmd_exit_invalid_arg;
	}
	return retval;
}

shell_ret_t cmd_timings_f(void){
	printf_P(PSTR(TIMING_TEXT),CONTROL_PID_FREQ, CONTROL_CC_FREQ, 1.0e3*MOTOR_TAU_DIV/CONTROL_PID_FREQ, CPUMP_REC_FREQ, 1.0e3/(CPUMP_THR_FREQ),  CPUMP_TICKS_THR, 1.0e3*2*CPUMP_TICKS_THR/CONTROL_PID_FREQ);
	return cmd_exit_success;
}

shell_ret_t cmd_get_val_f(void){
	shell_ret_t retval = cmd_exit_missing_arg;
	if(shell_check_arg(CMD_ALL_ARG)){		//Get all parameters
		print_param_header();
		for(uint8_t i = 0; i < SETTING_NUM(); i++){
			if(strstr(setting_get_title(i),NV_HIDDEN_HDR) == NULL){	//Don't show hidden parameters
				print_param(i);
			}				
		}
		retval = cmd_exit_success;
	}else if(shell_check_arg(CMD_PARAM_ARG)){	//Get one parameter
		char param[shell_extract_arg_len(CMD_PARAM_ARG)+1];
		shell_extract_arg(CMD_PARAM_ARG,&param[0],sizeof(param)/sizeof(char));
		if(strstr(param,NV_HIDDEN_HDR) != NULL){ //If requested parameter is of hidden type, pretend it wasn't found...
			retval = cmd_exit_invalid_arg;
		}else{
			NV_IDX_T idx = setting_get_idx(param);
			if(idx >= 0){
				print_param_header();
				print_param(idx);
				retval = cmd_exit_success;
			}else{
				retval = cmd_exit_invalid_arg;	
			}
		}	
	}		
	return retval;
}

/************************************************************************/
/* Master list of commands, populate with the desired commands          */
/************************************************************************/

/** @brief Shell command array, all valid commands must be contained here */
// IMPORTANT! BE SURE TO UPDATE SHELL_NUM_CMD WHEN CHANGING BELOW!
//Note; The order of commands in help list is the same as this order.
const shell_cmd_t shell_cmd_array[SHELL_NUM_CMD] PROGMEM = {
// 	{	//Everyone needs bats!
// 		cmd_bats_cmd,
// 		cmd_bats_help,
// 		&cmd_bats_f
// 	},
	{
		cmd_calibrate_cmd,
		cmd_calibrate_help,
		&cmd_calibrate_f
	},
	{
		cmd_get_val_cmd,
		cmd_get_val_help,
		&cmd_get_val_f
	},
	{
		cmd_load_cmd,
		cmd_load_help,
		&cmd_load_f
	},
	{
		cmd_log_cmd,
		cmd_log_help,
		&cmd_log_f
	},
	{
		cmd_rst_cmd,
		cmd_rst_help,
		&cmd_rst_f
	},
	{
		cmd_save_cmd,
		cmd_save_help,
		&cmd_save_f
	},
	{
		cmd_set_val_cmd,
		cmd_set_val_help,
		&cmd_set_val_f
	},
	{
		cmd_timings_cmd,
		cmd_timing_help,
		&cmd_timings_f
	},
	{
		cmd_tune_cmd,
		cmd_tune_help,
		&cmd_tune_f
	}
};


static void print_param_header(void){
	printf_P(PSTR("PARAMETER |TYPE   |VALUE     |MINIMUM   |MAXIMUM\n"));
	#define TYPELEN		7		//Number of characters in "type" heading
	#define TYPELEN_STR STR(TYPELEN)
	#define VALUELEN	10		//Number of character in "value", "minimum" and "maximum" headings
	#define VALUELEN_STR STR(VALUELEN)
	//Yes, ugly, whine whine, but simple enough here.
}

static void print_param(NV_IDX_T idx){
	//Output name
	printf_P(PSTR("%-"VALUELEN_STR"s|"),setting_get_title(idx));

	//Output type
	char type[TYPELEN+1];
	switch(setting_get_type_idx(idx)){
		case SETTING_REAL_T:
		strcpy_P(type,PSTR("Real"));
		break;
		case SETTING_INTEGER_T:
		strcpy_P(type,PSTR("Integer"));
		break;
		default:
		strcpy_P(type,PSTR(""));
		break;
	}
	printf_P(PSTR("%-"TYPELEN_STR"s|"),type);
	
	//Output value, min, max
	switch(setting_get_type_idx(idx)){
		case SETTING_REAL_T:
		printf_P(PSTR("%-#"VALUELEN_STR"g|%-#"VALUELEN_STR"g|%-#"VALUELEN_STR"g\n"),
		setting_get_real_idx(idx,SETTING_VAL),setting_get_real_idx(idx,SETTING_MIN),setting_get_real_idx(idx,SETTING_MAX));
		break;
		case SETTING_INTEGER_T:
		printf_P(PSTR("%-"VALUELEN_STR"ld|%-"VALUELEN_STR"ld|%-"VALUELEN_STR"ld\n"),
		setting_get_int_idx(idx,SETTING_VAL),setting_get_int_idx(idx,SETTING_MIN),setting_get_int_idx(idx,SETTING_MAX));
		break;
		default:
		break;
	}
}