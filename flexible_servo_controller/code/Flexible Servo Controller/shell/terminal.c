#include <util/atomic.h>
#include <avr/pgmspace.h>

#include "terminal.h"
#include "uart.h"
#include "shell.h"
#include "../dynbuf/dynbuf.h"

static int terminal_stream_putc(char c, FILE *stream);	/** wrapper function to set up stdio for writes */
static int terminal_stream_getc(FILE *stream);			/** wrapper function to set up stdio for reads */

FILE terminal_str = FDEV_SETUP_STREAM(terminal_stream_putc, terminal_stream_getc, _FDEV_SETUP_RW);

static dynbuf_t buffer_term = {0,0,TERMINAL_BUFSIZE,1,{[0 ... TERMINAL_BUFSIZE*1]=0}};	/** receive buffer struct */
static uint8_t esc_char = 0;/** Stores 1 if the last character was \e 2 if the last character was '[' preceeded by \e (the first and second ANSI escape characters). Zero for an ordinary input */
static int8_t history_pos = -1;	/** Stores the current history index */
static uint8_t lines = 0;	/** Number of lines in buffer */
static uint8_t row = 0;		/** Number of characters in current row */

void terminal_idle(void){
	while(uart_available() && dynbuf_query_write(&buffer_term)){	//While there's a byte to read and there's space in the local buffer
		char data = uart_getc();
		if(data == '\e'){	//If the input was the escape character, mark this
			esc_char = 1;
			//printf("<esc1>");
		}else if(esc_char == 1){	//If the last input was the escape character and we got the second control character now...
			if(data == '['){
				esc_char = 2;	
				//printf("<esc2>");
			}else{
				esc_char = 0;
				//printf("<escfail>");
			}
		}else if(esc_char == 2){		
			if(data == ESC_UPARROW || data == ESC_DOWNARROW){
				if(data == ESC_UPARROW && history_pos < SHELL_NUMLINES - 1){
					history_pos++;
				}else if(data == ESC_DOWNARROW && history_pos > 0){
					history_pos--;
				}
				//Remove whatever is currently displayed
				while(row > 1){
					uart_putc('\b');
					uart_putc(' ');
					uart_putc('\b');
					row--;
				}
				//Then remove it from the input buffer
				while(dynbuf_query_read(&buffer_term) &&
						(*(char*) dynbuf_peek_newest(&buffer_term)) != '\n' &&
						(*(char*) dynbuf_peek_newest(&buffer_term)) != '\r'){
					dynbuf_read(&buffer_term);
				}
				//Then output the stored line to the terminal and the input buffer
				char* nextchar = shell_history(history_pos);
				while(dynbuf_query_write(&buffer_term) &&
				*nextchar != '\n' &&
				*nextchar != '\r' &&
				*nextchar != '\0'){
					uart_putc(*nextchar);
					row++;
					dynbuf_write(nextchar++,&buffer_term);
				}
				//printf("<escud>");
				esc_char = 0;
			}else{
				esc_char = 0;
				//printf("<escfail2>");
			}				
		}else if(data >= ' ' && data <= '~' && row < TERMINAL_MAXROWLENGTH){	//If we got an 'ordinary' character and there's space in the row (if there's no space disregard the character)
			uart_putc(data);			//Echo it
			history_pos = -1;
			//printf("<norm>");
			dynbuf_write(&data,&buffer_term); //Add it to the local buffer
			row++;						//And increment the current character count
		}else if(data == '\b' && dynbuf_query_read(&buffer_term)){	//If it was a backspace character and there's something in the input buffer...
			history_pos = -1;
			if(*(char *) dynbuf_peek_newest(&buffer_term) != '\n'){	//...and it's not the start of a row
				uart_putc('\b');			//---
				uart_putc(' ');				//"remove" one character from the echo'd output
				uart_putc('\b');			//---
				dynbuf_read_newest(&buffer_term);	//And remove it from the local buffer
				row--;						//Decrement the current row count
			}else{
				uart_putc('\a');		//Otherwise make a beep on the host (can't backspace at start of line)
			}
		}else if(data == '\n' || data == '\r'){	//If it was a newline character of some sort
			uart_putc('\r');			//Output a CR...
			uart_putc('\n');			//...and a LF
			history_pos = -1;
			uint8_t invalid_newline = 0;
			if(dynbuf_query_read(&buffer_term)){			
				if(*(char*)dynbuf_peek_newest(&buffer_term) == '\n'){	//If we just added a newline...
					invalid_newline = 1;								//...don't add another
					//printf("<inv.newline>");
				}
			}
			if(!invalid_newline){					//If it was valid...
				//printf("<newline>");
				char foo = '\n';
				dynbuf_write(&foo,&buffer_term);	//...add it now
				lines++;							//Update the total number of lines available...
				row = 0;							//...and reset the characters in row count.
			}
		}else{					//Else data was invalid...
			uart_putc('\a');	//...and output a beep on the host
		}
	}
}

uint8_t terminal_line_available(void){
	return lines;
}

char terminal_getc(void){
	while(!lines){}; //Wait until there's input
	char retval = *(char *) dynbuf_read(&buffer_term);
	if(retval == '\n'){
		lines--;
	}
	return retval;
}

char terminal_getc_incomplete(void){
	char retval = '\0';
	if(dynbuf_query_read(&buffer_term)){
		char lastchar = *(char *) dynbuf_peek_newest(&buffer_term);
		if(lastchar == '\r' || lastchar == '\n'){	//If the most recent character was a newline...
			lastchar = '\0';						//...don't remove it and return a NULL character
		}else{
			dynbuf_read_newest(&buffer_term);		//Otherwise remove it from the input
		}
		retval = lastchar;
	}
	return retval;
}

void terminal_putc(char data){
	row++;
	if(data == '\n'){
		uart_putc('\r');
	}	
	if(data == '\n' || data == '\r'){
		row = 0;
	}
	uart_putc(data);
}

static int terminal_stream_putc(char c, FILE *stream){
	(void)stream;	//remove warning about unused parameter
	terminal_putc(c);
	return 0;		//return a value to match specification for function type
}

static int terminal_stream_getc(FILE *stream){
	(void)stream;	//remove warning about unused parameter
	char retval = terminal_getc();
	return (int) retval;
}