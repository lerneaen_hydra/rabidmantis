/*
 * control.c
 *
 * Created: 5/13/2013 12:14:23 PM
 *  Author: VBOX
 */ 

#include "control.h"
#include <avr/interrupt.h>
#include "../board.h"
#include "../config.h"
#include "../nv_settings/eeprom.h"
#include "../state.h"
#include "cc.h"
#include "../dynbuf/dynbuf.h"

#define CONTROL_TUNE_BUFLEN (TUNE_N_SAMPLES + 1)

enum control_state_t {control_state_undefined = 0, control_state_disable, control_state_active,};
static volatile enum control_state_t control_state = control_state_undefined;

enum tune_state_t {tune_state_inactive = 0, tune_state_init, tune_state_active};
static volatile enum tune_state_t tune_state;

//Tuning buffers
//Ref buffer is used both for input and output
static dynbuf_t ref_buf = {0, 0, CONTROL_TUNE_BUFLEN, sizeof(int16_t), {[0 ... CONTROL_TUNE_BUFLEN * sizeof(int16_t)] = 0}};
static dynbuf_t plant_buf = {0, 0, CONTROL_TUNE_BUFLEN, sizeof(int16_t), {[0 ... CONTROL_TUNE_BUFLEN * sizeof(int16_t)] = 0}};
// Error can be constructed from ref - plant
static dynbuf_t dpow_buf = {0, 0, CONTROL_TUNE_BUFLEN, sizeof(float), {[0 ... CONTROL_TUNE_BUFLEN * sizeof(float)] = 0}};
static dynbuf_t ppow_buf = {0, 0, CONTROL_TUNE_BUFLEN, sizeof(float), {[0 ... CONTROL_TUNE_BUFLEN * sizeof(float)] = 0}};
static dynbuf_t ipow_buf = {0, 0, CONTROL_TUNE_BUFLEN, sizeof(float), {[0 ... CONTROL_TUNE_BUFLEN * sizeof(float)] = 0}};
// Total can be constructed from dpow + ppow + ipow

static volatile uint8_t tune_divisor_ref = 0;	//PID loop divisor reference, if zero every sample is output, if one every other, etc..
static volatile uint8_t tune_divisor = 0;		//PID loop divisor, takes range 0 -> tune_divisor_ref

//Indices of nonvolatile settings, seeded on initialization
static NV_IDX_T idx_kd;
static NV_IDX_T idx_kdf;
static NV_IDX_T idx_kp;
static NV_IDX_T idx_ki;
static NV_IDX_T idx_inp_pow;
static NV_IDX_T idx_i_max;
static NV_IDX_T idx_i_friction;

static int16_t err = 0;			//Most recent tracking error
static uint16_t last_err = 0;	//Next-most recent tracking error
static float d_filt = 0;		//IIR lowpass filter storage element
static float err_acc = 0;		//Accumulated error level
static float out = 0;			//Most recent output current

#ifdef CONTROL_DFILT
float controller_query_dfilt(void){
	float retval;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		retval = d_filt;
	}
	return retval;
}
#endif

void control_init(void){
	if(control_state == control_state_undefined){
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
			idx_kd = setting_get_idx(TITLE_KD);
			idx_kdf = setting_get_idx(TITLE_KDF);
			idx_kp = setting_get_idx(TITLE_KP);
			idx_ki = setting_get_idx(TITLE_KI);
			idx_inp_pow = setting_get_idx(TITLE_INPUT_POW);
			idx_i_max = setting_get_idx(TITLE_I_MAX);
			idx_i_friction = setting_get_idx(TITLE_I_FRICTION);
			control_state = control_state_disable;
		}		
	}		
}

void controller_disable(void){
	if(control_state != control_state_undefined){
		control_state = control_state_disable;
		tune_state = tune_state_inactive;
	}
}

void controller_reset(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		err_acc = 0;
		board_reset_enc_ref();
		d_filt = 0;
		last_err = 0;
	}
}

void controller_enable(void){
	if(control_state != control_state_undefined){
		controller_reset();
		control_tune_reset();
		control_state = control_state_active;
	}
}

void controller_idle(void){
	if(control_state == control_state_active){
		// Get the control parameters for the current run
		uint8_t inp_pow = setting_get_int_idx(idx_inp_pow,SETTING_VAL);
		float kd = setting_get_real_idx(idx_kd,SETTING_VAL);
		float kdf = setting_get_real_idx(idx_kdf,SETTING_VAL);
		float kp = setting_get_real_idx(idx_kp,SETTING_VAL);
		float ki = setting_get_real_idx(idx_ki,SETTING_VAL);
		float i_max = setting_get_real_idx(idx_i_max,SETTING_VAL);
		float i_friction = setting_get_real_idx(idx_i_friction,SETTING_VAL);
		
		// Collect plant and reference input
		uint16_t ref = 0;
		switch(tune_state){
			case tune_state_init:
				controller_reset();
				tune_state = tune_state_active;	//Now continue directly into the actions performed in the active state (no break statement!)
			case tune_state_active:
				if(dynbuf_query_read(&ref_buf)){	//If there's still data in the tuning input buffer
					if(tune_divisor == tune_divisor_ref){	//If this is the last time to use the current reference...
						ref = *(uint16_t*) dynbuf_read(&ref_buf) << inp_pow;	//...remove it from the buffer. The next call control_idle() call will use the next stored reference level.
					}else{
						ref = *(uint16_t*) dynbuf_peek(&ref_buf) << inp_pow;
					}
					break;
				}else{	//If there's nothing left in the input buffer
					controller_reset();
					tune_state = tune_state_inactive;	//Now continue directly to the actions performed in the inactive state (no break statement!)
				}
			case tune_state_inactive:
				ref = board_query_ref() << inp_pow;
				break;
			default:
				tune_state = tune_state_inactive;	//Safety precaution, should never happen
		}

		uint16_t plant = board_query_enc();
				
		//Generate error level
		err = plant-ref;	//This wraps around nicely, giving an error in the range -INT16_MAX to INT16_MAX
		
		//Generate derivative contribution
		d_filt += kdf * (((int16_t)(err - last_err)) - d_filt);	//Update derivative IIR lowpass filter, equivalent to d_filt = kdf * ((int16_t)(err - last_err)) + (1 - kdf)*d_filt
		last_err = err;	//This must be after last_err is used in d_filt
		float dpow = d_filt * kd;
		
		
		//Generate proportional contribution
		float ppow = err * kp;
		
		//Generate integral contribution
		float ipow = (err_acc + err) * ki;
		
		//Generate total contribution
		out = dpow + ppow + ipow;
		
		//Limit to current bounds and apply output
		uint8_t sat = 0;
		if(out > i_max){
			out = i_max;
			sat = 1;
		}else if(out < -1 * i_max){
			out = -1 * i_max;
			sat = 1;
		}
		
		//Update accumulated error only if output not saturated (anti-windup for integral term)
		if(!sat && !cc_sat()){
			err_acc += err;
		}
		
		if(out > 0){
			out += i_friction;
		}else if(out < 0){
			out -= i_friction;
		}
		cc_control(out);
		
		if(tune_state == tune_state_active){
			if(tune_divisor == tune_divisor_ref){
				if(dynbuf_query_write(&plant_buf)){	//If there is still space free, write more
					dynbuf_write(&ref,&ref_buf);		//Re-write to ref_buf to make ordering between buffers consistent
					dynbuf_write(&plant,&plant_buf);
					dynbuf_write(&dpow,&dpow_buf);
					dynbuf_write(&ppow,&ppow_buf);
					dynbuf_write(&ipow,&ipow_buf);
				}else{	//If there's no more space, the tuning run is complete
					tune_state = tune_state_inactive;
					controller_reset();
				}
				tune_divisor = 0;
			}else{			
				tune_divisor++;
			}				
		}		
		
		if(get_systime() == 0){
			//printf_P(PSTR("Set current%.3g A\n"),out);
			//printf("got error %d\n",err);
		}
	}		
}


uint8_t control_tune_query_add_ref(void){
	uint8_t retval = 0;
	retval = dynbuf_query_write(&ref_buf);
	return retval;
}

void control_tune_reset(void){
	dynbuf_reset(&ref_buf);
	dynbuf_reset(&plant_buf);
	dynbuf_reset(&dpow_buf);
	dynbuf_reset(&ppow_buf);
	dynbuf_reset(&ipow_buf);
}

void control_tune_add_ref(int16_t ref){
	if(tune_state == tune_state_inactive && dynbuf_query_write(&ref_buf)){
		dynbuf_write(&ref, &ref_buf);
	}	
}

void control_tune_start(uint8_t divisor){
	if(control_state == control_state_active){
		tune_divisor_ref = divisor;
		tune_divisor = 0;
		tune_state = tune_state_init;
	}
}


int16_t controller_query_tracking_error(void){
	int16_t retval;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		retval = err;
	}
	return retval;
}

float controller_query_out(void){
	float retval;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		retval = out;
	}	
	return retval;
}

uint8_t control_tune_query_busy(void){
	uint8_t retval = 1;
	if(tune_state == tune_state_inactive){
		retval = 0;
	}
	return retval;
}

uint8_t control_tune_len(void){
	return dynbuf_query_numel_used(&plant_buf);	//All buffers have the same number of stored elements, reading any will do
}

float control_tune_query_ref(uint16_t n){
	int16_t retval = 0;
	uint8_t len = control_tune_len();
	if(n < len){
		n = len - n -1;
		retval = *(int16_t*) dynbuf_peek_n(n,&ref_buf);
	}
	return (float) retval;
}

float control_tune_query_plant(uint16_t n){
	int16_t retval = 0;
	uint8_t len = control_tune_len();
	if(n < len){
		n = len - n -1;
		retval = *(int16_t*) dynbuf_peek_n(n,&plant_buf);
	}
	return (float) retval;
}

float control_tune_query_err(uint16_t n){
	return (float) (control_tune_query_plant(n) - control_tune_query_ref(n));
}

float control_tune_query_dpow(uint16_t n){
	float retval = 0;
	uint8_t len = control_tune_len();
	if(n < len){
		n = len - n -1;
		retval = *(float*) dynbuf_peek_n(n,&dpow_buf);
	}
	return retval;
}

float control_tune_query_ppow(uint16_t n){
	float retval = 0;
	uint8_t len = control_tune_len();
	if(n < len){
		n = len - n -1;
		retval = *(float*) dynbuf_peek_n(n,&ppow_buf);
	}
	return retval;
}

float control_tune_query_ipow(uint16_t n){
	float retval = 0;
	uint8_t len = control_tune_len();
	if(n < len){
		n = len - n -1;
		retval = *(float*) dynbuf_peek_n(n,&ipow_buf);
	}
	return retval;
}

float control_tune_query_tpow(uint16_t n){
	float i_max = setting_get_real_idx(idx_i_max,SETTING_VAL);
	float retval = control_tune_query_dpow(n) + control_tune_query_ppow(n) + control_tune_query_ipow(n);
	if (retval > i_max){
		retval = i_max;
	}else if(retval < -1*i_max){
		retval = -1*i_max;
	}
	return retval;
}