/*
 * control.h
 * A basic PID implementation with logging support for plotting reference,
 * plant, error, and the effects from the derivative, proportional, and
 * integral terms. An adjustable first order IIR filter on the derivative
 * power reduces measurement noise and a basic anti-windup routine for the
 * integral term handles output saturation.
 * 
 * This PID controller is made to be used with the current controller (cc.h/.c)
 * and the nv_settings library has various hard-coded assumptions related to
 * this.
 * 
 * On startup, run the control_init() command once, this seeds parameters from
 * the nv_settings library.
 * To use and enable the controller, call controller_enable(). This reads plant
 * and reference signals that are hard-coded, and applies a floating-point PID
 * algorithm executed periodically as (psuedocode)
 * 
 * 		integer err = plant-ref;	//current error, an integer as measured by the reference and plant
 * 		float d_filt += kdf * (err - last_err - d_filt); //filtered derivative
 * 		integer last_err = err;		//update the last error
 * 		float dpow = d_filt * kd;	//generate the derivative power
 * 		float ppow = err * kp;		//generate the proportional power
 * 		float ipow = (err_acc + err) * ki;	//generate the integral power
 * 		float out = dpow + ppow + ipow;	//generate the total power
 * 		if(out > OUT_MAX)			//Limit the output to upper...
 * 				out = OUT_MAX;
 * 		if(out < -1*OUT_MAX)		//...and lower bounds
 * 					out = -1*OUT_MIN;
 * 		if(out_not_saturated())		//Only accumulate error if the output is not saturated
 * 				err_acc += err;
 * 		cc_control(out);			//and finally apply the new output
 * 
 * Support for logging setpoint, plant, and the power of the different parts
 * of the controller is described more below.
 * 
 * Though there are various hard-coded aspects to this library, it should be
 * fairly straight-forward to convert this to a more general-purpose lib.
 */
 
#ifndef CONTROL_H_
#define CONTROL_H_

#include <stdint.h>

#include "../config.h"

/** @brief Define to add support for reading the most recent filtered derivative value */
//#define CONTROL_DFILT

/** @brief Controls the variable type of the tuning divisor */
//Note: BE SURE THESE TWO DEFINES ARE SYNCHRONIZED!
#define CONTROL_TUNE_DIVISOR_T		uint8_t
#define CONTROL_TUNE_DIVISOR_MAX	UINT8_MAX

/** @brief Initializes the PID controller. Must be run after EEPROM settings are loaded and before the controller can be used. */
void control_init(void);

/** @brief Disables the PID controller. DOES NOT CHANGE THE CC OUTPUT STATE! */
void controller_disable(void);

/** @brief Enables and resets the PID controller (to be used after a call to controller_disable()) */
void controller_enable(void);

/** @brief Resets the PID controller terms with memory and the reference and plant input, but does not disable the controller */
void controller_reset(void);

/** @brief Executes one run of the PID controller. Should be called with an even period */
void controller_idle(void);

/** @brief Returns the most recent tracking error. Caller *MUST* not interrupt controller_idle()! */
int16_t controller_query_tracking_error(void);

/** @brief Returns the most recent output from the PID controller. Caller *MUST* not interrupt controller_idle() */
float controller_query_out(void);

#ifdef CONTROL_DFILT
/** @brief Returns the most recent filtered derviative value */
float controller_query_dfilt(void);
#endif

/************************************************************************/
/*							Tuning/plotting tools						*/
/************************************************************************/
/*  //Usage example;
 *  int foo = 0;
 *	control_tune_reset();	//Resets the tune internal state
 *	while(control_tune_query_add_ref()){	//Adds as many elements as possible to the
 *		control_tune_add_ref(foo++);		//tuning run, which will be a ramp of the position
 *	}
 *	control_tune_start(0);					//Start the tune, with a sample stored for every control_idle() call
 *	while(control_tune_query_busy()){		//Wait until the tuning is complete
 *		wdt_reset();
 *	};
 *	foo = 0;
 *	float bar;
 *	int elem = control_tune_len();
 *	printf("got %d values\n",elem);
 *	while(elem--){	//Prints the plant output from start to finish
 *		bar = control_tune_query_plant(foo) - control_tune_query_ref(foo);
 *		printf("At sample %u the error was %g\n",foo++,(double)bar);
 *		wdt_reset();
 *	}
 */

/** @brief Returns true if there is space for more entries in the reference buffer */
uint8_t control_tune_query_add_ref(void);

/** @brief Adds an element to the reference buffer. Input to function is equivalent to 'ref' steps on quadrature input*/
void control_tune_add_ref(int16_t ref);

/** @brief Starts tuning run, stops automatically. Note that the output should be stationary before calling */
void control_tune_start(uint8_t divisor);

/** @brief Returns nonzero if a tuning run is in progress */
uint8_t control_tune_query_busy(void);

/** @brief Returns the number of entries stored in the tuning buffers */
uint8_t control_tune_len(void);

/** @brief Resets the tuning system. Must be run between successive tuning runs */
void control_tune_reset(void);

/** @brief Gets the n'th entry from the reference buffer. n = 0 is the oldest entry */
float control_tune_query_ref(uint16_t n);

/** @brief Gets the n'th entry from the plant buffer. n = 0 is the oldest entry */
float control_tune_query_plant(uint16_t n);

/** @brief Gets the n'th entry from the error buffer. n = 0 is the oldest entry */
float control_tune_query_err(uint16_t n);

/** @brief Gets the n'th entry from the derivative power buffer. n = 0 is the oldest entry */
float control_tune_query_dpow(uint16_t n);

/** @brief Gets the n'th entry from the proportional power buffer. n = 0 is the oldest entry */
float control_tune_query_ppow(uint16_t n);

/** @brief Gets the n'th entry from the integral power buffer. n = 0 is the oldest entry */
float control_tune_query_ipow(uint16_t n);

/** @brief Gets the n'th entry from the total power buffer. n = 0 is the oldest entry */
float control_tune_query_tpow(uint16_t n);

#endif /* CONTROL_H_ */