/*
 * cc.h
 * Simple constant-current control algorithm and functions for setting states.
 * 
 * There are four different possible states the current control algorithm can
 * be in; servo-pos, servo-neg, short, and open. In servo-pos the H-bridge is
 * configured so that VBUS is applied to the load in one direction, while in
 * servo-neg VBUS is applied to the load in the opposite direction. In short
 * the lower H-bridge transistors are kept active, shorting the load, and in
 * open all transistors are kept inactive, open-circuiting the load.
 * 
 * On startup, cc_init() must be called, which seeds various global variables.
 * This need only be done once.
 * 
 * cc_brake() enables brake mode; the output is kept in the shorted state until
 * the measured current exceeds I_MAX in the non-volatile settings. When
 * exceeded the output switches to an the open state until the current falls
 * below I_MAX, at which point the state is switched to short again, and so on.
 * In other words, cc_brake slows down the load as fast as possible while
 * respecting a maximum current.
 * 
 * cc_control(new_current) sets the current controller to servo-pos and
 * servo-neg alternatingly, if the current is below some threshold current
 * i_min the state is set to servo_pos, increasing the current, and if the
 * current is above some threshold current i_max the state is set to servo-neg,
 * decreasing the current.
 * 
 * A parameter labeled 'i_skip' is used to reduce switching losses, which
 * blocks the h-bridge output from changing state until i_skip periods of the
 * cc interrupt have been called. Additionally, an 'i_ripple' parameter in
 * cc_control(...) allows for setting an optional ripple current, further
 * reducing switching losses. Generally, choose an i_skip that results in a
 * low enough maximum switching frequency, on the order of 10-50kHz. Setting
 * i_ripple to a nonzero value allows for reducing the switching rate, but
 * may (read: probably will) increase audible switching noise.
 * 
 * This implementation is hard-coded to use the nv_settings eeprom/setting
 * library and also assumes an interrupt is set up for it!
 * 
 * Created: 5/16/2013 12:26:07 PM
 *  Author: VBOX
 */ 

#ifndef CC_H_
#define CC_H_

#include <inttypes.h>

/** @brief Run once on startup to seed internal values. Controller does nothing until this is performed. Guaranteed atomic. */
void cc_init(void);

/** @brief Enables the CC controller in brake mode, braking a load up to I_OUT = I_MAX */
//Note: **MUST NOT** be called from a position of higher or equal priority than the controller interrupt!
void cc_brake(void);

/** @brief Disables the CC controller, and places the hardware in a safe idle state (open circuit) */
void cc_disable(void);

/** @brief Sets the output to control mode, current in enabled mode */
//Note: **MUST NOT** be called from a position of higher or equal priority than the controller interrupt!
void cc_control(float new_current);

/** @brief Function to temporarily set the output in a safe state for long operations blocking interrupt */
void board_bridge_en_brake_open(void);

/** @brief Returns true if the output in control mode has been saturated* since the last cc_control() call*/
//* saturated here meaining the output has not transitioned to both servo_pos
//  and servo_neg states since the last cc_control call
uint8_t cc_sat(void);

#endif /* CC_H_ */