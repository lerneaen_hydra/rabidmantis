#include <util/atomic.h>
#include "cc.h"

#include "../board.h"
#include "../nv_settings/eeprom.h"

static volatile uint8_t skip_ref;	//Internal counter for skip_ref parameter, seeded from nv_params and used in ISR

static NV_IDX_T idx_i_ripple;
static NV_IDX_T idx_i_max;
static NV_IDX_T idx_skip;
static NV_IDX_T idx_i_offset;

enum cc_state_t {cc_state_undefined = 0, cc_state_off , cc_state_control, cc_state_brake};	//Default output_short is undefined, internal variables need to be seeded.
static volatile enum cc_state_t cc_state = cc_state_undefined;

// Flags for output saturation detection
static uint8_t sat_pos = 0;
static uint8_t sat_neg = 0;

/************************************************************************/
/* Helper functions for hardware										*/
/************************************************************************/
/** @brief Returns nonzero if I_MEAS > I_SET_HIGH */
static inline uint8_t check_pos_AC(void){
	return CHECK_AC_POS();
}

/** @brief Returns nonzero if I_MEAS < I_SET_LOW */
static inline uint8_t check_neg_AC(void){
	return CHECK_AC_NEG();
}


/** @brief Sets up the h-bridge for positive servo current */
static inline void board_bridge_en_servo_pos(void){
	BRIDGE_PORT.OUTCLR = BRIDGE_PHASE_bm;
	BRIDGE_PORT.OUTSET = BRIDGE_PWMH_bm | BRIDGE_PWML_bm | BRIDGE_SR_bm;
}

/** @brief Sets up the h-bridge for negative servo current */
static inline void board_bridge_en_servo_neg(void){
	BRIDGE_PORT.OUTSET = BRIDGE_PWMH_bm | BRIDGE_PWML_bm | BRIDGE_SR_bm | BRIDGE_PHASE_bm;
}

/** @brief Sets up the h-bridge for active (shorted) brake mode */
static inline void board_bridge_en_brake_short(void){
	BRIDGE_PORT.OUTSET = BRIDGE_PWML_bm | BRIDGE_SR_bm;
	BRIDGE_PORT.OUTCLR = BRIDGE_PWMH_bm | BRIDGE_PHASE_bm;
}

/** @brief Sets up the h-bridge for decay (open) brake mode*/
void board_bridge_en_brake_open(void){
	BRIDGE_PORT.OUTCLR = BRIDGE_PWMH_bm | BRIDGE_PWML_bm | BRIDGE_PHASE_bm | BRIDGE_SR_bm;
}


void cc_init(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		idx_i_ripple = setting_get_idx(TITLE_I_RIPPLE);
		idx_i_max = setting_get_idx(TITLE_I_MAX);
		idx_i_offset = setting_get_idx(TITLE_I_OFFSET);
		idx_skip = setting_get_idx(TITLE_I_SKIP);
		cc_state = cc_state_off;
	}
}

void cc_brake(void){
	if(cc_state != cc_state_undefined){
		float i_brake = setting_get_real_idx(idx_i_max,SETTING_VAL);
		float i_offset = setting_get_real_idx(idx_i_offset,SETTING_VAL);
		int16_t dac_pos = (int16_t)((i_brake - i_offset) * CC_SCALING + CC_ZERO);
		int16_t dac_neg = (int16_t)(-1* (i_brake - i_offset) * CC_SCALING + CC_ZERO);
		uint8_t skip_int = setting_get_int_idx(idx_skip,SETTING_VAL);
		// Limit DAC codes to DAC range. Should not strictly be required, as i_brake + i_offset has limits that are well conditioned, but do it anyway to be safe!
		if(dac_pos > DAC_MAXVAL){
			dac_pos = DAC_MAXVAL;
		}else if(dac_pos < DAC_MINVAL){
			dac_pos = DAC_MINVAL;
		}
		if(dac_neg > DAC_MAXVAL){
			dac_neg = DAC_MAXVAL;
		}else if(dac_neg < DAC_MINVAL){
			dac_neg = DAC_MINVAL;
		}
		
		//printf_P(PSTR("BRAKE: Max brake current; %g.Skip: %d, POSDAC: %d, NEGDAC %d\n"),i_brake,skip_int,dac_pos,dac_neg);
		
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
			cc_state = cc_state_brake;
			skip_ref = skip_int;
			set_pos_dac(dac_pos);
			set_neg_dac(dac_neg);
		}
	}
}

void cc_disable(void){
	if(cc_state != cc_state_undefined){
		cc_state = cc_state_off;
	}
}

void cc_control(float new_current){
	if(cc_state != cc_state_undefined){
		float ripple = setting_get_real_idx(idx_i_ripple,SETTING_VAL);
		float i_max = setting_get_real_idx(idx_i_max,SETTING_VAL);
		float i_offset = setting_get_real_idx(idx_i_offset,SETTING_VAL);
		uint8_t skip_int = setting_get_int_idx(idx_skip,SETTING_VAL);
		// Clamp current setpoint to user specified limits
		if(new_current > i_max){
			new_current = i_max;
		}else if(new_current < -1 * i_max){
			new_current = -1 * i_max;
		}

		// Calculate equivalent DAC count, as an int for speed
		int16_t dac_center = (int16_t) ((new_current + i_offset) * CC_SCALING + CC_ZERO);
		int16_t dac_ripple_2 = (int16_t) (ripple * CC_SCALING/2);
		
		// Convert to positive (high) and negative (low) dac output codes
		int16_t dac_pos = dac_center + dac_ripple_2;
		int16_t dac_neg = dac_center - dac_ripple_2;
		
		// Limit DAC codes to DAC range. Required when new_current = i_max and ripple is high.
		if(dac_pos > DAC_MAXVAL){
			dac_pos = DAC_MAXVAL;
		}else if(dac_pos < DAC_MINVAL){
			dac_pos = DAC_MINVAL;
		}
		if(dac_neg > DAC_MAXVAL){
			dac_neg = DAC_MAXVAL;
		}else if(dac_neg < DAC_MINVAL){
			dac_neg = DAC_MINVAL;
		}
		
		//printf_P(PSTR("CC: Current; %g.Skip: %d, POSDAC: %d, NEGDAC %d\n"),new_current,skip_int,dac_pos,dac_neg);
		
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
			cc_state = cc_state_control;
			skip_ref = skip_int;
			sat_pos = 1;
			sat_neg = 1;
			set_pos_dac(dac_pos);
			set_neg_dac(dac_neg);
		}
	}
}

uint8_t cc_sat(void){
	uint8_t retval = 0;
	if(sat_pos || sat_neg){
		retval = 1;
	}
	return retval;
}


ISR(CC_VECT){
	//Remaining number of interrupt calls to skip before output can change.
	//Note this value is preserved even when switching between states!	
	static uint8_t skip_rem = 0;
	switch(cc_state){
		case cc_state_control:
			if(skip_rem){
				skip_rem--;
			}else{
				if(check_pos_AC()){
					board_bridge_en_servo_neg();
					skip_rem = skip_ref;
					sat_pos = 0;
				}else if(check_neg_AC()){
					board_bridge_en_servo_pos();
					skip_rem = skip_ref;
					sat_neg = 0;
				}
			}
			break;
		case cc_state_brake:
			if(skip_rem){
				skip_rem--;
			}else{
				// Short the output while the I_OUT < I_MAX, when exceeded open circuit (fast decay).
				// There had better be something to absorb the generated energy placed on the bus!
				static uint8_t output_short = 0; //Used to limit the output switching frequency
				if(check_pos_AC() || check_neg_AC()){
					board_bridge_en_brake_open();
					if(output_short == 1){
						skip_rem = skip_ref;
						output_short = 0;
					}
				}else{
					board_bridge_en_brake_short();
					if(output_short == 0){
						skip_rem = skip_ref;
						output_short = 1;
					}
				}
			}
			break;
		case cc_state_off:
			if(skip_rem){
				skip_rem--;
			}
			board_bridge_en_brake_open();
			break;
		case cc_state_undefined:	//If output_short is undefined, set h-bridge into a safe output_short.
			board_bridge_en_brake_open();
			break;
		default:					//Should never occur
			board_bridge_en_brake_open();
			return;
	}
}