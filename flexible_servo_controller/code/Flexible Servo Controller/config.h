#ifndef CONFIG_H_
#define CONFIG_H_
/************************************************************************/
/* Global configuration for various compile-time attributes				*/
/************************************************************************/

#include <stdint.h>
#include <float.h>

// UART configuration. Follows table 23-5 in Xmega datasheet, CLK2X = 0.
// Settings for a baud rate of 115200
// #define UART_BSEL 131
// #define UART_BSCALE -3
// Settings for a baud rate of 57600
#define UART_BSEL 135
#define UART_BSCALE -2

// Undervoltage/overvoltage hysteresis level [V]
// Undervoltage lockout disabled above VMIN_LOCKOUT + VBUS_HYST
// Overvoltage lockout disabled below VMAX_LOCKOUT - VBUS_HYST
#define VBUS_HYST	2.0

// Prescaler for CC and PID controllers and systime timer
#define CONTROL_PRESCALE TC_CLKSEL_DIV8_gc
#define CONTROL_PRESCALE_VALUE	8	// NOTE: **MUST** be defined to the prescaler value that CONTROL_PRESCALE sets!
//Keep in mind that periods too short in one and/or both *will* hang the device! Ensure there's margin!
#define CONTROL_CC_TOP	19			// Sets the current controller period
#define CONTROL_CC_FREQ (F_CPU / (CONTROL_PRESCALE_VALUE * (CONTROL_CC_TOP+1)))
#define CONTROL_PID_TOP 1999			// Sets the PID controller and systime period
#define CONTROL_PID_FREQ (F_CPU / (CONTROL_PRESCALE_VALUE * (CONTROL_PID_TOP+1)))

// Timeout period for charge pump, t = (CPUMP_PRESCALE * CPUMP_TIMEOUT)/F_CPU
#define CPUMP_TIMEOUT	313
#define CPUMP_PRESCALE_bm TC_CLKSEL_DIV256_gc	//Be sure to update this if changing CPUMP_PRESCALE!
#define CPUMP_PRESCALE	256						//Be sure to update this if changing CPUMP_PRESCALE_bm!
// Charge pump timeout time.
#define CPUMP_THR_FREQ (F_CPU / (2UL * CPUMP_PRESCALE * CPUMP_TIMEOUT))
// Safety factor for recommended minimum charge pump frequency
#define CPUMP_REC_FREQ (CPUMP_THR_FREQ * 2)
// Minimum number of charge pump ticks required for transition to an active state
// At "high" frequencies (above CONTROL_PID_FREQ/2) some pump ticks are ignored, the maximum delay time is CPUMP_TICKS_THR / CONTROL_PID_FREQ
#define CPUMP_TICKS_THR	5

//Motor time-constant calculation period, period is by MOTOR_TAU_DIV / CONTROL_PID_FREQ seconds
#define MOTOR_TAU_DIV	200

// Digital filter for encoder input. Maximum input frequency is 32MHz / nSAMPLES
// Valid values are EVSYS_DIGFILT_1SAMPLE_gc through EVSYS_DIGFILT_8SAMPLES_gc
#define ENC_FILT_gc	EVSYS_DIGFILT_8SAMPLES_gc	
// Digital filter for reference input. Similar to ENC_FILT_gc. Keep ADuM1201 (digital isolator) bandwidth into account!
#define REF_FILT_gc EVSYS_DIGFILT_8SAMPLES_gc

// Parameter default settings
#define I_MAX_D			5	//Default maximum current
#define I_RIPPLE_D		0	//Default ripple current
#define I_SKIP_D		3	//Default current controller skip value
#define I_OFFSET_D		0	//Default offset current
#define I_FRICTION_D	0	//Default friction current
#define INPUT_POW_D		0	//Default input power factor, where the input is scaled by 2^INPUT_SCALER * INPUT_STEPS
#define CPUMP_D			1	//Default input charge pump state (1 -> charge pump active, 0 -> inactive)
#define BRAKE_D			1	//Default brake choice (1 -> brake enabled, 0 -> brake disabled)
#define OVERVOLT_D		45	//Default overvoltage cutoff level
#define UNDERVOLT_D		8	//Default undervoltage cutoff level
#define KD_D			0	//Default Kd value
#define KDF_D			0.83	//Default Kdf value
#define KP_D			0	//Default Kp value
#define KI_D			0	//Default Ki value
#define TRK_ERR_D		1000	//Default maximum tracking error
#define I_NOM_D			1.5		//Default nominal current
#define MOTOR_TAU_D		10		//Default motor temperature time constant
#define HIGH_I_D		0	//Default high-current-mode state

// Parameter min/max settings
#define I_MAX_MAX		25	//Absolute maximum current
#define I_MAX_MIN		1
#define I_RIPPLE_MAX	50	//Maximum ripple current
#define I_SKIP_MAX		100	//Maximum current controller divisor factor.
#define I_OFFSET_MAX	5	//Maximum offset current calibration value
#define I_OFFSET_MIN	-I_OFFSET_MAX
#define I_FRICTION_MAX	25	//Maximum friction current calibration value
#define I_FRICTION_MIN	-I_FRICTION_MAX
#define INPUT_POW_MIN	0	//Minimum input scaling value
#define INPUT_POW_MAX	6	//Maximum input scaling value
#define OVERVOLT_MAX	50	//Maximum overvoltage cutoff
#define UNDERVOLT_MIN	8	//Minimum undervoltage cutoff
#define OVERVOLT_MIN	(UNDERVOLT_MIN + VBUS_HYST)	//Minimum overvoltage cutoff
#define UNDERVOLT_MAX	(OVERVOLT_MAX - VBUS_HYST)	//Maximum undervoltage cutoff
#define KD_MAX			FLT_MAX	//Maxmimum KD value
#define KD_MIN			0		//Minimum KD value
#define KDF_MIN			1e-3		//Minimum KDF parameter
#define KDF_MAX			(1-KDF_MIN)		//Maximum KDF parameter
#define KP_MAX			FLT_MAX	//Maximum KP value
#define KP_MIN			0		//Minimum KP value
#define KI_MAX			FLT_MAX	//Maximum KI value
#define KI_MIN			0		//Minimum KI value
#define TRK_ERR_MIN		0		//Minimum maximum tracking error
#define TRK_ERR_MAX		(INT16_MAX/2-1)	//Maximum maximum tracking error
#define I_PEAK_MIN		0		//Minimum peak current
#define I_PEAK_MAX		25		//Maximum peak current
#define I_NOM_MIN		0		//Minimum nominal current
#define I_NOM_MAX		25		//Maximum nominal current
#define MOTOR_TAU_MIN	1		//Minimum motor time constant
#define MOTOR_TAU_MAX	100		//Maximum motor time constant

// Parameter titles. **THESE MUST** these must be <= NV_SETTINGS_NAME_LEN, excluding null terminator!
#define TITLE_I_MAX		"i_max"
#define TITLE_I_RIPPLE	"i_ripple"
#define TITLE_I_SKIP	"i_skip"
#define TITLE_I_OFFSET	"i_offset"
#define TITLE_I_FRICTION	"i_fric"
#define TITLE_INPUT_POW "inp_pow"
#define TITLE_CPUMP		"cpump_en"
#define TITLE_BRAKE		"brake_en"
#define TITLE_UNDERVOLT	"v_min"
#define TITLE_OVERVOLT	"v_max"
#define TITLE_KD		"k_d"
#define TITLE_KDF		"k_df"
#define TITLE_KP		"k_p"
#define TITLE_KI		"k_i"
#define TITLE_TRK_ERR	"trk_err"
#define TITLE_I_PEAK	"i_peak"
#define TITLE_T_PEAK	"t_peak"
#define TITLE_I_NOM		"i_nom"
#define TITLE_MOTOR_TAU	"motor_tc"
#define TITLE_HIGH_I	"high_i_en"

/** @brief Calibration current decay delay time [milliseconds]. The calibration routine waits this long before starting. */
//This time must be large enough for the current in the load to decay to zero
#define CAL_DELAY			100

/** @brief Default tune plot height (lines) */
#define TUNE_H_D	23

/** @brief Length of buffers and output columns in terminal when tuning */
#define TUNE_N_SAMPLES	80
/** @brief Various plot-related settings */
#define TUNE_XTICKS		5	        //<!- Approximate number of ticks to place along X-axis
#define TUNE_YTICK_DENSITY		5	//<!- Approximate number of ticks to place along Y-axis
#define TUNE_ANTIALIAS	0	        //<!- Set nonzero (and enable AA in plot.h) to enable psuedo-antialiasing on the plots (better horizontal resolution but more crowded display)

/** @brief Characters to use in tuning plot for representing the different traces */
#define TUNE_REF_CHAR	'R'
#define TUNE_ERR_CHAR	'E'
#define TUNE_PLANT_CHAR	'P'
#define TUNE_DERIV_CHAR	'D'
#define TUNE_PROP_CHAR	'P'
#define TUNE_INTEGRAL_CHAR	'I'
#define TUNE_TOTAL_CHAR	'T'

/** @brief Number of samples to keep at zero on a tune before the movement starts (and after for velocity and acceleration steps) */
#define TUNE_STEP_DELAY	4

/** @brief Define to periodically print the number of unused bytes of RAM to the output */
//#define STACKMSG

// Various string literals
#define WDT_ERRMSG	"\nSystem lockup (watchdog overflow).\nRestart (power cycle) to resore functionality."

#define STARTUP_CRCFAIL "\
NOTE! CRC check failed; stored settings corrupt.\n\
\tStored CRC: 0x%04X\n\
\tCalcd. CRC: 0x%04X\n\
Start up in config mode and press enter to load default settings.\n\
(Perform a calibration before operation!)"

#define STARTUP_CRCPASS "CRC check passed with checksum 0x%04X.\n"

#define STARTUP_ERRLOG_ERRS "NOTE! %u critical error(s) logged. Run 'log' to list them.\n"

#define STARTUP_ERRLOG_NOERRS "No errors logged.\n"

#define SW_VERSION	"0.1"

#define SW_DATE		__DATE__

#define STARTUP_TEXT   "\n  ___      _    _    _   __  __          _   _\n\
 | _ \\__ _| |__(_)__| | |  \\/  |__ _ _ _| |_(_)___\n\
 |   / _` | '_ \\ / _` | | |\\/| / _` | ' \\  _| (_-<\n\
 |_|_\\__,_|_.__/_\\__,_| |_|  |_\\__,_|_||_\\__|_/__/\n\
| __| |_____ _(_) |__| |___  / __| ___ _ ___ _____\n\
| _|| / -_) \\ / | '_ \\ / -_) \\__ \\/ -_) '_\\ V / _ \\\n\
|_| |_\\___/_\\_\\_|_.__/_\\___| |___/\\___|_|  \\_/\\___/\n\
\n\
Version "SW_VERSION", "SW_DATE". http://www.rabidmantis.se\n\
Type 'help' and press enter for a list of commands.\n\
Be sure to save settings when done!\n\n"	//Comment highlighting in Atmel studio incorrect, "//" is valid in a string literal.

#define STARTUP_CONFIG "Started up in config mode. RX input enabled.\n"
#define STARTUP_NOCONFIG "Started up in normal mode. RX input disabled.\n"

//ASCII Art generated by http://patorjk.com/software/taag/#p=display&f=Small&t=%20Rabid%20Mantis%0AFlexible%20Servo

//Check configured parameters for validity
#if I_SKIP_MAX >= 1<<8 - 1
#error "I_SKIP_MAX" must be >= 255!
#endif

#if I_SKIP_D >= 1<<8 - 1
#error I_SKIP_D must be >= 255!
#endif

#if INPUT_POW_MAX > 8
#error INPUT_POW_MAX must be reduced!
#endif

#if MOTOR_TAU_DIV > UINT8_MAX
#error MOTOR_TAU_DIV must be reduced!
#endif

#if TUNE_ANTIALIAS
#ifndef PLOT_AA
#error PLOT_AA must be defined in plot.c!
#endif
#endif

#endif /* CONFIG_H_ */