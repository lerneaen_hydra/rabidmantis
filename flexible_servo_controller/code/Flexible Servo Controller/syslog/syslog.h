/*
 * syslog.h
 * 
 * Rudimentary system log-like support. Stores a control word in an EEPROM/RAM
 * circular buffer and has a simple output function that prints a more 
 * extensive text in flash to stdout.
 *
 * NOTE: These functions are **NOT** re-entrant! Do **NOT** run from multiple
 * locations (such as in a main loop and an interrupt).
 *
 * Total EEPROM and RAM use is 2*DYNBUF_INDEXTYPE + sizeof(syslog_t)*SYSLOG_LEN
 *
 * Updates are written to EEPROM immediately if there is space free, printed
 * output is written on a FIFO basis.
 *
 * Created: 5/11/2013 4:19:05 PM
 *  Author: VBOX
 */ 


#ifndef SYSLOG_H_
#define SYSLOG_H_

#include <stdint.h>
#include "../dynbuf/dynbuf.h"

/** @brief Define to reset the WDT when writing to EEPROM */
#define SYSLOG_WDT

/** @brief Size of log, up to SYSLOG_LEN - 1 entries can be stored */
#define SYSLOG_LEN	16

/** @brief Size of each element in syslog, syslog_control_t must fit in this */
#define syslog_t uint8_t

/** @brief Define to print help text on adding log data */
#define SYSLOG_PRINT_ON_ADD

/** @brief Define to block repetitions of the same entry in the log */
// Note: Blocks patterns of type A,A,A... only, will not affect A,B,A,B... or
// A,B,C,A,B... at all.
#define SYSLOG_ANTISPAM

/**
 * @brief Control words for system log. **MUST** start at zero!
 * Text to print is defined in syslog.c and must be in the same order!
 */
enum syslog_control_t {syslog_short = 0,\
					syslog_overheat_bridge,\
					syslog_bridge_undervoltage,\
					syslog_bus_overvoltage,\
					syslog_sensor_overcurrent,\
					syslog_wdt_overflow};

/**
 * @brief Initializes the system log (reads EEPROM data to RAM).
 * Must be run, once, before the syslog is operational.
 */
void syslog_init(void);

/**
 * @brief Prints the text corresponding to the n'th newest entry in the log.
 * Tries to match the control code to the array of stored descriptive texts. If
 * the control code is larger than the number of descriptive texts or n is 
 * larger than the number of entries nothing is output.
 * @param n The event to read, where 0 is the newest and m is the oldest.
 */
void syslog_print(syslog_t n);

/**
 * @brief Adds a new event to the log, if there's space and writes to EEPROM
 * @param event Event control word.
 */
void syslog_add(enum syslog_control_t new_event);

/**
 * @brief Checks if there's space to add a new event to the log
 * Returns The number of free spaces in the log.
 */
syslog_t syslog_query_add(void);

/**
 * @brief Checks if there is anything stored in the log.
 * Returns The number of entries in the log.
 */
syslog_t syslog_query_read(void);

/**
 * @brief Reads the n'th newest event from the log. Does not modify the log.
 * @param n The event to read, where 0 is the newest and m is the oldest.
 * @return Returns data from the log. Returns 0 if there was nothing to read
 *		or m exceeds the number of entries.
 */
enum syslog_control_t syslog_read(syslog_t n);

/**
 * @brief Clears the oldest entry from the log.
 */
void syslog_clear_oldest(void);

/**
 * @brief Clears the entire log.
 */
void syslog_clear_all(void);

#endif /* SYSLOG_H_ */