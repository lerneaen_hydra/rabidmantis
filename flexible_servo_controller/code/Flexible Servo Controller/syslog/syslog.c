/*
 * syslog.c
 *
 * Created: 5/11/2013 4:19:17 PM
 *  Author: VBOX
 */ 

#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include "syslog.h"
#include "../dynbuf/dynbuf.h"
#include "../shell/terminal.h"

/** Define texts that correspond to the commands in syslog_control_t */
static const char PROGMEM syslog_0[] = "ERR0: Output short-circuit\n";
static const char PROGMEM syslog_1[] = "ERR1: H-bridge overheat\n";
static const char PROGMEM syslog_2[] = "ERR2: H-bridge undervoltage\n";
static const char PROGMEM syslog_3[] = "ERR3: Bus overvoltage\n";
static const char PROGMEM syslog_4[] = "ERR4: Output overcurrent (through current sensor).\nRestart (power cycle) to restore functionality.\n";
static const char PROGMEM syslog_5[] = "ERR5: System lockup (watchdog timer overflow)\n";

/** Place texts in the same order as the types in syslog_control_t, with NULL for no text */
static const char* const PROGMEM syslog_texts[] = {syslog_0,\
													syslog_1,\
													syslog_2,\
													syslog_3,\
													syslog_4,\
													syslog_5};

static dynbuf_t syslog_ram = {0,0,SYSLOG_LEN,sizeof(syslog_t),{[0 ... SYSLOG_LEN * sizeof(syslog_t)] = 0}};
static dynbuf_t EEMEM syslog_eeprom = {0,0,SYSLOG_LEN,sizeof(syslog_t),{[0 ... SYSLOG_LEN * sizeof(syslog_t)] = 0}};

#define SYSLOG_SIZE (sizeof(dynbuf_t) + SYSLOG_LEN * sizeof(syslog_t))

// Lifted from http://www.pixelbeat.org/programming/gcc/static_assert.html
/* Note we need the 2 concats below because arguments to ##
 * are not expanded, so we need to expand __LINE__ with one indirection
 * before doing the actual concatenation. */
#define ASSERT_CONCAT_(a, b) a##b
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
#define ct_assert(e) enum { ASSERT_CONCAT(assert_line_, __LINE__) = 1/(!!(e)) }

//syslog_ram and syslog_eeprom **must** be the same size! This gives an error otherwise.
ct_assert(sizeof(syslog_ram) == sizeof(syslog_eeprom));

/** @brief Internal function that copies the the RAM log to the EEPROM log */
static void syslog_ram2eeprom(void){
//	printf_P(PSTR("Saving log with addresses and data;\n"));
	const void* src = (void*) &syslog_ram;
	void* dst = (void*) &syslog_eeprom;
	size_t n = SYSLOG_SIZE;
	for(size_t i = 0; i < n; i++){
//		printf_P(PSTR("	ram:%u, eeprom;%u, val %x\n"),src,dst,*(uint8_t*)src);
		eeprom_update_block(src++,dst++,1);
		#ifdef SYSLOG_WDT
		wdt_reset();
		#endif
	}
}

/** @brief Internal function that copies the the EEPROM log to the RAM log */
static void syslog_eeprom2ram(void){
	eeprom_read_block((void*)&syslog_ram,(void*)&syslog_eeprom,SYSLOG_SIZE);
/*
	//Verbose equivalent, display addresses and content
	printf_P(PSTR("Loading log with addresses and data;\n"));
	void* src = (void*) &syslog_ram;
	const void* dst = (void*) &syslog_eeprom;
	size_t n = SYSLOG_SIZE;
	for(size_t i = 0; i < n; i++){
		eeprom_read_block(src,dst,1);
		printf_P(PSTR("	ram;%u, eeprom; %u, val %x\n"),src,dst,*(uint8_t*)src);
		src++;
		dst++;
	}	
*/	
}

void syslog_init(void){
	syslog_eeprom2ram();
}

void syslog_print(syslog_t n){
	if(n < dynbuf_query_numel_used(&syslog_ram)){
		enum syslog_control_t control = *(enum syslog_control_t*) dynbuf_peek_n(n, &syslog_ram);
		if( ((syslog_t) control) < sizeof(syslog_texts)/sizeof(char*) && pgm_read_word(&syslog_texts[control] != NULL)){
			printf_P((char*)pgm_read_word(&syslog_texts[control]));
		}
	}
}

void syslog_add(enum syslog_control_t new_event){
	#ifdef SYSLOG_ANTISPAM
	if(dynbuf_query_read(&syslog_ram)){
		enum syslog_control_t last_event = *(enum syslog_control_t*) dynbuf_peek_newest(&syslog_ram);
		if(last_event == new_event){
			return;
		}			
	}
	#endif
	if(dynbuf_query_write(&syslog_ram) && ((syslog_t)new_event) < sizeof(syslog_texts)/sizeof(char*)){
		dynbuf_write(&new_event,&syslog_ram);
		#ifdef SYSLOG_PRINT_ON_ADD
		syslog_print(0);
		#endif
		syslog_ram2eeprom();
	}
}

syslog_t syslog_query_add(void){
	return dynbuf_query_numel_free(&syslog_ram);
}

syslog_t syslog_query_read(void){
	return dynbuf_query_numel_used(&syslog_ram);
}

enum syslog_control_t syslog_read(syslog_t n){
	enum syslog_control_t retval = 0;
	if(n < dynbuf_query_numel_used(&syslog_ram)){
		retval = *(enum syslog_control_t*) dynbuf_peek_n(n,&syslog_ram);
	}
	return retval;
}

void syslog_clear_oldest(void){
	if(dynbuf_query_read(&syslog_ram)){
		dynbuf_read(&syslog_ram);
		syslog_ram2eeprom();
	}
}

void syslog_clear_all(void){
	dynbuf_reset(&syslog_ram);
	syslog_ram2eeprom();
}
