/*
 * state.h
 * Global state-machine for servo-controller.
 * Checks the enable and fault inputs, going into one of five different states
 * accordingly. Also handles the system time functions, used for short-term
 * timing.
 * Created: 5/16/2013 12:26:07 PM
 *  Author: VBOX
 */ 


#ifndef STATE_H_
#define STATE_H_

#include <inttypes.h>

/**
 * @brief System states available
 * state_startup is the startup (pre state_init()) state.
 * state_idle is the "normal" state when the output is not active
 * state_active is the "normal" state when the output is active
 * state_fault is the state transitioned to when a fault is detected, which moves to state_idle_latch when the fault is removed
 * state_idle_latch waits for the enable input to be false before transitioning to the state_idle state (to force re-asserting the enable input)
 */
enum state_t {state_startup = 0, state_idle, state_idle_latch, state_active, state_fault};

/** @brief Initializes the state-machine. Must be run after the EEPROM settings are loaded and before any action can be taken */
void state_init(void);

/** @brief Updates the system state and performs associated actions based on recent changes */
void state_update(void);

/** @brief Returns the current system state */
enum state_t state_get_state(void);

/** @brief Prints any queued state transition messages if available */
//This is intended to run in the main idle loop or other equivalent low-priority location
void state_print();

/** @brief Returns the current systime, one tick defined as controller period (config.h) */
//This is to be used for limited short-term time checks. This overflows every 255 systicks.
uint8_t get_systime(void);

/** @brief Sets the long-term i^2*t overcurrent state to oc_state, where nonzero implies overcurrent */
void state_set_i2t_oc(uint8_t oc_state);

#endif /* STATE_H_ */