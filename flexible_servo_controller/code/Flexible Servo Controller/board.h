#ifndef BOARD_H
#define BOARD_H

/************************************************************************/
/* Fuse settings for device. Be sure MCU is programmed with these fuses!*/
/************************************************************************
WDWP = 8CLK
WDP = 512CLK
BOOTRST = APPLICATION
TOSCSEL = XTAL
BODPD = CONTINUOUS
RSTDISBL = [X]
SUT = 64MS
WDLOCK = [X]
BODACT = CONTINUOUS
EESAVE = [ ]
BODLVL = 2V8

FUSEBYTE1 = 0x06 (valid)
FUSEBYTE2 = 0xFE (valid)
FUSEBYTE4 = 0xE1 (valid)
FUSEBYTE5 = 0xE9 (valid)
*/

/************************************************************************/
/* Board pinout, fuse, startup, pin-level interrupt, timer defs, and	*/
/* component mounting choices.											*/
/************************************************************************/

#define LED_PORT			PORTE
#define LED_FAULT_bm		PIN0_bm
#define LED_ENABLE_bm		PIN1_bm
#define LED_PWR_bm			PIN2_bm

#define CONFIG_PORT			PORTE
#define CONFIG_PIN_bm		PIN3_bm
#define CONFIG_PINCTRL		PIN3CTRL

#define BRIDGE_PORT			PORTC
#define BRIDGE_PWMH_bm		PIN0_bm
#define BRIDGE_PWML_bm		PIN1_bm
#define BRIDGE_PHASE_bm		PIN2_bm
#define BRIDGE_SR_bm		PIN3_bm
#define BRIDGE_RESET_bm		PIN4_bm
#define BRIDGE_PWMH_PINCTRL PIN0CTRL
#define BRIDGE_PWML_PINCTRL PIN1CTRL
#define BRIDGE_PHASE_PINCTRL PIN2CTRL
#define BRIDGE_SR_PINCTRL	PIN3CTRL
#define BRIDGE_RESET_PINCTRL PIN4CTRL

#define DAC_PORT			PORTB
#define DAC0_PINCTRL		PIN2CTRL
#define DAC1_PINCTRL		PIN3CTRL

#define ANALOG_PORT			PORTA
#define ADC_PINCTRL			PIN0CTRL
#define ACA_PINCTRL			PIN1CTRL	
#define ACB_PINCTRL			PIN2CTRL
#define ACC_PINCTRL			PIN3CTRL
#define ACD_PINCTRL			PIN5CTRL

#define ENC_PORT			PORTC
#define ENC_A_bm			PIN5_bm
#define ENC_A_PINCTRL		PIN5CTRL
#define	ENC_A_PINMUX		EVSYS_CHMUX_PORTC_PIN5_gc
#define ENC_B_bm			PIN6_bm
#define ENC_B_PINCTRL		PIN6CTRL
#define ENC_B_PINCTRL		PIN6CTRL
#define ENC_Z_bm			PIN7_bm
#define ENC_Z_PINCTRL		PIN7CTRL

#define FF_PORT				PORTR
#define FF_1_bm				PIN1_bm
#define FF_2_bm				PIN0_bm
#define FF_1_PINCTRL		PIN1CTRL
#define FF_2_PINCTRL		PIN0CTRL

#define OVERCURRENT_PORT	PORTA
#define OVERCURRENT_PIN_bm	PIN6_bm
#define OVERCURRENT_PINCTRL PIN6CTRL

#define IO_PORT				PORTD
#define IO_A_bm				PIN0_bm
#define IO_A_PINCTRL		PIN0CTRL
#define IO_A_PINMUX			EVSYS_CHMUX_PORTD_PIN0_gc
#define IO_B_bm				PIN1_bm
#define IO_B_PINCTRL		PIN1CTRL
#define IO_IDX_bm			PIN2_bm
#define IO_ACTIVE_bm		PIN3_bm
#define IO_FAULT_bm			PIN4_bm
#define IO_CP_bm			PIN5_bm
#define IO_CP_PINCTRL		PIN5CTRL
#define IO_CP_PINMUX		EVSYS_CHMUX_PORTD_PIN5_gc
#define IO_RX_bm			PIN6_bm
#define IO_TX_bm			PIN7_bm

#define USART_PORT			USARTD1

//Unused pins to activate pulldown resistors for
#define UNUSED_A_0			PIN4CTRL
#define UNUSED_A_1			PIN7CTRL
#define UNUSED_B_0			PIN0CTRL
#define UNUSED_B_1			PIN1CTRL

//Event-system channel mapping
#define EVSYS_ENCMUX		EVSYS_CH0MUX
#define EVSYS_ENCCTRL		EVSYS_CH0CTRL
#define EVSYS_ENC_TIMERCH	TC_CLKSEL_EVCH0_gc
// Channel 1 also used for quadrature decoding
#define EVSYS_REFMUX		EVSYS_CH2MUX
#define EVSYS_REFCTRL		EVSYS_CH2CTRL
#define EVSYS_REF_TIMERCH	TC_CLKSEL_EVCH2_gc
// Channel 3 also used for quadrature decoding
#define EVSYS_CPUMPMUX		EVSYS_CH4MUX
#define EVSYS_CPUMPCTRL		EVSYS_CH4CTRL
#define EVSYS_CPUMP_TIMERCH	TC_EVSEL_CH4_gc

//Timer/vector mapping
#define CC_TIMER			TCC0
#define CC_VECT				TCC0_CCA_vect
#define SYSTIME_TIMER		TCC1
#define SYSTIME_VECT		TCC1_CCA_vect
#define ENC_TIMER			TCD0
#define REF_TIMER			TCD1
#define CPUMP_TIMER			TCE0
#define CPUMP_TIMEOUT_VECT	TCE0_CCA_vect
#define CPUMP_TRANS_VECT	PORTD_INT0_vect
#define ENC_IDX_VECT		PORTC_INT0_vect

/** @brief Set to the scaling factor (voltage divider) that drives the bus voltage ADC input */
#define BOARD_VBUS_DIVIDER (10.0/(10.0+560.0))
/** @brief Typical voltage drop over reverse polarity protection diode */
#define BOARD_DIODE_DROP 0.4
// Scaling factor for VBUS measurement. Expressed in VBUS/ADC_COUNT [V].
#define BOARD_VBUS_SCALING (1.0/(BOARD_VBUS_DIVIDER * 0x7FF))
// Macro to convert ADC counts to bus voltage [V]
#define BOARD_ADC2VBUS(x) ((x * BOARD_VBUS_SCALING) + BOARD_DIODE_DROP)

#include <stddef.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include "config.h"

/** @brief Reads a calibration bye from the NVM. Example; ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,DACB0GAINCAL)); */
inline uint8_t ReadCalibrationByte(uint8_t index){
	uint8_t retval;

	/* Load the NVM Command register to read the calibration row. */
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	retval = pgm_read_byte(index);

	/* Clean up NVM Command register. */
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	return retval;
}

/** @brief Sets up the hardware for the controller
 * Sets up the DAC, comparators, timers for current controller and PID
 * controller, and pin directions for the H-bridge controller
 */
static inline void board_init_controller(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
	//Set up the DAC, enable outputs 0 and 1
	//Positive current is current that flows from OUT_POS to OUT_NEG, which causes a *decrease* in the sensor output voltage
	DACB.CTRLA |= DAC_CH1EN_bm | DAC_CH0EN_bm | DAC_ENABLE_bm;
	DACB.CTRLB |= DAC_CHSEL_DUAL_gc;
	DACB.CTRLC |= DAC_REFSEL_AVCC_gc;
	DACB.CH0GAINCAL = ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,DACB0GAINCAL));
	DACB.CH1GAINCAL = ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,DACB0GAINCAL)); // DACB1GAINCAL is not factory calibrated (set to 0xFF) so use DACB0GAINCAL
	DACB.CH0OFFSETCAL = ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,DACB0OFFCAL));
	DACB.CH0OFFSETCAL = ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,DACB0OFFCAL)); //Similarly for the offset
	//Set up the comparators, high-speed mode, no hysteresis (polled operation)
	ACA.AC0CTRL |= AC_HSMODE_bm | AC_ENABLE_bm;
	ACA.AC1CTRL |= AC_HSMODE_bm | AC_ENABLE_bm;

	#define CHECK_AC_POS() (ACA.STATUS & AC_AC0STATE_bm)	//Define to a macro that returns nonzero when I_MEAS > I_SET_POS
	#define CHECK_AC_NEG() (ACA.STATUS & AC_AC1STATE_bm)	//Define to a macro that returns nonzero when I_MEAS < I_SET_NEG
		
	/** @brief Scaling factor for DAC output. Expressed in LOAD_CURRENT/DAC_COUNT [A]. (DAC_COUNT = CC_SCALING * I_OUT + CC_ZERO) */
	#define CC_SCALING	(-1 * ((1<<12) - 1) / (60.0))
	/** @brief Zero current offset. If DAC = CC_ZERO then I_OUT = 0 */
	#define CC_ZERO		((1<<11) - 1)
	/** @brief Maximum DAC value */
	#define DAC_MAXVAL	((1<<12) - 1)
	/** @brief Minimum DAC value */
	#define DAC_MINVAL	0

	ACA.AC0MUXCTRL |= AC_MUXPOS_PIN5_gc | AC_MUXNEG_PIN1_gc;	//Comparator 0 connected to DAC 0, output high when I_MEAS > I_SET_DAC_0
	ACA.AC1MUXCTRL |= AC_MUXPOS_PIN2_gc | AC_MUXNEG_PIN3_gc;	//Comparator 1 connected to DAC 1, output high when I_MEAS < I_SET_DAC_1
	
	//Set up ADCA to measure the bus voltage.
	ADCA.CTRLA |= ADC_ENABLE_bm;
	ADCA.CTRLB |= ADC_CONMODE_bm | ADC_FREERUN_bm;	//Freerunning signed mode
	ADCA.REFCTRL |= ADC_REFSEL_INT1V_gc | ADC_BANDGAP_bm;	//Select 1V bandgap reference and enable it.
	ADCA.EVCTRL |= ADC_SWEEP_012_gc;
	ADCA.PRESCALER |= ADC_PRESCALER_DIV512_gc;	//Divide by 256, giving an ADC clock of 125 kHz
	ADCA.CALL = ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,ADCACAL0));
	ADCA.CALH = ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,ADCACAL1));		
	
	ADCA.CH0.CTRL |= ADC_CH_INPUTMODE_SINGLEENDED_gc;	//Single-ended signed mode
	ADCA.CH0.MUXCTRL |= ADC_CH_MUXPOS_PIN0_gc;			//Positive input on pin 0 (negative internally grounded)
	
	//Set up timer for current control
	CC_TIMER.CTRLA |= CONTROL_PRESCALE;			//Prescale timer
	CC_TIMER.INTCTRLB |= TC_CCAINTLVL_HI_gc;
	CC_TIMER.CCA = CONTROL_CC_TOP;
	
	//Set up timer for PID control and systime
	SYSTIME_TIMER.CTRLA |= CONTROL_PRESCALE;
	SYSTIME_TIMER.INTCTRLB |= TC_CCAINTLVL_MED_gc;	//Do not change without updating the i_friction calibration routine!
	SYSTIME_TIMER.CCA = CONTROL_PID_TOP;
	
	CC_TIMER.CTRLB |= TC_WGMODE_FRQ_gc;	//Start timers, run in frequency mode
	SYSTIME_TIMER.CTRLB |= TC_WGMODE_FRQ_gc;
	
	}	
}

/** @brief Atomic function to set the positive DAC output */
// As the DAC data is 16-bit this must be atomic!
static inline void set_pos_dac(uint16_t raw){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		DACB.CH0DATA = raw;
	}
}

/** @brief Atomic function to set the negative DAC output */
// As the DAC data is 16-bit this must be atomic!
static inline void set_neg_dac(uint16_t raw){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		DACB.CH1DATA = raw;
	}
}

/** @brief An atomic function that returns the output from the bus voltage ADC */
// As ADCx.CHxRES is 16-bit this must be atomic!
static inline uint16_t check_bus_adc_raw(){
	uint16_t retval;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		retval = ADCA.CH0RES;
	}
	return retval;
}

/** @brief Calculates and returns the current bus voltage */
static inline float query_vbus(){
	return BOARD_ADC2VBUS(((float)check_bus_adc_raw()));
}


/** @brief Sets up the UART hardware.
*/
static inline void board_init_uart(uint8_t rx_en){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		IO_PORT.OUTSET = IO_TX_bm;	//Set output high (idle state)
		USART_PORT.CTRLA |= USART_RXCINTLVL_LO_gc;	//Set interrupt level to low
		USART_PORT.CTRLB |= USART_TXEN_bm;	//Enable TX
		if(rx_en){
			USART_PORT.CTRLB |= USART_RXEN_bm;	//Conditionally enable RX
		}
		USART_PORT.CTRLC |= USART_CHSIZE_8BIT_gc;	//Set up for 8-bit communication
		USART_PORT.BAUDCTRLA |= (uint8_t)(UART_BSEL);
		USART_PORT.BAUDCTRLB |= (UART_BSCALE << 4) | (0x0F & (UART_BSEL >> 8));
	}	
}

/** @brief Sets up all board I/O settings (sets outputs, pullup/down, pin interrupts, etc). */
static inline __attribute__ ((always_inline)) void board_init_io(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
	LED_PORT.DIRSET = LED_FAULT_bm | LED_ENABLE_bm | LED_PWR_bm;	//Set LEDs as outputs
	
	CONFIG_PORT.CONFIG_PINCTRL |= PORT_INVEN_bm | PORT_OPC_PULLUP_gc; //Enable pullup and invert on config pin
	// CONFIG_PORT.DIRSET = CONFIG_PIN_bm;	//configure config_pin as output when used for debugging purposes
	
	IO_PORT.DIRSET = IO_IDX_bm | IO_ACTIVE_bm | IO_FAULT_bm | IO_TX_bm; //Set IO outputs accordingly
	IO_PORT.OUTCLR = IO_IDX_bm | IO_ACTIVE_bm | IO_FAULT_bm | IO_TX_bm; //And force them low
	
	BRIDGE_PORT.DIRSET = BRIDGE_PWMH_bm | BRIDGE_PWML_bm | BRIDGE_PHASE_bm | BRIDGE_SR_bm | BRIDGE_RESET_bm; //Set bridge outputs accordingly
	BRIDGE_PORT.BRIDGE_PWMH_PINCTRL |= PORT_INVEN_bm;	//And set them all to inverted mode (as the level translator in turn inverts the output)
	BRIDGE_PORT.BRIDGE_PWML_PINCTRL |= PORT_INVEN_bm;
	BRIDGE_PORT.BRIDGE_PHASE_PINCTRL |= PORT_INVEN_bm;
	BRIDGE_PORT.BRIDGE_SR_PINCTRL |= PORT_INVEN_bm;
	BRIDGE_PORT.BRIDGE_RESET_PINCTRL |= PORT_INVEN_bm;
	BRIDGE_PORT.OUTSET = BRIDGE_RESET_bm;	//Bring the bridge out of reset...
	BRIDGE_PORT.OUTCLR = BRIDGE_PWMH_bm | BRIDGE_PWML_bm | BRIDGE_PHASE_bm | BRIDGE_SR_bm;//...and place it in an open-circuit state initially

	OVERCURRENT_PORT.OVERCURRENT_PINCTRL |= PORT_INVEN_bm | PORT_OPC_PULLUP_gc; // Set pullup and inverted input on fault pin
	FF_PORT.FF_1_PINCTRL |= PORT_OPC_PULLUP_gc;
	FF_PORT.FF_2_PINCTRL |= PORT_OPC_PULLUP_gc;
	
	DAC_PORT.DAC0_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;		// Disable digital input on analog inputs
	DAC_PORT.DAC1_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;
	ANALOG_PORT.ADC_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;
	ANALOG_PORT.ACA_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;
	ANALOG_PORT.ACB_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;
	ANALOG_PORT.ACC_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;
	ANALOG_PORT.ACD_PINCTRL |= PORT_ISC_INPUT_DISABLE_gc;
	
	PORTA.UNUSED_A_0 |= PORT_OPC_PULLDOWN_gc;	//Set pulldown on unused I/O pins
	PORTA.UNUSED_A_1 |= PORT_OPC_PULLDOWN_gc;
	PORTB.UNUSED_B_0 |= PORT_OPC_PULLDOWN_gc;
	PORTB.UNUSED_B_1 |= PORT_OPC_PULLDOWN_gc;
	
	// Set up encoder input to use the event system for quadrature decoding
	ENC_PORT.ENC_A_PINCTRL |= PORT_ISC_LEVEL_gc | PORT_INVEN_bm;
	ENC_PORT.ENC_B_PINCTRL |= PORT_ISC_LEVEL_gc | PORT_INVEN_bm;
	EVSYS_ENCMUX |= ENC_A_PINMUX;
	EVSYS_ENCCTRL |= EVSYS_QDEN_bm | ENC_FILT_gc;
	ENC_TIMER.CTRLA |= EVSYS_ENC_TIMERCH;
	ENC_TIMER.CTRLD |= TC_EVACT_QDEC_gc | EVSYS_ENC_TIMERCH;
	ENC_TIMER.PER = UINT16_MAX;
	// Set up reference input to use the event system for quadrature decoding
	IO_PORT.IO_A_PINCTRL |= PORT_ISC_LEVEL_gc;
	IO_PORT.IO_B_PINCTRL |= PORT_ISC_LEVEL_gc;
	EVSYS_REFMUX |= IO_A_PINMUX;
	EVSYS_REFCTRL |= EVSYS_QDEN_bm | REF_FILT_gc;
	REF_TIMER.CTRLA |= EVSYS_REF_TIMERCH;
	REF_TIMER.CTRLD |= TC_EVACT_QDEC_gc | EVSYS_REF_TIMERCH;
	REF_TIMER.PER = UINT16_MAX;
	// Set the index input trigger high-priority interrupts on level transitions
	ENC_PORT.ENC_Z_PINCTRL |= PORT_INVEN_bm;
	ENC_PORT.INT0MASK |= ENC_Z_bm;
	ENC_PORT.INTCTRL |= PORT_INT0LVL_HI_gc;
	
	// Set up the charge-pump hardware.
	// The timer is configured to reset on charge pump pin level change. On
	// timer overflow the device is placed in the inactive state. If there are
	// enough level translations without a timeout the device is brought into
	// the active state.
	// Though it is possible to miss calls to both the overflow and transition
	// interrupts, as long as T_CHARGE_PUMP_IN + T_DELAY < T_TIMEOUT this is safe
	CPUMP_TIMER.CTRLA |= CPUMP_PRESCALE_bm;

	CPUMP_TIMER.CTRLB |= TC_WGMODE_FRQ_gc;
	CPUMP_TIMER.CTRLD |= TC_EVACT_RESTART_gc | EVSYS_CPUMP_TIMERCH;
	CPUMP_TIMER.CCA |= CPUMP_TIMEOUT;
	CPUMP_TIMER.INTCTRLB |= TC_CCAINTLVL_MED_gc;
	IO_PORT.INT0MASK |= IO_CP_bm;
	// Interrupts enabled periodically in the systime loop
 	
	 // Set the input pin to trigger events on level change
	EVSYS_CPUMPMUX |= IO_CP_PINMUX;
	EVSYS_CPUMPCTRL |= EVSYS_DIGFILT_8SAMPLES_gc;	//And do some free filtering
	}	
}

/** @brief Enable the CPUMP pin change interrupt */
static inline void board_cpump_pin_int_en(void){
	IO_PORT.INTCTRL = PORT_INT0LVL_MED_gc;	//Directly assigned for atomicity, INT1 not used anyway
}

/** @brief Disable the CPUMP pin change interrupt */
static inline void board_cpump_pin_int_dis(void){
	IO_PORT.INTCTRL = PORT_INT0LVL_OFF_gc;	//Directly assigned for atomicity, INT1 not used anyway
}

/** @brief Returns nonzero if the CPUMP pin is active (used when CPUMP disabled) */
static inline uint8_t board_query_cpump_pin_active(void){
	return !(IO_PORT.IN & IO_CP_bm);
}

/** @brief Sets the index output high */
static inline void board_set_idx_high(void){
	IO_PORT.OUTSET = IO_IDX_bm;
}

/** @brief Sets the index output low */
static inline void board_set_idx_low(void){
	IO_PORT.OUTCLR = IO_IDX_bm;
}

/** @brief Returns nonzero if the index input is set */
static inline uint8_t board_query_idx_in(void){
	return ENC_PORT.IN & ENC_Z_bm;
}

/** @brief Gets the position of the servo encoder */
static inline uint16_t board_query_enc(void){
	uint16_t retval;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		retval = ENC_TIMER.CNT;
	}
	return retval;
}

/** @brief Gets the position of the reference (quadrature) input */
static inline uint16_t board_query_ref(void){
	uint16_t retval;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		retval = REF_TIMER.CNT;
	}
	return retval;
}

/** @brief Resets the reference (quadrature) and servo encoder inputs to zero */
static inline void board_reset_enc_ref(void){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		REF_TIMER.CNT = 0;
		ENC_TIMER.CNT = 0;
	}
}

/** @brief Returns nonzero if the config input is set (shorted by jumper)*/
static inline uint8_t board_query_config_status(void){
	return CONFIG_PORT.IN & CONFIG_PIN_bm;
}

static inline uint8_t board_query_sensor_overcurrent(void){
	return OVERCURRENT_PORT.IN & OVERCURRENT_PIN_bm;
}

enum board_led_t {board_led_pwr = 0, board_led_en, board_led_fault};

/** @brief Sets an LED from the board_led_t enum */
static inline void board_set_led(enum board_led_t led){
	switch(led){
		case board_led_pwr:
			LED_PORT.OUTSET = LED_PWR_bm;
			break;
		case board_led_en:
			LED_PORT.OUTSET = LED_ENABLE_bm;
			break;
		case board_led_fault:
			LED_PORT.OUTSET = LED_FAULT_bm;
			break;
		default:
			break;
	};	
}

/** @brief Clears and LED from the board_led_t enum */
static inline void board_clr_led(enum board_led_t led){
	switch(led){
		case board_led_pwr:
			LED_PORT.OUTCLR = LED_PWR_bm;
			break;
		case board_led_en:
			LED_PORT.OUTCLR = LED_ENABLE_bm;
			break;
		case board_led_fault:
			LED_PORT.OUTCLR = LED_FAULT_bm;
			break;
		default:
			break;	
	};	
}

/** @brief Sets the !FAULT output active */
static inline void board_set_fault_out(void){
	IO_PORT.OUTCLR = IO_FAULT_bm;
}

/** @brief Clears the !FAULT output (no fault) */
static inline void board_clr_fault_out(void){
	IO_PORT.OUTSET = IO_FAULT_bm;
}

/** @brief Sets the 'active' (enable) output */
static inline void board_set_active_out(void){
	IO_PORT.OUTSET = IO_ACTIVE_bm;
}

/** @brief Clears the 'active' (enable) output */
static inline void board_clr_active_out(void){
	IO_PORT.OUTCLR = IO_ACTIVE_bm;
}

/** @brief Checks the fault flag status from the H-bridge driver, LSB is FF1, LSB+1 is FF2. Normal operation is both zero.
*/
#define FF_NOMINAL 0x00
#define FF_SHORT 0x01
#define FF_OVERHEAT 0x02
#define FF_UNDERVOLTAGE 0x03
inline uint8_t board_query_ff_status(void){
	uint8_t ff1 = (FF_PORT.IN & FF_1_bm)?1:0;
	uint8_t ff2 = (FF_PORT.IN & FF_2_bm)?1:0;
	return (ff2<<1 | ff1);
}

#endif