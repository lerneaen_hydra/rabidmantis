#ifndef UTIL_H_
#define UTIL_H_
/* Small utility functions that don't belong anywhere else and don't warrant
an entire .h/.c file pair. */

#include <avr/io.h>
#include <util/atomic.h>
#include <avr/wdt.h>

#include "board.h"

/** @brief Sets the system clock to the 32MHz internal oscillator and enables
 *			the calibration DFLL.
 */
static inline void init_clock(){
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		OSC.CTRL |= OSC_RC32KEN_bm | OSC_RC32MEN_bm;	//Enable 32MHz and 32.768kHz oscillators
		while(!(OSC.STATUS & OSC_RC32KRDY_bm)){};	//Wait for 32khz oscillator...
		while(!(OSC.STATUS & OSC_RC32MRDY_bm)){};	//...and 32MHz oscillator to stabilize
	
		DFLLRC32M.CTRL |= DFLL_ENABLE_bm;	//Enable DFLL for 32MHz oscillator (defaults to 32kHz reference clock)
	
		uint8_t foo = CLK.CTRL | CLK_SCLKSEL_RC32M_gc;
		CCP = CCP_IOREG_gc;					//Changing the clock requires writing to the protect register beforehand...
		CLK.CTRL = foo;						//...and now the clock source is changed
		foo = CLK.LOCK | CLK_LOCK_bm;
		CCP = CCP_IOREG_gc;					//Locking the clock requires writing to the protect register beforehand...
		CLK.LOCK |= foo;					//...and now the clock source is permanently locked.
	}	
}

// This functions used for debugging purposes only!
// Normally the config pin is an input, but can
// be used as an output pin and measured with an
// oscilliscope to for example measure the execution
// time of various ISR's.
// static inline void toggle_config_pin(void){
// 	CONFIG_PORT.OUTTGL = CONFIG_PIN_bm;
// }
// 
// static inline void set_config_pin(void){
// 	CONFIG_PORT.OUTSET = CONFIG_PIN_bm;
// }
// 
// static inline void clr_config_pin(void){
// 	CONFIG_PORT.OUTCLR = CONFIG_PIN_bm;
// }

static inline void set_led(uint8_t num, uint8_t state){
	if(num<=2){
		if(state){
			LED_PORT.OUTSET = 1<<num;
		}else{
			LED_PORT.OUTCLR = 1<<num;
		}
	}
}

#endif /* UTIL_H_ */