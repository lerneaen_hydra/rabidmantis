/*
 * dynbuf.c
 *
 *  Created on: Dec 5, 2011
 *      Author: hydra
 */

#include "dynbuf.h"

void dynbuf_reset(dynbuf_t* buffer_meta){
	buffer_meta->tail=0;
	buffer_meta->head=0;
}

DYNBUF_INDEXTYPE dynbuf_query_numel_used(dynbuf_t* buffer_meta){
	DYNBUF_INDEXTYPE el_free = dynbuf_query_numel_free(buffer_meta);
	return buffer_meta->length - el_free -1;
}

DYNBUF_INDEXTYPE dynbuf_query_numel_free(dynbuf_t* buffer_meta){
	DYNBUF_INDEXTYPE el_free = buffer_meta->length - (buffer_meta->head - buffer_meta->tail + buffer_meta->length)%buffer_meta->length - 1;	//number of free elements in buffer
	return el_free;
}

uint8_t dynbuf_query_write(dynbuf_t* buffer_meta){
	uint8_t retval;
	DYNBUF_INDEXTYPE el_free = dynbuf_query_numel_free(buffer_meta);
	if(el_free){
		retval=1;
	}else{
		retval=0;
	}
	return retval;
}

uint8_t dynbuf_query_read(dynbuf_t* buffer_meta){
	return (buffer_meta->tail!=buffer_meta->head);
}

void dynbuf_write(void *data, dynbuf_t* buffer_meta){
	buffer_meta->head=(buffer_meta->head+1)%buffer_meta->length;	//update position to write to
	for(DYNBUF_INDEXTYPE i=0;i<buffer_meta->data_size;i++){	//copy data from *data to *buffer[head] byte for byte
		buffer_meta->data[ buffer_meta->head * buffer_meta->data_size + i ] = *(uint8_t *)(data+i);
	}
}


void* dynbuf_read(dynbuf_t* buffer_meta){
	buffer_meta->tail = (buffer_meta->tail+1)%buffer_meta->length;
	void* retval = (void*) &buffer_meta->data[0]+((buffer_meta->tail) * buffer_meta->data_size);
	return retval;
}

void* dynbuf_peek(dynbuf_t* buffer_meta){
	void* retval = (void*) &buffer_meta->data[0]+((buffer_meta->tail+1)%buffer_meta->length * buffer_meta->data_size);
	return retval;
}

void* dynbuf_peek_newest(dynbuf_t* buffer_meta){
	void* retval = (void*) &buffer_meta->data[0]+(buffer_meta->head * buffer_meta->data_size);
	return retval;
}

void* dynbuf_read_newest(dynbuf_t* buffer_meta){
	void* retval = (void*) &buffer_meta->data[0]+(buffer_meta->head * buffer_meta->data_size);
	buffer_meta->head = (buffer_meta->head - 1) % buffer_meta->length;
	return retval;
}

void* dynbuf_peek_n(DYNBUF_INDEXTYPE n, dynbuf_t* buffer_meta){
	void* retval = (void*) &buffer_meta->data[0]+( ( (buffer_meta->head - n + buffer_meta->length)%buffer_meta->length ) * buffer_meta->data_size);
	return retval;
}
