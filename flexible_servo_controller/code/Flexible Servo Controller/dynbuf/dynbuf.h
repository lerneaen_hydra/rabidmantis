/*
 * dynbuf.h
 * An improved ring buffer. Each ring buffer can be of different length (up to DYNBUF_INDEXTYPE) and payload type.
 * This is at cost of a more cumbersome interface.
 *
 * Usage example: A buffer with 16 elements of 4 bytes in length (int32_t). Allocate space and use it:
 *
 * static dynbuf_t buffer={0,0,16,4,{[0 ... 16*4]=0}};	//this uses GCC specific functionality!
 * int32_t bar=1;
 * dynbuf_write(&bar,&buffer);
 * bar++;
 * dynbuf_write(&bar,&buffer);
 * bar++;
 * dynbuf_write(&bar,&buffer);
 * bar++;
 *
 * //buffer now contains 1,2,3
 *
 * dynbuf_query_read(&buffer);	//returns zero
 * dynbuf_query_write(&buffer);	//returns nonzero
 * dynbuf_peek(&buffer);		//returns 1
 * dynbuf_peek(&buffer);		//returns 1
 * *(int32_t *)dynbuf_read(&buffer);		//returns 1
 * dynbuf_peek_newest(&buffer); //returns 3
 * *(int32_t *)dynbyf_read(&buffer);		//returns 2
 * dynbuf_query_read(&buffer);	//returns nonzero
 * dynbuf_query_write(&buffer);	//returns nonzero
 * *(int32_t *)dynbuf_read(&buffer);		//returns 3
 * dynbuf_query_read(&buffer);	//returns zero
 * dynbuf_query_write(&buffer);	//returns nonzero
 *
 *  Created on: Dec 5, 2011
 *      Author: hydra
 */

#ifndef DYNBUF_H_
#define DYNBUF_H_

#include <stdint.h>

/**
 * @brief Index type for buffer, choose the smallest type that will contain enough indices in the buffer
 */
#define DYNBUF_INDEXTYPE	uint8_t

/**
 * @brief Index type 2 for buffer, choose smallest type that can contain max(data_size*length)
 */
#define DYNBUF_INDEXTYPE2	uint8_t

/**
 * @brief Struct used for storing variable data for buffer. Be sure to initialize before use!
 */
typedef struct dynbyf_t{
	DYNBUF_INDEXTYPE head;		//set to 0 on init
	DYNBUF_INDEXTYPE tail;		//set to 0 on init
	DYNBUF_INDEXTYPE length;	//set to the number of elements to store
	DYNBUF_INDEXTYPE data_size;	//set to the number of bytes per element
	uint8_t data[]; 			//must be length*data_size bytes in length
} dynbuf_t;

/**
 * @brief Checks whether there is space in a buffer to add another element
 * @param buffer_len	The length of the buffer (indices)
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @return nonzero if there is space free
 */
uint8_t dynbuf_query_write(dynbuf_t* buffer_meta);

/**
 * @brief Checks whether there is something to read from a buffer
 * @param buffer_len	The length of the buffer (indices)
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @return nonzero if there an element to read
 */
uint8_t dynbuf_query_read(dynbuf_t* buffer_meta);

/**
 * @brief Checks the number of elements currently stored in the buffer
 * @param buffer_meta the buffer to check
 * @return the number of data elements stored in the buffer (number of elements of length data_size)
 */
DYNBUF_INDEXTYPE dynbuf_query_numel_used(dynbuf_t* buffer_meta);

/**
 * @brief Checks the number of elements available in the buffer
 * @param buffer_meta the buffer to check
 * @return the number of data elements available in the buffer (number of elements of length data_size)
 */
DYNBUF_INDEXTYPE dynbuf_query_numel_free(dynbuf_t* buffer_meta);

/**
 * @brief Writes data to a buffer
 * @param data			Pointer to the data to write
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @param buffer		Pointer to the buffer to write to
 */
void dynbuf_write(void* data, dynbuf_t* buffer_meta);

/**
 * @brief Reads data from a buffer
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @param buffer		Pointer to the buffer to write to
 * @return				Pointer to data element from buffer
 */
void* dynbuf_read(dynbuf_t* buffer_meta);

/**
 * @brief Reads data from a buffer whithout marking the read data as used
 * @param buffer_meta	Metadata (head/tail) for the buffer
 * @param buffer		Pointer to the buffer to write to
 * @return				Pointer to data element from buffer
 */
void* dynbuf_peek(dynbuf_t* buffer_meta);

/**
 * @brief Reads the newest data from a buffer without marking anything in the buffer as read
 */
void* dynbuf_peek_newest(dynbuf_t* buffer_meta);

/**
 * @brief Reads the newest data from a buffer
 */
void* dynbuf_read_newest(dynbuf_t* buffer_meta);

/**
 * @brief Reads the n'th newest value from a buffer without marking anything in the buffer as read
 * @param n the element to read (values larger than buffer_meta.length will wrap around. Ie if length=4, n=5 will check the same element as n=1). The newest element is element 0
 * @param buffer_meta the buffer to check
 * @return Pointer to n'th data element
 */
void* dynbuf_peek_n(DYNBUF_INDEXTYPE n, dynbuf_t* buffer_meta);

/**
 * @brief Resets the buffer to an initial state (metadata only)
 */
void dynbuf_reset(dynbuf_t* buffer_meta);

#endif /* DYNBUF_H_ */
