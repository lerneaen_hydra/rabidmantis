/*
 * eeprom.c
 *
 *  Created on: Jan 8, 2013
 *      Author: hydra
 */

#include "eeprom.h"

#include <avr/eeprom.h>
#include <util/atomic.h>
#include <string.h>
#include <math.h>

#ifdef EEPROM_CRC_CHECK
#include <util/crc16.h>
#endif

#ifdef EEPROM_RESET_TO_DEFAULT
#include <avr/pgmspace.h>
#endif

#ifdef EEPROM_RESET_WDT
#include <avr/wdt.h>
#endif

#include "nv_settings.h"

#ifdef EEPROM_DEBUG
#include "../shell/shell.h"	//Include what's needed for STDOUT
#endif

/** @brief Working set of user-modifiable variables */
static struct nv_setting_t RAM_params[NV_NUM_PARAM];

/** @brief EEPROM storage container for nonvolatile parameters */
static struct nv_setting_t EEMEM EE_params[NV_NUM_PARAM] = {NV_DEFAULT_VALS};

#ifdef EEPROM_CRC_CHECK
static uint16_t EEMEM EE_crc = EEPROM_INITCRC;
#endif

#ifdef EEPROM_RAID
/** @brief Second, backup, EEPROM storage */
static struct nv_setting_t EEMEM EE_params_2[NV_NUM_PARAM] = {NV_DEFAULT_VALS};
static uint16_t EEMEM EE_crc_2 = EEPROM_INITCRC;
#endif

#ifdef EEPROM_RESET_TO_DEFAULT
/** @brief Compile-time defaults for nonvolatile parameters */
static const struct nv_setting_t PROGMEM PGM_params[NV_NUM_PARAM] = {NV_DEFAULT_VALS};
#endif

#ifdef EEPROM_CRC_CHECK
/**
 * @brief Calculates the CRC of the RAM copy of the user data.
 * @return The CCITT CRC (0x8408 polynomial) of the data.
 */
static uint16_t calc_crc(struct nv_setting_t (*params)[NV_NUM_PARAM]){
	uint8_t *addr = (uint8_t*) params;			//base address of struct
	uint16_t crc = 0xFFFF;					//crc initializer (as per <util/crc16.h>
	while((size_t)addr < (size_t)params + sizeof(RAM_params)/sizeof(uint8_t)){
		crc = _crc_ccitt_update(crc,*addr);
		addr++;
	}
	return crc;
}
#endif

/** @brief Internal function that adds a wdt reset after each byte written to EEPROM */
static void eeprom_wdt_update_block(const void* src, void* dst, size_t n){
	for(size_t i = 0; i < n; i++){
		eeprom_update_block(src++,dst++,1);
		#ifdef EEPROM_RESET_WDT
		wdt_reset();
		#endif
	}
}

uint8_t eeprom_eeprom2ram(void){
	uint8_t retval = 0;
#ifndef EEPROM_RAID
	eeprom_read_block((void*)&RAM_params,(const void*)&EE_params,sizeof(RAM_params));
#ifdef EEPROM_CRC_CHECK
	if(calc_crc(&RAM_params) == eeprom_read_word(&EE_crc)){	//If EEPROM check passed
		retval = 0;
	}else{
		retval = 1;
	}
#endif

#else
	//Check first copy of EEPROM data
	uint8_t state1 = 0;
	eeprom_read_block((void*)&RAM_params,(const void*)&EE_params,sizeof(RAM_params));
	if(calc_crc(&RAM_params) == eeprom_read_word(&EE_crc)){
		state1 = 1;		//first copy is valid
	}
#ifdef EEPROM_DEBUG
	printf_P(PSTR("EEPROM: bank 1: stored CRC: '%x', calculated CRC '%x'\n"),eeprom_read_word(&EE_crc),calc_crc(&RAM_params));
#endif

	//Check second copy of EEPROM data
	uint8_t state2 = 0;
	eeprom_read_block((void*)&RAM_params,(const void*)&EE_params_2,sizeof(RAM_params));
	if(calc_crc(&RAM_params) == eeprom_read_word(&EE_crc_2)){
		state2 = 1;		//second copy is valid
	}
#ifdef EEPROM_DEBUG
	printf_P(PSTR("EEPROM: bank 1: stored CRC: '%x', calculated CRC '%x'\n"),eeprom_read_word(&EE_crc_2),calc_crc(&RAM_params));
#endif

	if(state1 && state2){	//if both copies are valid
#ifdef EEPROM_DEBUG
		printf_P(PSTR("EEPROM: both copies valid. Yay! ^___^\n"));
#endif
		//Load one to ram and return
		eeprom_read_block((void*)&RAM_params,(const void*)&EE_params,sizeof(RAM_params));
		retval = 0;
	}else if(!state1 && state2){	//if first copy is invalid
#ifdef EEPROM_DEBUG
		printf_P(PSTR("EEPROM: first copy invalid, second copied to first\n"));
#endif
		//Load second copy to ram, write to first copy, update CRC.
		eeprom_read_block((void*)&RAM_params,(const void*)&EE_params_2,sizeof(RAM_params));
		eeprom_wdt_update_block((const void*)&RAM_params,(void*)&EE_params,sizeof(RAM_params));
		eeprom_update_word(&EE_crc,calc_crc(&RAM_params));
		retval = 0;
	}else if(state1 && !state2){	//if second copy is invalid
#ifdef EEPROM_DEBUG
		printf_P(PSTR("EEPROM: second copy invalid, first copied to second\n"));
#endif
		//Load first copy to ram, write to second copy, update CRC.
		eeprom_read_block((void*)&RAM_params,(const void*)&EE_params,sizeof(RAM_params));
		eeprom_wdt_update_block((const void*)&RAM_params,(void*)&EE_params_2,sizeof(RAM_params));
		eeprom_update_word(&EE_crc_2,calc_crc(&RAM_params));
		retval = 0;
	}else{	//both copies are invalid. Tough luck! Return...
#ifdef EEPROM_DEBUG
		printf_P(PSTR("EEPROM: both copies invalid.\n"));
#endif
		retval = 1;
	}
#endif
	return retval;
}

#ifdef EEPROM_CRC_CHECK
uint16_t eeprom_get_crc_stored(void){
	return eeprom_read_word(&EE_crc);
}

uint16_t eeprom_get_crc_calc(void){
	return calc_crc(&RAM_params);
}
#endif

void eeprom_ram2eeprom(void){
	eeprom_wdt_update_block((const void*)&RAM_params,(void*)&EE_params,sizeof(RAM_params));
#ifdef EEPROM_CRC_CHECK
	eeprom_update_word(&EE_crc,calc_crc(&RAM_params));
#endif

#ifdef EEPROM_RAID
	eeprom_wdt_update_block((const void*)&RAM_params,(void*)&EE_params_2,sizeof(RAM_params));
	eeprom_update_word(&EE_crc_2,calc_crc(&RAM_params));
#endif

#ifdef EEPROM_DEBUG
	printf_P(PSTR("EEPROM: New data written to bank(s)\n"));
#endif

}

#ifdef EEPROM_RESET_TO_DEFAULT
void eeprom_reset_defaults(void){
	memcpy_P((void*)&RAM_params,(const void*)&PGM_params,sizeof(RAM_params));
	eeprom_ram2eeprom();
}
#endif

/**************************** Parameter API ****************************/

char* setting_get_title(NV_IDX_T idx){
	char* retval = NULL;
	if(idx < NV_NUM_PARAM){
		retval = &RAM_params[idx].title[0];	
	}
	return retval;
}

NV_IDX_T setting_get_idx(char* title){
	NV_IDX_T retval = -1;
	for(NV_IDX_T i = 0; i < NV_NUM_PARAM ; i++){
		if(strcmp(title,&RAM_params[i].title[0]) == 0){
			retval = i;
		}
	}
	return retval;
}

char setting_get_type_idx(NV_IDX_T idx){
	char retval = SETTING_INVALID_T;
	if(idx < NV_NUM_PARAM){
		switch(RAM_params[idx].nv_type){
			case REAL:
				retval = SETTING_REAL_T;
				break;
			case INTEGER:
				retval = SETTING_INTEGER_T;
				break;
			default:
				break;
		}
	}
	return retval;
}

char setting_get_type_title(char* title){
	NV_IDX_T idx = setting_get_idx(title);
	return setting_get_type_idx(idx);
}

double setting_get_real_idx(NV_IDX_T idx, char param){
	double retval = NAN;
	if(idx < NV_NUM_PARAM && RAM_params[idx].nv_type == REAL){
		switch(param){
			case SETTING_VAL:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					retval = RAM_params[idx].val.real;
				}				
				break;
			case SETTING_MIN:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					retval = RAM_params[idx].min.real;
				}					
				break;
			case SETTING_MAX:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					retval = RAM_params[idx].max.real;
				}				
				break;
			default:
				break;
		}
	}
	return retval;
}

int32_t setting_get_int_idx(NV_IDX_T idx, char param){
	int32_t retval = 0;
	if(idx < NV_NUM_PARAM && RAM_params[idx].nv_type == INTEGER){
		switch(param){
			case SETTING_VAL:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					retval = RAM_params[idx].val.integer;
				}				
				break;
			case SETTING_MIN:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					retval = RAM_params[idx].min.integer;
				}				
				break;
			case SETTING_MAX:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					retval = RAM_params[idx].max.integer;
				}				
				break;
			default:
				break;
		}
	}
	return retval;
}

double setting_get_real_title(char* title, char param){
	NV_IDX_T idx = setting_get_idx(title);
	return setting_get_real_idx(idx,param);
}

int32_t setting_get_int_title(char* title, char param){
	NV_IDX_T idx = setting_get_idx(title);
	return setting_get_int_idx(idx,param);
}

NV_IDX_T setting_set_real_idx(NV_IDX_T idx, char param, double new_setting){
	NV_IDX_T retval = -1;
	if(idx < NV_NUM_PARAM && RAM_params[idx].nv_type == REAL){
		switch(param){
			double stored_min;
			double stored_max;
			double stored_val;
			case SETTING_VAL:
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				stored_min = RAM_params[idx].min.real;
			}
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){			
				stored_max = RAM_params[idx].max.real;
			}			
			if(new_setting >= stored_min && new_setting <= stored_max){
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					RAM_params[idx].val.real = new_setting;
				}				
				retval = 0;
			}			
				break;
			case SETTING_MIN:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					RAM_params[idx].min.real = new_setting;
				}		
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					stored_val = RAM_params[idx].val.real;
				}		
				if(new_setting > stored_val){
					ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
						RAM_params[idx].val.real = new_setting;
					}					
				}
				retval = 0;
				break;
			case SETTING_MAX:
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					RAM_params[idx].max.real = new_setting;
				}
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					stored_val = RAM_params[idx].val.real;
				}	
				if(new_setting < stored_val){
					ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
						RAM_params[idx].val.real = new_setting;
					}						
				}
				retval = 0;
				break;
			default:
				break;
		}
	}
	return retval;
}

NV_IDX_T setting_set_int_idx(NV_IDX_T idx, char param, int32_t new_setting){
	NV_IDX_T retval = -1;
	if(idx < NV_NUM_PARAM && RAM_params[idx].nv_type == INTEGER){
		switch(param){
		int32_t stored_val;
		int32_t stored_min;
		int32_t stored_max;
		case SETTING_VAL:
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				stored_min = RAM_params[idx].min.integer;
			}
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				stored_max = RAM_params[idx].max.integer;
			}			
			if(new_setting >= stored_min &&  new_setting <= stored_max){
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					RAM_params[idx].val.integer = new_setting;
				}					
				retval = 0;
			}				
			break;
		case SETTING_MIN:
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				RAM_params[idx].min.integer = new_setting;
			}
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				stored_val = RAM_params[idx].val.integer;
			}							
			if(new_setting > stored_val){
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					RAM_params[idx].val.integer = new_setting;
				}					
			}
			retval = 0;
			break;
		case SETTING_MAX:
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				RAM_params[idx].max.integer = new_setting;
			}
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				stored_val = RAM_params[idx].val.integer;
			}			
			if(new_setting < stored_val){
				ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
					RAM_params[idx].val.integer = new_setting;
				}					
			}
			retval = 0;
			break;
		default:
			break;
		}
	}
	return retval;
}

NV_IDX_T setting_set_real_title(char* title, char param, double val){
	NV_IDX_T idx = setting_get_idx(title);
	return setting_set_real_idx(idx,param,val);
}

NV_IDX_T setting_set_int_title(char* title, char param, int32_t val){
	NV_IDX_T idx = setting_get_idx(title);
	return setting_set_int_idx(idx,param,val);
}