/*
 * eeprom.h
 * Flexible EEPROM library for reading/writing a predefined structure with
 * integer and/or floating point elements to EEPROM, as well as reading the
 * stored elements in a simple and atomic way.
 * 
 * This library can be seperated into two functional sections; one part
 * handling reading from / writing to EEPROM, with optional redundancy and CRC
 * checks; and one parts that returns the a paramter's value (or set minimum/
 * maximum values).
 * 
 * The first part of the library, handling EEPROM/RAM loading and saving, is
 * quite flexible and is configured by the compile-time switches starting 
 * with "EEPROM_" below. These allow for storing a CRC checksum in EEPROM to
 * detect data corruption as well as an optional redundant copy of all data,
 * allowing complete recovery of corrupt data (guaranteed recovery of any one
 * bit error). Note that this ensures that a power failure during an EEPROM
 * write will not corrupt data, as one copy is guaranteed to be valid.
 * 
 * The functions ram2eeprom and eeprom2ram are the core functions, and copy
 * data from ram to eeprom and from eeprom to ram respectively. If the stored
 * data is corrupted and could not be restored, eeprom2ram will return nonzero
 * to signal this. In this case using eeprom_reset_defaults may be a good idea
 * to return to a known-good (compile-time) default setting. If there is
 * corruption that can be recovered the valid data is copied over and replaces
 * the corrupted data automatically.
 * 
 * Note that the ram2eeprom and eeprom2ram functions are *NOT* atomic, meaning
 * that calls to them must ensure that, in the case of ram2eeprom, no paramters
 * are modified, and in the case of eeprom2ram, that no parameters are read.
 * This can generally be done by inserting any calls to these functions in an
 * ATOMIC_BLOCK(...) group; see <atomic.h> for more details.
 * 
 * The second part of the library handles reading from and writing to settings
 * that are seeded by the eeprom2ram function. Parameters must be defined at
 * compile-time in nv_settings.h, and can be read and set using the various
 * setting_xxx(...) functions which are relatively self-explanatory.
 * 
 * The following is a usage example of this library;
 * 
 * config.h
 * 	#define PARAM1_TITLE	"param1"	//Stores a real (float) parameter
 * 	#define PARAM2_TITLE	"param2"	//Stores an integer parameter
 * 
 * nv_settings.h
 * 	...
 * 	#define NV_DEFAULT_VALS				\
 *		{								\
 *			.title = PARAM1_TITLE,		\
 *			.nv_type = REAL,			\
 *			.val.real = 0,				\
 *			.max.real = 100,			\
 *			.min.real = -20				\
 *		},								\
 *		{								\
 *			.title = PARAM2_TITLE,		\
 *			.nv_type = INTEGER,			\
 *			.val.real = 2,				\
 *			.max.real = 5,				\
 *			.min.real = -1				\
 *		}
 * 	...
 * 
 * main.c
 * 	#include <stdio.h>
 * 	#include "config.h"
 * 	static NV_IDX_T param1_idx;
 * 	int main(void){
 * 		idx = setting_get_idx(PARAM1_TITLE);
 *  	for(;;){
 * 			setting_set_real_idx(idx,SETTING_VAL,3.14);	//Param1 now contains 3.14 as its value
 * 			setting_set_int_title(PARAM2_TITLE,SETTING_MAX,2);	//Param2's maximum is now 2, the value is changed from 5 to 2
 * 			float p1 = setting_get_real_idx(idx,SETTING_VAL);
 * 			int32_t p2 = setting_get_int_title(PARAM2_TITLE,SETTING_VAL);
 * 			printf("param1 %f, param2 %d",p1,p2);	//outputs "param1 3.14, param2 2"
 * 		}
 * 	}
 * 
 * Keep in mind that getting a parameter through it's title is slower than by
 * it's index. As all access is atomic, all interrupts are blocked during read
 * and write. This time is kept as short as possible and is on the order of 5-6
 * clocks.
 * 
 * At the moment the library does not support changing the type (integer/real)
 * or title (string) of settings, however this is feasible and coule be added
 * relatively easily. Be sure to make these changes atomic as well!
 *
 *  Created on: Jan 8, 2013
 *      Author: hydra
 */

#ifndef EEPROM_H_
#define EEPROM_H_

#include <stdint.h>
#include "nv_settings.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/** @brief Control character to read minimum allowable value for a parameter */
#define SETTING_MIN	'n'

/** @brief Control character to read maximum allowable value for a parameter */
#define SETTING_MAX	'x'

/** @brief Control character to read value for a parameter */
#define SETTING_VAL	'v'

/** @brief Return result for invalid result in check parameter type */
#define SETTING_INVALID_T	0

/** @brief Return type for real type in check parameter type */
#define SETTING_REAL_T		'r'

/** @brief Return type for integer type in check parameter type */
#define SETTING_INTEGER_T	'i'

/** @brief Define to reset the watchdog timer when reading/writing to EEPROM. (Needed for short WDT timeout and large eeprom use) */
#define EEPROM_RESET_WDT

/** @brief Define to print status messages to stdout on various actions */
//#define EEPROM_DEBUG

/**
 * @brief Define to add support for resetting to compile-time defaults (an
 * 		extra copy of the main struct is stored in program (flash) memory).
 * If CRC support is enabled this will also result in calculating a CRC
 * checksum for the stored data and writing it to EEPROM together with the
 * default data. This may be a reasonable default behavior on CRC fail in some
 * situations.
 */
#define EEPROM_RESET_TO_DEFAULT

/**
 * @brief Define to add support for verifying the data read from EEPROM with a
 * 		CRC word (16 bits). If the data is corrupt eeprom_eeprom2ram() will return
 * 		nonzero.
 * NOTE: As the CRC checksum is not initialized on compile this must be
 * specified below in order to not fail on initial (and potentially subsequent)
 * startups.
 */
#define EEPROM_CRC_CHECK

/**
 * @brief Define to store two copies of the EEPROM data, with separate CRC
 * 		checksums.
 * Each copy is written to at different times, and on eeprom_eeprom2ram()
 * both will be scanned, if both are valid the RAM copy is updated, if one is
 * corrupt the valid copy will overwrite the corrupt copy, and if both are
 * invalid the CRC check fails and eeprom_eeprom2ram returns nonzero.
 * NOTE: This requires EEPROM_CRC_CHECK to be defined!
 */
#ifdef EEPROM_CRC_CHECK
#define EEPROM_RAID
#endif


#ifdef EEPROM_CRC_CHECK
/**
 * @brief Initial EEPROM CRC value to write. Set to zero to
 * trigger a CRC fail, and recalculate with eeprom_load_defaults() on the first
 * run. Alternately, manually calculate the CRC based on initial data and enter
 * here.
 */
#define EEPROM_INITCRC 0
#endif

/**
 * @brief Reads the nonvolatile parameters from EEPROM and stores them in a
 * 		RAM copy. Will seed undefined data if EEPROM corrupted!
 * @return Typically zero, nonzero if CRC support added and the CRC check
 * 		failed.
 */
uint8_t eeprom_eeprom2ram(void);

/**
 * @brief Updates the eeprom parameters with the current RAM parameters.
 */
void eeprom_ram2eeprom(void);

#ifdef EEPROM_CRC_CHECK
/** @brief Gets stored CRC */
uint16_t eeprom_get_crc_stored(void);
/** @brief Calculates the CRC for the current data */
uint16_t eeprom_get_crc_calc(void);
#endif

#ifdef EEPROM_RESET_TO_DEFAULT
/**
 * @brief Reloads the compile-time set default parameters from flash memory
 * 		to the RAM and EEPROM copies. If CRC support is enabled this calculates
 * 		a CRC checksum and writes it to eeprom.
 */
void eeprom_reset_defaults(void);
#endif

/**************************** Parameter API ****************************/

/** @brief Gets the number of settings stored in total. */
#define SETTING_NUM()	NV_NUM_PARAM

/** @brief Gets the maximum length of titles. */
#define SETTING_TITLE_LEN() NV_SETTINGS_NAME_LEN

/** @brief Gets the maximum length of titles, expressed as a string. */
#define SETTING_TITLE_LEN_STR()	STR(NV_SETTINGS_NAME_LEN)

/** @brief Gets the title of a parameter with index idx. Returns NULL if invalid idx. */
char* setting_get_title(NV_IDX_T idx);

/** @brief Gets the index of a title, if it exists. Returns non-negative if found, negative if not */
NV_IDX_T setting_get_idx(char* title);

/**@brief Gets the type of data stored in an index. Returns SETTING_XXX_T values */
char setting_get_type_idx(NV_IDX_T idx);

/**@brief Gets the type of data stored in a title. Returns SETTING_XXX_T values */
char setting_get_type_title(char* title);

/** @brief Gets value/min/max of a real (double) at index idx.
 * @param idx The index to read from.
 * @param param The value to read, SETTING_MIN reads the minimum value,
 *		SETTING_MAX reads the maximum value, SETTING_VAL returns the value.
 * @return The value stored, NAN if invalid index, param type, or if content was integer
 */
double setting_get_real_idx(NV_IDX_T idx, char param);

/** @brief Gets value/min/max of an integer at index idx.
 * @param idx The index to read from.
 * @param param The value to read, SETTING_MIN reads the minimum value,
 *		SETTING_MAX reads the maximum value, SETTING_VAL returns the value.
 * @return The value stored, 0 if invalid index, param type, or if content was real
 */
int32_t setting_get_int_idx(NV_IDX_T idx, char param);

/** @brief Gets value/min/max of a real (double) of a given title.
 * @param title The title of the parameter to get.
 * @param param The value to read, SETTING_MIN reads the minimum value,
 *		SETTING_MAX reads the maximum value, SETTING_VAL returns the value.
 * @return The value stored, NAN if invalid index, param type, or if content was integer
 */
double setting_get_real_title(char* title, char param);

/** @brief Gets value/min/max of an integer of a given title.
 * @param title The title of the parameter to get.
 * @param param The value to read, SETTING_MIN reads the minimum value,
 *		SETTING_MAX reads the maximum value, SETTING_VAL returns the value.
 * @return The value stored, 0 if invalid index, param type, or if content was real
 */
int32_t setting_get_int_title(char* title, char param);

/** @brief Sets value/min/max of a real (double) at index idx.
 * @param idx The index to set.
 * @param param The type of value to set, SETTING_MIN sets the minimum value,
 *		SETTING_MAX sets the maximum value, SETTING_VAL sets the value. If
 *		setting the value and the value is not min <= val <= max the value is
 *		not changed.
 * @param val The new value to set.
 * @return Zero if successful, nonzero otherwise.
 */
NV_IDX_T setting_set_real_idx(NV_IDX_T idx, char param, double val);

/** @brief Sets value/min/max of an integer at index idx.
 * @param idx The index to set.
 * @param param The type of value to set, SETTING_MIN sets the minimum value,
 *		SETTING_MAX sets the maximum value, SETTING_VAL sets the value. If
 *		setting the value and the value is not min <= val <= max the value is
 *		not changed.
 * @param val The new value to set.
 * @return Zero if successful, nonzero otherwise.
 */
NV_IDX_T setting_set_int_idx(NV_IDX_T idx, char param, int32_t val);

/** @brief Sets value/min/max of a real (double) of a given title.
 * @param idx The index to set.
 * @param param The type of value to set, SETTING_MIN sets the minimum value,
 *		SETTING_MAX sets the maximum value, SETTING_VAL sets the value. If
 *		setting the value and the value is not min <= val <= max the value is
 *		not changed.
 * @param val The new value to set.
 * @return Zero if successful, nonzero otherwise.
 */
NV_IDX_T setting_set_real_title(char* title, char param, double val);

/** @brief Sets value/min/max of an integer of a given title.
 * @param idx The index to set.
 * @param param The type of value to set, SETTING_MIN sets the minimum value,
 *		SETTING_MAX sets the maximum value, SETTING_VAL sets the value. If
 *		setting the value and the value is not min <= val <= max the value is
 *		not changed.
 * @param val The new value to set.
 * @return Zero if successful, nonzero otherwise.
 */
NV_IDX_T setting_set_int_title(char* title, char param, int32_t val);

#endif /* EEPROM_H_ */
