#ifndef STRUCT_H_
#define STRUCT_H_

/* Settings and definitions for non-volatile parameters are stored here */

#include <stdint.h>
#include "../config.h"

/**
 * @brief Control string for a hidden parameter.
 * Parameters whose titles start with this string will be hidden from the shell parameter display
 */
#define NV_HIDDEN_HDR		"~"

/** @brief Maximum length for setting names (including null-termination) */
#define NV_SETTINGS_NAME_LEN	9

/** @brief Number of parameters available */
#define NV_NUM_PARAM			18

/** @brief Internal type, indexing parameter list. NV_NUM_PARAM (*NOT* NV_NUM_PARAM-1) must fit in this, and must be signed */
#define NV_IDX_T			int8_t

/** @brief Identifier of type of nonvolatile parameter stored */
enum nv_type_t {INTEGER=0, REAL};

/** @brief Individual parameter structure */
struct nv_setting_t{
	char title[NV_SETTINGS_NAME_LEN+1];	//Title of attribute. MUST BE UNIQUE!
	enum nv_type_t nv_type;				//Type of attribute
	union {								//Value of attribute
		int32_t integer;
		double	real;
		} val;
	union {								//Maximum value of attribute
		int32_t integer;
		double	real;
	} max;
	union {								//Minimum value of attribute
		int32_t integer;
		double real;
	} min;
};

/** @brief Controls the initial parameter names, types, values, and limits. */
// NOTE: Respect NV_NUM_PARAM and make sure titles are unique!
#define NV_DEFAULT_VALS					\
		{								\
			.title = TITLE_I_MAX,		\
			.nv_type = REAL,			\
			.val.real = I_MAX_D,		\
			.max.real = I_MAX_MAX,		\
			.min.real = I_MAX_MIN		\
		},								\
		{								\
			.title = TITLE_I_RIPPLE,	\
			.nv_type = REAL,			\
			.val.real = I_RIPPLE_D,		\
			.max.real = I_RIPPLE_MAX,	\
			.min.real = 0				\
		},								\
		{								\
			.title = TITLE_I_SKIP,		\
			.nv_type = INTEGER,			\
			.val.integer = I_SKIP_D,	\
			.max.integer = I_SKIP_MAX,	\
			.min.integer = 0			\
		},								\
		{								\
			.title = TITLE_I_OFFSET,	\
			.nv_type = REAL,			\
			.val.real = I_OFFSET_D,		\
			.max.real = I_OFFSET_MAX,	\
			.min.real = I_OFFSET_MIN	\
		},								\
		{								\
			.title = TITLE_I_FRICTION,	\
			.nv_type = REAL,			\
			.val.real = I_FRICTION_D,	\
			.max.real = I_FRICTION_MAX,	\
			.min.real = I_FRICTION_MIN	\
		},								\
		{								\
			.title = TITLE_KD,			\
			.nv_type = REAL,			\
			.val.real = KD_D,			\
			.max.real = KD_MAX,			\
			.min.real = KD_MIN			\
		},								\
		{								\
			.title = TITLE_KDF,			\
			.nv_type = REAL,			\
			.val.real = KDF_D,			\
			.max.real = KDF_MAX,		\
			.min.real = KDF_MIN			\
		},								\
		{								\
			.title = TITLE_KP,			\
			.nv_type = REAL,			\
			.val.real = KP_D,			\
			.max.real = KP_MAX,			\
			.min.real = KP_MIN			\
		},								\
		{								\
			.title = TITLE_KI,			\
			.nv_type = REAL,			\
			.val.real = KI_D,			\
			.max.real = KI_MAX,			\
			.min.real = KI_MIN			\
		},								\
		{								\
			.title = TITLE_TRK_ERR,		\
			.nv_type = INTEGER,			\
			.val.integer = TRK_ERR_D,	\
			.max.integer = TRK_ERR_MAX,	\
			.min.integer = TRK_ERR_MIN	\
		},								\
		{								\
			.title = TITLE_UNDERVOLT,	\
			.nv_type = REAL,			\
			.val.real = UNDERVOLT_D,	\
			.max.real = UNDERVOLT_MAX,	\
			.min.real = UNDERVOLT_MIN	\
		},								\
		{								\
			.title = TITLE_OVERVOLT,	\
			.nv_type = REAL,			\
			.val.real = OVERVOLT_D,		\
			.max.real = OVERVOLT_MAX,	\
			.min.real = OVERVOLT_MIN	\
		},								\
		{								\
			.title = TITLE_CPUMP,		\
			.nv_type = INTEGER,			\
			.val.integer = CPUMP_D,		\
			.max.integer = 1,			\
			.min.integer = 0			\
		},								\
		{								\
			.title = TITLE_BRAKE,		\
			.nv_type = INTEGER,			\
			.val.integer = BRAKE_D,		\
			.max.integer = 1,			\
			.min.integer = 0			\
		},								\
		{								\
			.title = TITLE_HIGH_I,		\
			.nv_type = INTEGER,			\
			.val.integer = HIGH_I_D,	\
			.max.integer = 1,			\
			.min.integer = 0			\
		},								\
		{								\
			.title = TITLE_INPUT_POW,	\
			.nv_type = INTEGER,			\
			.val.integer = INPUT_POW_D,	\
			.max.integer = INPUT_POW_MAX,\
			.min.integer = INPUT_POW_MIN\
		},								\
		{								\
			.title = TITLE_I_NOM,		\
			.nv_type = REAL,			\
			.val.real = I_NOM_D,		\
			.max.real = I_NOM_MAX,		\
			.min.real = I_NOM_MIN		\
		},								\
		{								\
			.title = TITLE_MOTOR_TAU,	\
			.nv_type = REAL,			\
			.val.real = MOTOR_TAU_D,	\
			.max.real = MOTOR_TAU_MAX,	\
			.min.real = MOTOR_TAU_MIN	\
		}
			

#endif /* STRUCT_H_ */
