/*
 * Flexible_Servo_Controller.c
 * See config.h for the most useful application-specific compile-time constants.
 * Board.h contains most hardware definitions and initialization routines.
 * Created: 4/7/2013 12:09:27 PM
 *  Author: VBOX
 */

#include <avr/io.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <util/atomic.h>
#include <math.h>

#include "board.h"
#include "util.h"
#include "shell/shell.h"
#include "shell/terminal.h"
#include "nv_settings/eeprom.h"
#include "syslog/syslog.h"
#include "control/control.h"
#include "state.h"
#include "control/cc.h"
#include "stackcount/stackcount.h"
#include "plot/plot.h"

NV_IDX_T i_nom_idx;
NV_IDX_T motor_tau_idx;
static uint8_t motor_tau_time;
static float motor_i2t = 0;	//Current long-term motor overcurrent value

int main(void)
{	
	ATOMIC_BLOCK(ATOMIC_FORCEON){
		uint8_t config_status = 0;
		board_init_io();
		board_init_controller();
		init_clock();
		if(board_query_config_status()){	//Check the status of the config pin
			config_status = 1;
		}			
		board_init_uart(config_status);		//and only enable the RX input on the uart if set (shorted)
		shell_stream_init();
		if(RST.STATUS & RST_WDRF_bm){	//If reset was caused by watchdog
			syslog_add(syslog_wdt_overflow);
			board_set_led(board_led_fault);
			printf_P(PSTR(WDT_ERRMSG));
			for(;;){					//Wait for a power cycle to return to normal operation
				wdt_reset();
			}
		}
		PMIC.CTRL |= PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm;
		printf_P(PSTR(STARTUP_TEXT));
		if(config_status){
			printf_P(PSTR(STARTUP_CONFIG));
		}else{
			printf_P(PSTR(STARTUP_NOCONFIG));
		}
		syslog_init();
		// Load EEPROM settings
		if(eeprom_eeprom2ram()){
			board_set_fault_out();
			printf_P(PSTR(STARTUP_CRCFAIL),eeprom_get_crc_stored(),eeprom_get_crc_calc());
			NONATOMIC_BLOCK(ATOMIC_RESTORESTATE){	//We need interrupts to read the serial interface
				while(!terminal_line_available()){
					terminal_idle();
					// Twiddle the fault LED to signal FUBAR'ed state
					for(uint8_t i = 0; i < 250; i++){
						wdt_reset();
						_delay_ms(1);	
					}
					board_clr_led(board_led_fault);
					for(uint8_t i = 0; i < 250; i++){
						wdt_reset();
						_delay_ms(1);
					}
					board_set_led(board_led_fault);
				}
				while(terminal_line_available()){
					terminal_getc();
				}
				board_clr_led(board_led_fault);
			}
			eeprom_reset_defaults();
		}else{
			printf_P(PSTR(STARTUP_CRCPASS),eeprom_get_crc_stored());
		}
		// Check error log
		if(syslog_query_read()){
			printf_P(PSTR(STARTUP_ERRLOG_ERRS),syslog_query_read());
		}else{
			printf_P(PSTR(STARTUP_ERRLOG_NOERRS));
		}
		// All the following init functions need values from EEPROM to be seeded
		cc_init();
		state_init();
		control_init();
		// Initialize variables used in the main loop
		i_nom_idx = setting_get_idx(TITLE_I_NOM);
		motor_tau_idx = setting_get_idx(TITLE_MOTOR_TAU);
		// Set the power LED to indicate the startup has finished
		board_set_led(board_led_pwr);
	}
    for(;;){
		wdt_reset();
#ifdef STACKMSG
		static uint8_t foo = 0;
		if(get_systime() == foo){
			foo += 100;
			static uint16_t divisor = 9;
			divisor = (divisor+1)%10;
			if(divisor == 0){
				shell_extmsg_prefix();
				printf_P(PSTR("%u bytes of RAM unused\n"),StackCount());
				shell_extmsg_cleanup();
			}			
		}
#endif
		shell_idle();
		state_print();
		// Calculate the effects of i^2 heating in the motor and turn off if i^2 has been too large for too long
		if(get_systime() == motor_tau_time){	//Runs once every MOTOR_TAU_DIV systicks
			motor_tau_time += MOTOR_TAU_DIV;
			float motor_tau = setting_get_real_idx(motor_tau_idx,SETTING_VAL);	
			float i_nom = pow(setting_get_real_idx(i_nom_idx,SETTING_VAL),2);
			float alpha = (1.0 * MOTOR_TAU_DIV/CONTROL_PID_FREQ)/(motor_tau);
			float last_iout = 0;
			if(state_get_state() == state_active){
				last_iout = controller_query_out();
			}
			motor_i2t += alpha * (pow(last_iout,2) - motor_i2t);
			if(motor_i2t > i_nom){
				state_set_i2t_oc(1);
			}else{
				state_set_i2t_oc(0);
			}
		}
		// Set the error LED to the current error syslog status
		if(syslog_query_read()){
			board_set_led(board_led_fault);
		}else{
			board_clr_led(board_led_fault);
		}					
    }
}