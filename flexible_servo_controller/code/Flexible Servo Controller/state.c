/*
 * state.c
 *
 * Created: 5/16/2013 12:26:22 PM
 *  Author: VBOX
 */ 

#include "state.h"
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "shell/shell.h"
#include "board.h"
#include "nv_settings/eeprom.h"
#include "control/control.h"
#include "config.h"
#include "control/cc.h"
#include "syslog/syslog.h"
#include "dynbuf/dynbuf.h"

#define STATE_MSG_BUF_LEN	16	//Number of elements in buffer for printing state changes to the terminal.

enum state_msg_t {state_idle2active = 0, state_active2idle, state_undervoltage, state_overcurrent, state_track_error};	// *NON-FAULT* messages to print on state transitions

static volatile enum state_t state = state_startup;	// Total system-state state variable.

static const char PROGMEM state_msg_idle2active[] = "EVENT0: Enabled, output on\n";
static const char PROGMEM state_msg_active2idle[] = "EVENT1: Disabled, output off\n";
static const char PROGMEM state_msg_undervoltage[] = "EVENT2: Bus undervoltage, output off\n";
static const char PROGMEM state_msg_overcurrent[] = "EVENT3: Motor long-term overcurrent, output off\n";
static const char PROGMEM state_msg_tracking_error[] = "EVENT4: Maximum tracking error exceeded, output off\n";
//Array of non-fault messages to print, in the same order as the labels in state_msg_t
static const char* const PROGMEM state_msg_arr[] = {
	state_msg_idle2active,\
	state_msg_active2idle,\
	state_msg_undervoltage,\
	state_msg_overcurrent,\
	state_msg_tracking_error};

static NV_IDX_T cpump_idx;
static NV_IDX_T brake_idx;
static NV_IDX_T vmin_idx;
static NV_IDX_T vmax_idx;
static NV_IDX_T trk_err_idx;
static NV_IDX_T high_i_idx;

static dynbuf_t msg_buf = {0,0,STATE_MSG_BUF_LEN,1,{[0 ... 1*STATE_MSG_BUF_LEN]=0}};
static volatile uint8_t initialized = 0;	// Set true when the system has been initialized
static volatile uint8_t cp_ticks = 0;		// Internal storage for the charge pump state. Zero when timed out, increases with one per level transition up to CPUMP_TICKS_THR
static volatile uint8_t systime_int = 0;	// Systime variable
static volatile uint8_t ff_state = 0;		// Fault-flag state

static volatile uint8_t i2t_oc = 0;		// Long-term overcurrent state (overcurrent caused by motor heating). Nonzero when limits exceeded, zero otherwise.

/** @brief Resets the h-bridge error flags. NOTE: *MUST* not be blocked "too" long (>10us) by interrupts! */
static inline void board_bridge_reset(void){
	BRIDGE_PORT.OUTCLR = BRIDGE_RESET_bm;
	_delay_us(5);
	BRIDGE_PORT.OUTSET = BRIDGE_RESET_bm;
}

/** @brief Returns nonzero if the enable input is set (automatically switching between charge pump and enable as needed) */
static uint8_t query_enable_input(void);
/** @brief Updates the fault flag state variable */
static void update_ff_state(void);
/** @brief Returns nonzero if the bus voltage is in a region where it is safe to enter active mode */
static uint8_t query_vbus_go(void);
/** @brief Returns nonzero if the bus voltage is below VMIN */
static uint8_t query_vbus_undervoltage(void);
/** @brief Returns nonzero if the bus voltage exceeds VMAX */
static uint8_t query_vbus_overvoltage(void);
/** @brief Returns nonzero if there is an overcurrent condition */
static uint8_t query_overcurrent(void);
/** @brief Returns nonzero if the tracking error exceeds set limits */
static uint8_t query_tracking_error_exceeded(void);
/** @brief Returns nonzero if there is at least one fault detected */
static uint8_t query_faults(void);
/** @brief Adds a message to the message buffer */
static void add_msg(enum state_msg_t);

void state_init(void){
	cpump_idx = setting_get_idx(TITLE_CPUMP);
	brake_idx = setting_get_idx(TITLE_BRAKE);
	vmax_idx = setting_get_idx(TITLE_OVERVOLT);
	vmin_idx = setting_get_idx(TITLE_UNDERVOLT);
	trk_err_idx = setting_get_idx(TITLE_TRK_ERR);
	high_i_idx = setting_get_idx(TITLE_HIGH_I);
	initialized = 1;
}

void state_print(){
	while(dynbuf_query_read(&msg_buf)){
		enum state_msg_t state_msg = *(enum state_msg_t*) dynbuf_read(&msg_buf);
		if( (state_msg) < sizeof(state_msg_arr)/sizeof(char*) && pgm_read_word(&state_msg_arr[state_msg] != NULL)){
			shell_extmsg_prefix();
			printf_P((char*)pgm_read_word(&state_msg_arr[state_msg]));
			shell_extmsg_cleanup();
		}
	}
}

enum state_t state_get_state(void){
	return state;
	};

static void add_msg(enum state_msg_t msg){
	if(dynbuf_query_write(&msg_buf)){
		dynbuf_write(&msg,&msg_buf);
	}
};

uint8_t get_systime(void){
	return systime_int;
}

void state_update(void){
	switch(state){	            //Main state machine
		case state_startup:                 //Default state on startup
			board_clr_active_out();         //Clear the enable output...
			board_clr_led(board_led_en);    //...and enable LED.
			if(initialized){
				state = state_idle;         //If initialized, transition to the "normal" state
			}
			break;
		default:                            //state should always be one of the specified states...
            for(;;){};                      //...if not then hang the device as something very bad has happened
			break;
		case state_idle:					//"Ordinary" system state when output not active
			if(setting_get_int_idx(brake_idx,SETTING_VAL)){
				cc_brake();					//Apply the brake if enabled in settings...
			}else{
				cc_disable();				//...otherwise open-circuit the motor.
			}
			board_clr_active_out();			//Clear the enable output...
			board_clr_led(board_led_en);	//...and enable LED.
			
			if(query_faults()){				//If there's a fault...
				state = state_fault;		//...set the state to fault...
				controller_disable();		//...disable the position controller...
				cc_disable();				//...and the current controller...
				board_set_fault_out();		//...and set the fault output.
			}else if(query_enable_input() && query_vbus_go()){	//If the enable pin is high and the bus voltage is OK...
				state = state_active;		//...set the state fo active...
				add_msg(state_idle2active);	//...write the idle-to-active message...
				controller_enable();		//...and (re-)enable the controller. (Clearing any terms with memory)
			}
			break;
		case state_idle_latch:				//If we are transitioning to the idle state...
			if(setting_get_int_idx(brake_idx,SETTING_VAL)){
				cc_brake();					//Appy the brake if enabled...
			}else{
				cc_disable();				//...otherwise just open-circuit the output.
			}
			board_clr_active_out();			//Clear the active output...
			board_clr_led(board_led_en);	//...and active LED.
			
			if(query_faults()){				//If there's a fault now...
				state = state_fault;		//...set the state to fault...
				controller_disable();		//...disable the position controller...
				cc_disable();				//...and current controller...
				board_set_fault_out();		//...and set the fault output.
			}else if(!query_enable_input()){//Only if the enable input is *not* set...
				state = state_idle;			//...do we move to the "normal" idle state.
			}
			break;
		case state_active:					//If we're in the active mode...
			board_set_active_out();			//...show this on the output...
			board_set_led(board_led_en);	//...and light the enable LED.
			if(query_faults()){				//If there is a fault...
				state = state_fault;		//...transition to the fault state...
				controller_disable();		//...disable the position controller...
				cc_disable();				//...and the current controller...
				board_set_fault_out();		//...and set the fault output.
			}else if(!query_enable_input()){//If the enable input is not set anymore...
				add_msg(state_active2idle);	//...write the active-to-idle message...
				state = state_idle_latch;	//...go to the idle_latch state...
				controller_disable();		//...and disable the position controller. (the current controller will be set correctly on the next call of state_update()).
			}else if(query_vbus_undervoltage()){	//If there's a bus undervoltage...
				add_msg(state_undervoltage);//...write this...
				state = state_idle_latch;	//...go to the idle_latch state...
				controller_disable();		//...and disable the position controller. (the current controller will be set correctly on the next call of state_update()).
			}else if(query_overcurrent()){	//If there's a long-term overcurrent present...
				add_msg(state_overcurrent);	//...write this...
				state = state_idle_latch;	//...go to the idle_latch state...
				controller_disable();		//..and disable the position controller. (the current controller will be set correctly on the next call of state_update()).
			}else if(query_tracking_error_exceeded()){	//If the tracking error is exceeded...
				add_msg(state_track_error);	//...write this...
				state = state_idle_latch;	//..go the the idle_latch state...
				controller_disable();		//..and disable the position controller. (the current controller will be set correctly on the next call of state_update()).
			}
			break;
		case state_fault:					//If there was a fault detected in some other state...
			board_clr_active_out();			//Clear the active output...
			board_clr_led(board_led_en);	//...and active LED...
			controller_disable();			//...and disable the position controller...
			cc_disable();					//...and current controller.
			if(ff_state != FF_NOMINAL){		//If the fault was from the H-bridge...
				board_bridge_reset();		//...clear them.
			}				
			if(!query_faults() && !query_enable_input() && query_vbus_go()){	//If there are no more faults present...
				state = state_idle_latch;	//...go to the idle_latch state...
				board_clr_fault_out();		//...and clear the fault output.
			}
			break;
	}
}

void state_set_i2t_oc(uint8_t new_i2t_oc){
	i2t_oc = new_i2t_oc;
}

static inline uint8_t query_overcurrent(void){
	return i2t_oc;
}

static uint8_t query_tracking_error_exceeded(void){
	uint8_t retval = 0;
	int16_t dummy = controller_query_tracking_error();
	if(dummy < 0){
		dummy = -1 * dummy;
	}
	if(dummy > setting_get_int_idx(trk_err_idx,SETTING_VAL)){
		retval = 1;
	}
	return retval;
}

/** @brief Returns nonzero if there is at least one fault detected and writes to the syslog if applicable */
// NOTE: This function must test *all* faults!
static uint8_t query_faults(void){
	uint8_t retval = 0;
	if(query_vbus_overvoltage()){
		syslog_add(syslog_bus_overvoltage);
		retval = 1;
	}if(ff_state != FF_NOMINAL){
		switch(ff_state){
			case FF_OVERHEAT:
				syslog_add(syslog_overheat_bridge);
				break;
			case FF_SHORT:
				syslog_add(syslog_short);
				break;
			case FF_UNDERVOLTAGE:
				syslog_add(syslog_bridge_undervoltage);
				break;
			default:
				break;
		}
		retval = 1;
	}if(!setting_get_int_idx(high_i_idx,SETTING_VAL)){	//If high-current mode disabled
		if(board_query_sensor_overcurrent()){
			syslog_add(syslog_sensor_overcurrent);
			retval = 1;
		}			
	}
	return retval;
}

/** @brief Returns nonzero if the bus voltage is within the safe starting range */
//The safe starting range is VMIN + HYST < VBUS < VMAX - HYST
static uint8_t query_vbus_go(void){
	uint8_t retval = 0;
	float vbus = query_vbus();
	if(vbus > (setting_get_real_idx(vmin_idx,SETTING_VAL) + VBUS_HYST) &&
	   vbus < (setting_get_real_idx(vmax_idx,SETTING_VAL) - VBUS_HYST)){
		   retval = 1;
	}
	return retval;
}

/** @brief Returns nonzero if the bus voltage is below VMIN */
static uint8_t query_vbus_undervoltage(void){
	uint8_t retval = 0;
	float vbus = query_vbus();
	if(vbus < setting_get_real_idx(vmin_idx,SETTING_VAL)){
		retval = 1;
	}
	return retval;
}

/** @brief Retuns nonzero if the bus voltage exceeds VMAX */
static uint8_t query_vbus_overvoltage(void){
	uint8_t retval = 0;
	if(query_vbus() > setting_get_real_idx(vmax_idx,SETTING_VAL)){
		retval = 1;
	}
	return retval;
}

static void update_ff_state(void){
	// Check H-Bridge fault flags and update shadow register as needed
	static uint8_t ff_rolling[2] = {FF_NOMINAL};	// Rolling fault flag state
	ff_rolling[1] = ff_rolling[0];
	ff_rolling [0] = board_query_ff_status();
	if(ff_rolling[0] == ff_rolling[1]){	// If FF state has been constant over a large-ish period it's stable
		ff_state = ff_rolling[0];
	}
}

static uint8_t query_enable_input(void){
	uint8_t retval = 0;
	if(setting_get_int_idx(cpump_idx,SETTING_VAL)){	// If charge pump function enabled...
		if(cp_ticks == CPUMP_TICKS_THR){			// ...and input is correct return true
			retval = 1;
		}
	}else{											// Else charge pump disabled...
		if(board_query_cpump_pin_active()){			// ...so just check pin logic level
			retval = 1;
		}
	}
	return retval;
}	

ISR(SYSTIME_VECT){
	systime_int++;
	board_cpump_pin_int_en();	//Re-enable the charge-pump pin-change interrupt
	update_ff_state();			//Update the fault-flag state
	state_update();
	controller_idle();
}	

ISR(ENC_IDX_VECT){
	if(board_query_idx_in()){
		board_set_idx_high();
	}else{
		board_set_idx_low();
	}
}

ISR(CPUMP_TIMEOUT_VECT){
	cp_ticks = 0;
}

ISR(CPUMP_TRANS_VECT){
	if(cp_ticks < CPUMP_TICKS_THR){
		cp_ticks++;
	}
	board_cpump_pin_int_dis();
}