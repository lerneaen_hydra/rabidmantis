/*
 * stackcount.h
 *
 * Based on http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&t=52249
 */

#ifndef STACKCOUNT_H_
#define STACKCOUNT_H_

#include "stdint.h"

#define STACK_CANARY 0xC5

void StackPaint(void) __attribute__ ((naked)) __attribute__ ((section (".init1")));
uint16_t StackCount(void);

#endif /* STACKCOUNT_H_ */
