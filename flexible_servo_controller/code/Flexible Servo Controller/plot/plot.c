/*
 * plot.c
 *
 * Created: 5/18/2013 3:32:49 PM
 *  Author: VBOX
 */ 

#include <stdio.h>
#include <math.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include "plot.h"

// Converts a define'd integer to a string, for example;
// #define FOO 3
// STR(FOO) //results in "3"
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define PLOT_SIZEERRMSG	"PLOT ERROR: Plot size must be nonzero!\n"

static const uint8_t PROGMEM autoscale[] = {PLOT_AUTOSCALE_VALS};

/** @brief Gets the magnitude of an integer */
static int16_t iabs(int16_t n){
	return (n < 0) ? -n : n;
}

void plot(float (*fp_array[])(uint16_t), char labels[], uint8_t num, struct plot_setting_t* ps){
	if(ps->x_len == 0 || ps->y_len == 0){
		printf_P(PSTR(PLOT_SIZEERRMSG));
		return;
	}
	float y_max = 0;
	float y_min = 0;
	if(ps->y_min == 0 && ps->y_max == 0){	// If the Y-axis bounds should be auto-scaled
		// Get the maximum and minimum values to be displayed
		plot_getextremes(fp_array,num,ps->x_len,&y_max,&y_min);
		//printf("Found max %f and min %f\n",(double)y_max, (double)y_min);
		// Determine the closest end value for each direction of the axis, rounded up
 		y_max = plot_autoscale(y_max);
 		y_min = plot_autoscale(y_min);
	}else{								// The Y-axis bounds are manually specified
		y_max = ps->y_max;
		y_min = ps->y_min;
		if(y_max < y_min){				// Fix mixed y_max and y_min parameters
			float dummy = y_max;
			y_max = y_min;
			y_min = dummy;
		}
	}
	//printf("Final max %g, min %g\n",(double)y_max,(double)y_min);
	
	// Each printed row corresponds to a range of values equal to bin_width
	float bin_width = (y_max - y_min)/(ps->y_len-1);
	
	// Find the center value of the bin closest to y = 0, this is used for finding the location of the X-axis
	float zeroline = INFINITY;
	for(uint16_t i = 0; i < ps->y_len ; i++){
		float candidate = i * bin_width + y_min;
		//printf("testing with idx %u, giving a candidate of %g\n",i,candidate);
		if(fabs(candidate) < fabs(zeroline)){
			wdt_reset();
			//printf("this was better than the previous!\n");
			zeroline = candidate;
		}
	}
//	printf("Bin width %g\n",bin_width);
//	printf("got zeroline %f\n",zeroline);
	
	// Generate location of axis tick markers
	float ytick_vals[ps->y_ticks+2];	// A y-tick is printed on every line corresponding to these Y values. Ordering has no importance.
	uint16_t xtick_vals[ps->x_ticks+2];	// An x-tick is printed on every column of the values in this array. Ordering has no importance.

	float xtick_multiple = (1.0 * ps->x_len - 1)/(ps->x_ticks);
	//printf("xticks every %g at\n",xtick_multiple);
	for(uint16_t i = 0; i < sizeof(xtick_vals)/sizeof(uint16_t); i++){
		//printf("	%u\n",(int)round(i*xtick_multiple));
		xtick_vals[i] = round(i * xtick_multiple);
	}
		
//	printf("bin width %f\n",(double)bin_width);
	// Make the y-tick multiple match the largest of y_max and -y_min
	float y_axis_absmax = fmax(fabs(y_max),fabs(y_min));
	// Try to match the requested number of ticks
	float ytick_multiple = y_axis_absmax/round(ps->y_ticks*y_axis_absmax/(fabs(y_max) + fabs(y_min)));

	//printf("ytick_position every %g at\n", ytick_multiple);
	{
		int8_t dir = 1;		// Start by adding positive y-ticks
		uint16_t k = 0;		// At the zero-line
		for(uint16_t i = 0; i < sizeof(ytick_vals)/sizeof(float); i++){
			//Introduce scaling factor, is zero at y = +/- y_axis_absmax and 1 at y = 0. Some line counts make zeroline = 0.5 * binwidth, creating the risk of missing the tick marks otherwise
			float factor = 1 - iabs(k) * ytick_multiple / y_axis_absmax;
//			printf("	factor %g\n",factor);
			float newpos = dir * ytick_multiple * k + zeroline * factor;
//			printf("	%f",(double)newpos);
			if(newpos < y_max + 0.5 * bin_width){	//If the tick was in the valid range of data, add it
//				printf(", added\n");
				ytick_vals[i] = newpos;
				k++;
			}else{									//If the tick was above y_max, switch to adding the negative ticks
				if(dir > 0){
//					printf(", *not* added\n");
					dir = -1;
					k = 1;
					i--;	
				}
			}
		}
	}
	
	//Determine the location of the X-axis
	float xaxis_level;
	if(y_max <= 0){				//If all data is negative, print it at the top of the plot
		xaxis_level = y_max;
	}else if(y_min >= 0){		//If all data is positive, print it at the bottom of the plot
		xaxis_level = y_min;
	}else{						//If data is positive and negative, print it at the origin
		xaxis_level = zeroline;
	}
	
	for(uint16_t row = 0; row < ps->y_len; row++){
		// The center of the current row corresponds to the numerical value stored in bin_ctr
		float bin_ctr = y_max - bin_width * row;
//		printf("bin_ctr %g\n",bin_ctr);
		// Now seed the different bin sublevels. For non-antialiased output display a symbol
		// when the value lies within bin_nom_top and bin_norm_btm, and for antialiased output
		// display a symbol when the value lies within bin_aa_top and bin_aa_btm.
		float bin_norm_top = bin_ctr + 0.5 * bin_width;
		float bin_norm_btm = bin_ctr - 0.5 * bin_width;
		#ifdef PLOT_AA
		float bin_aa_top = bin_ctr + 0.75 * bin_width;
		float bin_aa_btm = bin_ctr - 0.75 * bin_width;
		#endif
//		printf("row %d has bin_ctr %g, bin_top %g, bin_btm %g\n",row,bin_ctr,bin_norm_top,bin_norm_btm);
		for(uint16_t col = 0; col < ps->x_len; col++){
			char printed = ' ';	// Buffer of what should be printed at the current position, empty by default
			// Y-axis related items
			if(col == 0){
				printed = PLOT_YAXIS;	// Column zero normally contains the Y axis
				//PLOT_YAXIS_TOP is checked for below
			}
			// X-axis related items
			if(bin_norm_top > xaxis_level && bin_norm_btm < xaxis_level ){	//Check if this is the line to print the X-axis on
				//printf("x axis, bintop %g binbtm %g\n",bin_norm_top,bin_norm_btm);
				printed = PLOT_XAXIS;
				//PLOT_XAXIS_END is checked for below
			}
			
			// Tick-related items
			uint8_t tick_here_x = 0;
			uint8_t tick_here_y = 0;
			// Check if the current column matches the position for a tick
			for(uint8_t i = 0; i < sizeof(xtick_vals)/sizeof(uint16_t); i++){
				if(col == xtick_vals[i]){
					tick_here_x = 1;
					break;
				}
			}
			// Check if the current row matches the position for a tick
			for(uint8_t i = 0; i < sizeof(ytick_vals)/sizeof(float); i++){
				wdt_reset();
				if(ytick_vals[i] <= bin_norm_top && ytick_vals[i] >= bin_norm_btm){
					tick_here_y = 1;
					break;
				}
			}
			if(tick_here_x && tick_here_y){
				printed = PLOT_TICK;
			}
			
			//Plot the function traces, which overwrite axes and tick marks
			{
				char foo = '\0';
				for(uint16_t i = 0; i < num; i++){	//Sweep over all functions
					float func_val = 0;
					if(fp_array[i] != NULL){		//Do what little safety-checking is possible...
						#if PLOT_WDT
						wdt_reset();
						#endif
						func_val = (*fp_array[i])(col);	//...and then call the function
					}
					if((func_val > y_max && row == 0) || (func_val < y_min && row == ps->y_len - 1)){
						foo = PLOT_DATA_EXCEED;
					#ifdef PLOT_AA
					}else if(ps->aa){
						if(func_val < bin_aa_top && func_val >= bin_aa_btm){
							if(foo == '\0'){
								foo = labels[i];
							}else{
								foo = PLOT_OVERLAP;
							}
						}
					#endif
					}else{
						if(func_val < bin_norm_top && func_val >= bin_norm_btm){
							if(foo == '\0'){
								foo = labels[i];
							}else{
								foo = PLOT_OVERLAP;
							}
						}
					}
				}
				
				if(foo != '\0'){
					printed = foo;
				}
			}
			
			// Plot axis endpoints
			// Y-axis endpoints
			if(col == 0 && row == 0){
				printed = PLOT_YAXIS_TOP;
			}
			if(col == 0 && row == ps->y_len - 1){
				printed = PLOT_YAXIS_BTM;
			}
			
			
			// Y-axis max marker
			if(col == 1 && row == 0){
				uint8_t chars = printf_P(PSTR("%+."STR(PLOT_SIGDIG)"g"),y_max);
				col += chars - 1;
			}else if(col == 1 && row == ps->y_len - 1){	// Y-axis min marker
				uint8_t chars = printf_P(PSTR("%+."STR(PLOT_SIGDIG)"g"),y_min);
				col += chars - 1;
			}else if(bin_norm_top > zeroline && bin_norm_btm < zeroline && col == ps->x_len - PLOT_SIGDIG - 1 && isfinite(ps->x_label)){	//X-axis end marker
				uint8_t chars = printf_P(PSTR("%+."STR(PLOT_SIGDIG)"g%c%c"),ps->x_label,PLOT_XAXIS,PLOT_XAXIS_END);
				col += chars - 1;
			}else{				
				putchar(printed);	//Finally print the data	
			}
		}
		printf_P(PSTR("\n"));
	}
}

float plot_autoscale(float raw){
	float retval = 0;
	if(raw == 0.0){
		return 0.0;
	}
	int8_t sign = raw < 0 ? -1 : 1;
	float scaled_val = fabs(raw);
	int16_t scaled_pow = 0;
	uint8_t idx = 0;
	//printf("got raw %g",scaled_val);
	while(scaled_val < pgm_read_byte(&autoscale[0])){	// If input value is below the reference decade, scale it up until it is in range
		scaled_val *= 10;
		scaled_pow++;									// Keep track of the number of scalings performed
	}
	while(scaled_val >= 10 * pgm_read_byte(&autoscale[0])){	// If input value is above the reference decade, scale it down until it is in range
		scaled_val /= 10;
		scaled_pow--;									// Keep track of the number of scalings performed
	}
	//printf(" scaled to %g\n",scaled_val);
	// Scaled is now guaranteed to be in the range autoscale[0] <= scaled < 10*autoscale[0]
	// Find the smallest value in autoscale that is greater than scaled
	for(int8_t i = sizeof(autoscale)/sizeof(uint8_t) - 1; i >= 0 ; i--){
		if(scaled_val < pgm_read_byte(&autoscale[i])){
			idx = i;
		}
	}
	//printf("found idx %u\n",idx);
	//printf("giving a sign of %d, base of %g, and mantissa of %d\n",sign,pow(10,-1*scaled_pow),pgm_read_byte(&autoscale[idx]));
	// Finally, generate the output value
	retval = sign * pow(10,-1*scaled_pow) * pgm_read_byte(&autoscale[idx]);
	//printf("this gives a total value of %g\n",retval);
	return retval;
}

void plot_getextremes(float (*fp_array[])(uint16_t),uint8_t num, uint16_t xnum, float* max, float* min){
	float max_int = 0;
	float min_int = 0;
	for(uint8_t i = 0; i < num; i++){
		for(uint16_t j = 0; j < xnum; j++){
			#if PLOT_WDT
			wdt_reset();
			#endif
			float candidate = INFINITY;
			if(*fp_array[i] != NULL){
				candidate = (*fp_array[i])(j);
			}			
			if(candidate < min_int && isfinite(candidate)){
				min_int = candidate;
			}
			if(candidate > max_int && isfinite(candidate)){
				max_int = candidate;
			}
		}
	}
	*max = max_int;
	*min = min_int;
}