/*
 * modular_bms.c
 *
 * Created: 2018-09-03 16:31:34
 * Author : vbox
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <avr/fuse.h>
#include <util/atomic.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include "board/board.h"
#include "config.h"
#include "globals.h"
#include "macro.h"
#include "seg_disp/seg_disp.h"

// Set up fuses to defaults
FUSES = {
	.low = LFUSE_DEFAULT,
	.high = HFUSE_DEFAULT,
	.extended = EFUSE_DEFAULT,
};

/** @brief Precomputed arrays of the segments to activate when displaying status messages */
// Alarm messages
static uint_fast8_t lcd_segs_syserr[LCD_SEGS];
static uint_fast8_t lcd_segs_undervoltage[LCD_SEGS];
static uint_fast8_t lcd_segs_overvoltage[LCD_SEGS];

/* Order of system status lines to be printed */
enum sys_status_msg_e {SYS_STATUS_MSG_FIRST = 0,
	SYS_STATUS_MSG_SELFTEST_FIRST = SYS_STATUS_MSG_FIRST,
	sys_status_msg_lcd0 = SYS_STATUS_MSG_SELFTEST_FIRST,//!<- LCD selftest segment 0 ...
	sys_status_msg_lcd1,
	sys_status_msg_lcd2,
	sys_status_msg_lcd3,
	sys_status_msg_lcd4,
	sys_status_msg_lcd5,
	sys_status_msg_lcd6,
	sys_status_msg_lcd7,								//!<- ... LCD selftest segment 7
	SYS_STATUS_MSG_SELFTEST_LAST = sys_status_msg_lcd7,	//!<- Last selftest index
	sys_status_msg_uv_txt,							//!<- Undervoltage alarm threshold text message
	sys_status_msg_uv_val,							//!<- Undervoltage alarm threshold value
	sys_status_msg_ov_txt,							//!<- Overvoltage alarm threshold
	sys_status_msg_ov_val,
	sys_status_msg_delta_txt,						//!<- Alarm hysteresis
	sys_status_msg_delta_val,
	sys_status_msg_disch_start_txt,					//!<- Discharge start
	sys_status_msg_disch_start_val,
	sys_status_msg_disch_stop_txt,					//!<- Discharge stop
	sys_status_msg_disch_stop_val,
	sys_status_msg_chgdis_start_txt,				//!<- Charge disable start
	sys_status_msg_chgdis_start_val,
	sys_status_msg_chgdis_stop_txt,					//!<- Charge disable stop
	sys_status_msg_chgdis_stop_val,
	sys_status_msg_chgdis_tmin_txt,					//!<- Minimum charge disable time
	sys_status_msg_chgdis_tmin_val,
	sys_status_msg_uv_tmin_txt,						//!<- Minimum time to be below undervoltage alarm threshold to trigger alarm
	sys_status_msg_uv_tmin_val,
	SYS_STATUS_MSG_NUM
};

// Allocate space for all system status messages
static uint_fast8_t lcd_segs_lcd_msgs[SYS_STATUS_MSG_NUM][LCD_SEGS];

int main(void){
	// The delay library requires F_CPU to be defined as a symbol.
	BUILD_BUG_ON(F_CPU != F_CPU_Hz);							// Generate an error if the internally used parameter F_CPU_Hz is a different value.
	BUILD_BUG_ON(VTHRS_UV_ALARM_V >= VTHRS_OV_ALARM_V);			// Undervoltage threshold must be below overvoltage threshold
	BUILD_BUG_ON(VTHRS_DISCH_MAX_V < VTHRS_DISCH_MIN_V);		// Threshold voltage for triggering the discharge output must be above the voltage for deactivation
	BUILD_BUG_ON(VTHRS_CHGDIS_START_V < VTHRS_CHGDIS_STOP_V);	// Threshold voltage for triggering the charge-disable output must be above the voltage for deactivation
	ATOMIC_BLOCK(ATOMIC_FORCEON){
		board_init();							// Initialize hardware
		
		// Precompute non-numerical messages
		// Alarm messages
		seg_disp_str27segs("BUG", lcd_segs_syserr, NUMEL(lcd_segs_syserr));
		seg_disp_str27segs("HI", lcd_segs_overvoltage, NUMEL(lcd_segs_overvoltage));
		seg_disp_str27segs("LO", lcd_segs_undervoltage, NUMEL(lcd_segs_undervoltage));
		
		// LCD self-test, activate each segment of each digit seperately.
		for(uint8_t i = 0; i <= SYS_STATUS_MSG_SELFTEST_LAST - SYS_STATUS_MSG_SELFTEST_FIRST; i++){
			for(uint8_t j = 0; j < LCD_SEGS; j++){
				lcd_segs_lcd_msgs[i][j] = 1<<i;	
			}
		}
		
		// UV
		seg_disp_str27segs("ALO", lcd_segs_lcd_msgs[sys_status_msg_uv_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VTHRS_UV_ALARM_V), lcd_segs_lcd_msgs[sys_status_msg_uv_val], LCD_SEGS);
		
		//OV
		seg_disp_str27segs("AHI", lcd_segs_lcd_msgs[sys_status_msg_ov_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VTHRS_OV_ALARM_V), lcd_segs_lcd_msgs[sys_status_msg_ov_val], LCD_SEGS);
		
		//Hysteresis
		seg_disp_str27segs("AHY", lcd_segs_lcd_msgs[sys_status_msg_delta_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VHYST_ALARMS_V), lcd_segs_lcd_msgs[sys_status_msg_delta_val], LCD_SEGS);
		
		//Discharge low voltage
		seg_disp_str27segs("BLO", lcd_segs_lcd_msgs[sys_status_msg_disch_start_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VTHRS_DISCH_MIN_V), lcd_segs_lcd_msgs[sys_status_msg_disch_start_val], LCD_SEGS);
		
		//Discharge high voltage
		seg_disp_str27segs("BHI", lcd_segs_lcd_msgs[sys_status_msg_disch_stop_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VTHRS_DISCH_MAX_V), lcd_segs_lcd_msgs[sys_status_msg_disch_stop_val], LCD_SEGS);
		
		//Charge disable stop
		seg_disp_str27segs("CLO", lcd_segs_lcd_msgs[sys_status_msg_chgdis_start_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VTHRS_CHGDIS_STOP_V), lcd_segs_lcd_msgs[sys_status_msg_chgdis_start_val], LCD_SEGS);
				
		//Charge disable start
		seg_disp_str27segs("CHI", lcd_segs_lcd_msgs[sys_status_msg_chgdis_stop_txt], LCD_SEGS);
		seg_disp_str27segs(STR(VTHRS_CHGDIS_START_V), lcd_segs_lcd_msgs[sys_status_msg_chgdis_stop_val], LCD_SEGS);
		
		//Minimum charger off-time
		seg_disp_str27segs("TC", lcd_segs_lcd_msgs[sys_status_msg_chgdis_tmin_txt], LCD_SEGS);
		seg_disp_str27segs(STR(TMIN_CHGDIS_S), lcd_segs_lcd_msgs[sys_status_msg_chgdis_tmin_val], LCD_SEGS);
		
		//Minimum undervoltage time to trigger alarm
		seg_disp_str27segs("TLO", lcd_segs_lcd_msgs[sys_status_msg_uv_tmin_txt], LCD_SEGS);
		seg_disp_str27segs(STR(TMIN_UV_ALARM_S), lcd_segs_lcd_msgs[sys_status_msg_uv_tmin_val], LCD_SEGS);
	}
	// All normal operations will be taken in the WDT interrupt
	for(;;){
			// If here, we are either starting up or have completed one WDT interrupt.
			// In both cases, it's time to go to sleep.
			board_sleep();
	};
	return 0;
}

// Functions for handling sections of actions to perform
static void handle_buzzer(void);
static void handle_analog(void);
static void handle_lcd(void);

ISR(WDT_vect){
	// Post-sleep clean-up
	sleep_disable();
	
	// Perform main actions
	// Update LCD first for consistent timing, important to avoid damage to display
	handle_lcd();
	handle_analog();
	handle_buzzer();
	
	// Suppress alarm on button press and leave syserr state
	if(board_get_btn()){
		g_alarm_suppressed = true;
		if(g_alarm_state == alarm_state_syserr){
			g_alarm_state = alarm_state_off;
		}
	}
	
	// Return to main context, will enter sleep mode
}

//Do nothing here, continue with code after ADC triggered
EMPTY_INTERRUPT(ADC_vect)

static void handle_lcd(void){
	// Do LCD housekeeping
	board_set_chargepump(false);
	board_lcd_idle();
	board_set_chargepump(true);
	
	// Periodically display alarm messages
	static uint8_t display_counter = 0;
	display_counter++;
	
	// Generate string to display
	uint_fast8_t * seg_ptr = g_lcd_segs_vmeas;	// Pointer to segment data to display, default to measured voltage
	static uint_fast8_t sys_status_timer = 0;
	static enum sys_status_msg_e sys_msg = SYS_STATUS_MSG_FIRST;
	if(board_get_btn()){
		// On pressed button, sweep through stored status messages
		if((sys_msg >= SYS_STATUS_MSG_SELFTEST_FIRST && sys_msg <= SYS_STATUS_MSG_SELFTEST_LAST && sys_status_timer >= SYS_STATUS_TIMER_COUNTS/4) ||
			sys_status_timer >= SYS_STATUS_TIMER_COUNTS){
			sys_msg++;
			if(sys_msg >= SYS_STATUS_MSG_NUM){
				sys_msg = SYS_STATUS_MSG_FIRST;
			}
			sys_status_timer = 0;
		}else{
			sys_status_timer++;
		}
		seg_ptr = lcd_segs_lcd_msgs[sys_msg];
	}else{
		// Reset system status message state on button release
		sys_status_timer = 0;
		sys_msg = SYS_STATUS_MSG_FIRST;
		// Override default message periodically
		if(display_counter < ALARM_TEXT_DUTY){
			switch(g_alarm_state){
				default:
					// Do nothing
					break;
				case alarm_state_syserr:
					seg_ptr = lcd_segs_syserr;
					break;
				case alarm_state_overvoltage:
					seg_ptr = lcd_segs_overvoltage;
					break;
				case alarm_state_undervoltage:
					seg_ptr = lcd_segs_undervoltage;
					break;
			}
		}
	}
	// Copy desired message into lcd buffer
	for(seg_disp_idx_t i = 0; i < NUMEL(g_lcd_segs); i++){
		g_lcd_segs[i] = seg_ptr[i];
	}
	
}

static void handle_analog(void){
	static uint16_t adc_counter = 0;				// The number of WDT interrupts since the last ADC measurement
	static uint16_t discharge_counter = 0;			// The number of WDT interrupts until the discharge output should be disabled
	static bool buzzer_enabled;
	
	adc_counter++;
	
	// Handle discharge timer, turn off if elapsed
	if(discharge_counter == 0){
		board_set_batt_discharge(false);
	}else{
		discharge_counter--;
	}
	
	if(adc_counter >= ADC_COUNTER_PER - ADC_STABLE_COUNTS){
		// Time to temporarily inhibit the discharge and buzzer output
		board_set_batt_discharge_inhibit(true);
		buzzer_enabled = board_get_buzzer_enable();
		board_set_buzzer_enable(false);
	}
	if(adc_counter >= ADC_COUNTER_PER){
		adc_counter = 0;
		g_vmeas_mv = board_get_adc();				// Update the current supply voltage value.
		board_set_batt_discharge_inhibit(false);	// Stop inhibiting the discharge output.
		board_set_buzzer_enable(buzzer_enabled);	// Stop inhibiting the buzzer.
		
		// Update the LCD segment activation corresponding to the measured voltage.
		// As this operation runs often we must be efficient. The inputs voltage for
		// operation is always in the range 1 <= V_IN < 9.99V, so we can just convert
		// the read value and place the decimal point after the first digit.
		char str[8 * sizeof (unsigned int) + 1];	// Allocate space that is long enough for any conversion in any base
		utoa(g_vmeas_mv, str, 10);
		char str_v[5];				// Array of format "X.YZ", to be shown on display. Requires at most n_lcd_segs segments, plus one decimal point, plus null termination.
		str_v[0] = str[0];
		str_v[1] = '.';
		str_v[2] = str[1];
		str_v[3] = str[2];
		str_v[4] = '\0';
		seg_disp_str27segs(str_v, g_lcd_segs_vmeas, NUMEL(g_lcd_segs_vmeas));
		
		// Update the current alarm state
		switch(g_alarm_state){
			default:
				//Should never occur
				g_alarm_state = alarm_state_syserr;
				// Flow into next statement
			case alarm_state_syserr:
				// If in system error alarm state, never leave
				break;
			case alarm_state_overvoltage:
				// Leave overvoltage alarm state if voltage low enough
				if(g_vmeas_mv < MAKE_FIXPOINT(VTHRS_OV_ALARM_V - VHYST_ALARMS_V)){
					g_alarm_state = alarm_state_off;
				}
				break;
			case alarm_state_undervoltage:
				// Leave undervoltage alarm state if voltage high enough
				if(g_vmeas_mv > MAKE_FIXPOINT(VTHRS_UV_ALARM_V + VHYST_ALARMS_V)){
					g_alarm_state = alarm_state_off;
				}
				break;
			case alarm_state_off:
				// Enter alarm state based on voltage thresholds
				if(g_vmeas_mv > MAKE_FIXPOINT(VTHRS_OV_ALARM_V)){
					g_alarm_state = alarm_state_overvoltage;
					g_alarm_suppressed = false;
				}
				{
					// Undervoltage alarm is time-based, only enter alarm state
					// if voltage has been below threshold for a consecutive
					// number of samples
					static uint16_t uv_alarm_timer = 0;
					if(g_vmeas_mv < MAKE_FIXPOINT(VTHRS_UV_ALARM_V)){
						// Increment uv_alarm_timer if voltage is below alarm threshold, saturating to valid numeric range
						if(uv_alarm_timer < UINT16_MAX){
							uv_alarm_timer++;
						}
					}else{
						uv_alarm_timer = 0;
					}
					
					//If alarm timer large enough, activate alarm
					if(uv_alarm_timer > (TMIN_UV_ALARM_S*1000ULL)/(WDT_ISR_PER_ms*ADC_COUNTER_PER)){
						g_alarm_state = alarm_state_undervoltage;
						g_alarm_suppressed = false;	
					}
				}
				break;
		}
		
		// Update the discharge output timer
		if(g_vmeas_mv >= MAKE_FIXPOINT(VTHRS_DISCH_MAX_V)){
			// Voltage is above maximum-duty voltage, set duty cycle to 100% and enable output
			discharge_counter = ADC_COUNTER_PER;
			board_set_batt_discharge(true);
		}else if(g_vmeas_mv >= MAKE_FIXPOINT(VTHRS_DISCH_MIN_V)){
			// Voltage is above minimum-duty voltage, set duty cycle to linear ramp and enable output
			const uint16_t delta_v = g_vmeas_mv - MAKE_FIXPOINT(VTHRS_DISCH_MIN_V);		// Excess voltage above minimum-threshold
			
			// Sanity check to ensure product is in valid range
			BUILD_BUG_ON( ((long long) MAKE_FIXPOINT(VTHRS_DISCH_MAX_V) - MAKE_FIXPOINT(VTHRS_DISCH_MIN_V)) * ADC_COUNTER_PER >= UINT16_MAX);
			
			// Map delta_v in range [0 - (MAKE_FIXPOINT(VTHRS_DISCH_MAX_V) - MAKE_FIXPOINT(VTHRS_DISCH_MIN_V))] linearly to range [0, ADC_COUNTER_PER]
			discharge_counter = (delta_v * ADC_COUNTER_PER) / (MAKE_FIXPOINT(VTHRS_DISCH_MAX_V) - MAKE_FIXPOINT(VTHRS_DISCH_MIN_V));
			
			// Enable output
			board_set_batt_discharge(true);
		}else{
			// Voltage is below minimum-duty voltage, set timer to zero
			discharge_counter = 0;
		}
		
		// Update the charge-disable output
		static bool chgdsb_active = false;
		static uint16_t chgdsb_timer = 0;
		
		// If the charge-disable timer is active, decrement it
		if(chgdsb_timer){
			chgdsb_timer--;
		}
		
		// If measured voltage exceeds upper limit, disable charger and start timer
		if(g_vmeas_mv > MAKE_FIXPOINT(VTHRS_CHGDIS_START_V)){
			chgdsb_active = true;
			chgdsb_timer = (TMIN_CHGDIS_S*1000ULL)/(WDT_ISR_PER_ms*ADC_COUNTER_PER);
		}
		// Refresh the charge disable timer as long as the measured voltage
		// exceeds the lower limit
		if(g_vmeas_mv > MAKE_FIXPOINT(VTHRS_CHGDIS_STOP_V) && chgdsb_timer){
			chgdsb_timer = (TMIN_CHGDIS_S*1000ULL)/(WDT_ISR_PER_ms*ADC_COUNTER_PER);
		}
		// If timer has elapsed, we can now re-enable the charger
		if(!chgdsb_timer){
			chgdsb_active = false;	
		}
		board_set_chg_disable(chgdsb_active);
	}
}

static void handle_buzzer(void){
	static uint16_t buzzer_silence_ticks = 0;				// The number of WDT interrupts since the buzzer was activated
	static uint16_t buzzer_active_remaining_ticks = 0;		// The number of WDT interrupts the buzzer should be kept active
	
	// Handle buzzer activation timer
	buzzer_silence_ticks++;
	if(buzzer_silence_ticks >= BUZZER_COUNTER_PER){
		buzzer_silence_ticks = 0;
		if(g_alarm_state == alarm_state_off || g_alarm_suppressed){
			// No error, disable buzzer
			board_set_buzzer_enable(false);
			g_sleep_mode = sleep_mode_powerdown;
		}else{
			// Some error source, manage all common properties
			board_set_buzzer_enable(true);
			g_sleep_mode = sleep_mode_idle;
			switch(g_alarm_state){
				case alarm_state_undervoltage:
					buzzer_active_remaining_ticks = BUZZER_UNDERVOLTAGE_COUNTS;
					break;
				case alarm_state_overvoltage:
					buzzer_active_remaining_ticks = BUZZER_OVERVOLTAGE_COUNTS;
					break;
				default:
					g_alarm_state = alarm_state_syserr;
					// Flow into next case
				case alarm_state_syserr:
					buzzer_active_remaining_ticks = BUZZER_SYSERR_COUNTS;
					break;
			}
		}
	}
	
	// Decrement buzzer counter, disable buzzer if counter was zero
	if(buzzer_active_remaining_ticks != 0){
		buzzer_active_remaining_ticks--;
	}else{
		board_set_buzzer_enable(false);
		g_sleep_mode = sleep_mode_powerdown;
	}
}
