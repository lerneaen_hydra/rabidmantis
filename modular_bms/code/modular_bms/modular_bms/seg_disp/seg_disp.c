/*
 * lcd.c
 *
 * Created: 2018-09-19 09:52:11
 *  Author: vbox
 */ 

#include <stdint.h>
#include <stdbool.h>
#include "seg_disp.h"

static const uint_fast8_t seg_lut_num[] = {
	0x3F,	/* 0 */
	0x06,	/* 1 */
	0x5B,	/* 2 */
	0x4F,	/* 3 */
	0x66,	/* 4 */
	0x6D,	/* 5 */
	0x7D,	/* 6 */
	0x07,	/* 7 */
	0x7F,	/* 8 */
	0x6F	/* 9 */
};

/** @brief Look-up table for converting A-Z to raw 7-segment equivalent value */
static const uint_fast8_t seg_lut_alpha[] = {
	0x77,	/* A */
	0x7C,	/* B */
	0x39,	/* C */
	0x5E,	/* D */
	0x79,	/* E */
	0x71,	/* F */
	0x6F,	/* G */
	0x76,	/* H */
	0x30,	/* I */
	0x1E,	/* J */
	0x76,	/* K */
	0x38,	/* L */
	0x15,	/* M */
	0x54,	/* N */
	0x3F,	/* O */
	0x73,	/* P */
	0x67,	/* Q */
	0x50,	/* R */
	0x6D,	/* S */
	0x78,	/* T */
	0x3E,	/* U */
	0x1C,	/* V */
	0x2A,	/* W */
	0x76,	/* X */
	0x6E,	/* Y */
	0x5,	/* Z */
};

#define seg_dp		1<<7
#define seg_empty	0x00

void seg_disp_str27segs(char * str, uint_fast8_t * segs, seg_disp_idx_t n_segs){
	// Convert str to 7-segment activation
	bool str_end = false;
	for(seg_disp_idx_t i = 0; i < n_segs; i++){
		// Read the next character from the string
		char nextchar = *(str++);
		
		// Check/handle end-of-string
		if(nextchar == '\0'){
			str_end = true;
		}
		if(str_end){
			segs[i] = seg_empty;
			continue;
		}
		
		// String not empty, try to parse character
		if(nextchar >= '0' && nextchar <= '9'){
			segs[i] = seg_lut_num[nextchar - '0'];
		}else if(nextchar >= 'a' && nextchar <= 'z'){
			segs[i] = seg_lut_alpha[nextchar - 'a'];
		}else if(nextchar >= 'A' && nextchar <= 'Z'){
			segs[i] = seg_lut_alpha[nextchar - 'A'];
		}
		else{
			segs[i] = seg_empty;
		}
		
		// Handle decimal point case, is part of the current 7segment digit
		// If this happens to be the end of the string we will detect this
		// on the next iteration.
		if(*str == '.'){
			str++;
			segs[i] |= seg_dp;
		}
	}
}
