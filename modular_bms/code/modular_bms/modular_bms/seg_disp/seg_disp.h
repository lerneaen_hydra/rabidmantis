/*
 * 7seg.h
 *
 * Created: 2018-09-19 09:51:40
 *  Author: vbox
 */ 


#ifndef SEG_DISP_H_
#define SEG_DISP_H_

/** @brief Datatype large enough to address all the segments of a display */
typedef uint_fast8_t seg_disp_idx_t;

/** @brief Convert  a string to a 7-segment activation sequence.
 * Assumes 7-segment display is addressed as:
 * a -> LSB, h -> MSB
 *      a
 *  ----------
 *  |        |
 * f|        | b
 *  |        |
 *  |--------|
 *  |   g    |
 * e|        | c
 *  |        |
 *  ----------
 *      d       ||
 *                h
 * Only 0-9, a-z, A-Z, '.', and ' ' characters supported. Any others will be
 * replaced with ' '. If string is shorter than the number of segments,
 * data will be left-aligned with empty output. If string is longer than number
 * of segments, the excess will be discarded.
 * @param str	 An ascii string to represent
 * @param segs   The 7-segment activation to apply
 * @param n_segs The length of segs  */
void seg_disp_str27segs(char * str, uint_fast8_t * segs, seg_disp_idx_t n_segs);

#endif /* SEG_DISP_H_ */