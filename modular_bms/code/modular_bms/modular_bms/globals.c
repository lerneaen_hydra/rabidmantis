/*
 * globals.c
 *
 * Created: 2018-11-15 13:10:49
 *  Author: vbox
 */ 

#include "globals.h"

enum alarm_state_e g_alarm_state = alarm_state_syserr;		// Default to the system error state, indicates any system reset (wdt reset, glitchy power connection, etc)
enum sleep_mode_e g_sleep_mode = sleep_mode_powerdown;		// Default to the powerdown sleep mode
uint16_t g_vmeas_mv = 0;
uint_fast8_t g_lcd_segs_vmeas[LCD_SEGS] = {0};
uint_fast8_t g_lcd_segs[LCD_SEGS] = {0};
bool g_alarm_suppressed = false;