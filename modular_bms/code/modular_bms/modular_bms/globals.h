/*
 * globals.h
 *
 * Created: 2018-11-15 13:07:53
 *  Author: vbox
 */ 


#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <stdbool.h>
#include <stdint.h>

#include "board/board.h"

/** @brief Possible alarm states */
enum alarm_state_e {alarm_state_off = 0, alarm_state_undervoltage, alarm_state_overvoltage, alarm_state_syserr};
	
/** @brief Possible sleep modes, dependent on current program execution */
enum sleep_mode_e {sleep_mode_powerdown = 0, sleep_mode_idle};

/** @brief Current target alarm status */
extern enum alarm_state_e g_alarm_state;

/** @brief If true, the alarm is suppressed and the buzzer will be deactivated */
extern bool g_alarm_suppressed;

/** @brief Current target sleep mode */
extern enum sleep_mode_e g_sleep_mode;

/** @brief Current measured supply voltage [mv] */
extern uint16_t g_vmeas_mv;

/** @brief LCD segment activation corresponding to most recent voltage measurement */
extern uint_fast8_t g_lcd_segs_vmeas[LCD_SEGS];

/** @brief Current target lcd segment activation */
extern uint_fast8_t g_lcd_segs[LCD_SEGS]; 

#endif /* GLOBALS_H_ */