/*
 * macro.h
 *
 * Created: 2018-09-19 09:41:06
 *  Author: vbox
 */ 


#ifndef MACRO_H_
#define MACRO_H_

#define BUILD_BUG_ON(x) ((void)sizeof(char[1 - 2*!!(x)]))
#define NUMEL(x) (sizeof(x)/sizeof(x[0]))
#define XSTR(s) #s
#define STR(s) XSTR(s)


#endif /* MACRO_H_ */