/*
 * config.h
 *
 * Created: 2018-11-15 12:06:23
 *  Author: vbox
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

/** @brief CPU frequency [Hz] */
#define F_CPU_Hz						(1000000.0f)

/** @brief Nominal watchdog timer interrupt frequency [Hz] */
#define WDT_ISR_PER_ms					(16)

/************************************************************************
* Note on adjustable parameters:
* All the following parameters are internally represented as an integer
* number, scaled by 1000. I.E. voltage thresholds are internally represented
* as an integer number of millivolts and times as an integer number of
* milliseconds. This means that all parameters set here will be truncated
* to the nearest integer after scaling!
* Furthermore, these parameters are by the preprocessor converted to strings
* for display on the LCD. Be sure to format these parameters in a way that
* makes sense for the display! Typically values should be of the form
* X.YZ, XY.Z, or XYZ in order to be able to be nicely printed.
*************************************************************************/

/** @brief Voltage which gives 100%-active discharge output [volt]
 * Voltages above this value will also keep the discharge output fully on */
#define VTHRS_DISCH_MAX_V				3.52

/** @brief Voltage which gives 0%-active discharge output [volt]
 * Voltages below this value will also keep the discharge output fully off */
#define VTHRS_DISCH_MIN_V				3.48

/** @brief Threshold voltage for activating the charge-disable output [volt] */
#define VTHRS_CHGDIS_START_V			3.60

/** @brief Threshold voltage for de-activating the charge-disable output [volt] */
#define VTHRS_CHGDIS_STOP_V				3.50

/** @brief Threshold voltage for triggering an undervoltage alarm [volt]
 * See also TMIN_UV_ALARM_S */
#define VTHRS_UV_ALARM_V				3.05

/** @brief Threshold voltage for triggering an overvoltage alarm [volt] */
#define VTHRS_OV_ALARM_V				3.65

/** @brief Voltage hysteresis for leaving alarm state [volt]
 * To leave the undervoltage alarm the sampled voltage must exceed
 * VTHRS_UV_ALARM_V + VHYST_ALARMS_V, and to leave the overvoltage
 * alarm the sampled voltage must be below
 * VTHRS_OV_ALARM_V - VHYST_ALARMS_V */
#define VHYST_ALARMS_V					0.05

/** @brief Minimum time the charger will be kept disabled after voltage exceeds
* VTHRS_CHGDIS_START_mv [seconds] */
#define TMIN_CHGDIS_S					10.0

/** @brief Minimum time voltage must be below VTHRS_UV_ALARM_V to trigger an
 * alarm [seconds] */
#define TMIN_UV_ALARM_S					60.0

/************************************************************************/
/* End scaled units                                                     */
/************************************************************************/

/** @brief Function to scale parameters to fixed-point represenation */
#define MAKE_FIXPOINT(x)						((unsigned) ((x) * 1000))

/** @brief Relative time display will show alarm text.
 * May be in range 0 - 255. Larger values will display alarm text for relatively longer times. */
#define ALARM_TEXT_DUTY					(64)

/** @brief Number of WDT periods the buzzer should be active for different alarm states.
 * NOTE: Increasing these values will significantly increase the average current consumption
 * of the device! For alarms that are triggered by low battery voltages this may be undesirable!
 * Typical current consumption while generating an alarm sound is ~100 times higher than
 * normal operation. The current consumption is typically increased by a factor of
 * approximately BUZZER_COUNTER_PER/BUZZER_XXX_COUNTS. */
#define BUZZER_UNDERVOLTAGE_COUNTS		(2)		//!<- Alarm state for battery undervoltage
#define BUZZER_OVERVOLTAGE_COUNTS       (128)	//!<- Alarm state for battery overvoltage
#define BUZZER_SYSERR_COUNTS            (2)		//!<- Alarm state for system error (watchdog/system reset, etc)

/** @brief Number of WDT periods between each system status message */
#define SYS_STATUS_TIMER_COUNTS			(64)

/** @brief Number of WDT periods between buzzer activations */
#define BUZZER_COUNTER_PER				(8192/WDT_ISR_PER_ms)

/** @brief Number of WDT periods between ADC measurements */
#define ADC_COUNTER_PER					(1024/WDT_ISR_PER_ms)

/** @brief Number of WDT periods before performing an ADC measurement to
 * disable the discharge output, to ensure the nominal (unloaded) cell voltage
 * is measured. For nearly all applications, one period will be sufficient. */
#define ADC_STABLE_COUNTS				(1)

/** @brief Nominal analog reference voltage */
#define ADC_AREF_V						(1.225f)

/** @brief Input voltage scaler, upper resistor value [kR] */
#define ADC_VDIV_RH						(32.4f)

/** @brief Input voltage scaler, lower resistor value [kR] */
#define ADC_VDIV_RL						(10.2f)

/** @brief Maximum ADC value */
#define ADC_TOP							(1023)

/** @brief Net scaling: input supply voltage (expressed in millivolts) is ADC_RAWVAL * ADC_SCALE */
#define ADC_SCALE						((unsigned) ((1.0e3 * ADC_AREF_V * (ADC_VDIV_RH + ADC_VDIV_RL))/(ADC_VDIV_RL * ADC_TOP)))

/** @brief Time to wait to allow analog circuitry to stabilize after supplying
* power to components.
 * With 10.2k, 32.4k resistors and 10nF decoupling tests show full accuracy
 * after ~400us. For margin, set delay to at least twice this value. */
#define ANALOG_STABLE_DELAY_ms			(1.0f)

#endif /* CONFIG_H_ */