/*
 * board.h
 *
 * Created: 2018-09-19 08:51:16
 *  Author: vbox
 */ 


#ifndef BOARD_H_
#define BOARD_H_

#include <stdbool.h>

/** @brief Number of LCD 7-segment displays */
#define LCD_SEGS 3

/** @brief Perform all start-up initialization */
void board_init(void);

/** @brief Enters the currently configured sleep mode */
void board_sleep(void);

/** @brief Update battery discharge (bleeder resistor) state */
void board_set_batt_discharge(bool discharge);

/** @brief Override the battery discharge (bleeder resistor) state.
 * If true, will force the battery discharge to be disabled. If false,
 * will restore the battery discarhge state. */
void board_set_batt_discharge_inhibit(bool inhibit);

/** @brief Update charger (globally shared relay-output) state */
void board_set_chg_disable(bool disable);

/** @brief Update buzzer state */
void board_set_buzzer_enable(bool enable);

/** @brief Gets the current buzzer state */
bool board_get_buzzer_enable(void);

/** @brief Updates the LCD */
void board_lcd_idle(void);

/** @brief Set chargepump pin level */
void board_set_chargepump(bool state);

/** @brief Gets the current button state, not debounced */
bool board_get_btn(void);

/** @brief Gets one ADC measurement, managing all ADC power states */
uint16_t board_get_adc(void);

#endif /* BOARD_H_ */