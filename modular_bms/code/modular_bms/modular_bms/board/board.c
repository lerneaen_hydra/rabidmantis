/*
 * board.c
 *
 * Created: 2018-09-19 08:52:11
 *  Author: vbox
 */ 

#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <util/atomic.h>
#include "board.h"
#include "../globals.h"
#include "../gpio/gpio.h"
#include "../config.h"
#include "../macro.h"
#include "../seg_disp/seg_disp.h"

#define GPIO_DISCHARGE		GPIO_B0
#define GPIO_CHARGE_DSBL	GPIO_B1
#define GPIO_LCD_LATCH		GPIO_B2
#define GPIO_LCD_SER		GPIO_B3
#define GPIO_LCD_CLK		GPIO_B5
#define GPIO_LCD_NCLR		GPIO_B6
#define GPIO_PUSHBUTTON		GPIO_B7

#define GPIO_VBUS_EN		GPIO_D2
#define GPIO_VREF_EN		GPIO_D3
#define GPIO_LCD_NEN		GPIO_D4
#define GPIO_ALARM_0		GPIO_D5
#define GPIO_ALARM_1		GPIO_D6
#define GPIO_CPUMP			GPIO_D7

#define PIN_ADC_INPUT		5	//!<- Adc input for measurement

/** @brief Initialize all I/O pins */
static void board_init_io(void);

static bool batt_discharge_active = false;
static bool batt_discharge_inhibit = false;

void board_init(void){
	// Watchdog interrupt period is hard-coded immediately below
	BUILD_BUG_ON(WDT_ISR_PER_ms != 16);
	// Enable WDT
	wdt_enable(WDTO_15MS);
	
	// Disable unused peripherals
	power_twi_disable();
	power_timer0_disable();
	power_timer1_disable();
	power_timer2_disable();
	power_usart0_disable();
	
	// Disable comparator
	ACSR = 0;
	ACSR |= 1<<ACD;
	
	board_init_io();
	board_set_batt_discharge(false);
	board_set_chg_disable(false);
	board_set_buzzer_enable(false);
}


static void board_init_io(void){
	// Disable digital input buffers for ADC pins
	DIDR0 = 1<<ADC5D | 1<<ADC4D | 1<<ADC3D | 1<<ADC2D | 1<<ADC1D | 1<<ADC0D;
	DIDR1 = 1<<AIN1D | 1<<AIN0D;
	
	// Enable pull-ups for unused pins
	gpio_set_pullup(GPIO_B4);
	gpio_set_pullup(GPIO_C0);
	gpio_set_pullup(GPIO_C1);
	gpio_set_pullup(GPIO_C2);
	gpio_set_pullup(GPIO_C3);
	gpio_set_pullup(GPIO_C4);
	
	// Disable pull-ups for used ADC input
	gpio_clr_pullup(GPIO_C5);
	
	// Enable pull-up for UART RX input
	gpio_set_pullup(GPIO_D0);
	
	// Set UART TX output to output, high level
	gpio_set_pin(GPIO_D1);
	gpio_set_out(GPIO_D1);
	
	// Enable pull-up for alarm silence button
	gpio_set_pullup(GPIO_PUSHBUTTON);
	
	// Set up discharge pin, initialize to low
	gpio_clr_pin(GPIO_DISCHARGE);
	gpio_set_out(GPIO_DISCHARGE);
	
	// Set up charge disable pin, initialize to low
	gpio_clr_pin(GPIO_CHARGE_DSBL);
	gpio_set_out(GPIO_CHARGE_DSBL);
	
	// Set up LCD pins, initialize all to low
	gpio_clr_pin(GPIO_LCD_NCLR);
	gpio_set_out(GPIO_LCD_NCLR);
	gpio_clr_pin(GPIO_LCD_NEN);
	gpio_set_out(GPIO_LCD_NEN);
	gpio_clr_pin(GPIO_LCD_LATCH);
	gpio_set_out(GPIO_LCD_LATCH);
	gpio_clr_pin(GPIO_LCD_SER);
	gpio_set_out(GPIO_LCD_SER);
	gpio_clr_pin(GPIO_LCD_CLK);
	gpio_set_out(GPIO_LCD_CLK);
	
	// Set up vbus, vref measurement pins, initialize to low
	gpio_clr_pin(GPIO_VBUS_EN);
	gpio_set_out(GPIO_VBUS_EN);
	gpio_clr_pin(GPIO_VREF_EN);
	gpio_set_out(GPIO_VREF_EN);
	
	// Set up alarm pins, initialize low
	gpio_clr_pin(GPIO_ALARM_0);
	gpio_set_out(GPIO_ALARM_0);
	gpio_clr_pin(GPIO_ALARM_1);
	gpio_set_out(GPIO_ALARM_1);
	
	// Set up charge-pump, initialize low
	gpio_clr_pin(GPIO_CPUMP);
	gpio_set_out(GPIO_CPUMP);
	
	// Finally, bring  LCD clear pin high to allow data to be shifted in later
	gpio_set_pin(GPIO_LCD_NCLR);
}

void board_sleep(void){
	// Re-enable WDT as interrupt
	WDTCSR |= 1<<WDIE;
	// Enter sleep state
	switch(g_sleep_mode){
		default:
			// Should never occur
			g_sleep_mode = sleep_mode_powerdown;
			// Flow into next statement
		case sleep_mode_powerdown:
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			break;
		case sleep_mode_idle:
			set_sleep_mode(SLEEP_MODE_IDLE);
			break;
	}
	sleep_mode();
}

void board_set_batt_discharge(bool discharge){
	batt_discharge_active = discharge;
	if(!batt_discharge_inhibit){
		gpio_set_statement(discharge, GPIO_DISCHARGE);
	}
}

void board_set_batt_discharge_inhibit(bool inhibit){
	batt_discharge_inhibit = inhibit;
	if(inhibit){
		gpio_clr_pin(GPIO_DISCHARGE);
	}else if(batt_discharge_active){
		gpio_set_pin(GPIO_DISCHARGE);
	}
	
}

void board_set_chg_disable(bool disable){
	gpio_set_statement(disable, GPIO_CHARGE_DSBL);
}

void board_set_buzzer_enable(bool enable){
	// Configuration assumes a clock frequency of 1 MHz.
	// Buzzer manufactured for operation at 4kHz. With fast-PWM, counting up to 255,
	// this gives a PWM frequency of 1e6/256 ~= 3.9kHz, which happens to be a
	// near-perfect match for the buzzer.
	BUILD_BUG_ON(F_CPU_Hz != 1000000);
	if(enable){
		// Enable buzzer, use timer-counter 0, outputs A and B, to drive buzzer
		power_timer0_enable();
		TCCR0A = 1<<COM0A1 | 1<<COM0B1 | 1<<COM0B0 | 1<<WGM01 | 1<<WGM00; // Set up timer for fast-PWM mode, OC0A cleared and OC0B set on exceeding OCR0A/B, set and cleared on bottom respectively.
		OCR0A = UINT8_MAX/2;
		OCR0B = UINT8_MAX/2;
		TCNT0 = 0;
		TCCR0B = 1<<CS00;
	}else{
		// Disable buzzer, set pins low
		TCCR0A = 0; // Return pins to GPIO
		TCCR0B = 0; // Stop timer (clear clock source)
		power_timer0_disable();
	}
}

bool board_get_buzzer_enable(void){
	return !!TCCR0A;
}

void board_lcd_idle(void){
	// Internal copy of LCD segments to display, periodically inverted
	static uint_fast8_t lcd_buf_shdw[NUMEL(g_lcd_segs)] = {0};
	
	// State-keeping variable for when to invert LCD output
	static bool lcd_inv = false;
	
	// Immediately latch data loaded into shifter registers on last call
	gpio_set_pin(GPIO_LCD_LATCH);
	_delay_us(1);
	gpio_clr_pin(GPIO_LCD_LATCH);
	
	// Now update shift registers
	// To avoid any bias, only update lcd_buf_shdw on every other call to this
	// function. When lcd_buf_shdw is not updated, bitwise invert it.
	
	if(lcd_inv){
		for(seg_disp_idx_t i = 0; i < NUMEL(lcd_buf_shdw); i++){
			lcd_buf_shdw[i] = ~lcd_buf_shdw[i];
		}
	}else{
		for(seg_disp_idx_t i = 0; i < NUMEL(lcd_buf_shdw); i++){
			lcd_buf_shdw[i] = g_lcd_segs[i];
		}
		// As the COM line uses the decimal-point output from the last shift
		// register, manually ensure it is low.
		lcd_buf_shdw[NUMEL(lcd_buf_shdw) - 1] &= ~(1<<7);
	}
	lcd_inv = !lcd_inv;
	
	
	// Configure SPI to drive shift registers
	power_spi_enable();
	SPCR = 1<<SPE | 1<<MSTR;
	SPSR = 1<<SPI2X;
	
	// Send data
	// Transmit bytes in reverse order (first sent byte ends up in last shift register)
	seg_disp_idx_t n_segs = NUMEL(lcd_buf_shdw);
	while(n_segs){
		SPDR = lcd_buf_shdw[n_segs - 1];
		n_segs--;
		// Wait until transmission complete
		while(!(SPSR & (1<<SPIF))){};
	}
	
	// Disable SPI peripheral
	SPCR &= ~(1<<SPE);
	power_spi_disable();
}

void board_set_chargepump(bool state){
	gpio_set_statement(state, GPIO_CPUMP);
}

bool board_get_btn(void){
	return !gpio_get_state(GPIO_PUSHBUTTON);
}

uint16_t board_get_adc(void){
	// Power up the external analog circuitry, wait until stable
	gpio_set_pin(GPIO_VBUS_EN);
	gpio_set_pin(GPIO_VREF_EN);
	_delay_ms(ANALOG_STABLE_DELAY_ms);
	
	power_adc_enable();
	// Reconfigure all ADC registers, as they were corrupted when power was
	// removed from the ADC.
	ADCSRA = 1<<ADEN | 1<<ADIE | 1<<ADPS0 | 1<<ADPS1;
	ADMUX = PIN_ADC_INPUT & (1<<MUX0 | 1<<MUX1 | 1<<MUX2 | 1<<MUX3);
	
	// Temporarily enable interrupts, typically needed as this function
	// is executed from the WDT ISR context, which by default disables interrupts.
	
	NONATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		// Generate one ADC conversion, whose value will be discarded because
		// it is less accurate than successive conversions.
		set_sleep_mode(SLEEP_MODE_ADC);
		sleep_mode();
	
		// Generate another ADC conversion, whose value will be kept
		set_sleep_mode(SLEEP_MODE_ADC);
		sleep_mode();	
	}

	uint16_t adcval = ADC;
	
	// Disable ADC
	ADCSRA &= ~ (1<<ADEN);
	power_adc_disable();
	
	// Power down external circuitry
	gpio_clr_pin(GPIO_VBUS_EN);
	gpio_clr_pin(GPIO_VREF_EN);
	return adcval * ADC_SCALE;
}