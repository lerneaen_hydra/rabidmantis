#!/bin/bash

#User adjustable paratmers

#Eagle install directory
#EAGLEDIR=/usr/local/eagle-5.10.0/bin/eagle
#EAGLEDIR=/usr/bin/eagle
#EAGLEDIR=~/eagle-6.5.0/bin/eagle
#EAGLEDIR=/opt/eagle-6.5.0/bin/eagle
EAGLEDIR=/home/hydra/eagle-9.1.3/eagle
#directory to scan recursively
SCANDIR=~/Desktop/tmp/inp
#Set TRUE to output image from schematic files
GENERATE_SCH=TRUE
#Set TRUE to output image from board files
GENERATE_BRD=TRUE
#Set TRUE to gemerate a bill of materials
GENERATE_BOM=TRUE
#Set TRUE to split documents over one page in length to multiple documents (IE. for insertion as graphics)
SPLIT_PDF=TRUE
#Set TRUE to crop whitespace from the output PDF files
CROP_PDF=TRUE
#Output directory for all files
OUTPUTDIR=~/Desktop/tmp/out
#Temporary working directory
TMPDIR=/tmp/eagle_export_image
#Layers to export for board
BRD_TOP_LAYERS="Top Pads Vias Dimension Tplace Tnames Tdocu"
BRD_TOP_2_LAYERS="Dimension Tplace Tnames Tdocu"
BRD_BTM_LAYERS="Bottom Pads Vias Dimension Bplace Bnames Bdocu"
BRD_BTM_2_LAYERS="Dimension Bplace Bnames Bdocu"

# Don't touch the following

#if temp dir already exists remove it, probably left over from some previous execution
if [ -d $TMPDIR ]
then
	echo "removing contents of $TMPDIR"
	rm -R $TMPDIR
fi


echo "Creating $TMPDIR"
mkdir $TMPDIR

cp -R $SCANDIR $TMPDIR

if [ "$GENERATE_SCH" == "TRUE" ]
then
	echo "Generating schematic PDFs"
	find $TMPDIR -type f -name '*.sch' -exec $EAGLEDIR -C "DISPLAY ALL -PINS; PRINT - 10 -1 -CAPTION PAPER A4 SHEETS ALL FILE {}.pdf; QUIT" {} \;

    if [ "$SPLIT_PDF" == "TRUE" ]
    then
        echo "Splitting output"
        find $TMPDIR -type f -name '*.pdf' -exec pdftk {} burst output {}_%02d.pdf \;
    fi
    
fi

if [ "$GENERATE_BRD" == "TRUE" ]
then
	echo "Generating board PDFs (top layer)"
	find $TMPDIR -type f -name '*.brd' -exec $EAGLEDIR -C "DISPLAY NONE $BRD_TOP_LAYERS; RATSNEST; PRINT - 10 -1 -CAPTION PAPER A4 FILE {}_top.pdf; QUIT" {} \;
    find $TMPDIR -type f -name '*.brd' -exec $EAGLEDIR -C "DISPLAY NONE $BRD_TOP_2_LAYERS; RATSNEST; PRINT - 10 -1 -CAPTION PAPER A4 FILE {}_tplc.pdf; QUIT" {} \;
	echo "Generating board PDFs (bottom layer)"
	find $TMPDIR -type f -name '*.brd' -exec $EAGLEDIR -C "DISPLAY NONE $BRD_BTM_LAYERS; RATSNEST; PRINT - 10 -1 -CAPTION MIRROR PAPER A4 FILE {}_btm.pdf; QUIT" {} \;
    find $TMPDIR -type f -name '*.brd' -exec $EAGLEDIR -C "DISPLAY NONE $BRD_BTM_2_LAYERS; RATSNEST; PRINT - 10 -1 -CAPTION MIRROR PAPER A4 FILE {}_bplc.pdf; QUIT" {} \;
fi

if [ "$GENERATE_BOM" == "TRUE" ]
then
	echo "Generating BOMs"
	find $TMPDIR -type f -name '*.sch' -exec $EAGLEDIR -C "EXPORT PARTLIST {}.txt; QUIT" {} \;
fi

if [ "$CROP_PDF" == "TRUE" ]
then
	echo "Cropping output"
	find $TMPDIR -type f -name '*.pdf' -exec pdfcrop {} {} \;
fi

#remove all files that were not just created

find $TMPDIR -type f ! \( -name '*.pdf' -o -name '*.txt' \) -exec rm {} \;

cp -R $TMPDIR $OUTPUTDIR

#cleanup
echo "removing contents of $TMPDIR"
rm -R $TMPDIR
