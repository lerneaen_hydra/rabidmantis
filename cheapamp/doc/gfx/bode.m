clc
clear all
format short eng

%plot frequency response of Cheapamp based on sampled transfer function
%magnitude

f =     [3      5       10      15      25      45      70      100     400     1e3     2e3     4e3     8e3     12e3    18e3    25e3    35e3    50e3    70e3];
v_in =  [1      .5      .5      .5      .5      .2      .2      .2      .2      .2      .2      .2      .2      .2      .2      .2      .2      .5      .5];
v_out = [4.4    3.5     6.2     8.17    10.2    4.6     4.95    4.97    5.0     5.0     4.97    4.95    4.74    4.50    4.27    3.75    2.93    4.86    2.92];

h = v_out ./ v_in;

set(0,'DefaultAxesColorOrder',[0 0 0],...
      'DefaultAxesLineStyleOrder','-')
semilogx(f,20*log10(h)-max(20*log10(h)))
grid on
axis([5 70e3 -10 3])
title('Relative power output over frequency')
ylabel('Relative power [dB]')
xlabel('Frequency [Hz]')
New_XTickLabel = get(gca,'xtick');
set(gca,'XTickLabel',New_XTickLabel);
set(gcf,'PaperUnits','centimeters','PaperSize',[5 5],'PaperPosition',[0,0,12,8])
print('-deps ','freq_response')